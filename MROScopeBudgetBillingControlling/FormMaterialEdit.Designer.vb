﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormMaterialEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.activity_info_grp = New System.Windows.Forms.GroupBox()
        Me.change_activity_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.change_activity_val = New System.Windows.Forms.ComboBox()
        Me.change_activity_lbl = New System.Windows.Forms.Label()
        Me.activity_details_lay = New System.Windows.Forms.TableLayoutPanel()
        Me.act_sold_freight_val = New System.Windows.Forms.TextBox()
        Me.act_repair_val = New System.Windows.Forms.TextBox()
        Me.act_service_val = New System.Windows.Forms.TextBox()
        Me.act_sold_mat_val = New System.Windows.Forms.TextBox()
        Me.act_lab_sold_val = New System.Windows.Forms.TextBox()
        Me.act_rev_ctrl_txt_val = New System.Windows.Forms.TextBox()
        Me.act_rev_ctrl_status_val = New System.Windows.Forms.TextBox()
        Me.act_operator_code_val = New System.Windows.Forms.TextBox()
        Me.act_warr_code_val = New System.Windows.Forms.TextBox()
        Me.act_check_type_val = New System.Windows.Forms.TextBox()
        Me.act_long_desc_val = New System.Windows.Forms.TextBox()
        Me.act_desc_val = New System.Windows.Forms.TextBox()
        Me.act_status_val = New System.Windows.Forms.TextBox()
        Me.act_notif_val = New System.Windows.Forms.TextBox()
        Me.act_activity_val = New System.Windows.Forms.TextBox()
        Me.act_sold_freight_lbl = New System.Windows.Forms.Label()
        Me.act_repair_lbl = New System.Windows.Forms.Label()
        Me.act_sold_mat_lbl = New System.Windows.Forms.Label()
        Me.act_lab_sold_lbl = New System.Windows.Forms.Label()
        Me.act_sold_hrs_lbl = New System.Windows.Forms.Label()
        Me.act_is_init_lbl = New System.Windows.Forms.Label()
        Me.act_rev_ctrl_status_lbl = New System.Windows.Forms.Label()
        Me.act_check_type_lbl = New System.Windows.Forms.Label()
        Me.act_operator_code_lbl = New System.Windows.Forms.Label()
        Me.act_warr_code_lbl = New System.Windows.Forms.Label()
        Me.act_long_desc_lbl = New System.Windows.Forms.Label()
        Me.ct_rev_ctrl_txt_lbl = New System.Windows.Forms.Label()
        Me.mat_desc_lbl = New System.Windows.Forms.Label()
        Me.act_status_lbl = New System.Windows.Forms.Label()
        Me.act_activity_lbl = New System.Windows.Forms.Label()
        Me.act_notif_lbl = New System.Windows.Forms.Label()
        Me.act_service_lbl = New System.Windows.Forms.Label()
        Me.act_sold_hrs_val = New System.Windows.Forms.TextBox()
        Me.act_is_init_val = New System.Windows.Forms.CheckBox()
        Me.act_is_small_mat_lbl = New System.Windows.Forms.Label()
        Me.act_is_small_mat_val = New System.Windows.Forms.CheckBox()
        Me.act_mat_info_lbl = New System.Windows.Forms.Label()
        Me.act_mat_info_val = New System.Windows.Forms.TextBox()
        Me.flow_panel_activity_details = New System.Windows.Forms.FlowLayoutPanel()
        Me.material_general_grp = New System.Windows.Forms.GroupBox()
        Me.mat_general_info_lay = New System.Windows.Forms.TableLayoutPanel()
        Me.mat_cancel_awq_val = New System.Windows.Forms.TextBox()
        Me.mat_awq_val = New System.Windows.Forms.TextBox()
        Me.mat_cancel_awq_lbl = New System.Windows.Forms.Label()
        Me.mat_awq_lbl = New System.Windows.Forms.Label()
        Me.po_cost_val = New System.Windows.Forms.TextBox()
        Me.po_estimated_val = New System.Windows.Forms.TextBox()
        Me.po_warr_code_val = New System.Windows.Forms.TextBox()
        Me.po_orea_val = New System.Windows.Forms.TextBox()
        Me.po_item_val = New System.Windows.Forms.TextBox()
        Me.po_number_val = New System.Windows.Forms.TextBox()
        Me.po_uom_val = New System.Windows.Forms.TextBox()
        Me.po_qty_val = New System.Windows.Forms.TextBox()
        Me.mat_desc_val = New System.Windows.Forms.TextBox()
        Me.mat_infos_val = New System.Windows.Forms.TextBox()
        Me.mat_infos_lbl = New System.Windows.Forms.Label()
        Me.rev_ctrl_text_val = New System.Windows.Forms.TextBox()
        Me.rev_ctrl_status_val = New System.Windows.Forms.TextBox()
        Me.po_created_date_val = New System.Windows.Forms.TextBox()
        Me.po_estimated_lbl = New System.Windows.Forms.Label()
        Me.po_curr_lbl = New System.Windows.Forms.Label()
        Me.po_cost_lbl = New System.Windows.Forms.Label()
        Me.po_orea_lbl = New System.Windows.Forms.Label()
        Me.po_uom_lbl = New System.Windows.Forms.Label()
        Me.po_item_lbl = New System.Windows.Forms.Label()
        Me.po_number_lbl = New System.Windows.Forms.Label()
        Me.po_qty_lbl = New System.Windows.Forms.Label()
        Me.po_warr_code_lbl = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.rev_ctrl_text_lbl = New System.Windows.Forms.Label()
        Me.po_created_date_lbl = New System.Windows.Forms.Label()
        Me.rev_ctrl_status_lbl = New System.Windows.Forms.Label()
        Me.po_curr_val = New System.Windows.Forms.TextBox()
        Me.material_general_edit_grp = New System.Windows.Forms.GroupBox()
        Me.material_general_edit_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.is_mat_reported_val = New System.Windows.Forms.CheckBox()
        Me.mat_payer_val = New System.Windows.Forms.ComboBox()
        Me.sn_on_val = New System.Windows.Forms.TextBox()
        Me.part_number_val = New System.Windows.Forms.TextBox()
        Me.material_type_lbl = New System.Windows.Forms.Label()
        Me.part_number_lbl = New System.Windows.Forms.Label()
        Me.mat_text5_lbl = New System.Windows.Forms.Label()
        Me.sn_on_lbl = New System.Windows.Forms.Label()
        Me.is_mat_reported_lbl = New System.Windows.Forms.Label()
        Me.mat_payer_lbl = New System.Windows.Forms.Label()
        Me.mat_nte_val = New System.Windows.Forms.ComboBox()
        Me.mat_discount_val = New System.Windows.Forms.ComboBox()
        Me.mat_nte_lbl = New System.Windows.Forms.Label()
        Me.is_init_scope_lbl = New System.Windows.Forms.Label()
        Me.mat_discount_lbl = New System.Windows.Forms.Label()
        Me.billing_work_status_lbl = New System.Windows.Forms.Label()
        Me.billing_work_comment_lbl = New System.Windows.Forms.Label()
        Me.billing_work_comment_val = New System.Windows.Forms.TextBox()
        Me.billing_work_status_val = New System.Windows.Forms.ComboBox()
        Me.material_type_val = New System.Windows.Forms.ComboBox()
        Me.is_init_scope_val = New System.Windows.Forms.CheckBox()
        Me.transac_cond_lbl = New System.Windows.Forms.Label()
        Me.mat_text5_val = New System.Windows.Forms.TextBox()
        Me.transac_cond_val = New System.Windows.Forms.ComboBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.zprm_zstm_grp = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.total_sales_price_zprm_val = New System.Windows.Forms.TextBox()
        Me.total_sales_price_zprm_lbl = New System.Windows.Forms.Label()
        Me.unit_sales_price_zstm_lbl = New System.Windows.Forms.Label()
        Me.zprm_zstm_curr_lbl = New System.Windows.Forms.Label()
        Me.unit_sales_price_zstm_val = New System.Windows.Forms.TextBox()
        Me.zprm_zstm_curr_val = New System.Windows.Forms.ComboBox()
        Me.zprm_zstm_curr_tagk_val = New System.Windows.Forms.TextBox()
        Me.zprm_zstm_curr_tagk_lbl = New System.Windows.Forms.Label()
        Me.total_sales_price_zprm_flag_val = New System.Windows.Forms.CheckBox()
        Me.markup_zmar_grp = New System.Windows.Forms.GroupBox()
        Me.markup_zmar_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.markup_zmar_def_val = New System.Windows.Forms.TextBox()
        Me.markup_zmar_def_lbl = New System.Windows.Forms.Label()
        Me.markup_zmar_is_overwr_lbl = New System.Windows.Forms.Label()
        Me.markup_zmar_overwritten_lbl = New System.Windows.Forms.Label()
        Me.markup_zmar_is_overwr_val = New System.Windows.Forms.CheckBox()
        Me.markup_zmar_overwritten_val = New System.Windows.Forms.TextBox()
        Me.calc_markup_zmar_value_lbl = New System.Windows.Forms.Label()
        Me.calc_markup_zmar_value_val = New System.Windows.Forms.TextBox()
        Me.freight_zfra_grp = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.freight_zfra_default_val = New System.Windows.Forms.TextBox()
        Me.freight_zfra_default_lbl = New System.Windows.Forms.Label()
        Me.freight_zfra_is_overwr_lbl = New System.Windows.Forms.Label()
        Me.freight_zfra_overwriten_lbl = New System.Windows.Forms.Label()
        Me.freight_zfra_is_overwr_val = New System.Windows.Forms.CheckBox()
        Me.freight_zfra_overwriten_val = New System.Windows.Forms.TextBox()
        Me.calc_freight_zfra_value_val = New System.Windows.Forms.TextBox()
        Me.calc_freight_zfra_value_lbl = New System.Windows.Forms.Label()
        Me.total_mat_price_grp = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.total_mat_in_ce_curr_bef_disc_val = New System.Windows.Forms.TextBox()
        Me.total_mat_in_ce_curr_bef_disc_lbl = New System.Windows.Forms.Label()
        Me.total_mat_in_ce_curr_disc_lbl = New System.Windows.Forms.Label()
        Me.total_mat_in_ce_curr_after_disc_lbl = New System.Windows.Forms.Label()
        Me.total_mat_in_ce_curr_disc_val = New System.Windows.Forms.TextBox()
        Me.total_mat_in_ce_curr_after_disc_val = New System.Windows.Forms.TextBox()
        Me.freight_zfrp_grp = New System.Windows.Forms.GroupBox()
        Me.freight_zfrp_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.freight_zfrp_tagk_lbl = New System.Windows.Forms.Label()
        Me.freight_zfrp_lbl = New System.Windows.Forms.Label()
        Me.overwrit_freight_zfrp_val_lbl = New System.Windows.Forms.Label()
        Me.overwrit_freight_zfrp_curr_lbl = New System.Windows.Forms.Label()
        Me.freight_zfrp_val = New System.Windows.Forms.ComboBox()
        Me.overwrit_freight_zfrp_val_val = New System.Windows.Forms.TextBox()
        Me.freight_zfrp_tagk_val = New System.Windows.Forms.TextBox()
        Me.overwrit_freight_zfrp_curr_val = New System.Windows.Forms.ComboBox()
        Me.freight_zfrp_in_ce_curr_aft_disc_val = New System.Windows.Forms.TextBox()
        Me.freight_zfrp_in_ce_curr_disc_val = New System.Windows.Forms.TextBox()
        Me.freight_zfrp_in_ce_curr_aft_disc_lbl = New System.Windows.Forms.Label()
        Me.freight_zfrp_in_ce_curr_disc_lbl = New System.Windows.Forms.Label()
        Me.freight_zfrp_in_ce_curr_bef_disc_val = New System.Windows.Forms.TextBox()
        Me.freight_zfrp_in_ce_curr_bef_disc_lbl = New System.Windows.Forms.Label()
        Me.zcuss_grp = New System.Windows.Forms.GroupBox()
        Me.handling_zcus_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.handling_zcus_in_ce_curr_aft_disc_val = New System.Windows.Forms.TextBox()
        Me.handling_zcus_in_ce_curr_disc_val = New System.Windows.Forms.TextBox()
        Me.handling_zcus_in_ce_curr_aft_disc_lbl = New System.Windows.Forms.Label()
        Me.handling_zcus_in_ce_curr_disc_lbl = New System.Windows.Forms.Label()
        Me.handling_zcus_in_ce_curr_bef_disc_val = New System.Windows.Forms.TextBox()
        Me.handling_zcus_in_ce_curr_bef_disc_lbl = New System.Windows.Forms.Label()
        Me.handling_zcus_value_val = New System.Windows.Forms.TextBox()
        Me.handling_zcus_curr_val = New System.Windows.Forms.ComboBox()
        Me.handling_zcus_tagk_val = New System.Windows.Forms.TextBox()
        Me.handling_zcus_value_lbl = New System.Windows.Forms.Label()
        Me.handling_zcus_curr_lbl = New System.Windows.Forms.Label()
        Me.handling_zcus_tagk_lbl = New System.Windows.Forms.Label()
        Me.select_material_grp = New System.Windows.Forms.GroupBox()
        Me.material_error_state_pic = New System.Windows.Forms.PictureBox()
        Me.material_select_right_pic = New System.Windows.Forms.PictureBox()
        Me.material_select_left_pic = New System.Windows.Forms.PictureBox()
        Me.cb_material = New System.Windows.Forms.ComboBox()
        Me.top_panel_material = New System.Windows.Forms.Panel()
        Me.activity_info_grp.SuspendLayout()
        Me.change_activity_ly.SuspendLayout()
        Me.activity_details_lay.SuspendLayout()
        Me.flow_panel_activity_details.SuspendLayout()
        Me.material_general_grp.SuspendLayout()
        Me.mat_general_info_lay.SuspendLayout()
        Me.material_general_edit_grp.SuspendLayout()
        Me.material_general_edit_ly.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.zprm_zstm_grp.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.markup_zmar_grp.SuspendLayout()
        Me.markup_zmar_ly.SuspendLayout()
        Me.freight_zfra_grp.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.total_mat_price_grp.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.freight_zfrp_grp.SuspendLayout()
        Me.freight_zfrp_ly.SuspendLayout()
        Me.zcuss_grp.SuspendLayout()
        Me.handling_zcus_ly.SuspendLayout()
        Me.select_material_grp.SuspendLayout()
        CType(Me.material_error_state_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.material_select_right_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.material_select_left_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.top_panel_material.SuspendLayout()
        Me.SuspendLayout()
        '
        'activity_info_grp
        '
        Me.activity_info_grp.Controls.Add(Me.change_activity_ly)
        Me.activity_info_grp.Controls.Add(Me.activity_details_lay)
        Me.activity_info_grp.Location = New System.Drawing.Point(5, 5)
        Me.activity_info_grp.Margin = New System.Windows.Forms.Padding(5, 5, 5, 10)
        Me.activity_info_grp.Name = "activity_info_grp"
        Me.activity_info_grp.Size = New System.Drawing.Size(1494, 189)
        Me.activity_info_grp.TabIndex = 1
        Me.activity_info_grp.TabStop = False
        Me.activity_info_grp.Text = "Activity Details"
        '
        'change_activity_ly
        '
        Me.change_activity_ly.ColumnCount = 2
        Me.change_activity_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.38873!))
        Me.change_activity_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 86.61127!))
        Me.change_activity_ly.Controls.Add(Me.change_activity_val, 1, 0)
        Me.change_activity_ly.Controls.Add(Me.change_activity_lbl, 0, 0)
        Me.change_activity_ly.Location = New System.Drawing.Point(13, 29)
        Me.change_activity_ly.Name = "change_activity_ly"
        Me.change_activity_ly.RowCount = 1
        Me.change_activity_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.change_activity_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23.0!))
        Me.change_activity_ly.Size = New System.Drawing.Size(1083, 23)
        Me.change_activity_ly.TabIndex = 2
        '
        'change_activity_val
        '
        Me.change_activity_val.FormattingEnabled = True
        Me.change_activity_val.Location = New System.Drawing.Point(144, 0)
        Me.change_activity_val.Margin = New System.Windows.Forms.Padding(0)
        Me.change_activity_val.Name = "change_activity_val"
        Me.change_activity_val.Size = New System.Drawing.Size(504, 21)
        Me.change_activity_val.TabIndex = 4
        '
        'change_activity_lbl
        '
        Me.change_activity_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.change_activity_lbl.AutoSize = True
        Me.change_activity_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.change_activity_lbl.Location = New System.Drawing.Point(3, 5)
        Me.change_activity_lbl.Name = "change_activity_lbl"
        Me.change_activity_lbl.Size = New System.Drawing.Size(104, 13)
        Me.change_activity_lbl.TabIndex = 4
        Me.change_activity_lbl.Text = "Change Activity :"
        '
        'activity_details_lay
        '
        Me.activity_details_lay.ColumnCount = 8
        Me.activity_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.95552!))
        Me.activity_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.50906!))
        Me.activity_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.555189!))
        Me.activity_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.56837!))
        Me.activity_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.566722!))
        Me.activity_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.81549!))
        Me.activity_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.566722!))
        Me.activity_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.95717!))
        Me.activity_details_lay.Controls.Add(Me.act_sold_freight_val, 1, 4)
        Me.activity_details_lay.Controls.Add(Me.act_repair_val, 5, 3)
        Me.activity_details_lay.Controls.Add(Me.act_service_val, 7, 3)
        Me.activity_details_lay.Controls.Add(Me.act_sold_mat_val, 3, 3)
        Me.activity_details_lay.Controls.Add(Me.act_lab_sold_val, 1, 3)
        Me.activity_details_lay.Controls.Add(Me.act_rev_ctrl_txt_val, 3, 2)
        Me.activity_details_lay.Controls.Add(Me.act_rev_ctrl_status_val, 1, 2)
        Me.activity_details_lay.Controls.Add(Me.act_operator_code_val, 7, 1)
        Me.activity_details_lay.Controls.Add(Me.act_warr_code_val, 5, 1)
        Me.activity_details_lay.Controls.Add(Me.act_check_type_val, 3, 1)
        Me.activity_details_lay.Controls.Add(Me.act_long_desc_val, 1, 1)
        Me.activity_details_lay.Controls.Add(Me.act_desc_val, 7, 0)
        Me.activity_details_lay.Controls.Add(Me.act_status_val, 5, 0)
        Me.activity_details_lay.Controls.Add(Me.act_notif_val, 3, 0)
        Me.activity_details_lay.Controls.Add(Me.act_activity_val, 1, 0)
        Me.activity_details_lay.Controls.Add(Me.act_sold_freight_lbl, 0, 4)
        Me.activity_details_lay.Controls.Add(Me.act_repair_lbl, 4, 3)
        Me.activity_details_lay.Controls.Add(Me.act_sold_mat_lbl, 2, 3)
        Me.activity_details_lay.Controls.Add(Me.act_lab_sold_lbl, 0, 3)
        Me.activity_details_lay.Controls.Add(Me.act_sold_hrs_lbl, 6, 2)
        Me.activity_details_lay.Controls.Add(Me.act_is_init_lbl, 4, 2)
        Me.activity_details_lay.Controls.Add(Me.act_rev_ctrl_status_lbl, 0, 2)
        Me.activity_details_lay.Controls.Add(Me.act_check_type_lbl, 2, 1)
        Me.activity_details_lay.Controls.Add(Me.act_operator_code_lbl, 6, 1)
        Me.activity_details_lay.Controls.Add(Me.act_warr_code_lbl, 4, 1)
        Me.activity_details_lay.Controls.Add(Me.act_long_desc_lbl, 0, 1)
        Me.activity_details_lay.Controls.Add(Me.ct_rev_ctrl_txt_lbl, 2, 2)
        Me.activity_details_lay.Controls.Add(Me.mat_desc_lbl, 6, 0)
        Me.activity_details_lay.Controls.Add(Me.act_status_lbl, 4, 0)
        Me.activity_details_lay.Controls.Add(Me.act_activity_lbl, 0, 0)
        Me.activity_details_lay.Controls.Add(Me.act_notif_lbl, 2, 0)
        Me.activity_details_lay.Controls.Add(Me.act_service_lbl, 6, 3)
        Me.activity_details_lay.Controls.Add(Me.act_sold_hrs_val, 7, 2)
        Me.activity_details_lay.Controls.Add(Me.act_is_init_val, 5, 2)
        Me.activity_details_lay.Controls.Add(Me.act_is_small_mat_lbl, 2, 4)
        Me.activity_details_lay.Controls.Add(Me.act_is_small_mat_val, 3, 4)
        Me.activity_details_lay.Controls.Add(Me.act_mat_info_lbl, 4, 4)
        Me.activity_details_lay.Controls.Add(Me.act_mat_info_val, 5, 4)
        Me.activity_details_lay.Location = New System.Drawing.Point(13, 58)
        Me.activity_details_lay.Name = "activity_details_lay"
        Me.activity_details_lay.RowCount = 5
        Me.activity_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.activity_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.activity_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.activity_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.activity_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.activity_details_lay.Size = New System.Drawing.Size(1481, 125)
        Me.activity_details_lay.TabIndex = 1
        '
        'act_sold_freight_val
        '
        Me.act_sold_freight_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_sold_freight_val.Location = New System.Drawing.Point(164, 103)
        Me.act_sold_freight_val.Name = "act_sold_freight_val"
        Me.act_sold_freight_val.Size = New System.Drawing.Size(100, 20)
        Me.act_sold_freight_val.TabIndex = 2
        '
        'act_repair_val
        '
        Me.act_repair_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_repair_val.Location = New System.Drawing.Point(858, 78)
        Me.act_repair_val.Name = "act_repair_val"
        Me.act_repair_val.Size = New System.Drawing.Size(100, 20)
        Me.act_repair_val.TabIndex = 3
        '
        'act_service_val
        '
        Me.act_service_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_service_val.Location = New System.Drawing.Point(1217, 78)
        Me.act_service_val.Name = "act_service_val"
        Me.act_service_val.Size = New System.Drawing.Size(100, 20)
        Me.act_service_val.TabIndex = 4
        '
        'act_sold_mat_val
        '
        Me.act_sold_mat_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_sold_mat_val.Location = New System.Drawing.Point(503, 78)
        Me.act_sold_mat_val.Name = "act_sold_mat_val"
        Me.act_sold_mat_val.Size = New System.Drawing.Size(100, 20)
        Me.act_sold_mat_val.TabIndex = 3
        '
        'act_lab_sold_val
        '
        Me.act_lab_sold_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_lab_sold_val.Location = New System.Drawing.Point(164, 78)
        Me.act_lab_sold_val.Name = "act_lab_sold_val"
        Me.act_lab_sold_val.Size = New System.Drawing.Size(100, 20)
        Me.act_lab_sold_val.TabIndex = 4
        '
        'act_rev_ctrl_txt_val
        '
        Me.act_rev_ctrl_txt_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_rev_ctrl_txt_val.Location = New System.Drawing.Point(503, 53)
        Me.act_rev_ctrl_txt_val.Name = "act_rev_ctrl_txt_val"
        Me.act_rev_ctrl_txt_val.Size = New System.Drawing.Size(182, 20)
        Me.act_rev_ctrl_txt_val.TabIndex = 7
        '
        'act_rev_ctrl_status_val
        '
        Me.act_rev_ctrl_status_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_rev_ctrl_status_val.Location = New System.Drawing.Point(164, 53)
        Me.act_rev_ctrl_status_val.Name = "act_rev_ctrl_status_val"
        Me.act_rev_ctrl_status_val.Size = New System.Drawing.Size(100, 20)
        Me.act_rev_ctrl_status_val.TabIndex = 8
        '
        'act_operator_code_val
        '
        Me.act_operator_code_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_operator_code_val.Location = New System.Drawing.Point(1217, 28)
        Me.act_operator_code_val.Name = "act_operator_code_val"
        Me.act_operator_code_val.Size = New System.Drawing.Size(100, 20)
        Me.act_operator_code_val.TabIndex = 9
        '
        'act_warr_code_val
        '
        Me.act_warr_code_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_warr_code_val.Location = New System.Drawing.Point(858, 28)
        Me.act_warr_code_val.Name = "act_warr_code_val"
        Me.act_warr_code_val.Size = New System.Drawing.Size(100, 20)
        Me.act_warr_code_val.TabIndex = 10
        '
        'act_check_type_val
        '
        Me.act_check_type_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_check_type_val.Location = New System.Drawing.Point(503, 28)
        Me.act_check_type_val.Name = "act_check_type_val"
        Me.act_check_type_val.Size = New System.Drawing.Size(100, 20)
        Me.act_check_type_val.TabIndex = 11
        '
        'act_long_desc_val
        '
        Me.act_long_desc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_long_desc_val.Location = New System.Drawing.Point(164, 28)
        Me.act_long_desc_val.Name = "act_long_desc_val"
        Me.act_long_desc_val.Size = New System.Drawing.Size(161, 20)
        Me.act_long_desc_val.TabIndex = 12
        '
        'act_desc_val
        '
        Me.act_desc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_desc_val.Location = New System.Drawing.Point(1217, 3)
        Me.act_desc_val.Name = "act_desc_val"
        Me.act_desc_val.Size = New System.Drawing.Size(216, 20)
        Me.act_desc_val.TabIndex = 13
        '
        'act_status_val
        '
        Me.act_status_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_status_val.Location = New System.Drawing.Point(858, 3)
        Me.act_status_val.Name = "act_status_val"
        Me.act_status_val.Size = New System.Drawing.Size(100, 20)
        Me.act_status_val.TabIndex = 14
        '
        'act_notif_val
        '
        Me.act_notif_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_notif_val.Location = New System.Drawing.Point(503, 3)
        Me.act_notif_val.Name = "act_notif_val"
        Me.act_notif_val.Size = New System.Drawing.Size(100, 20)
        Me.act_notif_val.TabIndex = 15
        '
        'act_activity_val
        '
        Me.act_activity_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_activity_val.Location = New System.Drawing.Point(164, 3)
        Me.act_activity_val.Name = "act_activity_val"
        Me.act_activity_val.Size = New System.Drawing.Size(100, 20)
        Me.act_activity_val.TabIndex = 1
        '
        'act_sold_freight_lbl
        '
        Me.act_sold_freight_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_sold_freight_lbl.AutoSize = True
        Me.act_sold_freight_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_sold_freight_lbl.Location = New System.Drawing.Point(3, 106)
        Me.act_sold_freight_lbl.Name = "act_sold_freight_lbl"
        Me.act_sold_freight_lbl.Size = New System.Drawing.Size(83, 13)
        Me.act_sold_freight_lbl.TabIndex = 8
        Me.act_sold_freight_lbl.Text = "Sold Freight :"
        '
        'act_repair_lbl
        '
        Me.act_repair_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_repair_lbl.AutoSize = True
        Me.act_repair_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_repair_lbl.Location = New System.Drawing.Point(732, 81)
        Me.act_repair_lbl.Name = "act_repair_lbl"
        Me.act_repair_lbl.Size = New System.Drawing.Size(81, 13)
        Me.act_repair_lbl.TabIndex = 10
        Me.act_repair_lbl.Text = "Sold Repair :"
        '
        'act_sold_mat_lbl
        '
        Me.act_sold_mat_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_sold_mat_lbl.AutoSize = True
        Me.act_sold_mat_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_sold_mat_lbl.Location = New System.Drawing.Point(363, 81)
        Me.act_sold_mat_lbl.Name = "act_sold_mat_lbl"
        Me.act_sold_mat_lbl.Size = New System.Drawing.Size(89, 13)
        Me.act_sold_mat_lbl.TabIndex = 9
        Me.act_sold_mat_lbl.Text = "Sold Material :"
        '
        'act_lab_sold_lbl
        '
        Me.act_lab_sold_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_lab_sold_lbl.AutoSize = True
        Me.act_lab_sold_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_lab_sold_lbl.Location = New System.Drawing.Point(3, 81)
        Me.act_lab_sold_lbl.Name = "act_lab_sold_lbl"
        Me.act_lab_sold_lbl.Size = New System.Drawing.Size(76, 13)
        Me.act_lab_sold_lbl.TabIndex = 10
        Me.act_lab_sold_lbl.Text = "Sold Labor :"
        '
        'act_sold_hrs_lbl
        '
        Me.act_sold_hrs_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_sold_hrs_lbl.AutoSize = True
        Me.act_sold_hrs_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_sold_hrs_lbl.Location = New System.Drawing.Point(1091, 56)
        Me.act_sold_hrs_lbl.Name = "act_sold_hrs_lbl"
        Me.act_sold_hrs_lbl.Size = New System.Drawing.Size(77, 13)
        Me.act_sold_hrs_lbl.TabIndex = 11
        Me.act_sold_hrs_lbl.Text = "Sold Hours :"
        '
        'act_is_init_lbl
        '
        Me.act_is_init_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_is_init_lbl.AutoSize = True
        Me.act_is_init_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_is_init_lbl.Location = New System.Drawing.Point(732, 56)
        Me.act_is_init_lbl.Name = "act_is_init_lbl"
        Me.act_is_init_lbl.Size = New System.Drawing.Size(89, 13)
        Me.act_is_init_lbl.TabIndex = 12
        Me.act_is_init_lbl.Text = "Initial Scope ?"
        '
        'act_rev_ctrl_status_lbl
        '
        Me.act_rev_ctrl_status_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_rev_ctrl_status_lbl.AutoSize = True
        Me.act_rev_ctrl_status_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_rev_ctrl_status_lbl.Location = New System.Drawing.Point(3, 56)
        Me.act_rev_ctrl_status_lbl.Name = "act_rev_ctrl_status_lbl"
        Me.act_rev_ctrl_status_lbl.Size = New System.Drawing.Size(109, 13)
        Me.act_rev_ctrl_status_lbl.TabIndex = 13
        Me.act_rev_ctrl_status_lbl.Text = "Rev. Ctrl. Status :"
        '
        'act_check_type_lbl
        '
        Me.act_check_type_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_check_type_lbl.AutoSize = True
        Me.act_check_type_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_check_type_lbl.Location = New System.Drawing.Point(363, 31)
        Me.act_check_type_lbl.Name = "act_check_type_lbl"
        Me.act_check_type_lbl.Size = New System.Drawing.Size(83, 13)
        Me.act_check_type_lbl.TabIndex = 12
        Me.act_check_type_lbl.Text = "Check Type :"
        '
        'act_operator_code_lbl
        '
        Me.act_operator_code_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_operator_code_lbl.AutoSize = True
        Me.act_operator_code_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_operator_code_lbl.Location = New System.Drawing.Point(1091, 31)
        Me.act_operator_code_lbl.Name = "act_operator_code_lbl"
        Me.act_operator_code_lbl.Size = New System.Drawing.Size(93, 13)
        Me.act_operator_code_lbl.TabIndex = 5
        Me.act_operator_code_lbl.Text = "Operator Code:"
        '
        'act_warr_code_lbl
        '
        Me.act_warr_code_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_warr_code_lbl.AutoSize = True
        Me.act_warr_code_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_warr_code_lbl.Location = New System.Drawing.Point(732, 31)
        Me.act_warr_code_lbl.Name = "act_warr_code_lbl"
        Me.act_warr_code_lbl.Size = New System.Drawing.Size(75, 13)
        Me.act_warr_code_lbl.TabIndex = 6
        Me.act_warr_code_lbl.Text = "Warr Code :"
        '
        'act_long_desc_lbl
        '
        Me.act_long_desc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_long_desc_lbl.AutoSize = True
        Me.act_long_desc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_long_desc_lbl.Location = New System.Drawing.Point(3, 31)
        Me.act_long_desc_lbl.Name = "act_long_desc_lbl"
        Me.act_long_desc_lbl.Size = New System.Drawing.Size(76, 13)
        Me.act_long_desc_lbl.TabIndex = 10
        Me.act_long_desc_lbl.Text = "Long Desc :"
        '
        'ct_rev_ctrl_txt_lbl
        '
        Me.ct_rev_ctrl_txt_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.ct_rev_ctrl_txt_lbl.AutoSize = True
        Me.ct_rev_ctrl_txt_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ct_rev_ctrl_txt_lbl.Location = New System.Drawing.Point(363, 56)
        Me.ct_rev_ctrl_txt_lbl.Name = "ct_rev_ctrl_txt_lbl"
        Me.ct_rev_ctrl_txt_lbl.Size = New System.Drawing.Size(98, 13)
        Me.ct_rev_ctrl_txt_lbl.TabIndex = 7
        Me.ct_rev_ctrl_txt_lbl.Text = "Rev. Ctrl. Text :"
        '
        'mat_desc_lbl
        '
        Me.mat_desc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_desc_lbl.AutoSize = True
        Me.mat_desc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mat_desc_lbl.Location = New System.Drawing.Point(1091, 6)
        Me.mat_desc_lbl.Name = "mat_desc_lbl"
        Me.mat_desc_lbl.Size = New System.Drawing.Size(79, 13)
        Me.mat_desc_lbl.TabIndex = 6
        Me.mat_desc_lbl.Text = "Description :"
        '
        'act_status_lbl
        '
        Me.act_status_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_status_lbl.AutoSize = True
        Me.act_status_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_status_lbl.Location = New System.Drawing.Point(732, 6)
        Me.act_status_lbl.Name = "act_status_lbl"
        Me.act_status_lbl.Size = New System.Drawing.Size(51, 13)
        Me.act_status_lbl.TabIndex = 4
        Me.act_status_lbl.Text = "Status :"
        '
        'act_activity_lbl
        '
        Me.act_activity_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_activity_lbl.AutoSize = True
        Me.act_activity_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_activity_lbl.Location = New System.Drawing.Point(3, 6)
        Me.act_activity_lbl.Name = "act_activity_lbl"
        Me.act_activity_lbl.Size = New System.Drawing.Size(57, 13)
        Me.act_activity_lbl.TabIndex = 0
        Me.act_activity_lbl.Text = "Activity :"
        '
        'act_notif_lbl
        '
        Me.act_notif_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_notif_lbl.AutoSize = True
        Me.act_notif_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_notif_lbl.Location = New System.Drawing.Point(363, 6)
        Me.act_notif_lbl.Name = "act_notif_lbl"
        Me.act_notif_lbl.Size = New System.Drawing.Size(65, 13)
        Me.act_notif_lbl.TabIndex = 2
        Me.act_notif_lbl.Text = "Job Card :"
        '
        'act_service_lbl
        '
        Me.act_service_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_service_lbl.AutoSize = True
        Me.act_service_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_service_lbl.Location = New System.Drawing.Point(1091, 81)
        Me.act_service_lbl.Name = "act_service_lbl"
        Me.act_service_lbl.Size = New System.Drawing.Size(87, 13)
        Me.act_service_lbl.TabIndex = 9
        Me.act_service_lbl.Text = "Sold Service :"
        '
        'act_sold_hrs_val
        '
        Me.act_sold_hrs_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_sold_hrs_val.Location = New System.Drawing.Point(1217, 53)
        Me.act_sold_hrs_val.Name = "act_sold_hrs_val"
        Me.act_sold_hrs_val.Size = New System.Drawing.Size(100, 20)
        Me.act_sold_hrs_val.TabIndex = 5
        '
        'act_is_init_val
        '
        Me.act_is_init_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_is_init_val.AutoSize = True
        Me.act_is_init_val.Location = New System.Drawing.Point(858, 55)
        Me.act_is_init_val.Name = "act_is_init_val"
        Me.act_is_init_val.Size = New System.Drawing.Size(15, 14)
        Me.act_is_init_val.TabIndex = 16
        Me.act_is_init_val.UseVisualStyleBackColor = True
        '
        'act_is_small_mat_lbl
        '
        Me.act_is_small_mat_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_is_small_mat_lbl.AutoSize = True
        Me.act_is_small_mat_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_is_small_mat_lbl.Location = New System.Drawing.Point(363, 106)
        Me.act_is_small_mat_lbl.Name = "act_is_small_mat_lbl"
        Me.act_is_small_mat_lbl.Size = New System.Drawing.Size(91, 13)
        Me.act_is_small_mat_lbl.TabIndex = 17
        Me.act_is_small_mat_lbl.Text = "Is Small Mat. ?"
        '
        'act_is_small_mat_val
        '
        Me.act_is_small_mat_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_is_small_mat_val.AutoSize = True
        Me.act_is_small_mat_val.Location = New System.Drawing.Point(503, 105)
        Me.act_is_small_mat_val.Name = "act_is_small_mat_val"
        Me.act_is_small_mat_val.Size = New System.Drawing.Size(15, 14)
        Me.act_is_small_mat_val.TabIndex = 18
        Me.act_is_small_mat_val.UseVisualStyleBackColor = True
        '
        'act_mat_info_lbl
        '
        Me.act_mat_info_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_mat_info_lbl.AutoSize = True
        Me.act_mat_info_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_mat_info_lbl.Location = New System.Drawing.Point(732, 106)
        Me.act_mat_info_lbl.Name = "act_mat_info_lbl"
        Me.act_mat_info_lbl.Size = New System.Drawing.Size(96, 13)
        Me.act_mat_info_lbl.TabIndex = 19
        Me.act_mat_info_lbl.Text = "Material Infos. :"
        '
        'act_mat_info_val
        '
        Me.act_mat_info_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_mat_info_val.Location = New System.Drawing.Point(858, 103)
        Me.act_mat_info_val.Name = "act_mat_info_val"
        Me.act_mat_info_val.Size = New System.Drawing.Size(201, 20)
        Me.act_mat_info_val.TabIndex = 20
        '
        'flow_panel_activity_details
        '
        Me.flow_panel_activity_details.AutoScroll = True
        Me.flow_panel_activity_details.AutoSize = True
        Me.flow_panel_activity_details.Controls.Add(Me.activity_info_grp)
        Me.flow_panel_activity_details.Controls.Add(Me.material_general_grp)
        Me.flow_panel_activity_details.Controls.Add(Me.material_general_edit_grp)
        Me.flow_panel_activity_details.Controls.Add(Me.FlowLayoutPanel1)
        Me.flow_panel_activity_details.Controls.Add(Me.freight_zfrp_grp)
        Me.flow_panel_activity_details.Controls.Add(Me.zcuss_grp)
        Me.flow_panel_activity_details.Dock = System.Windows.Forms.DockStyle.Fill
        Me.flow_panel_activity_details.Location = New System.Drawing.Point(5, 68)
        Me.flow_panel_activity_details.Name = "flow_panel_activity_details"
        Me.flow_panel_activity_details.Size = New System.Drawing.Size(1273, 609)
        Me.flow_panel_activity_details.TabIndex = 2
        '
        'material_general_grp
        '
        Me.material_general_grp.Controls.Add(Me.mat_general_info_lay)
        Me.material_general_grp.Location = New System.Drawing.Point(5, 209)
        Me.material_general_grp.Margin = New System.Windows.Forms.Padding(5, 5, 5, 10)
        Me.material_general_grp.Name = "material_general_grp"
        Me.material_general_grp.Size = New System.Drawing.Size(1494, 146)
        Me.material_general_grp.TabIndex = 3
        Me.material_general_grp.TabStop = False
        Me.material_general_grp.Text = "General Info ( Read-only )"
        '
        'mat_general_info_lay
        '
        Me.mat_general_info_lay.ColumnCount = 8
        Me.mat_general_info_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.90164!))
        Me.mat_general_info_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.44262!))
        Me.mat_general_info_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.508196!))
        Me.mat_general_info_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.4918!))
        Me.mat_general_info_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.52459!))
        Me.mat_general_info_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.73771!))
        Me.mat_general_info_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.52459!))
        Me.mat_general_info_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.86886!))
        Me.mat_general_info_lay.Controls.Add(Me.mat_cancel_awq_val, 7, 3)
        Me.mat_general_info_lay.Controls.Add(Me.mat_awq_val, 5, 3)
        Me.mat_general_info_lay.Controls.Add(Me.mat_cancel_awq_lbl, 6, 3)
        Me.mat_general_info_lay.Controls.Add(Me.mat_awq_lbl, 4, 3)
        Me.mat_general_info_lay.Controls.Add(Me.po_cost_val, 5, 2)
        Me.mat_general_info_lay.Controls.Add(Me.po_estimated_val, 1, 3)
        Me.mat_general_info_lay.Controls.Add(Me.po_warr_code_val, 3, 2)
        Me.mat_general_info_lay.Controls.Add(Me.po_orea_val, 1, 2)
        Me.mat_general_info_lay.Controls.Add(Me.po_item_val, 7, 1)
        Me.mat_general_info_lay.Controls.Add(Me.po_number_val, 5, 1)
        Me.mat_general_info_lay.Controls.Add(Me.po_uom_val, 3, 1)
        Me.mat_general_info_lay.Controls.Add(Me.po_qty_val, 1, 1)
        Me.mat_general_info_lay.Controls.Add(Me.mat_desc_val, 7, 0)
        Me.mat_general_info_lay.Controls.Add(Me.mat_infos_val, 3, 3)
        Me.mat_general_info_lay.Controls.Add(Me.mat_infos_lbl, 2, 3)
        Me.mat_general_info_lay.Controls.Add(Me.rev_ctrl_text_val, 5, 0)
        Me.mat_general_info_lay.Controls.Add(Me.rev_ctrl_status_val, 3, 0)
        Me.mat_general_info_lay.Controls.Add(Me.po_created_date_val, 1, 0)
        Me.mat_general_info_lay.Controls.Add(Me.po_estimated_lbl, 0, 3)
        Me.mat_general_info_lay.Controls.Add(Me.po_curr_lbl, 6, 2)
        Me.mat_general_info_lay.Controls.Add(Me.po_cost_lbl, 4, 2)
        Me.mat_general_info_lay.Controls.Add(Me.po_orea_lbl, 0, 2)
        Me.mat_general_info_lay.Controls.Add(Me.po_uom_lbl, 2, 1)
        Me.mat_general_info_lay.Controls.Add(Me.po_item_lbl, 6, 1)
        Me.mat_general_info_lay.Controls.Add(Me.po_number_lbl, 4, 1)
        Me.mat_general_info_lay.Controls.Add(Me.po_qty_lbl, 0, 1)
        Me.mat_general_info_lay.Controls.Add(Me.po_warr_code_lbl, 2, 2)
        Me.mat_general_info_lay.Controls.Add(Me.Label13, 6, 0)
        Me.mat_general_info_lay.Controls.Add(Me.rev_ctrl_text_lbl, 4, 0)
        Me.mat_general_info_lay.Controls.Add(Me.po_created_date_lbl, 0, 0)
        Me.mat_general_info_lay.Controls.Add(Me.rev_ctrl_status_lbl, 2, 0)
        Me.mat_general_info_lay.Controls.Add(Me.po_curr_val, 7, 2)
        Me.mat_general_info_lay.Location = New System.Drawing.Point(13, 27)
        Me.mat_general_info_lay.Name = "mat_general_info_lay"
        Me.mat_general_info_lay.RowCount = 4
        Me.mat_general_info_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.mat_general_info_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.mat_general_info_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.mat_general_info_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.mat_general_info_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.mat_general_info_lay.Size = New System.Drawing.Size(1481, 109)
        Me.mat_general_info_lay.TabIndex = 1
        '
        'mat_cancel_awq_val
        '
        Me.mat_cancel_awq_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_cancel_awq_val.Location = New System.Drawing.Point(1217, 85)
        Me.mat_cancel_awq_val.Name = "mat_cancel_awq_val"
        Me.mat_cancel_awq_val.Size = New System.Drawing.Size(216, 20)
        Me.mat_cancel_awq_val.TabIndex = 10
        '
        'mat_awq_val
        '
        Me.mat_awq_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_awq_val.Location = New System.Drawing.Point(858, 85)
        Me.mat_awq_val.Name = "mat_awq_val"
        Me.mat_awq_val.Size = New System.Drawing.Size(190, 20)
        Me.mat_awq_val.TabIndex = 9
        '
        'mat_cancel_awq_lbl
        '
        Me.mat_cancel_awq_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_cancel_awq_lbl.AutoSize = True
        Me.mat_cancel_awq_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mat_cancel_awq_lbl.Location = New System.Drawing.Point(1091, 88)
        Me.mat_cancel_awq_lbl.Name = "mat_cancel_awq_lbl"
        Me.mat_cancel_awq_lbl.Size = New System.Drawing.Size(104, 13)
        Me.mat_cancel_awq_lbl.TabIndex = 13
        Me.mat_cancel_awq_lbl.Text = "Cancel by AWQ :"
        '
        'mat_awq_lbl
        '
        Me.mat_awq_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_awq_lbl.AutoSize = True
        Me.mat_awq_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mat_awq_lbl.Location = New System.Drawing.Point(732, 88)
        Me.mat_awq_lbl.Name = "mat_awq_lbl"
        Me.mat_awq_lbl.Size = New System.Drawing.Size(44, 13)
        Me.mat_awq_lbl.TabIndex = 13
        Me.mat_awq_lbl.Text = "AWQ :"
        '
        'po_cost_val
        '
        Me.po_cost_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_cost_val.Location = New System.Drawing.Point(858, 57)
        Me.po_cost_val.Name = "po_cost_val"
        Me.po_cost_val.Size = New System.Drawing.Size(100, 20)
        Me.po_cost_val.TabIndex = 8
        '
        'po_estimated_val
        '
        Me.po_estimated_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_estimated_val.Location = New System.Drawing.Point(164, 85)
        Me.po_estimated_val.Name = "po_estimated_val"
        Me.po_estimated_val.Size = New System.Drawing.Size(100, 20)
        Me.po_estimated_val.TabIndex = 4
        '
        'po_warr_code_val
        '
        Me.po_warr_code_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_warr_code_val.Location = New System.Drawing.Point(503, 57)
        Me.po_warr_code_val.Name = "po_warr_code_val"
        Me.po_warr_code_val.Size = New System.Drawing.Size(100, 20)
        Me.po_warr_code_val.TabIndex = 7
        '
        'po_orea_val
        '
        Me.po_orea_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_orea_val.Location = New System.Drawing.Point(164, 57)
        Me.po_orea_val.Name = "po_orea_val"
        Me.po_orea_val.Size = New System.Drawing.Size(100, 20)
        Me.po_orea_val.TabIndex = 8
        '
        'po_item_val
        '
        Me.po_item_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_item_val.Location = New System.Drawing.Point(1217, 30)
        Me.po_item_val.Name = "po_item_val"
        Me.po_item_val.Size = New System.Drawing.Size(100, 20)
        Me.po_item_val.TabIndex = 9
        '
        'po_number_val
        '
        Me.po_number_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_number_val.Location = New System.Drawing.Point(858, 30)
        Me.po_number_val.Name = "po_number_val"
        Me.po_number_val.Size = New System.Drawing.Size(190, 20)
        Me.po_number_val.TabIndex = 10
        '
        'po_uom_val
        '
        Me.po_uom_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_uom_val.Location = New System.Drawing.Point(503, 30)
        Me.po_uom_val.Name = "po_uom_val"
        Me.po_uom_val.Size = New System.Drawing.Size(100, 20)
        Me.po_uom_val.TabIndex = 11
        '
        'po_qty_val
        '
        Me.po_qty_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_qty_val.Location = New System.Drawing.Point(164, 30)
        Me.po_qty_val.Name = "po_qty_val"
        Me.po_qty_val.Size = New System.Drawing.Size(100, 20)
        Me.po_qty_val.TabIndex = 12
        '
        'mat_desc_val
        '
        Me.mat_desc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_desc_val.Location = New System.Drawing.Point(1217, 3)
        Me.mat_desc_val.Name = "mat_desc_val"
        Me.mat_desc_val.Size = New System.Drawing.Size(216, 20)
        Me.mat_desc_val.TabIndex = 13
        '
        'mat_infos_val
        '
        Me.mat_infos_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_infos_val.Location = New System.Drawing.Point(503, 85)
        Me.mat_infos_val.Name = "mat_infos_val"
        Me.mat_infos_val.Size = New System.Drawing.Size(100, 20)
        Me.mat_infos_val.TabIndex = 11
        '
        'mat_infos_lbl
        '
        Me.mat_infos_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_infos_lbl.AutoSize = True
        Me.mat_infos_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mat_infos_lbl.Location = New System.Drawing.Point(363, 88)
        Me.mat_infos_lbl.Name = "mat_infos_lbl"
        Me.mat_infos_lbl.Size = New System.Drawing.Size(76, 13)
        Me.mat_infos_lbl.TabIndex = 11
        Me.mat_infos_lbl.Text = "Mat. Infos. :"
        '
        'rev_ctrl_text_val
        '
        Me.rev_ctrl_text_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.rev_ctrl_text_val.Location = New System.Drawing.Point(858, 3)
        Me.rev_ctrl_text_val.Name = "rev_ctrl_text_val"
        Me.rev_ctrl_text_val.Size = New System.Drawing.Size(190, 20)
        Me.rev_ctrl_text_val.TabIndex = 14
        '
        'rev_ctrl_status_val
        '
        Me.rev_ctrl_status_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.rev_ctrl_status_val.Location = New System.Drawing.Point(503, 3)
        Me.rev_ctrl_status_val.Name = "rev_ctrl_status_val"
        Me.rev_ctrl_status_val.Size = New System.Drawing.Size(100, 20)
        Me.rev_ctrl_status_val.TabIndex = 15
        '
        'po_created_date_val
        '
        Me.po_created_date_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_created_date_val.Location = New System.Drawing.Point(164, 3)
        Me.po_created_date_val.Name = "po_created_date_val"
        Me.po_created_date_val.Size = New System.Drawing.Size(100, 20)
        Me.po_created_date_val.TabIndex = 1
        '
        'po_estimated_lbl
        '
        Me.po_estimated_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_estimated_lbl.AutoSize = True
        Me.po_estimated_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.po_estimated_lbl.Location = New System.Drawing.Point(3, 88)
        Me.po_estimated_lbl.Name = "po_estimated_lbl"
        Me.po_estimated_lbl.Size = New System.Drawing.Size(64, 13)
        Me.po_estimated_lbl.TabIndex = 10
        Me.po_estimated_lbl.Text = "P/O Est. :"
        '
        'po_curr_lbl
        '
        Me.po_curr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_curr_lbl.AutoSize = True
        Me.po_curr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.po_curr_lbl.Location = New System.Drawing.Point(1091, 61)
        Me.po_curr_lbl.Name = "po_curr_lbl"
        Me.po_curr_lbl.Size = New System.Drawing.Size(69, 13)
        Me.po_curr_lbl.TabIndex = 11
        Me.po_curr_lbl.Text = "P/O Curr. :"
        '
        'po_cost_lbl
        '
        Me.po_cost_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_cost_lbl.AutoSize = True
        Me.po_cost_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.po_cost_lbl.Location = New System.Drawing.Point(732, 61)
        Me.po_cost_lbl.Name = "po_cost_lbl"
        Me.po_cost_lbl.Size = New System.Drawing.Size(67, 13)
        Me.po_cost_lbl.TabIndex = 12
        Me.po_cost_lbl.Text = "P/O Cost :"
        '
        'po_orea_lbl
        '
        Me.po_orea_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_orea_lbl.AutoSize = True
        Me.po_orea_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.po_orea_lbl.Location = New System.Drawing.Point(3, 61)
        Me.po_orea_lbl.Name = "po_orea_lbl"
        Me.po_orea_lbl.Size = New System.Drawing.Size(78, 13)
        Me.po_orea_lbl.TabIndex = 13
        Me.po_orea_lbl.Text = "P/O OrRea :"
        '
        'po_uom_lbl
        '
        Me.po_uom_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_uom_lbl.AutoSize = True
        Me.po_uom_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.po_uom_lbl.Location = New System.Drawing.Point(363, 34)
        Me.po_uom_lbl.Name = "po_uom_lbl"
        Me.po_uom_lbl.Size = New System.Drawing.Size(41, 13)
        Me.po_uom_lbl.TabIndex = 12
        Me.po_uom_lbl.Text = "UoM :"
        '
        'po_item_lbl
        '
        Me.po_item_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_item_lbl.AutoSize = True
        Me.po_item_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.po_item_lbl.Location = New System.Drawing.Point(1091, 34)
        Me.po_item_lbl.Name = "po_item_lbl"
        Me.po_item_lbl.Size = New System.Drawing.Size(66, 13)
        Me.po_item_lbl.TabIndex = 5
        Me.po_item_lbl.Text = "P/O Item :"
        '
        'po_number_lbl
        '
        Me.po_number_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_number_lbl.AutoSize = True
        Me.po_number_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.po_number_lbl.Location = New System.Drawing.Point(732, 34)
        Me.po_number_lbl.Name = "po_number_lbl"
        Me.po_number_lbl.Size = New System.Drawing.Size(85, 13)
        Me.po_number_lbl.TabIndex = 6
        Me.po_number_lbl.Text = "P/O Number :"
        '
        'po_qty_lbl
        '
        Me.po_qty_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_qty_lbl.AutoSize = True
        Me.po_qty_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.po_qty_lbl.Location = New System.Drawing.Point(3, 34)
        Me.po_qty_lbl.Name = "po_qty_lbl"
        Me.po_qty_lbl.Size = New System.Drawing.Size(34, 13)
        Me.po_qty_lbl.TabIndex = 10
        Me.po_qty_lbl.Text = "Qty :"
        '
        'po_warr_code_lbl
        '
        Me.po_warr_code_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_warr_code_lbl.AutoSize = True
        Me.po_warr_code_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.po_warr_code_lbl.Location = New System.Drawing.Point(363, 61)
        Me.po_warr_code_lbl.Name = "po_warr_code_lbl"
        Me.po_warr_code_lbl.Size = New System.Drawing.Size(75, 13)
        Me.po_warr_code_lbl.TabIndex = 7
        Me.po_warr_code_lbl.Text = "Warr Code :"
        '
        'Label13
        '
        Me.Label13.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(1091, 7)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(79, 13)
        Me.Label13.TabIndex = 6
        Me.Label13.Text = "Description :"
        '
        'rev_ctrl_text_lbl
        '
        Me.rev_ctrl_text_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.rev_ctrl_text_lbl.AutoSize = True
        Me.rev_ctrl_text_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rev_ctrl_text_lbl.Location = New System.Drawing.Point(732, 7)
        Me.rev_ctrl_text_lbl.Name = "rev_ctrl_text_lbl"
        Me.rev_ctrl_text_lbl.Size = New System.Drawing.Size(98, 13)
        Me.rev_ctrl_text_lbl.TabIndex = 4
        Me.rev_ctrl_text_lbl.Text = "Rev. Ctrl. Text :"
        '
        'po_created_date_lbl
        '
        Me.po_created_date_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_created_date_lbl.AutoSize = True
        Me.po_created_date_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.po_created_date_lbl.Location = New System.Drawing.Point(3, 7)
        Me.po_created_date_lbl.Name = "po_created_date_lbl"
        Me.po_created_date_lbl.Size = New System.Drawing.Size(59, 13)
        Me.po_created_date_lbl.TabIndex = 0
        Me.po_created_date_lbl.Text = "Created :"
        '
        'rev_ctrl_status_lbl
        '
        Me.rev_ctrl_status_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.rev_ctrl_status_lbl.AutoSize = True
        Me.rev_ctrl_status_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rev_ctrl_status_lbl.Location = New System.Drawing.Point(363, 7)
        Me.rev_ctrl_status_lbl.Name = "rev_ctrl_status_lbl"
        Me.rev_ctrl_status_lbl.Size = New System.Drawing.Size(109, 13)
        Me.rev_ctrl_status_lbl.TabIndex = 2
        Me.rev_ctrl_status_lbl.Text = "Rev. Ctrl. Status :"
        '
        'po_curr_val
        '
        Me.po_curr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.po_curr_val.Location = New System.Drawing.Point(1217, 57)
        Me.po_curr_val.Name = "po_curr_val"
        Me.po_curr_val.Size = New System.Drawing.Size(100, 20)
        Me.po_curr_val.TabIndex = 5
        '
        'material_general_edit_grp
        '
        Me.material_general_edit_grp.Controls.Add(Me.material_general_edit_ly)
        Me.material_general_edit_grp.Location = New System.Drawing.Point(5, 370)
        Me.material_general_edit_grp.Margin = New System.Windows.Forms.Padding(5, 5, 5, 10)
        Me.material_general_edit_grp.Name = "material_general_edit_grp"
        Me.material_general_edit_grp.Size = New System.Drawing.Size(1494, 120)
        Me.material_general_edit_grp.TabIndex = 4
        Me.material_general_edit_grp.TabStop = False
        Me.material_general_edit_grp.Text = "General Info ( Editable )"
        '
        'material_general_edit_ly
        '
        Me.material_general_edit_ly.ColumnCount = 8
        Me.material_general_edit_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.90164!))
        Me.material_general_edit_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.44262!))
        Me.material_general_edit_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.508196!))
        Me.material_general_edit_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.4918!))
        Me.material_general_edit_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.52459!))
        Me.material_general_edit_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.73771!))
        Me.material_general_edit_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.52459!))
        Me.material_general_edit_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.86886!))
        Me.material_general_edit_ly.Controls.Add(Me.is_mat_reported_val, 5, 1)
        Me.material_general_edit_ly.Controls.Add(Me.mat_payer_val, 3, 1)
        Me.material_general_edit_ly.Controls.Add(Me.sn_on_val, 3, 0)
        Me.material_general_edit_ly.Controls.Add(Me.part_number_val, 1, 0)
        Me.material_general_edit_ly.Controls.Add(Me.material_type_lbl, 4, 0)
        Me.material_general_edit_ly.Controls.Add(Me.part_number_lbl, 0, 0)
        Me.material_general_edit_ly.Controls.Add(Me.mat_text5_lbl, 6, 2)
        Me.material_general_edit_ly.Controls.Add(Me.sn_on_lbl, 2, 0)
        Me.material_general_edit_ly.Controls.Add(Me.is_mat_reported_lbl, 4, 1)
        Me.material_general_edit_ly.Controls.Add(Me.mat_payer_lbl, 2, 1)
        Me.material_general_edit_ly.Controls.Add(Me.mat_nte_val, 5, 2)
        Me.material_general_edit_ly.Controls.Add(Me.mat_discount_val, 3, 2)
        Me.material_general_edit_ly.Controls.Add(Me.mat_nte_lbl, 4, 2)
        Me.material_general_edit_ly.Controls.Add(Me.is_init_scope_lbl, 0, 1)
        Me.material_general_edit_ly.Controls.Add(Me.mat_discount_lbl, 2, 2)
        Me.material_general_edit_ly.Controls.Add(Me.billing_work_status_lbl, 6, 1)
        Me.material_general_edit_ly.Controls.Add(Me.billing_work_comment_lbl, 0, 2)
        Me.material_general_edit_ly.Controls.Add(Me.billing_work_comment_val, 1, 2)
        Me.material_general_edit_ly.Controls.Add(Me.billing_work_status_val, 7, 1)
        Me.material_general_edit_ly.Controls.Add(Me.material_type_val, 5, 0)
        Me.material_general_edit_ly.Controls.Add(Me.is_init_scope_val, 1, 1)
        Me.material_general_edit_ly.Controls.Add(Me.transac_cond_lbl, 6, 0)
        Me.material_general_edit_ly.Controls.Add(Me.mat_text5_val, 7, 2)
        Me.material_general_edit_ly.Controls.Add(Me.transac_cond_val, 7, 0)
        Me.material_general_edit_ly.Location = New System.Drawing.Point(13, 27)
        Me.material_general_edit_ly.Name = "material_general_edit_ly"
        Me.material_general_edit_ly.RowCount = 3
        Me.material_general_edit_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.material_general_edit_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.material_general_edit_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.material_general_edit_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.material_general_edit_ly.Size = New System.Drawing.Size(1481, 87)
        Me.material_general_edit_ly.TabIndex = 1
        '
        'is_mat_reported_val
        '
        Me.is_mat_reported_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.is_mat_reported_val.AutoSize = True
        Me.is_mat_reported_val.Location = New System.Drawing.Point(858, 36)
        Me.is_mat_reported_val.Name = "is_mat_reported_val"
        Me.is_mat_reported_val.Size = New System.Drawing.Size(15, 14)
        Me.is_mat_reported_val.TabIndex = 23
        Me.is_mat_reported_val.UseVisualStyleBackColor = True
        '
        'mat_payer_val
        '
        Me.mat_payer_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_payer_val.FormattingEnabled = True
        Me.mat_payer_val.Location = New System.Drawing.Point(503, 33)
        Me.mat_payer_val.Name = "mat_payer_val"
        Me.mat_payer_val.Size = New System.Drawing.Size(182, 21)
        Me.mat_payer_val.TabIndex = 22
        '
        'sn_on_val
        '
        Me.sn_on_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.sn_on_val.Location = New System.Drawing.Point(503, 4)
        Me.sn_on_val.Name = "sn_on_val"
        Me.sn_on_val.Size = New System.Drawing.Size(112, 20)
        Me.sn_on_val.TabIndex = 15
        '
        'part_number_val
        '
        Me.part_number_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.part_number_val.Location = New System.Drawing.Point(164, 4)
        Me.part_number_val.Name = "part_number_val"
        Me.part_number_val.Size = New System.Drawing.Size(161, 20)
        Me.part_number_val.TabIndex = 1
        '
        'material_type_lbl
        '
        Me.material_type_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.material_type_lbl.AutoSize = True
        Me.material_type_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.material_type_lbl.Location = New System.Drawing.Point(732, 8)
        Me.material_type_lbl.Name = "material_type_lbl"
        Me.material_type_lbl.Size = New System.Drawing.Size(92, 13)
        Me.material_type_lbl.TabIndex = 4
        Me.material_type_lbl.Text = "Material Type :"
        '
        'part_number_lbl
        '
        Me.part_number_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.part_number_lbl.AutoSize = True
        Me.part_number_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.part_number_lbl.Location = New System.Drawing.Point(3, 8)
        Me.part_number_lbl.Name = "part_number_lbl"
        Me.part_number_lbl.Size = New System.Drawing.Size(85, 13)
        Me.part_number_lbl.TabIndex = 0
        Me.part_number_lbl.Text = "Part Number :"
        '
        'mat_text5_lbl
        '
        Me.mat_text5_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_text5_lbl.AutoSize = True
        Me.mat_text5_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mat_text5_lbl.Location = New System.Drawing.Point(1091, 66)
        Me.mat_text5_lbl.Name = "mat_text5_lbl"
        Me.mat_text5_lbl.Size = New System.Drawing.Size(47, 13)
        Me.mat_text5_lbl.TabIndex = 6
        Me.mat_text5_lbl.Text = "Text5 :"
        '
        'sn_on_lbl
        '
        Me.sn_on_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.sn_on_lbl.AutoSize = True
        Me.sn_on_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sn_on_lbl.Location = New System.Drawing.Point(363, 8)
        Me.sn_on_lbl.Name = "sn_on_lbl"
        Me.sn_on_lbl.Size = New System.Drawing.Size(56, 13)
        Me.sn_on_lbl.TabIndex = 2
        Me.sn_on_lbl.Text = "SN/ON :"
        '
        'is_mat_reported_lbl
        '
        Me.is_mat_reported_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.is_mat_reported_lbl.AutoSize = True
        Me.is_mat_reported_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.is_mat_reported_lbl.Location = New System.Drawing.Point(732, 37)
        Me.is_mat_reported_lbl.Name = "is_mat_reported_lbl"
        Me.is_mat_reported_lbl.Size = New System.Drawing.Size(92, 13)
        Me.is_mat_reported_lbl.TabIndex = 12
        Me.is_mat_reported_lbl.Text = "Is Reported ? :"
        '
        'mat_payer_lbl
        '
        Me.mat_payer_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_payer_lbl.AutoSize = True
        Me.mat_payer_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mat_payer_lbl.Location = New System.Drawing.Point(363, 37)
        Me.mat_payer_lbl.Name = "mat_payer_lbl"
        Me.mat_payer_lbl.Size = New System.Drawing.Size(47, 13)
        Me.mat_payer_lbl.TabIndex = 10
        Me.mat_payer_lbl.Text = "Payer :"
        '
        'mat_nte_val
        '
        Me.mat_nte_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_nte_val.FormattingEnabled = True
        Me.mat_nte_val.Location = New System.Drawing.Point(858, 62)
        Me.mat_nte_val.Name = "mat_nte_val"
        Me.mat_nte_val.Size = New System.Drawing.Size(112, 21)
        Me.mat_nte_val.TabIndex = 22
        '
        'mat_discount_val
        '
        Me.mat_discount_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_discount_val.FormattingEnabled = True
        Me.mat_discount_val.Location = New System.Drawing.Point(503, 62)
        Me.mat_discount_val.Name = "mat_discount_val"
        Me.mat_discount_val.Size = New System.Drawing.Size(112, 21)
        Me.mat_discount_val.TabIndex = 22
        '
        'mat_nte_lbl
        '
        Me.mat_nte_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_nte_lbl.AutoSize = True
        Me.mat_nte_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mat_nte_lbl.Location = New System.Drawing.Point(732, 66)
        Me.mat_nte_lbl.Name = "mat_nte_lbl"
        Me.mat_nte_lbl.Size = New System.Drawing.Size(40, 13)
        Me.mat_nte_lbl.TabIndex = 11
        Me.mat_nte_lbl.Text = "NTE :"
        '
        'is_init_scope_lbl
        '
        Me.is_init_scope_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.is_init_scope_lbl.AutoSize = True
        Me.is_init_scope_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.is_init_scope_lbl.Location = New System.Drawing.Point(3, 37)
        Me.is_init_scope_lbl.Name = "is_init_scope_lbl"
        Me.is_init_scope_lbl.Size = New System.Drawing.Size(97, 13)
        Me.is_init_scope_lbl.TabIndex = 6
        Me.is_init_scope_lbl.Text = "Initial Scope ? :"
        '
        'mat_discount_lbl
        '
        Me.mat_discount_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_discount_lbl.AutoSize = True
        Me.mat_discount_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mat_discount_lbl.Location = New System.Drawing.Point(363, 66)
        Me.mat_discount_lbl.Name = "mat_discount_lbl"
        Me.mat_discount_lbl.Size = New System.Drawing.Size(65, 13)
        Me.mat_discount_lbl.TabIndex = 12
        Me.mat_discount_lbl.Text = "Discount :"
        '
        'billing_work_status_lbl
        '
        Me.billing_work_status_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.billing_work_status_lbl.AutoSize = True
        Me.billing_work_status_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.billing_work_status_lbl.Location = New System.Drawing.Point(1091, 37)
        Me.billing_work_status_lbl.Name = "billing_work_status_lbl"
        Me.billing_work_status_lbl.Size = New System.Drawing.Size(89, 13)
        Me.billing_work_status_lbl.TabIndex = 16
        Me.billing_work_status_lbl.Text = "Billing Status :"
        '
        'billing_work_comment_lbl
        '
        Me.billing_work_comment_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.billing_work_comment_lbl.AutoSize = True
        Me.billing_work_comment_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.billing_work_comment_lbl.Location = New System.Drawing.Point(3, 66)
        Me.billing_work_comment_lbl.Name = "billing_work_comment_lbl"
        Me.billing_work_comment_lbl.Size = New System.Drawing.Size(104, 13)
        Me.billing_work_comment_lbl.TabIndex = 17
        Me.billing_work_comment_lbl.Text = "Billing Comment :"
        '
        'billing_work_comment_val
        '
        Me.billing_work_comment_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.billing_work_comment_val.Location = New System.Drawing.Point(164, 62)
        Me.billing_work_comment_val.Name = "billing_work_comment_val"
        Me.billing_work_comment_val.Size = New System.Drawing.Size(161, 20)
        Me.billing_work_comment_val.TabIndex = 18
        '
        'billing_work_status_val
        '
        Me.billing_work_status_val.FormattingEnabled = True
        Me.billing_work_status_val.Location = New System.Drawing.Point(1217, 32)
        Me.billing_work_status_val.Name = "billing_work_status_val"
        Me.billing_work_status_val.Size = New System.Drawing.Size(121, 21)
        Me.billing_work_status_val.TabIndex = 19
        '
        'material_type_val
        '
        Me.material_type_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.material_type_val.FormattingEnabled = True
        Me.material_type_val.Location = New System.Drawing.Point(858, 4)
        Me.material_type_val.Name = "material_type_val"
        Me.material_type_val.Size = New System.Drawing.Size(112, 21)
        Me.material_type_val.TabIndex = 21
        '
        'is_init_scope_val
        '
        Me.is_init_scope_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.is_init_scope_val.AutoSize = True
        Me.is_init_scope_val.Location = New System.Drawing.Point(164, 36)
        Me.is_init_scope_val.Name = "is_init_scope_val"
        Me.is_init_scope_val.Size = New System.Drawing.Size(15, 14)
        Me.is_init_scope_val.TabIndex = 20
        Me.is_init_scope_val.UseVisualStyleBackColor = True
        '
        'transac_cond_lbl
        '
        Me.transac_cond_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.transac_cond_lbl.AutoSize = True
        Me.transac_cond_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.transac_cond_lbl.Location = New System.Drawing.Point(1091, 8)
        Me.transac_cond_lbl.Name = "transac_cond_lbl"
        Me.transac_cond_lbl.Size = New System.Drawing.Size(112, 13)
        Me.transac_cond_lbl.TabIndex = 17
        Me.transac_cond_lbl.Text = "Trans. & Condition :"
        '
        'mat_text5_val
        '
        Me.mat_text5_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mat_text5_val.Location = New System.Drawing.Point(1217, 62)
        Me.mat_text5_val.Name = "mat_text5_val"
        Me.mat_text5_val.Size = New System.Drawing.Size(216, 20)
        Me.mat_text5_val.TabIndex = 10
        '
        'transac_cond_val
        '
        Me.transac_cond_val.FormattingEnabled = True
        Me.transac_cond_val.Location = New System.Drawing.Point(1217, 3)
        Me.transac_cond_val.Name = "transac_cond_val"
        Me.transac_cond_val.Size = New System.Drawing.Size(121, 21)
        Me.transac_cond_val.TabIndex = 20
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.zprm_zstm_grp)
        Me.FlowLayoutPanel1.Controls.Add(Me.markup_zmar_grp)
        Me.FlowLayoutPanel1.Controls.Add(Me.freight_zfra_grp)
        Me.FlowLayoutPanel1.Controls.Add(Me.total_mat_price_grp)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 503)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1496, 150)
        Me.FlowLayoutPanel1.TabIndex = 11
        '
        'zprm_zstm_grp
        '
        Me.zprm_zstm_grp.Controls.Add(Me.TableLayoutPanel1)
        Me.zprm_zstm_grp.Location = New System.Drawing.Point(5, 5)
        Me.zprm_zstm_grp.Margin = New System.Windows.Forms.Padding(5, 5, 5, 10)
        Me.zprm_zstm_grp.Name = "zprm_zstm_grp"
        Me.zprm_zstm_grp.Size = New System.Drawing.Size(357, 138)
        Me.zprm_zstm_grp.TabIndex = 5
        Me.zprm_zstm_grp.TabStop = False
        Me.zprm_zstm_grp.Text = "ZPRM/ZSTM"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.13235!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.86765!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.total_sales_price_zprm_val, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.total_sales_price_zprm_lbl, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.unit_sales_price_zstm_lbl, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.zprm_zstm_curr_lbl, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.unit_sales_price_zstm_val, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.zprm_zstm_curr_val, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.zprm_zstm_curr_tagk_val, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.zprm_zstm_curr_tagk_lbl, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.total_sales_price_zprm_flag_val, 2, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(10, 27)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(338, 105)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'total_sales_price_zprm_val
        '
        Me.total_sales_price_zprm_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_sales_price_zprm_val.Location = New System.Drawing.Point(178, 3)
        Me.total_sales_price_zprm_val.Name = "total_sales_price_zprm_val"
        Me.total_sales_price_zprm_val.Size = New System.Drawing.Size(91, 20)
        Me.total_sales_price_zprm_val.TabIndex = 1
        '
        'total_sales_price_zprm_lbl
        '
        Me.total_sales_price_zprm_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_sales_price_zprm_lbl.AutoSize = True
        Me.total_sales_price_zprm_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.total_sales_price_zprm_lbl.Location = New System.Drawing.Point(3, 6)
        Me.total_sales_price_zprm_lbl.Name = "total_sales_price_zprm_lbl"
        Me.total_sales_price_zprm_lbl.Size = New System.Drawing.Size(151, 13)
        Me.total_sales_price_zprm_lbl.TabIndex = 0
        Me.total_sales_price_zprm_lbl.Text = "Total Sales Price ZPRM :"
        '
        'unit_sales_price_zstm_lbl
        '
        Me.unit_sales_price_zstm_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.unit_sales_price_zstm_lbl.AutoSize = True
        Me.unit_sales_price_zstm_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.unit_sales_price_zstm_lbl.Location = New System.Drawing.Point(3, 32)
        Me.unit_sales_price_zstm_lbl.Name = "unit_sales_price_zstm_lbl"
        Me.unit_sales_price_zstm_lbl.Size = New System.Drawing.Size(133, 13)
        Me.unit_sales_price_zstm_lbl.TabIndex = 6
        Me.unit_sales_price_zstm_lbl.Text = "Unit List Price ZSTM :"
        '
        'zprm_zstm_curr_lbl
        '
        Me.zprm_zstm_curr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.zprm_zstm_curr_lbl.AutoSize = True
        Me.zprm_zstm_curr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zprm_zstm_curr_lbl.Location = New System.Drawing.Point(3, 58)
        Me.zprm_zstm_curr_lbl.Name = "zprm_zstm_curr_lbl"
        Me.zprm_zstm_curr_lbl.Size = New System.Drawing.Size(117, 13)
        Me.zprm_zstm_curr_lbl.TabIndex = 17
        Me.zprm_zstm_curr_lbl.Text = "ZPRM/ZSTM Curr :"
        '
        'unit_sales_price_zstm_val
        '
        Me.unit_sales_price_zstm_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.unit_sales_price_zstm_val.Location = New System.Drawing.Point(178, 29)
        Me.unit_sales_price_zstm_val.Name = "unit_sales_price_zstm_val"
        Me.unit_sales_price_zstm_val.Size = New System.Drawing.Size(91, 20)
        Me.unit_sales_price_zstm_val.TabIndex = 18
        '
        'zprm_zstm_curr_val
        '
        Me.zprm_zstm_curr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.zprm_zstm_curr_val.FormattingEnabled = True
        Me.zprm_zstm_curr_val.Location = New System.Drawing.Point(178, 55)
        Me.zprm_zstm_curr_val.Name = "zprm_zstm_curr_val"
        Me.zprm_zstm_curr_val.Size = New System.Drawing.Size(91, 21)
        Me.zprm_zstm_curr_val.TabIndex = 21
        '
        'zprm_zstm_curr_tagk_val
        '
        Me.zprm_zstm_curr_tagk_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.zprm_zstm_curr_tagk_val.Location = New System.Drawing.Point(178, 81)
        Me.zprm_zstm_curr_tagk_val.Name = "zprm_zstm_curr_tagk_val"
        Me.zprm_zstm_curr_tagk_val.Size = New System.Drawing.Size(91, 20)
        Me.zprm_zstm_curr_tagk_val.TabIndex = 23
        '
        'zprm_zstm_curr_tagk_lbl
        '
        Me.zprm_zstm_curr_tagk_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.zprm_zstm_curr_tagk_lbl.AutoSize = True
        Me.zprm_zstm_curr_tagk_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.zprm_zstm_curr_tagk_lbl.Location = New System.Drawing.Point(3, 85)
        Me.zprm_zstm_curr_tagk_lbl.Name = "zprm_zstm_curr_tagk_lbl"
        Me.zprm_zstm_curr_tagk_lbl.Size = New System.Drawing.Size(154, 13)
        Me.zprm_zstm_curr_tagk_lbl.TabIndex = 22
        Me.zprm_zstm_curr_tagk_lbl.Text = "ZPRM/ZSTM Curr TAGK :"
        '
        'total_sales_price_zprm_flag_val
        '
        Me.total_sales_price_zprm_flag_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_sales_price_zprm_flag_val.AutoSize = True
        Me.total_sales_price_zprm_flag_val.Location = New System.Drawing.Point(285, 6)
        Me.total_sales_price_zprm_flag_val.Name = "total_sales_price_zprm_flag_val"
        Me.total_sales_price_zprm_flag_val.Size = New System.Drawing.Size(15, 14)
        Me.total_sales_price_zprm_flag_val.TabIndex = 25
        Me.total_sales_price_zprm_flag_val.UseVisualStyleBackColor = True
        '
        'markup_zmar_grp
        '
        Me.markup_zmar_grp.Controls.Add(Me.markup_zmar_ly)
        Me.markup_zmar_grp.Location = New System.Drawing.Point(372, 5)
        Me.markup_zmar_grp.Margin = New System.Windows.Forms.Padding(5, 5, 5, 10)
        Me.markup_zmar_grp.Name = "markup_zmar_grp"
        Me.markup_zmar_grp.Size = New System.Drawing.Size(361, 138)
        Me.markup_zmar_grp.TabIndex = 6
        Me.markup_zmar_grp.TabStop = False
        Me.markup_zmar_grp.Text = "Markup ZMAR"
        '
        'markup_zmar_ly
        '
        Me.markup_zmar_ly.ColumnCount = 2
        Me.markup_zmar_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.32404!))
        Me.markup_zmar_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.67596!))
        Me.markup_zmar_ly.Controls.Add(Me.markup_zmar_def_val, 1, 0)
        Me.markup_zmar_ly.Controls.Add(Me.markup_zmar_def_lbl, 0, 0)
        Me.markup_zmar_ly.Controls.Add(Me.markup_zmar_is_overwr_lbl, 0, 1)
        Me.markup_zmar_ly.Controls.Add(Me.markup_zmar_overwritten_lbl, 0, 2)
        Me.markup_zmar_ly.Controls.Add(Me.markup_zmar_is_overwr_val, 1, 1)
        Me.markup_zmar_ly.Controls.Add(Me.markup_zmar_overwritten_val, 1, 2)
        Me.markup_zmar_ly.Controls.Add(Me.calc_markup_zmar_value_lbl, 0, 3)
        Me.markup_zmar_ly.Controls.Add(Me.calc_markup_zmar_value_val, 1, 3)
        Me.markup_zmar_ly.Location = New System.Drawing.Point(13, 27)
        Me.markup_zmar_ly.Name = "markup_zmar_ly"
        Me.markup_zmar_ly.RowCount = 4
        Me.markup_zmar_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.markup_zmar_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.markup_zmar_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.markup_zmar_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.markup_zmar_ly.Size = New System.Drawing.Size(342, 105)
        Me.markup_zmar_ly.TabIndex = 1
        '
        'markup_zmar_def_val
        '
        Me.markup_zmar_def_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.markup_zmar_def_val.Location = New System.Drawing.Point(212, 3)
        Me.markup_zmar_def_val.Name = "markup_zmar_def_val"
        Me.markup_zmar_def_val.Size = New System.Drawing.Size(105, 20)
        Me.markup_zmar_def_val.TabIndex = 1
        '
        'markup_zmar_def_lbl
        '
        Me.markup_zmar_def_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.markup_zmar_def_lbl.AutoSize = True
        Me.markup_zmar_def_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.markup_zmar_def_lbl.Location = New System.Drawing.Point(3, 6)
        Me.markup_zmar_def_lbl.Name = "markup_zmar_def_lbl"
        Me.markup_zmar_def_lbl.Size = New System.Drawing.Size(154, 13)
        Me.markup_zmar_def_lbl.TabIndex = 0
        Me.markup_zmar_def_lbl.Text = "Default Markup % ZMAR :"
        '
        'markup_zmar_is_overwr_lbl
        '
        Me.markup_zmar_is_overwr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.markup_zmar_is_overwr_lbl.AutoSize = True
        Me.markup_zmar_is_overwr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.markup_zmar_is_overwr_lbl.Location = New System.Drawing.Point(3, 32)
        Me.markup_zmar_is_overwr_lbl.Name = "markup_zmar_is_overwr_lbl"
        Me.markup_zmar_is_overwr_lbl.Size = New System.Drawing.Size(109, 13)
        Me.markup_zmar_is_overwr_lbl.TabIndex = 6
        Me.markup_zmar_is_overwr_lbl.Text = "Manual Markup? :"
        '
        'markup_zmar_overwritten_lbl
        '
        Me.markup_zmar_overwritten_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.markup_zmar_overwritten_lbl.AutoSize = True
        Me.markup_zmar_overwritten_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.markup_zmar_overwritten_lbl.Location = New System.Drawing.Point(3, 58)
        Me.markup_zmar_overwritten_lbl.Name = "markup_zmar_overwritten_lbl"
        Me.markup_zmar_overwritten_lbl.Size = New System.Drawing.Size(154, 13)
        Me.markup_zmar_overwritten_lbl.TabIndex = 17
        Me.markup_zmar_overwritten_lbl.Text = "Manual Markup % ZMAR :"
        '
        'markup_zmar_is_overwr_val
        '
        Me.markup_zmar_is_overwr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.markup_zmar_is_overwr_val.AutoSize = True
        Me.markup_zmar_is_overwr_val.Location = New System.Drawing.Point(212, 32)
        Me.markup_zmar_is_overwr_val.Name = "markup_zmar_is_overwr_val"
        Me.markup_zmar_is_overwr_val.Size = New System.Drawing.Size(15, 14)
        Me.markup_zmar_is_overwr_val.TabIndex = 24
        Me.markup_zmar_is_overwr_val.UseVisualStyleBackColor = True
        '
        'markup_zmar_overwritten_val
        '
        Me.markup_zmar_overwritten_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.markup_zmar_overwritten_val.Location = New System.Drawing.Point(212, 55)
        Me.markup_zmar_overwritten_val.Name = "markup_zmar_overwritten_val"
        Me.markup_zmar_overwritten_val.Size = New System.Drawing.Size(105, 20)
        Me.markup_zmar_overwritten_val.TabIndex = 7
        '
        'calc_markup_zmar_value_lbl
        '
        Me.calc_markup_zmar_value_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.calc_markup_zmar_value_lbl.AutoSize = True
        Me.calc_markup_zmar_value_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.calc_markup_zmar_value_lbl.Location = New System.Drawing.Point(3, 85)
        Me.calc_markup_zmar_value_lbl.Name = "calc_markup_zmar_value_lbl"
        Me.calc_markup_zmar_value_lbl.Size = New System.Drawing.Size(96, 13)
        Me.calc_markup_zmar_value_lbl.TabIndex = 11
        Me.calc_markup_zmar_value_lbl.Text = "Markup ZMAR :"
        '
        'calc_markup_zmar_value_val
        '
        Me.calc_markup_zmar_value_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.calc_markup_zmar_value_val.Location = New System.Drawing.Point(212, 81)
        Me.calc_markup_zmar_value_val.Name = "calc_markup_zmar_value_val"
        Me.calc_markup_zmar_value_val.Size = New System.Drawing.Size(105, 20)
        Me.calc_markup_zmar_value_val.TabIndex = 11
        '
        'freight_zfra_grp
        '
        Me.freight_zfra_grp.Controls.Add(Me.TableLayoutPanel2)
        Me.freight_zfra_grp.Location = New System.Drawing.Point(743, 5)
        Me.freight_zfra_grp.Margin = New System.Windows.Forms.Padding(5, 5, 5, 10)
        Me.freight_zfra_grp.Name = "freight_zfra_grp"
        Me.freight_zfra_grp.Size = New System.Drawing.Size(364, 138)
        Me.freight_zfra_grp.TabIndex = 7
        Me.freight_zfra_grp.TabStop = False
        Me.freight_zfra_grp.Text = "Freight ZFRA"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.64286!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.35714!))
        Me.TableLayoutPanel2.Controls.Add(Me.freight_zfra_default_val, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.freight_zfra_default_lbl, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.freight_zfra_is_overwr_lbl, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.freight_zfra_overwriten_lbl, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.freight_zfra_is_overwr_val, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.freight_zfra_overwriten_val, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.calc_freight_zfra_value_val, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.calc_freight_zfra_value_lbl, 0, 3)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(13, 27)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 4
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(345, 105)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'freight_zfra_default_val
        '
        Me.freight_zfra_default_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfra_default_val.Location = New System.Drawing.Point(208, 3)
        Me.freight_zfra_default_val.Name = "freight_zfra_default_val"
        Me.freight_zfra_default_val.Size = New System.Drawing.Size(112, 20)
        Me.freight_zfra_default_val.TabIndex = 1
        '
        'freight_zfra_default_lbl
        '
        Me.freight_zfra_default_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfra_default_lbl.AutoSize = True
        Me.freight_zfra_default_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.freight_zfra_default_lbl.Location = New System.Drawing.Point(3, 6)
        Me.freight_zfra_default_lbl.Name = "freight_zfra_default_lbl"
        Me.freight_zfra_default_lbl.Size = New System.Drawing.Size(148, 13)
        Me.freight_zfra_default_lbl.TabIndex = 0
        Me.freight_zfra_default_lbl.Text = "Default Freight % ZFRA :"
        '
        'freight_zfra_is_overwr_lbl
        '
        Me.freight_zfra_is_overwr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfra_is_overwr_lbl.AutoSize = True
        Me.freight_zfra_is_overwr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.freight_zfra_is_overwr_lbl.Location = New System.Drawing.Point(3, 32)
        Me.freight_zfra_is_overwr_lbl.Name = "freight_zfra_is_overwr_lbl"
        Me.freight_zfra_is_overwr_lbl.Size = New System.Drawing.Size(99, 13)
        Me.freight_zfra_is_overwr_lbl.TabIndex = 6
        Me.freight_zfra_is_overwr_lbl.Text = "Manual ZFRA? :"
        '
        'freight_zfra_overwriten_lbl
        '
        Me.freight_zfra_overwriten_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfra_overwriten_lbl.AutoSize = True
        Me.freight_zfra_overwriten_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.freight_zfra_overwriten_lbl.Location = New System.Drawing.Point(3, 58)
        Me.freight_zfra_overwriten_lbl.Name = "freight_zfra_overwriten_lbl"
        Me.freight_zfra_overwriten_lbl.Size = New System.Drawing.Size(148, 13)
        Me.freight_zfra_overwriten_lbl.TabIndex = 17
        Me.freight_zfra_overwriten_lbl.Text = "Manual Freight % ZFRA :"
        '
        'freight_zfra_is_overwr_val
        '
        Me.freight_zfra_is_overwr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfra_is_overwr_val.AutoSize = True
        Me.freight_zfra_is_overwr_val.Location = New System.Drawing.Point(208, 32)
        Me.freight_zfra_is_overwr_val.Name = "freight_zfra_is_overwr_val"
        Me.freight_zfra_is_overwr_val.Size = New System.Drawing.Size(15, 14)
        Me.freight_zfra_is_overwr_val.TabIndex = 24
        Me.freight_zfra_is_overwr_val.UseVisualStyleBackColor = True
        '
        'freight_zfra_overwriten_val
        '
        Me.freight_zfra_overwriten_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfra_overwriten_val.Location = New System.Drawing.Point(208, 55)
        Me.freight_zfra_overwriten_val.Name = "freight_zfra_overwriten_val"
        Me.freight_zfra_overwriten_val.Size = New System.Drawing.Size(112, 20)
        Me.freight_zfra_overwriten_val.TabIndex = 7
        '
        'calc_freight_zfra_value_val
        '
        Me.calc_freight_zfra_value_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.calc_freight_zfra_value_val.Location = New System.Drawing.Point(208, 81)
        Me.calc_freight_zfra_value_val.Name = "calc_freight_zfra_value_val"
        Me.calc_freight_zfra_value_val.Size = New System.Drawing.Size(112, 20)
        Me.calc_freight_zfra_value_val.TabIndex = 26
        '
        'calc_freight_zfra_value_lbl
        '
        Me.calc_freight_zfra_value_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.calc_freight_zfra_value_lbl.AutoSize = True
        Me.calc_freight_zfra_value_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.calc_freight_zfra_value_lbl.Location = New System.Drawing.Point(3, 85)
        Me.calc_freight_zfra_value_lbl.Name = "calc_freight_zfra_value_lbl"
        Me.calc_freight_zfra_value_lbl.Size = New System.Drawing.Size(90, 13)
        Me.calc_freight_zfra_value_lbl.TabIndex = 25
        Me.calc_freight_zfra_value_lbl.Text = "Freight ZFRA :"
        '
        'total_mat_price_grp
        '
        Me.total_mat_price_grp.Controls.Add(Me.TableLayoutPanel3)
        Me.total_mat_price_grp.Location = New System.Drawing.Point(1117, 5)
        Me.total_mat_price_grp.Margin = New System.Windows.Forms.Padding(5, 5, 5, 10)
        Me.total_mat_price_grp.Name = "total_mat_price_grp"
        Me.total_mat_price_grp.Size = New System.Drawing.Size(365, 138)
        Me.total_mat_price_grp.TabIndex = 8
        Me.total_mat_price_grp.TabStop = False
        Me.total_mat_price_grp.Text = "Total Material Price"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.34463!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.65537!))
        Me.TableLayoutPanel3.Controls.Add(Me.total_mat_in_ce_curr_bef_disc_val, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.total_mat_in_ce_curr_bef_disc_lbl, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.total_mat_in_ce_curr_disc_lbl, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.total_mat_in_ce_curr_after_disc_lbl, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.total_mat_in_ce_curr_disc_val, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.total_mat_in_ce_curr_after_disc_val, 1, 2)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(6, 27)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 4
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(354, 105)
        Me.TableLayoutPanel3.TabIndex = 1
        '
        'total_mat_in_ce_curr_bef_disc_val
        '
        Me.total_mat_in_ce_curr_bef_disc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_mat_in_ce_curr_bef_disc_val.Location = New System.Drawing.Point(205, 3)
        Me.total_mat_in_ce_curr_bef_disc_val.Name = "total_mat_in_ce_curr_bef_disc_val"
        Me.total_mat_in_ce_curr_bef_disc_val.Size = New System.Drawing.Size(140, 20)
        Me.total_mat_in_ce_curr_bef_disc_val.TabIndex = 1
        '
        'total_mat_in_ce_curr_bef_disc_lbl
        '
        Me.total_mat_in_ce_curr_bef_disc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_mat_in_ce_curr_bef_disc_lbl.AutoSize = True
        Me.total_mat_in_ce_curr_bef_disc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.total_mat_in_ce_curr_bef_disc_lbl.Location = New System.Drawing.Point(3, 6)
        Me.total_mat_in_ce_curr_bef_disc_lbl.Name = "total_mat_in_ce_curr_bef_disc_lbl"
        Me.total_mat_in_ce_curr_bef_disc_lbl.Size = New System.Drawing.Size(118, 13)
        Me.total_mat_in_ce_curr_bef_disc_lbl.TabIndex = 0
        Me.total_mat_in_ce_curr_bef_disc_lbl.Text = "Total Before Disc. :"
        '
        'total_mat_in_ce_curr_disc_lbl
        '
        Me.total_mat_in_ce_curr_disc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_mat_in_ce_curr_disc_lbl.AutoSize = True
        Me.total_mat_in_ce_curr_disc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.total_mat_in_ce_curr_disc_lbl.Location = New System.Drawing.Point(3, 32)
        Me.total_mat_in_ce_curr_disc_lbl.Name = "total_mat_in_ce_curr_disc_lbl"
        Me.total_mat_in_ce_curr_disc_lbl.Size = New System.Drawing.Size(65, 13)
        Me.total_mat_in_ce_curr_disc_lbl.TabIndex = 6
        Me.total_mat_in_ce_curr_disc_lbl.Text = "Discount :"
        '
        'total_mat_in_ce_curr_after_disc_lbl
        '
        Me.total_mat_in_ce_curr_after_disc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_mat_in_ce_curr_after_disc_lbl.AutoSize = True
        Me.total_mat_in_ce_curr_after_disc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.total_mat_in_ce_curr_after_disc_lbl.Location = New System.Drawing.Point(3, 58)
        Me.total_mat_in_ce_curr_after_disc_lbl.Name = "total_mat_in_ce_curr_after_disc_lbl"
        Me.total_mat_in_ce_curr_after_disc_lbl.Size = New System.Drawing.Size(130, 13)
        Me.total_mat_in_ce_curr_after_disc_lbl.TabIndex = 17
        Me.total_mat_in_ce_curr_after_disc_lbl.Text = "Total Mat in CE Curr :"
        '
        'total_mat_in_ce_curr_disc_val
        '
        Me.total_mat_in_ce_curr_disc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_mat_in_ce_curr_disc_val.Location = New System.Drawing.Point(205, 29)
        Me.total_mat_in_ce_curr_disc_val.Name = "total_mat_in_ce_curr_disc_val"
        Me.total_mat_in_ce_curr_disc_val.Size = New System.Drawing.Size(140, 20)
        Me.total_mat_in_ce_curr_disc_val.TabIndex = 7
        '
        'total_mat_in_ce_curr_after_disc_val
        '
        Me.total_mat_in_ce_curr_after_disc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.total_mat_in_ce_curr_after_disc_val.Location = New System.Drawing.Point(205, 55)
        Me.total_mat_in_ce_curr_after_disc_val.Name = "total_mat_in_ce_curr_after_disc_val"
        Me.total_mat_in_ce_curr_after_disc_val.Size = New System.Drawing.Size(140, 20)
        Me.total_mat_in_ce_curr_after_disc_val.TabIndex = 25
        '
        'freight_zfrp_grp
        '
        Me.freight_zfrp_grp.Controls.Add(Me.freight_zfrp_ly)
        Me.freight_zfrp_grp.Location = New System.Drawing.Point(1507, 505)
        Me.freight_zfrp_grp.Margin = New System.Windows.Forms.Padding(5, 5, 5, 10)
        Me.freight_zfrp_grp.Name = "freight_zfrp_grp"
        Me.freight_zfrp_grp.Size = New System.Drawing.Size(728, 150)
        Me.freight_zfrp_grp.TabIndex = 9
        Me.freight_zfrp_grp.TabStop = False
        Me.freight_zfrp_grp.Text = "Freight ZFRP"
        '
        'freight_zfrp_ly
        '
        Me.freight_zfrp_ly.ColumnCount = 4
        Me.freight_zfrp_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.82143!))
        Me.freight_zfrp_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.17857!))
        Me.freight_zfrp_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.71115!))
        Me.freight_zfrp_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.52433!))
        Me.freight_zfrp_ly.Controls.Add(Me.freight_zfrp_tagk_lbl, 0, 3)
        Me.freight_zfrp_ly.Controls.Add(Me.freight_zfrp_lbl, 0, 0)
        Me.freight_zfrp_ly.Controls.Add(Me.overwrit_freight_zfrp_val_lbl, 0, 1)
        Me.freight_zfrp_ly.Controls.Add(Me.overwrit_freight_zfrp_curr_lbl, 0, 2)
        Me.freight_zfrp_ly.Controls.Add(Me.freight_zfrp_val, 1, 0)
        Me.freight_zfrp_ly.Controls.Add(Me.overwrit_freight_zfrp_val_val, 1, 1)
        Me.freight_zfrp_ly.Controls.Add(Me.freight_zfrp_tagk_val, 1, 3)
        Me.freight_zfrp_ly.Controls.Add(Me.overwrit_freight_zfrp_curr_val, 1, 2)
        Me.freight_zfrp_ly.Controls.Add(Me.freight_zfrp_in_ce_curr_aft_disc_val, 3, 2)
        Me.freight_zfrp_ly.Controls.Add(Me.freight_zfrp_in_ce_curr_disc_val, 3, 1)
        Me.freight_zfrp_ly.Controls.Add(Me.freight_zfrp_in_ce_curr_aft_disc_lbl, 2, 2)
        Me.freight_zfrp_ly.Controls.Add(Me.freight_zfrp_in_ce_curr_disc_lbl, 2, 1)
        Me.freight_zfrp_ly.Controls.Add(Me.freight_zfrp_in_ce_curr_bef_disc_val, 3, 0)
        Me.freight_zfrp_ly.Controls.Add(Me.freight_zfrp_in_ce_curr_bef_disc_lbl, 2, 0)
        Me.freight_zfrp_ly.Location = New System.Drawing.Point(13, 27)
        Me.freight_zfrp_ly.Name = "freight_zfrp_ly"
        Me.freight_zfrp_ly.RowCount = 4
        Me.freight_zfrp_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.freight_zfrp_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.freight_zfrp_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.freight_zfrp_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.freight_zfrp_ly.Size = New System.Drawing.Size(709, 105)
        Me.freight_zfrp_ly.TabIndex = 1
        '
        'freight_zfrp_tagk_lbl
        '
        Me.freight_zfrp_tagk_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfrp_tagk_lbl.AutoSize = True
        Me.freight_zfrp_tagk_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.freight_zfrp_tagk_lbl.Location = New System.Drawing.Point(3, 85)
        Me.freight_zfrp_tagk_lbl.Name = "freight_zfrp_tagk_lbl"
        Me.freight_zfrp_tagk_lbl.Size = New System.Drawing.Size(84, 13)
        Me.freight_zfrp_tagk_lbl.TabIndex = 24
        Me.freight_zfrp_tagk_lbl.Text = "ZFRP TAGK :"
        '
        'freight_zfrp_lbl
        '
        Me.freight_zfrp_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfrp_lbl.AutoSize = True
        Me.freight_zfrp_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.freight_zfrp_lbl.Location = New System.Drawing.Point(3, 6)
        Me.freight_zfrp_lbl.Name = "freight_zfrp_lbl"
        Me.freight_zfrp_lbl.Size = New System.Drawing.Size(145, 13)
        Me.freight_zfrp_lbl.TabIndex = 0
        Me.freight_zfrp_lbl.Text = "Standard Freight ZFRP :"
        '
        'overwrit_freight_zfrp_val_lbl
        '
        Me.overwrit_freight_zfrp_val_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.overwrit_freight_zfrp_val_lbl.AutoSize = True
        Me.overwrit_freight_zfrp_val_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.overwrit_freight_zfrp_val_lbl.Location = New System.Drawing.Point(3, 32)
        Me.overwrit_freight_zfrp_val_lbl.Name = "overwrit_freight_zfrp_val_lbl"
        Me.overwrit_freight_zfrp_val_lbl.Size = New System.Drawing.Size(128, 13)
        Me.overwrit_freight_zfrp_val_lbl.TabIndex = 6
        Me.overwrit_freight_zfrp_val_lbl.Text = "Manual ZFRP Value :"
        '
        'overwrit_freight_zfrp_curr_lbl
        '
        Me.overwrit_freight_zfrp_curr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.overwrit_freight_zfrp_curr_lbl.AutoSize = True
        Me.overwrit_freight_zfrp_curr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.overwrit_freight_zfrp_curr_lbl.Location = New System.Drawing.Point(3, 58)
        Me.overwrit_freight_zfrp_curr_lbl.Name = "overwrit_freight_zfrp_curr_lbl"
        Me.overwrit_freight_zfrp_curr_lbl.Size = New System.Drawing.Size(127, 13)
        Me.overwrit_freight_zfrp_curr_lbl.TabIndex = 17
        Me.overwrit_freight_zfrp_curr_lbl.Text = "Manual ZFRP  Curr. :"
        '
        'freight_zfrp_val
        '
        Me.freight_zfrp_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfrp_val.FormattingEnabled = True
        Me.freight_zfrp_val.Location = New System.Drawing.Point(213, 3)
        Me.freight_zfrp_val.Name = "freight_zfrp_val"
        Me.freight_zfrp_val.Size = New System.Drawing.Size(108, 21)
        Me.freight_zfrp_val.TabIndex = 22
        '
        'overwrit_freight_zfrp_val_val
        '
        Me.overwrit_freight_zfrp_val_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.overwrit_freight_zfrp_val_val.Location = New System.Drawing.Point(213, 29)
        Me.overwrit_freight_zfrp_val_val.Name = "overwrit_freight_zfrp_val_val"
        Me.overwrit_freight_zfrp_val_val.Size = New System.Drawing.Size(108, 20)
        Me.overwrit_freight_zfrp_val_val.TabIndex = 10
        '
        'freight_zfrp_tagk_val
        '
        Me.freight_zfrp_tagk_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfrp_tagk_val.Location = New System.Drawing.Point(213, 81)
        Me.freight_zfrp_tagk_val.Name = "freight_zfrp_tagk_val"
        Me.freight_zfrp_tagk_val.Size = New System.Drawing.Size(108, 20)
        Me.freight_zfrp_tagk_val.TabIndex = 7
        '
        'overwrit_freight_zfrp_curr_val
        '
        Me.overwrit_freight_zfrp_curr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.overwrit_freight_zfrp_curr_val.FormattingEnabled = True
        Me.overwrit_freight_zfrp_curr_val.Location = New System.Drawing.Point(213, 55)
        Me.overwrit_freight_zfrp_curr_val.Name = "overwrit_freight_zfrp_curr_val"
        Me.overwrit_freight_zfrp_curr_val.Size = New System.Drawing.Size(108, 21)
        Me.overwrit_freight_zfrp_curr_val.TabIndex = 23
        '
        'freight_zfrp_in_ce_curr_aft_disc_val
        '
        Me.freight_zfrp_in_ce_curr_aft_disc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfrp_in_ce_curr_aft_disc_val.Location = New System.Drawing.Point(579, 55)
        Me.freight_zfrp_in_ce_curr_aft_disc_val.Name = "freight_zfrp_in_ce_curr_aft_disc_val"
        Me.freight_zfrp_in_ce_curr_aft_disc_val.Size = New System.Drawing.Size(113, 20)
        Me.freight_zfrp_in_ce_curr_aft_disc_val.TabIndex = 11
        '
        'freight_zfrp_in_ce_curr_disc_val
        '
        Me.freight_zfrp_in_ce_curr_disc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfrp_in_ce_curr_disc_val.Location = New System.Drawing.Point(579, 29)
        Me.freight_zfrp_in_ce_curr_disc_val.Name = "freight_zfrp_in_ce_curr_disc_val"
        Me.freight_zfrp_in_ce_curr_disc_val.Size = New System.Drawing.Size(113, 20)
        Me.freight_zfrp_in_ce_curr_disc_val.TabIndex = 10
        '
        'freight_zfrp_in_ce_curr_aft_disc_lbl
        '
        Me.freight_zfrp_in_ce_curr_aft_disc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfrp_in_ce_curr_aft_disc_lbl.AutoSize = True
        Me.freight_zfrp_in_ce_curr_aft_disc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.freight_zfrp_in_ce_curr_aft_disc_lbl.Location = New System.Drawing.Point(355, 58)
        Me.freight_zfrp_in_ce_curr_aft_disc_lbl.Name = "freight_zfrp_in_ce_curr_aft_disc_lbl"
        Me.freight_zfrp_in_ce_curr_aft_disc_lbl.Size = New System.Drawing.Size(192, 13)
        Me.freight_zfrp_in_ce_curr_aft_disc_lbl.TabIndex = 11
        Me.freight_zfrp_in_ce_curr_aft_disc_lbl.Text = "Total Freight ZFRP in CE Curr. : "
        '
        'freight_zfrp_in_ce_curr_disc_lbl
        '
        Me.freight_zfrp_in_ce_curr_disc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfrp_in_ce_curr_disc_lbl.AutoSize = True
        Me.freight_zfrp_in_ce_curr_disc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.freight_zfrp_in_ce_curr_disc_lbl.Location = New System.Drawing.Point(355, 32)
        Me.freight_zfrp_in_ce_curr_disc_lbl.Name = "freight_zfrp_in_ce_curr_disc_lbl"
        Me.freight_zfrp_in_ce_curr_disc_lbl.Size = New System.Drawing.Size(65, 13)
        Me.freight_zfrp_in_ce_curr_disc_lbl.TabIndex = 10
        Me.freight_zfrp_in_ce_curr_disc_lbl.Text = "Discount :"
        '
        'freight_zfrp_in_ce_curr_bef_disc_val
        '
        Me.freight_zfrp_in_ce_curr_bef_disc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfrp_in_ce_curr_bef_disc_val.Location = New System.Drawing.Point(579, 3)
        Me.freight_zfrp_in_ce_curr_bef_disc_val.Name = "freight_zfrp_in_ce_curr_bef_disc_val"
        Me.freight_zfrp_in_ce_curr_bef_disc_val.Size = New System.Drawing.Size(113, 20)
        Me.freight_zfrp_in_ce_curr_bef_disc_val.TabIndex = 12
        '
        'freight_zfrp_in_ce_curr_bef_disc_lbl
        '
        Me.freight_zfrp_in_ce_curr_bef_disc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.freight_zfrp_in_ce_curr_bef_disc_lbl.AutoSize = True
        Me.freight_zfrp_in_ce_curr_bef_disc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.freight_zfrp_in_ce_curr_bef_disc_lbl.Location = New System.Drawing.Point(355, 6)
        Me.freight_zfrp_in_ce_curr_bef_disc_lbl.Name = "freight_zfrp_in_ce_curr_bef_disc_lbl"
        Me.freight_zfrp_in_ce_curr_bef_disc_lbl.Size = New System.Drawing.Size(118, 13)
        Me.freight_zfrp_in_ce_curr_bef_disc_lbl.TabIndex = 12
        Me.freight_zfrp_in_ce_curr_bef_disc_lbl.Text = "Total Before Disc. :"
        '
        'zcuss_grp
        '
        Me.zcuss_grp.Controls.Add(Me.handling_zcus_ly)
        Me.zcuss_grp.Location = New System.Drawing.Point(5, 670)
        Me.zcuss_grp.Margin = New System.Windows.Forms.Padding(5, 5, 5, 10)
        Me.zcuss_grp.Name = "zcuss_grp"
        Me.zcuss_grp.Size = New System.Drawing.Size(752, 150)
        Me.zcuss_grp.TabIndex = 10
        Me.zcuss_grp.TabStop = False
        Me.zcuss_grp.Text = "Handling ZCUS"
        '
        'handling_zcus_ly
        '
        Me.handling_zcus_ly.ColumnCount = 4
        Me.handling_zcus_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.82143!))
        Me.handling_zcus_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.17857!))
        Me.handling_zcus_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.71115!))
        Me.handling_zcus_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.52433!))
        Me.handling_zcus_ly.Controls.Add(Me.handling_zcus_in_ce_curr_aft_disc_val, 3, 2)
        Me.handling_zcus_ly.Controls.Add(Me.handling_zcus_in_ce_curr_disc_val, 3, 1)
        Me.handling_zcus_ly.Controls.Add(Me.handling_zcus_in_ce_curr_aft_disc_lbl, 2, 2)
        Me.handling_zcus_ly.Controls.Add(Me.handling_zcus_in_ce_curr_disc_lbl, 2, 1)
        Me.handling_zcus_ly.Controls.Add(Me.handling_zcus_in_ce_curr_bef_disc_val, 3, 0)
        Me.handling_zcus_ly.Controls.Add(Me.handling_zcus_in_ce_curr_bef_disc_lbl, 2, 0)
        Me.handling_zcus_ly.Controls.Add(Me.handling_zcus_value_val, 1, 0)
        Me.handling_zcus_ly.Controls.Add(Me.handling_zcus_curr_val, 1, 1)
        Me.handling_zcus_ly.Controls.Add(Me.handling_zcus_tagk_val, 1, 2)
        Me.handling_zcus_ly.Controls.Add(Me.handling_zcus_value_lbl, 0, 0)
        Me.handling_zcus_ly.Controls.Add(Me.handling_zcus_curr_lbl, 0, 1)
        Me.handling_zcus_ly.Controls.Add(Me.handling_zcus_tagk_lbl, 0, 2)
        Me.handling_zcus_ly.Location = New System.Drawing.Point(13, 27)
        Me.handling_zcus_ly.Name = "handling_zcus_ly"
        Me.handling_zcus_ly.RowCount = 4
        Me.handling_zcus_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.handling_zcus_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.handling_zcus_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.handling_zcus_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.handling_zcus_ly.Size = New System.Drawing.Size(733, 105)
        Me.handling_zcus_ly.TabIndex = 1
        '
        'handling_zcus_in_ce_curr_aft_disc_val
        '
        Me.handling_zcus_in_ce_curr_aft_disc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.handling_zcus_in_ce_curr_aft_disc_val.Location = New System.Drawing.Point(599, 55)
        Me.handling_zcus_in_ce_curr_aft_disc_val.Name = "handling_zcus_in_ce_curr_aft_disc_val"
        Me.handling_zcus_in_ce_curr_aft_disc_val.Size = New System.Drawing.Size(115, 20)
        Me.handling_zcus_in_ce_curr_aft_disc_val.TabIndex = 11
        '
        'handling_zcus_in_ce_curr_disc_val
        '
        Me.handling_zcus_in_ce_curr_disc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.handling_zcus_in_ce_curr_disc_val.Location = New System.Drawing.Point(599, 29)
        Me.handling_zcus_in_ce_curr_disc_val.Name = "handling_zcus_in_ce_curr_disc_val"
        Me.handling_zcus_in_ce_curr_disc_val.Size = New System.Drawing.Size(115, 20)
        Me.handling_zcus_in_ce_curr_disc_val.TabIndex = 10
        '
        'handling_zcus_in_ce_curr_aft_disc_lbl
        '
        Me.handling_zcus_in_ce_curr_aft_disc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.handling_zcus_in_ce_curr_aft_disc_lbl.AutoSize = True
        Me.handling_zcus_in_ce_curr_aft_disc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.handling_zcus_in_ce_curr_aft_disc_lbl.Location = New System.Drawing.Point(368, 58)
        Me.handling_zcus_in_ce_curr_aft_disc_lbl.Name = "handling_zcus_in_ce_curr_aft_disc_lbl"
        Me.handling_zcus_in_ce_curr_aft_disc_lbl.Size = New System.Drawing.Size(192, 13)
        Me.handling_zcus_in_ce_curr_aft_disc_lbl.TabIndex = 11
        Me.handling_zcus_in_ce_curr_aft_disc_lbl.Text = "Total Freight ZFRP in CE Curr. : "
        '
        'handling_zcus_in_ce_curr_disc_lbl
        '
        Me.handling_zcus_in_ce_curr_disc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.handling_zcus_in_ce_curr_disc_lbl.AutoSize = True
        Me.handling_zcus_in_ce_curr_disc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.handling_zcus_in_ce_curr_disc_lbl.Location = New System.Drawing.Point(368, 32)
        Me.handling_zcus_in_ce_curr_disc_lbl.Name = "handling_zcus_in_ce_curr_disc_lbl"
        Me.handling_zcus_in_ce_curr_disc_lbl.Size = New System.Drawing.Size(65, 13)
        Me.handling_zcus_in_ce_curr_disc_lbl.TabIndex = 10
        Me.handling_zcus_in_ce_curr_disc_lbl.Text = "Discount :"
        '
        'handling_zcus_in_ce_curr_bef_disc_val
        '
        Me.handling_zcus_in_ce_curr_bef_disc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.handling_zcus_in_ce_curr_bef_disc_val.Location = New System.Drawing.Point(599, 3)
        Me.handling_zcus_in_ce_curr_bef_disc_val.Name = "handling_zcus_in_ce_curr_bef_disc_val"
        Me.handling_zcus_in_ce_curr_bef_disc_val.Size = New System.Drawing.Size(115, 20)
        Me.handling_zcus_in_ce_curr_bef_disc_val.TabIndex = 12
        '
        'handling_zcus_in_ce_curr_bef_disc_lbl
        '
        Me.handling_zcus_in_ce_curr_bef_disc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.handling_zcus_in_ce_curr_bef_disc_lbl.AutoSize = True
        Me.handling_zcus_in_ce_curr_bef_disc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.handling_zcus_in_ce_curr_bef_disc_lbl.Location = New System.Drawing.Point(368, 6)
        Me.handling_zcus_in_ce_curr_bef_disc_lbl.Name = "handling_zcus_in_ce_curr_bef_disc_lbl"
        Me.handling_zcus_in_ce_curr_bef_disc_lbl.Size = New System.Drawing.Size(118, 13)
        Me.handling_zcus_in_ce_curr_bef_disc_lbl.TabIndex = 12
        Me.handling_zcus_in_ce_curr_bef_disc_lbl.Text = "Total Before Disc. :"
        '
        'handling_zcus_value_val
        '
        Me.handling_zcus_value_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.handling_zcus_value_val.Location = New System.Drawing.Point(221, 3)
        Me.handling_zcus_value_val.Name = "handling_zcus_value_val"
        Me.handling_zcus_value_val.Size = New System.Drawing.Size(108, 20)
        Me.handling_zcus_value_val.TabIndex = 10
        '
        'handling_zcus_curr_val
        '
        Me.handling_zcus_curr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.handling_zcus_curr_val.FormattingEnabled = True
        Me.handling_zcus_curr_val.Location = New System.Drawing.Point(221, 29)
        Me.handling_zcus_curr_val.Name = "handling_zcus_curr_val"
        Me.handling_zcus_curr_val.Size = New System.Drawing.Size(108, 21)
        Me.handling_zcus_curr_val.TabIndex = 23
        '
        'handling_zcus_tagk_val
        '
        Me.handling_zcus_tagk_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.handling_zcus_tagk_val.Location = New System.Drawing.Point(221, 55)
        Me.handling_zcus_tagk_val.Name = "handling_zcus_tagk_val"
        Me.handling_zcus_tagk_val.Size = New System.Drawing.Size(108, 20)
        Me.handling_zcus_tagk_val.TabIndex = 7
        '
        'handling_zcus_value_lbl
        '
        Me.handling_zcus_value_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.handling_zcus_value_lbl.AutoSize = True
        Me.handling_zcus_value_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.handling_zcus_value_lbl.Location = New System.Drawing.Point(3, 6)
        Me.handling_zcus_value_lbl.Name = "handling_zcus_value_lbl"
        Me.handling_zcus_value_lbl.Size = New System.Drawing.Size(138, 13)
        Me.handling_zcus_value_lbl.TabIndex = 6
        Me.handling_zcus_value_lbl.Text = "Handling ZCUS Value :"
        '
        'handling_zcus_curr_lbl
        '
        Me.handling_zcus_curr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.handling_zcus_curr_lbl.AutoSize = True
        Me.handling_zcus_curr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.handling_zcus_curr_lbl.Location = New System.Drawing.Point(3, 32)
        Me.handling_zcus_curr_lbl.Name = "handling_zcus_curr_lbl"
        Me.handling_zcus_curr_lbl.Size = New System.Drawing.Size(137, 13)
        Me.handling_zcus_curr_lbl.TabIndex = 17
        Me.handling_zcus_curr_lbl.Text = "Handling ZCUS  Curr. :"
        '
        'handling_zcus_tagk_lbl
        '
        Me.handling_zcus_tagk_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.handling_zcus_tagk_lbl.AutoSize = True
        Me.handling_zcus_tagk_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.handling_zcus_tagk_lbl.Location = New System.Drawing.Point(3, 58)
        Me.handling_zcus_tagk_lbl.Name = "handling_zcus_tagk_lbl"
        Me.handling_zcus_tagk_lbl.Size = New System.Drawing.Size(85, 13)
        Me.handling_zcus_tagk_lbl.TabIndex = 24
        Me.handling_zcus_tagk_lbl.Text = "ZCUS TAGK :"
        '
        'select_material_grp
        '
        Me.select_material_grp.Controls.Add(Me.material_error_state_pic)
        Me.select_material_grp.Controls.Add(Me.material_select_right_pic)
        Me.select_material_grp.Controls.Add(Me.material_select_left_pic)
        Me.select_material_grp.Controls.Add(Me.cb_material)
        Me.select_material_grp.Dock = System.Windows.Forms.DockStyle.Top
        Me.select_material_grp.Location = New System.Drawing.Point(0, 0)
        Me.select_material_grp.Margin = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.select_material_grp.Name = "select_material_grp"
        Me.select_material_grp.Padding = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.select_material_grp.Size = New System.Drawing.Size(1273, 50)
        Me.select_material_grp.TabIndex = 0
        Me.select_material_grp.TabStop = False
        Me.select_material_grp.Text = "Select Material"
        '
        'material_error_state_pic
        '
        Me.material_error_state_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.material_error_state_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.no_errors
        Me.material_error_state_pic.Location = New System.Drawing.Point(82, 12)
        Me.material_error_state_pic.Name = "material_error_state_pic"
        Me.material_error_state_pic.Size = New System.Drawing.Size(36, 32)
        Me.material_error_state_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.material_error_state_pic.TabIndex = 3
        Me.material_error_state_pic.TabStop = False
        Me.material_error_state_pic.Tag = ""
        '
        'material_select_right_pic
        '
        Me.material_select_right_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.material_select_right_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.right_chevron
        Me.material_select_right_pic.Location = New System.Drawing.Point(905, 12)
        Me.material_select_right_pic.Name = "material_select_right_pic"
        Me.material_select_right_pic.Size = New System.Drawing.Size(36, 32)
        Me.material_select_right_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.material_select_right_pic.TabIndex = 2
        Me.material_select_right_pic.TabStop = False
        '
        'material_select_left_pic
        '
        Me.material_select_left_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.material_select_left_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.left_chevron
        Me.material_select_left_pic.Location = New System.Drawing.Point(276, 12)
        Me.material_select_left_pic.Name = "material_select_left_pic"
        Me.material_select_left_pic.Size = New System.Drawing.Size(36, 32)
        Me.material_select_left_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.material_select_left_pic.TabIndex = 1
        Me.material_select_left_pic.TabStop = False
        '
        'cb_material
        '
        Me.cb_material.FormattingEnabled = True
        Me.cb_material.Location = New System.Drawing.Point(343, 19)
        Me.cb_material.Name = "cb_material"
        Me.cb_material.Size = New System.Drawing.Size(534, 21)
        Me.cb_material.TabIndex = 0
        '
        'top_panel_material
        '
        Me.top_panel_material.Controls.Add(Me.select_material_grp)
        Me.top_panel_material.Dock = System.Windows.Forms.DockStyle.Top
        Me.top_panel_material.Location = New System.Drawing.Point(5, 5)
        Me.top_panel_material.Name = "top_panel_material"
        Me.top_panel_material.Size = New System.Drawing.Size(1273, 63)
        Me.top_panel_material.TabIndex = 1
        '
        'FormMaterialEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1283, 682)
        Me.Controls.Add(Me.flow_panel_activity_details)
        Me.Controls.Add(Me.top_panel_material)
        Me.Name = "FormMaterialEdit"
        Me.Padding = New System.Windows.Forms.Padding(5, 5, 5, 5)
        Me.Text = "Edit Material Data"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.activity_info_grp.ResumeLayout(False)
        Me.change_activity_ly.ResumeLayout(False)
        Me.change_activity_ly.PerformLayout()
        Me.activity_details_lay.ResumeLayout(False)
        Me.activity_details_lay.PerformLayout()
        Me.flow_panel_activity_details.ResumeLayout(False)
        Me.material_general_grp.ResumeLayout(False)
        Me.mat_general_info_lay.ResumeLayout(False)
        Me.mat_general_info_lay.PerformLayout()
        Me.material_general_edit_grp.ResumeLayout(False)
        Me.material_general_edit_ly.ResumeLayout(False)
        Me.material_general_edit_ly.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.zprm_zstm_grp.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.markup_zmar_grp.ResumeLayout(False)
        Me.markup_zmar_ly.ResumeLayout(False)
        Me.markup_zmar_ly.PerformLayout()
        Me.freight_zfra_grp.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.total_mat_price_grp.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.freight_zfrp_grp.ResumeLayout(False)
        Me.freight_zfrp_ly.ResumeLayout(False)
        Me.freight_zfrp_ly.PerformLayout()
        Me.zcuss_grp.ResumeLayout(False)
        Me.handling_zcus_ly.ResumeLayout(False)
        Me.handling_zcus_ly.PerformLayout()
        Me.select_material_grp.ResumeLayout(False)
        CType(Me.material_error_state_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.material_select_right_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.material_select_left_pic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.top_panel_material.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents activity_info_grp As GroupBox
    Friend WithEvents flow_panel_activity_details As FlowLayoutPanel
    Friend WithEvents select_material_grp As GroupBox
    Friend WithEvents material_select_right_pic As PictureBox
    Friend WithEvents material_select_left_pic As PictureBox
    Friend WithEvents cb_material As ComboBox
    Friend WithEvents top_panel_material As Panel
    Friend WithEvents activity_details_lay As TableLayoutPanel
    Friend WithEvents act_sold_freight_val As TextBox
    Friend WithEvents act_repair_val As TextBox
    Friend WithEvents act_service_val As TextBox
    Friend WithEvents act_sold_mat_val As TextBox
    Friend WithEvents act_lab_sold_val As TextBox
    Friend WithEvents act_rev_ctrl_txt_val As TextBox
    Friend WithEvents act_rev_ctrl_status_val As TextBox
    Friend WithEvents act_operator_code_val As TextBox
    Friend WithEvents act_warr_code_val As TextBox
    Friend WithEvents act_check_type_val As TextBox
    Friend WithEvents act_long_desc_val As TextBox
    Friend WithEvents act_desc_val As TextBox
    Friend WithEvents act_status_val As TextBox
    Friend WithEvents act_notif_val As TextBox
    Friend WithEvents act_activity_val As TextBox
    Friend WithEvents act_sold_freight_lbl As Label
    Friend WithEvents act_repair_lbl As Label
    Friend WithEvents act_sold_mat_lbl As Label
    Friend WithEvents act_lab_sold_lbl As Label
    Friend WithEvents act_sold_hrs_lbl As Label
    Friend WithEvents act_is_init_lbl As Label
    Friend WithEvents act_rev_ctrl_status_lbl As Label
    Friend WithEvents act_check_type_lbl As Label
    Friend WithEvents act_operator_code_lbl As Label
    Friend WithEvents act_warr_code_lbl As Label
    Friend WithEvents act_long_desc_lbl As Label
    Friend WithEvents ct_rev_ctrl_txt_lbl As Label
    Friend WithEvents mat_desc_lbl As Label
    Friend WithEvents act_status_lbl As Label
    Friend WithEvents act_activity_lbl As Label
    Friend WithEvents act_notif_lbl As Label
    Friend WithEvents act_service_lbl As Label
    Friend WithEvents act_sold_hrs_val As TextBox
    Friend WithEvents act_is_init_val As CheckBox
    Friend WithEvents material_general_grp As GroupBox
    Friend WithEvents mat_general_info_lay As TableLayoutPanel
    Friend WithEvents po_estimated_val As TextBox
    Friend WithEvents po_warr_code_val As TextBox
    Friend WithEvents po_orea_val As TextBox
    Friend WithEvents po_item_val As TextBox
    Friend WithEvents po_number_val As TextBox
    Friend WithEvents po_uom_val As TextBox
    Friend WithEvents po_qty_val As TextBox
    Friend WithEvents mat_desc_val As TextBox
    Friend WithEvents rev_ctrl_text_val As TextBox
    Friend WithEvents rev_ctrl_status_val As TextBox
    Friend WithEvents po_created_date_val As TextBox
    Friend WithEvents po_estimated_lbl As Label
    Friend WithEvents po_curr_lbl As Label
    Friend WithEvents po_cost_lbl As Label
    Friend WithEvents po_orea_lbl As Label
    Friend WithEvents po_uom_lbl As Label
    Friend WithEvents po_item_lbl As Label
    Friend WithEvents po_number_lbl As Label
    Friend WithEvents po_qty_lbl As Label
    Friend WithEvents po_warr_code_lbl As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents rev_ctrl_text_lbl As Label
    Friend WithEvents po_created_date_lbl As Label
    Friend WithEvents rev_ctrl_status_lbl As Label
    Friend WithEvents po_curr_val As TextBox
    Friend WithEvents po_cost_val As TextBox
    Friend WithEvents material_general_edit_grp As GroupBox
    Friend WithEvents material_general_edit_ly As TableLayoutPanel
    Friend WithEvents is_mat_reported_val As CheckBox
    Friend WithEvents mat_payer_val As ComboBox
    Friend WithEvents sn_on_val As TextBox
    Friend WithEvents part_number_val As TextBox
    Friend WithEvents material_type_lbl As Label
    Friend WithEvents part_number_lbl As Label
    Friend WithEvents sn_on_lbl As Label
    Friend WithEvents is_mat_reported_lbl As Label
    Friend WithEvents mat_payer_lbl As Label
    Friend WithEvents is_init_scope_lbl As Label
    Friend WithEvents mat_text5_lbl As Label
    Friend WithEvents mat_text5_val As TextBox
    Friend WithEvents billing_work_status_lbl As Label
    Friend WithEvents billing_work_comment_lbl As Label
    Friend WithEvents billing_work_comment_val As TextBox
    Friend WithEvents billing_work_status_val As ComboBox
    Friend WithEvents material_type_val As ComboBox
    Friend WithEvents is_init_scope_val As CheckBox
    Friend WithEvents zprm_zstm_grp As GroupBox
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents total_sales_price_zprm_val As TextBox
    Friend WithEvents total_sales_price_zprm_lbl As Label
    Friend WithEvents unit_sales_price_zstm_lbl As Label
    Friend WithEvents zprm_zstm_curr_lbl As Label
    Friend WithEvents unit_sales_price_zstm_val As TextBox
    Friend WithEvents zprm_zstm_curr_val As ComboBox
    Friend WithEvents zprm_zstm_curr_tagk_val As TextBox
    Friend WithEvents zprm_zstm_curr_tagk_lbl As Label
    Friend WithEvents markup_zmar_grp As GroupBox
    Friend WithEvents markup_zmar_ly As TableLayoutPanel
    Friend WithEvents markup_zmar_def_val As TextBox
    Friend WithEvents markup_zmar_def_lbl As Label
    Friend WithEvents markup_zmar_is_overwr_lbl As Label
    Friend WithEvents markup_zmar_overwritten_lbl As Label
    Friend WithEvents markup_zmar_is_overwr_val As CheckBox
    Friend WithEvents markup_zmar_overwritten_val As TextBox
    Friend WithEvents freight_zfra_grp As GroupBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents freight_zfra_default_val As TextBox
    Friend WithEvents freight_zfra_default_lbl As Label
    Friend WithEvents freight_zfra_is_overwr_lbl As Label
    Friend WithEvents freight_zfra_overwriten_lbl As Label
    Friend WithEvents freight_zfra_is_overwr_val As CheckBox
    Friend WithEvents freight_zfra_overwriten_val As TextBox
    Friend WithEvents total_mat_price_grp As GroupBox
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents total_mat_in_ce_curr_bef_disc_val As TextBox
    Friend WithEvents total_mat_in_ce_curr_bef_disc_lbl As Label
    Friend WithEvents total_mat_in_ce_curr_disc_lbl As Label
    Friend WithEvents total_mat_in_ce_curr_after_disc_lbl As Label
    Friend WithEvents total_mat_in_ce_curr_disc_val As TextBox
    Friend WithEvents total_mat_in_ce_curr_after_disc_val As TextBox
    Friend WithEvents freight_zfrp_grp As GroupBox
    Friend WithEvents freight_zfrp_ly As TableLayoutPanel
    Friend WithEvents freight_zfrp_lbl As Label
    Friend WithEvents overwrit_freight_zfrp_val_lbl As Label
    Friend WithEvents overwrit_freight_zfrp_curr_lbl As Label
    Friend WithEvents freight_zfrp_tagk_val As TextBox
    Friend WithEvents total_sales_price_zprm_flag_val As CheckBox
    Friend WithEvents freight_zfrp_val As ComboBox
    Friend WithEvents overwrit_freight_zfrp_val_val As TextBox
    Friend WithEvents overwrit_freight_zfrp_curr_val As ComboBox
    Friend WithEvents freight_zfrp_tagk_lbl As Label
    Friend WithEvents freight_zfrp_in_ce_curr_aft_disc_val As TextBox
    Friend WithEvents freight_zfrp_in_ce_curr_disc_val As TextBox
    Friend WithEvents freight_zfrp_in_ce_curr_aft_disc_lbl As Label
    Friend WithEvents freight_zfrp_in_ce_curr_disc_lbl As Label
    Friend WithEvents freight_zfrp_in_ce_curr_bef_disc_val As TextBox
    Friend WithEvents freight_zfrp_in_ce_curr_bef_disc_lbl As Label
    Friend WithEvents zcuss_grp As GroupBox
    Friend WithEvents handling_zcus_ly As TableLayoutPanel
    Friend WithEvents handling_zcus_tagk_lbl As Label
    Friend WithEvents handling_zcus_value_lbl As Label
    Friend WithEvents handling_zcus_curr_lbl As Label
    Friend WithEvents handling_zcus_value_val As TextBox
    Friend WithEvents handling_zcus_tagk_val As TextBox
    Friend WithEvents handling_zcus_curr_val As ComboBox
    Friend WithEvents handling_zcus_in_ce_curr_aft_disc_val As TextBox
    Friend WithEvents handling_zcus_in_ce_curr_disc_val As TextBox
    Friend WithEvents handling_zcus_in_ce_curr_aft_disc_lbl As Label
    Friend WithEvents handling_zcus_in_ce_curr_disc_lbl As Label
    Friend WithEvents handling_zcus_in_ce_curr_bef_disc_val As TextBox
    Friend WithEvents handling_zcus_in_ce_curr_bef_disc_lbl As Label
    Friend WithEvents mat_nte_val As ComboBox
    Friend WithEvents mat_discount_val As ComboBox
    Friend WithEvents mat_nte_lbl As Label
    Friend WithEvents mat_discount_lbl As Label
    Friend WithEvents material_error_state_pic As PictureBox
    Friend WithEvents calc_markup_zmar_value_lbl As Label
    Friend WithEvents calc_markup_zmar_value_val As TextBox
    Friend WithEvents calc_freight_zfra_value_val As TextBox
    Friend WithEvents calc_freight_zfra_value_lbl As Label
    Friend WithEvents mat_infos_val As TextBox
    Friend WithEvents mat_infos_lbl As Label
    Friend WithEvents act_is_small_mat_lbl As Label
    Friend WithEvents act_is_small_mat_val As CheckBox
    Friend WithEvents act_mat_info_lbl As Label
    Friend WithEvents act_mat_info_val As TextBox
    Friend WithEvents mat_cancel_awq_val As TextBox
    Friend WithEvents mat_awq_val As TextBox
    Friend WithEvents mat_cancel_awq_lbl As Label
    Friend WithEvents mat_awq_lbl As Label
    Friend WithEvents change_activity_ly As TableLayoutPanel
    Friend WithEvents change_activity_val As ComboBox
    Friend WithEvents change_activity_lbl As Label
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents transac_cond_lbl As Label
    Friend WithEvents transac_cond_val As ComboBox
End Class
