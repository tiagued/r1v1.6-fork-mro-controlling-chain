﻿Imports System.ComponentModel
Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CMaterialInitAddScopeDelegate

    Private _matList As BindingListView(Of CMaterial)

    'get mat list
    Public ReadOnly Property matList() As BindingListView(Of CMaterial)
        Get
            Return _matList
        End Get
    End Property

    'read data from material DB
    Public Sub model_read_material_relatedDB()
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "perform queries on material tables..."
        Try

            model_read_material()
        Catch ex As Exception
            Throw New Exception("Error occured when reading materials related data from the DB" & Chr(10) & ex.Message, ex)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End perform queries on material tables"
    End Sub

    'read data from quote DB
    Private Sub model_read_material()
        Dim listO As List(Of Object)
        Try
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "perform query to material table..."
            listO = MConstants.GLB_MSTR_CTRL.csDB.performSelectObject(MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_MATERIAL_DB_READ).getDefaultSelect, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_MATERIAL_DB_READ), True)
            _matList = New BindingListView(Of CMaterial)(listO)
        Catch ex As Exception
            Throw New Exception("Error occured when reading material data from the DB" & Chr(10) & ex.Message, ex)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End perform query to material table"
    End Sub

    'get quote reference from ID
    Public Function getMaterialObject(id As Long) As CMaterial

        For Each mat As CMaterial In _matList.DataSource
            If mat.id = id Then
                Return mat
            End If
        Next
        MGeneralFuntionsViewControl.displayMessage("Material not found", "Material object with ID " & id & " does not exist", MsgBoxStyle.Critical)
        Return Nothing
    End Function


    Public Sub bind_material_related_views()
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "bind material related views..."
        Try
            bind_mat_main_view()
        Catch ex As Exception
            Throw New Exception("Error occured when binding material related views" & Chr(10) & ex.Message, ex)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End bind material related views"
    End Sub


    'bind the controls items in the initial scope view view with the relevant data object
    Private Sub bind_mat_main_view()
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Bind main material to view controls..."
        Try
            'material datagridview
            MConstants.GLB_MSTR_CTRL.mainForm.material_main_dgrid.AllowUserToAddRows = True
            MViewEventHandler.addDefaultDatagridEventHandler(MConstants.GLB_MSTR_CTRL.mainForm.material_main_dgrid)

            bind_mat_generic_view(MConstants.GLB_MSTR_CTRL.mainForm.material_main_dgrid)
            'handle frozen col
            Dim i As Integer = 1
            While i <= CInt(MConstants.constantConf(MConstants.CONST_CONF_MAT_VIEW_FREEZE_FIRST_COL_CNT).value)
                MConstants.GLB_MSTR_CTRL.mainForm.material_main_dgrid.Columns(i - 1).Frozen = True
                i = i + 1
            End While

            'bind datasource
            MConstants.GLB_MSTR_CTRL.mainForm.material_main_dgrid.DataSource = _matList

            MGeneralFuntionsViewControl.resizeDataGridWidth(MConstants.GLB_MSTR_CTRL.mainForm.material_main_dgrid)

            'handler to init new objects
            AddHandler _matList.AddingNew, AddressOf material_view_AddingNew

            'row validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(MConstants.GLB_MSTR_CTRL.mainForm.material_main_dgrid, CMaterialAddRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_MAT_MAIN_VIEW_DISPLAY))
            dhler.validateAddDelegate = AddressOf material_view_ValidateAdd
            dhler.cancelAddDelegate = AddressOf material_view_CancelAdd

            'Refresh Activity dependant views whenever material list appears
            MViewEventHandler.addScopeListRefreshHandlerWhenVisible(MConstants.GLB_MSTR_CTRL.mainForm.material_main_dgrid)

            _matList.ApplyFilter(AddressOf filterDeletedMaterials)
            _matList.Refresh()

            'add row validator. Add this after the datasource has been set and refreshed. Cause if done before, data source will not refresh due to potential validation errors
            'CGridViewRowValidationHandler.addNew(MConstants.GLB_MSTR_CTRL.mainForm.material_main_dgrid, CMaterialRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_MAT_MAIN_VIEW_DISPLAY))
        Catch ex As Exception
            Throw New Exception("An error occured while loading material view", ex)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End Bind main material data to view controls"
    End Sub

    'when added new row to material view, set the activity object
    Private Sub material_view_AddingNew(ByVal sender As Object, ByVal e As AddingNewEventArgs)
        Dim mat As New CMaterial
        Dim bds As BindingListView(Of CMaterial) = sender
        mat.is_reported = True
        mat.po_created = DateTime.Now
        e.NewObject = mat
    End Sub

    'validate new
    Public Sub material_view_ValidateAdd(material As ObjectView(Of CMaterial), rowIndex As Integer)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(material.Object) Then
                'if new item
                material.Object.setID()
                If Not _matList.DataSource.Contains(material.Object) Then
                    _matList.EndNew(rowIndex)
                End If
                'add to activity material list
                If Not material.Object.activity_obj.material_list.Contains(material.Object) Then
                    material.Object.activity_obj.material_list.Add(material.Object)
                End If
                'update calculation
                material.Object.activity_obj.updateCal()
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new material entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'cancel new
    Public Function material_view_CancelAdd(material As ObjectView(Of CMaterial), rowIndex As Integer) As Boolean
        'return true to block leaving and return false to allow leaving
        Dim res As Boolean
        Try
            'check if new
            If material.Object.id > 0 Then
                res = True
            Else
                If _matList.DataSource.Contains(material.Object) Then
                    res = True
                Else
                    material.CancelEdit()
                    _matList.CancelNew(rowIndex)
                    res = False
                End If
            End If

        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new initial scope hour entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return res
    End Function

    Public Sub bind_mat_generic_view(mat_dgrid As DataGridView)
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Bind material to view controls..."
        Try

            Dim mapMat As CViewModelMapList

            'get material map view
            mapMat = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_MAT_MAIN_VIEW_DISPLAY)

            For Each map As CViewModelMap In mapMat.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapMat.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "Double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mat_dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapMat.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_MAT_MAIN_PAYER.ToLower Then
                        bds.DataSource = MConstants.listOfPayer.Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "label"
                        dgcol.ValueMember = "payer"
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_MAT_MAIN_ACT.ToLower Then
                        dgcol.DataSource = MConstants.GLB_MSTR_CTRL.add_scope_controller._scopeListSorted_bds
                        dgcol.DisplayMember = "act_notif_opcode"
                        dgcol.ValueMember = "id"
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_MAT_MAIN_NTE.ToLower Then
                        dgcol.DataSource = MConstants.GLB_MSTR_CTRL.billing_cond_controller._nteList_lov_mat_po
                        dgcol.DisplayMember = "label"
                        dgcol.ValueMember = "id"
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_MAT_MAIN_DISC.ToLower Then
                        dgcol.DataSource = MConstants.GLB_MSTR_CTRL.billing_cond_controller._discountList_lov_mat_po
                        dgcol.DisplayMember = "label"
                        dgcol.ValueMember = "id"
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_MAT_MAIN_FREIGHT_ZFRP.ToLower Then
                        bds.DataSource = GLB_MSTR_CTRL.billing_cond_controller.freightList.DataSource
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "freight_to_string"
                        dgcol.ValueMember = "id_str"
                    Else
                        Throw New Exception("Error occured when binding activity view in material tab. List " & map.view_list_name & "of column " & map.view_col_sys_name & " does not exist in LOV")
                    End If
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    dgcol.ReadOnly = Not map.col_editable
                    mat_dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mat_dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_BUTTON).value Then
                    Dim dgcol As New DataGridViewButtonColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Text = map.view_col
                    dgcol.UseColumnTextForButtonValue = True
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    mat_dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_IMAGE).value Then
                    Dim dgcol As New DataGridViewImageColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Image = My.Resources.ResourceManager.GetObject(map.view_col)
                    mat_dgrid.Columns.Add(dgcol)
                End If
            Next


        Catch ex As Exception
            Throw New Exception("An error occured while loading material view", ex)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End Bind material data to view controls"
    End Sub

    Public Function filterDeletedMaterials(ByVal mat As CMaterial)
        'delegate for filtering
        Return Not mat.system_status = MConstants.CONST_SYS_ROW_DELETED
    End Function

    'perform delete
    Public Sub deleteObjectMaterial(mat As CMaterial)
        Try
            If _matList.DataSource.Contains(mat) Then
                _matList.DataSource.Remove(mat)
            End If
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(mat)
        Catch ex As Exception
        End Try
    End Sub

End Class
