﻿Imports System.ComponentModel
Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CInitScopeEditDelegate
    Public form As FormInitScopeActivities
    Private allActSelectedItems As List(Of CScopeActivity)
    Private depActSelectedItems As List(Of CScopeActivity)
    Private latestEditedActicity As CScopeActivity
    Private blockHoursViewRefresh As Boolean

    Public Sub launchEditForm(product As CSalesQuoteEntry)
        Try
            _is_show_only_not_mapped = True

            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "preparing view to edit initial scope hours..."
            form = New FormInitScopeActivities

            allActSelectedItems = New List(Of CScopeActivity)
            depActSelectedItems = New List(Of CScopeActivity)
            blockHoursViewRefresh = False
            latestEditedActicity = Nothing

            'draw hours grid views before selecting product=> cause selection trigger view data filling
            drawInitHoursgrid()
            drawAddHoursgrid()
            drawInitMaterialgrid()
            drawAddMaterialgrid()

            'initialize quote product selection combobox
            Dim bds As New BindingSource
            bds.DataSource = MConstants.GLB_MSTR_CTRL.ini_scope_controller.quote_entries_list.DataSource
            'set combobox list
            form.cb_product.AutoCompleteMode = AutoCompleteMode.None
            form.cb_product.DropDownStyle = ComboBoxStyle.DropDownList
            form.cb_product.DataSource = bds
            form.cb_product.DisplayMember = "product"
            'ignore mouse wheel
            MViewEventHandler.addDefaultComboBoxEventHandler(form.cb_product)
            MViewEventHandler.addDefaultComboBoxEventHandler(form.select_main_act_cb)
            'set selected object
            form.cb_product.SelectedItem = product

            'fill data in views
            updateView()

            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End preparing view to edit initial scope hours"

            'bind item to filter items
            form.filter_not_mapped_val.DataBindings.Clear()
            form.filter_not_mapped_val.DataBindings.Add("Checked", Me, "is_show_only_not_mapped", False, DataSourceUpdateMode.OnPropertyChanged)
            form.filter_not_mapped_val.Enabled = True

            form.ShowDialog()
            form.BringToFront()
        Catch ex As Exception
            MsgBox("An error occured while preparing the windows to edit inital scope hours " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'navigate forward quote entries
    Public Sub selectedProductForward()
        If Not form.cb_product.SelectedIndex + 1 > MConstants.GLB_MSTR_CTRL.ini_scope_controller.quote_entries_list.DataSource.Count - 1 Then
            form.cb_product.SelectedIndex = form.cb_product.SelectedIndex + 1
        End If
    End Sub
    'navigate backward quote entries
    Public Sub selectedProductBackward()
        If Not form.cb_product.SelectedIndex - 1 < 0 Then
            form.cb_product.SelectedIndex = form.cb_product.SelectedIndex - 1
        End If
    End Sub

    Private _is_show_only_not_mapped As Boolean
    Public Property is_show_only_not_mapped() As Boolean
        Get
            Return _is_show_only_not_mapped
        End Get
        Set(ByVal value As Boolean)
            If Not _is_show_only_not_mapped = value Then
                _is_show_only_not_mapped = value
                updateDependantActivitiesView()
            End If
        End Set
    End Property


    Public Sub updateView()
        Try
            latestEditedActicity = Nothing
            'product details
            Dim product As CSalesQuoteEntry = form.cb_product.SelectedItem
            form.product_number_val.Text = product.product
            form.product_quote_val.Text = product.quote_obj.number
            form.product_labor_val.Text = formatInitPrice(product.labor_price, product.labor_price_adj, product.lab_curr)
            form.product_mat_val.Text = formatInitPrice(product.mat_price, product.mat_price_adj, product.mat_curr)
            form.product_repair_val.Text = formatInitPrice(product.repair_price, product.repair_price_adj, product.mat_curr)
            form.product_third_p_val.Text = formatInitPrice(product.third_party_price, product.third_party_price_adj, product.mat_curr)
            form.product_freight_val.Text = formatInitPrice(product.freight_price, product.freight_price_adj, product.mat_curr)

            updateDependantActivitiesView()

        Catch ex As Exception
            MsgBox("An error occured while loading data of quote product " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub updateDependantActivitiesView()
        Dim bds As New BindingSource
        Dim sortHandler As BindingListView(Of CScopeActivity)
        Try
            Dim product As CSalesQuoteEntry = form.cb_product.SelectedItem

            'potentials act
            'fill all activities view
            sortHandler = New BindingListView(Of CScopeActivity)(GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource)
            bds = New BindingSource
            sortHandler.RemoveFilter()
            sortHandler.RemoveSort()
            sortHandler.ApplyFilter(AddressOf filterAllPotentialDependantAct)
            sortHandler.ApplySort("act_notif_opcode ASC")
            bds.DataSource = MGeneralFuntionsViewControl.getActFromListView(sortHandler)
            form.act_dep_all_listb.DataSource = bds
            form.act_dep_all_listb.SelectionMode = SelectionMode.MultiExtended
            form.act_dep_all_listb.HorizontalScrollbar = True
            form.act_dep_all_listb.SelectedItems.Clear()
            'pre select act
            For Each act As CScopeActivity In allActSelectedItems
                form.act_dep_all_listb.SelectedItems.Add(act)
            Next

            allActSelectedItems.Clear()
            form.act_dep_all_listb.Refresh()

            'current act
            blockHoursViewRefresh = True
            sortHandler = New BindingListView(Of CScopeActivity)(product.getDependantActivitiesList)
            bds = New BindingSource
            sortHandler.RemoveFilter()
            sortHandler.RemoveSort()
            sortHandler.ApplyFilter(AddressOf filterCurrentDependantAct)
            sortHandler.ApplySort("act_notif_opcode ASC")
            Dim listO As List(Of CScopeActivity) = MGeneralFuntionsViewControl.getActFromListView(sortHandler)
            bds.DataSource = listO
            form.act_dep_selected_listb.DataSource = bds
            form.act_dep_selected_listb.SelectionMode = SelectionMode.MultiExtended
            form.act_dep_selected_listb.HorizontalScrollbar = True
            form.act_dep_selected_listb.SelectedItems.Clear()

            'pre select act
            For Each act As CScopeActivity In depActSelectedItems
                form.act_dep_selected_listb.SelectedItems.Add(act)
            Next
            depActSelectedItems.Clear()
            form.act_dep_selected_listb.Refresh()
            'deblock row refresh
            blockHoursViewRefresh = False

            'primary activity
            sortHandler.RemoveFilter()
            sortHandler.RemoveSort()
            sortHandler.ApplyFilter(AddressOf filterCurrentDependantActPotentialMain)
            sortHandler.ApplySort("act_notif_opcode ASC")
            listO = MGeneralFuntionsViewControl.getActFromListView(sortHandler)

            form.select_main_act_cb.Text = ""
            bds = New BindingSource
            bds.DataSource = listO
            form.select_main_act_cb.DataSource = bds
            form.select_main_act_cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            'set current value. if null put first list value
            If IsNothing(product.main_activity_obj) Then
                'if no main activity set, take first one
                If listO.Count > 0 Then
                    form.select_main_act_cb.SelectedItem = listO(0)
                    product.main_activity_obj = listO(0)
                End If
            Else
                form.select_main_act_cb.SelectedItem = product.main_activity_obj
            End If

            'update hours view
            updateLabMatViews()

        Catch ex As Exception
            MsgBox("An error occured while loading data of dependant activities " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'refresh table views when new activity is selected
    Public Sub updateLabMatViews()
        Try
            'if editing dependant activities not update
            If blockHoursViewRefresh Then
                Exit Sub
            End If

            'if no act selected block view edit
            If form.act_dep_selected_listb.SelectedItems.Count = 0 Then
                'reset views
                form.add_scope_hrs_dgrid.DataSource = Nothing
                form.init_scope_hours_dgrid.DataSource = Nothing
                form.add_scope_mat_dgrid.DataSource = Nothing
                form.init_scope_mat_dgrid.DataSource = Nothing

                form.init_scope_hours_dgrid.AllowUserToAddRows = False
                form.init_scope_hours_dgrid.AllowUserToDeleteRows = False

                form.init_scope_mat_dgrid.AllowUserToAddRows = False
                form.init_scope_mat_dgrid.AllowUserToDeleteRows = False
                Exit Sub
            Else
                form.init_scope_hours_dgrid.AllowUserToAddRows = True
                form.init_scope_hours_dgrid.AllowUserToDeleteRows = False

                form.init_scope_mat_dgrid.AllowUserToAddRows = True
                form.init_scope_mat_dgrid.AllowUserToDeleteRows = False
            End If

            Dim act As CScopeActivity = form.act_dep_selected_listb.SelectedItems(0)

            'avoid dirty refreshed due to databindings
            If act.Equals(latestEditedActicity) Then
                Exit Sub
            End If

            'update latest edited act
            latestEditedActicity = act

            'fill views
            Dim addView As New BindingListView(Of CSoldHours)(act.sold_hour_list)
            addView.ApplyFilter(AddressOf filterAddScope)
            form.add_scope_hrs_dgrid.DataSource = addView

            Dim initView As New BindingListView(Of CSoldHours)(act.sold_hour_list)
            'add new row handler
            AddHandler initView.AddingNew, AddressOf ini_sold_hours_view_AddingNew
            'AddHandler initView.ListChanged, AddressOf ini_sold_hours_view_ListChanged
            initView.ApplyFilter(AddressOf filterInitScope)
            form.init_scope_hours_dgrid.DataSource = initView
            'error list if focus
            CGridViewRowValidationHandler.initializeGridErrorState(form.init_scope_hours_dgrid)

            Dim matView As New BindingListView(Of CMaterial)(act.material_list)
            'add new row handler=> Activity to be set on new rows, material to be added in material list
            RemoveHandler matView.AddingNew, AddressOf material_view_AddingNew
            AddHandler matView.AddingNew, AddressOf material_view_AddingNew
            matView.ApplyFilter(AddressOf filterInitMaterials)
            form.init_scope_mat_dgrid.DataSource = matView
            form.init_scope_mat_dgrid.Refresh()
            'error list if focus
            CGridViewRowValidationHandler.initializeGridErrorState(form.init_scope_mat_dgrid)

            'add view readnly
            Dim matInitView As New BindingListView(Of CMaterial)(act.material_list)
            matInitView.ApplyFilter(AddressOf filterAddMaterials)
            form.add_scope_mat_dgrid.DataSource = matInitView

        Catch ex As Exception
            MsgBox("An error occured while loading working hours of the selected activity " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'get material of the current product or potential materials
    'is_reported is always true in additional scope
    Public Function filterInitMaterials(ByVal mat As CMaterial)
        'delegate for filtering
        If mat.system_status = MConstants.CONST_SYS_ROW_DELETED Then
            Return False
        ElseIf Not IsNothing(mat.init_quote_entry_obj) OrElse Not mat.is_reported Then
            Return True
        Else
            Return False
        End If
    End Function
    'filter add materials
    'is_reported is always true in additional scope
    Public Function filterAddMaterials(ByVal mat As CMaterial)
        'delegate for filtering
        If mat.system_status = MConstants.CONST_SYS_ROW_DELETED Then
            Return False
        ElseIf IsNothing(mat.init_quote_entry_obj) AndAlso mat.is_reported Then
            Return True
        Else
            Return False
        End If
    End Function

    'when added new row to material view, set the activity object
    Private Sub material_view_AddingNew(ByVal sender As Object, ByVal e As AddingNewEventArgs)
        Dim act As CScopeActivity = form.act_dep_selected_listb.SelectedItems(0)
        Dim product As CSalesQuoteEntry = form.cb_product.SelectedItem

        Dim mat As New CMaterial
        mat.is_reported = True
        mat.po_created = DateTime.Now
        mat.activity_obj = act
        mat.init_quote_entry_obj = product
        mat.payer_obj = product.quote_obj.payer_obj
        e.NewObject = mat
    End Sub


    Private Function filterAddScope(sold_hr As CSoldHours) As Boolean
        If sold_hr.system_status = MConstants.CONST_SYS_ROW_DELETED Then
            Return False
        ElseIf sold_hr.is_reported AndAlso IsNothing(sold_hr.init_quote_entry_obj) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function filterInitScope(sold_hr As CSoldHours) As Boolean
        'delegate for filtering
        If sold_hr.system_status = MConstants.CONST_SYS_ROW_DELETED Then
            Return False
        ElseIf Not IsNothing(sold_hr.init_quote_entry_obj) OrElse Not sold_hr.is_reported Then
            Return True
        Else
            Return False
        End If
    End Function

    'when added new row to view, set the activity object
    Private Sub ini_sold_hours_view_AddingNew(ByVal sender As Object, ByVal e As AddingNewEventArgs)
        Dim product As CSalesQuoteEntry = form.cb_product.SelectedItem
        Dim soldH As New CSoldHours

        Dim bds As BindingListView(Of CSoldHours) = sender
        soldH.is_reported = True
        soldH.act_object = form.act_dep_selected_listb.SelectedItems(0)
        soldH.init_quote_entry_obj = product
        soldH.payer_obj = product.quote_obj.payer_obj
        e.NewObject = soldH
    End Sub


    'filter potential dependant activities
    Private Function filterAllPotentialDependantAct(act As CScopeActivity) As Boolean
        If act.quote_entry_link_list.Count = 0 Then
            Return True
        ElseIf _is_show_only_not_mapped Then
            Return False
        Else
            Dim product As CSalesQuoteEntry = form.cb_product.SelectedItem
            For Each lk As CSaleQuoteEntryToActLink In act.quote_entry_link_list
                If lk.init_quote_entry_id = product.id Then
                    Return False
                End If
            Next
            Return True
        End If
    End Function

    'filter current dependant activities
    Private Function filterCurrentDependantAct(act As CScopeActivity) As Boolean
        If act.quote_entry_link_list.Count = 0 Then
            Return False
        Else
            Dim product As CSalesQuoteEntry = form.cb_product.SelectedItem
            For Each lk As CSaleQuoteEntryToActLink In act.quote_entry_link_list
                If lk.init_quote_entry_id = product.id Then
                    Return True
                End If
            Next
            Return False
        End If
    End Function

    'filter current dependant activities potential main
    Private Function filterCurrentDependantActPotentialMain(act As CScopeActivity) As Boolean
        If filterCurrentDependantAct(act) AndAlso Not act.is_cost_collector Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function formatInitPrice(price As Double, adjust As Double, curr As String) As String
        Dim res As String
        res = FormatNumber(price + adjust, 2) & " " & curr & " (" & FormatNumber(price, 2)
        Dim sign As String
        If adjust <= 0 Then
            sign = "-"
        Else
            sign = "+"
        End If
        res = res & sign & FormatNumber(adjust, 2) & ")"
        Return res
    End Function

    'add activities to the quote
    Public Sub addDependantActivities()
        Try
            Dim product As CSalesQuoteEntry = form.cb_product.SelectedItem
            For Each act As CScopeActivity In form.act_dep_all_listb.SelectedItems
                'if it is the first time that activities are binded to the quote, define a primary activity to avoid error in activity price caluclation (seting the product sum will triger calculation on act that need primary act)
                If IsNothing(product.main_activity_obj) AndAlso Not act.is_cost_collector Then
                    product.main_activity_obj = act
                    act.is_hidden = False
                Else
                    Dim isMain As Boolean = False
                    For Each lk As CSaleQuoteEntryToActLink In act.quote_entry_link_list
                        If Not IsNothing(lk.init_quote_entry_obj) AndAlso Object.Equals(lk.init_quote_entry_obj.main_activity_obj, act) Then
                            isMain = True
                            Exit For
                        End If
                    Next
                    If Not isMain Then
                        act.is_hidden = True
                    End If
                End If

                'new link. the constructor call the database insert delegate
                Dim newlk As New CSaleQuoteEntryToActLink(act, product)

                'to be selected
                depActSelectedItems.Add(act)
            Next
            For Each act As CScopeActivity In depActSelectedItems
                act.updateCal()
            Next
            updateDependantActivitiesView()
        Catch ex As Exception
            MsgBox("An error occured while adding data to dependant activities " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'remove activities from the quote
    Public Sub RemoveDependantActivities()
        Try
            Dim product As CSalesQuoteEntry = form.cb_product.SelectedItem
            For Each act As CScopeActivity In form.act_dep_selected_listb.SelectedItems
                If product.main_activity_id = act.id Then
                    MsgBox("Cannot remove activity " & act.tostring & " because it is set as primary activity")
                Else
                    'break link with activity
                    For Each lk As CSaleQuoteEntryToActLink In act.quote_entry_link_list
                        If Not IsNothing(lk.init_quote_entry_obj) AndAlso Object.Equals(lk.init_quote_entry_obj, product) Then
                            lk.deleteItem()
                            Exit For
                        End If
                    Next
                    'no more dependant activity ? unhide
                    If act.quote_entry_link_list.Count = 0 Then
                        act.is_hidden = False
                    End If

                    'break link  with hours
                    For Each soldH As CSoldHours In act.sold_hour_list
                        If Object.Equals(soldH.init_quote_entry_obj, product) Then
                            soldH.init_quote_entry_obj = Nothing
                            soldH.is_reported = False
                        End If
                    Next

                    'break link  with material
                    For Each mat As CMaterial In act.material_list
                        If Object.Equals(mat.init_quote_entry_obj, product) Then
                            mat.init_quote_entry_obj = Nothing
                            mat.is_reported = False
                        End If
                    Next

                    'select item at the end
                    allActSelectedItems.Add(act)
                End If
            Next
            updateDependantActivitiesView()
        Catch ex As Exception
            MsgBox("An error occured while removing data from dependant activities " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub main_activity_changed()
        Try
            Dim act As CScopeActivity = form.select_main_act_cb.SelectedItem
            Dim previousMainAct As CScopeActivity
            Dim product As CSalesQuoteEntry = form.cb_product.SelectedItem

            previousMainAct = product.main_activity_obj
            product.main_activity_obj = act
            act.is_hidden = False
            'update calc
            act.updateCal()
            If Not IsNothing(previousMainAct) Then
                previousMainAct.updateCal()
                Dim isMain As Boolean = False
                For Each lk As CSaleQuoteEntryToActLink In previousMainAct.quote_entry_link_list
                    If Not IsNothing(lk.init_quote_entry_obj) AndAlso Object.Equals(lk.init_quote_entry_obj.main_activity_obj, previousMainAct) Then
                        isMain = True
                        Exit For
                    End If
                Next
                If Not isMain Then
                    previousMainAct.is_hidden = True
                End If
            End If
            form.act_dep_selected_listb.SelectedItems.Clear()
            'will trigger hours grid refresh
            form.act_dep_selected_listb.SelectedItem = act
        Catch ex As Exception
            MsgBox("An error occured while setting the new primary activity " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub drawInitHoursgrid()
        Dim gridMap As CViewModelMapList

        'get  map view
        gridMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_INIT_HOURS_VIEW_DISPLAY)

        ' datagridview
        form.init_scope_hours_dgrid.AllowUserToAddRows = True
        MViewEventHandler.addDefaultDatagridEventHandler(form.init_scope_hours_dgrid)

        'call generic draw
        drawGenericInitLaborGrid(form.init_scope_hours_dgrid, gridMap)
        'add applicability columns
        addApplicabilityCols(form.init_scope_hours_dgrid)
        MGeneralFuntionsViewControl.resizeDataGridWidth(form.init_scope_hours_dgrid)
        form.main_panel_set_hours.Width = form.main_panel_set_hours.Width + MGeneralFuntionsViewControl.getSizeDataGridWidth(form.init_scope_hours_dgrid) - form.init_scope_hours_dgrid.Width

        'row validation event
        'handle row validation events
        Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(form.init_scope_hours_dgrid, CSoldHoursInitRowValidator.getInstance, gridMap)
        dhler.validateAddDelegate = AddressOf init_hours_view_ValidateAdd
        dhler.cancelAddDelegate = AddressOf init_hours_view_CancelAdd
    End Sub

    Public Sub drawGenericInitLaborGrid(dgrid As DataGridView, gridMap As CViewModelMapList)

        Dim bds As BindingSource
        For Each map As CViewModelMap In gridMap.list.Values
            If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                Dim dgcol As New DataGridViewTextBoxColumn
                'column name is the property name of binded object
                If map.col_width <> 0 Then
                    dgcol.Width = map.col_width
                End If
                If Not String.IsNullOrWhiteSpace(map.col_format) Then
                    dgcol.DefaultCellStyle.Format = map.col_format
                    Dim propInf As PropertyInfo
                    propInf = gridMap.object_class.GetProperty(map.class_field)
                    'format as numeric only for double
                    If propInf.PropertyType.Name = "double" Then
                        dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    End If
                End If
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = map.view_col
                dgcol.DataPropertyName = map.class_field
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                dgcol.ReadOnly = Not map.col_editable
                dgrid.Columns.Add(dgcol)
            ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                Dim dgcol As New DataGridViewComboBoxColumn
                'column name is the property name of binded object
                If map.col_width <> 0 Then
                    dgcol.Width = map.col_width
                End If
                If Not String.IsNullOrWhiteSpace(map.col_format) Then
                    dgcol.DefaultCellStyle.Format = map.col_format
                    Dim propInf As PropertyInfo
                    propInf = gridMap.object_class.GetProperty(map.class_field)
                    'format as numeric only for double
                    If propInf.PropertyType.Name = "double" Then
                        dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    End If
                End If
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = map.view_col
                dgcol.DataPropertyName = map.class_field
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                'values
                bds = New BindingSource
                If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                    bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "value"
                    dgcol.ValueMember = "key"
                ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                    bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "value"
                    dgcol.ValueMember = "key"
                ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_SOLD_HRS_CC.ToLower Then
                    bds.DataSource = MConstants.listOfCC.Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "label"
                    dgcol.ValueMember = "cc"
                ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_SOLD_HRS_PAYER.ToLower Then
                    bds.DataSource = MConstants.listOfPayer.Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "label"
                    dgcol.ValueMember = "payer"
                Else
                    Throw New Exception("Error occured when binding activity view in additional scope tab. List " & map.view_list_name & " does not exist in LOV")
                End If
                dgcol.ReadOnly = Not map.col_editable
                dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                dgrid.Columns.Add(dgcol)
            ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                Dim dgcol As New DataGridViewCheckBoxColumn
                'column name is the property name of binded object
                If map.col_width <> 0 Then
                    dgcol.Width = map.col_width
                End If
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = map.view_col
                dgcol.DataPropertyName = map.class_field
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                dgcol.ReadOnly = Not map.col_editable
                dgrid.Columns.Add(dgcol)
            End If
        Next
    End Sub

    'validate new
    Public Sub init_hours_view_ValidateAdd(soldHrs As ObjectView(Of CSoldHours), rowIndex As Integer)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(soldHrs.Object) Then
                Dim soldHrsListView As BindingListView(Of CSoldHours) = CType(form.init_scope_hours_dgrid.DataSource, BindingListView(Of CSoldHours))
                'if new item
                soldHrs.Object.setID()
                'to avoid duplication
                If Not soldHrsListView.DataSource.Contains(soldHrs.Object) Then
                    soldHrsListView.EndNew(rowIndex)
                End If
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new initial scope hour entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'cancel new
    Public Function init_hours_view_CancelAdd(soldHours As ObjectView(Of CSoldHours), rowIndex As Integer) As Boolean
        'return true to block leaving and return false to allow leaving
        Dim res As Boolean
        Try
            'check if new
            If soldHours.Object.id > 0 Then
                res = True
            Else
                Dim soldHrsListView As BindingListView(Of CSoldHours) = CType(form.init_scope_hours_dgrid.DataSource, BindingListView(Of CSoldHours))
                If soldHrsListView.DataSource.Contains(soldHours.Object) Then
                    res = True
                Else
                    soldHours.CancelEdit()
                    soldHrsListView.CancelNew(rowIndex)
                    res = False
                End If
            End If

        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new initial scope hour entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return res
    End Function

    Private Sub drawAddHoursgrid()
        Dim gridMap As CViewModelMapList
        'get  map view
        gridMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ADD_HOURS_VIEW_DISPLAY)

        ' datagridview
        form.add_scope_hrs_dgrid.AllowUserToAddRows = False
        MViewEventHandler.addDefaultDatagridEventHandler(form.add_scope_hrs_dgrid)
        MConstants.GLB_MSTR_CTRL.add_scope_hours_edit_ctrl.drawGenericAddHoursgrid(form.add_scope_hrs_dgrid, gridMap)
        form.add_scope_hrs_dgrid.ReadOnly = True

        MGeneralFuntionsViewControl.resizeDataGridWidth(form.add_scope_hrs_dgrid)
    End Sub

    Private Sub drawInitMaterialgrid()
        'add mat datagrid
        form.init_scope_mat_dgrid.AllowUserToAddRows = True
        form.init_scope_mat_dgrid.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
        MViewEventHandler.addDefaultDatagridEventHandler(form.init_scope_mat_dgrid)

        MConstants.GLB_MSTR_CTRL.material_controller.bind_mat_generic_view(form.init_scope_mat_dgrid)
        'set activity view as read only
        Dim act_col As DataGridViewColumn = form.init_scope_mat_dgrid.Columns(MConstants.CONST_CONF_MAT_MAIN_ACT)
        If Not IsNothing(act_col) Then
            act_col.ReadOnly = True
        End If

        addApplicabilityCols(form.init_scope_mat_dgrid)
        'refresh activity dependant list to avoid that new activity not yet in list trigger an error
        MConstants.GLB_MSTR_CTRL.add_scope_controller.refreshScopeDependantViews()
        MGeneralFuntionsViewControl.resizeDataGridWidth(form.init_scope_mat_dgrid)
        'row validator
        Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(form.init_scope_mat_dgrid, CMaterialInitRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_MAT_MAIN_VIEW_DISPLAY))
        dhler.validateAddDelegate = AddressOf material_view_ValidateAdd
        dhler.cancelAddDelegate = AddressOf material_view_CancelAdd
    End Sub

    'add
    Private Sub drawAddMaterialgrid()
        'add mat datagrid
        form.add_scope_mat_dgrid.ReadOnly = True
        form.add_scope_mat_dgrid.AllowUserToAddRows = False
        MConstants.GLB_MSTR_CTRL.material_controller.bind_mat_generic_view(form.add_scope_mat_dgrid)
        MViewEventHandler.addDefaultDatagridEventHandler(form.add_scope_mat_dgrid)
        MGeneralFuntionsViewControl.resizeDataGridWidth(form.add_scope_mat_dgrid)
    End Sub

    'validate new
    Public Sub material_view_ValidateAdd(material As ObjectView(Of CMaterial), rowIndex As Integer)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(material.Object) Then
                Dim matListView As BindingListView(Of CMaterial) = CType(form.init_scope_mat_dgrid.DataSource, BindingListView(Of CMaterial))
                'if new item
                material.Object.setID()
                If Not matListView.DataSource.Contains(material.Object) Then
                    matListView.EndNew(rowIndex)
                End If

                'add to whole material list
                If Not MConstants.GLB_MSTR_CTRL.material_controller.matList.DataSource.Contains(material.Object) Then
                    MConstants.GLB_MSTR_CTRL.material_controller.matList.DataSource.Add(material.Object)
                End If
                'update activity
                material.Object.activity_obj.updateCal()
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new material entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'cancel new
    Public Function material_view_CancelAdd(material As ObjectView(Of CMaterial), rowIndex As Integer) As Boolean
        'return true to block leaving and return false to allow leaving
        Dim res As Boolean
        Try
            'check if new
            If material.Object.id > 0 Then
                res = True
            Else
                Dim matListView As BindingListView(Of CMaterial) = CType(form.init_scope_mat_dgrid.DataSource, BindingListView(Of CMaterial))
                If matListView.DataSource.Contains(material.Object) Then
                    res = True
                Else
                    material.CancelEdit()
                    matListView.CancelNew(rowIndex)
                    res = False
                End If
            End If

        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new material entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return res
    End Function

    'add two checkbox columns at the beginning of the datagrid to allow user to add or delete a new labor or material to the awq
    Private Sub addApplicabilityCols(dgrid As DataGridView)
        Dim dgcol As New DataGridViewCheckBoxColumn
        dgcol.Width = 50
        dgcol.Name = MConstants.CONST_CONF_QUOTE_PRODUCT_SUMM_ADD_LABMAT
        dgcol.HeaderText = "Add"
        dgcol.SortMode = DataGridViewColumnSortMode.Automatic
        dgcol.ReadOnly = False
        dgcol.DataPropertyName = "is_quote_sum"
        dgrid.Columns.Insert(0, dgcol)
        'event to handle applicability
        AddHandler dgrid.CurrentCellDirtyStateChanged, AddressOf DataGridView_CurrentCellDirtyStateChanged
        AddHandler dgrid.CellValueChanged, AddressOf DataGridView_CellValueChanged
    End Sub

    'handle check box event
    Private Sub DataGridView_CurrentCellDirtyStateChanged(sender As DataGridView, e As EventArgs)
        Dim currentCell As DataGridViewCell = sender.CurrentCell
        'if current cell of grid is dirty, commits edit
        If currentCell.OwningColumn.Name = MConstants.CONST_CONF_QUOTE_PRODUCT_SUMM_ADD_LABMAT Then
            If sender.IsCurrentCellDirty Then
                sender.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If
        End If
    End Sub

    'change event cannot be handled in dirty cell state change becase changes are not yet commited
    Private Sub DataGridView_CellValueChanged(ByVal sender As DataGridView, ByVal e As DataGridViewCellEventArgs)
        Try
            Dim product As CSalesQuoteEntry = form.cb_product.SelectedItem
            Dim chkb As DataGridViewCheckBoxCell
            Dim chck_value As Boolean

            Dim currentCell As DataGridViewCell = sender.Rows(e.RowIndex).Cells(e.ColumnIndex)
            If currentCell.OwningColumn.Name = MConstants.CONST_CONF_QUOTE_PRODUCT_SUMM_ADD_LABMAT Then
                chkb = CType(currentCell, DataGridViewCheckBoxCell)
                chck_value = Not chkb.Value
            Else
                Exit Sub
            End If

            'try to cast databound object
            Dim rowObj As Object = Nothing
            Try
                rowObj = sender.Rows(e.RowIndex).DataBoundItem.Object
            Catch ex As Exception
            End Try

            'if no bounded object, end
            If IsNothing(rowObj) Then
                Exit Sub
            End If
            If rowObj.GetType Is GetType(CSoldHours) Then
                'check if item could be edited
                Dim soldH As CSoldHours = CType(rowObj, CSoldHours)
                If Not IsNothing(soldH.init_quote_entry_obj) AndAlso Not soldH.init_quote_entry_obj.id = product.id Then
                    MGeneralFuntionsViewControl.displayMessage("Not Allowed !", "It is not possible to change the mapping from a differenct product. Browse to product " & soldH.init_quote_entry_obj.product & " to perform this action", MsgBoxStyle.Critical)
                    Exit Sub
                End If
                If chck_value Then
                    soldH.init_quote_entry_obj = product
                    soldH.is_reported = True
                    soldH.payer_obj = product.quote_obj.payer_obj
                Else
                    soldH.init_quote_entry_obj = Nothing
                    soldH.is_reported = False
                    soldH.payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value
                End If
            ElseIf rowObj.GetType Is GetType(CMaterial) Then
                'check if item could be edited
                Dim mat As CMaterial = CType(rowObj, CMaterial)
                If Not IsNothing(mat.init_quote_entry_obj) AndAlso Not mat.init_quote_entry_obj.id = product.id Then
                    MGeneralFuntionsViewControl.displayMessage("Not Allowed !", "It is not possible to change the mapping from a differenct product. Browse to product " & mat.init_quote_entry_obj.product & " to perform this action", MsgBoxStyle.Critical)
                    Exit Sub
                End If
                If chck_value Then
                    mat.init_quote_entry_obj = product
                    mat.is_reported = True
                    mat.payer_obj = product.quote_obj.payer_obj
                Else
                    mat.init_quote_entry_obj = Nothing
                    mat.is_reported = False
                    mat.payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value
                End If
            End If
        Catch ex As Exception
            MsgBox("An error occured while mapping labor or material to product " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try

    End Sub
End Class
