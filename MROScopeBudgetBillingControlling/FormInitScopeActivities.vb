﻿Public Class FormInitScopeActivities
    'use arrow to browser product
    Private Sub product_select_left_pic_Click(sender As Object, e As EventArgs) Handles product_select_left_pic.Click
        MConstants.GLB_MSTR_CTRL.init_scope_hours_edit_ctrl.selectedProductBackward()
    End Sub
    'use arrow to browser product
    Private Sub product_select_right_pic_Click(sender As Object, e As EventArgs) Handles product_select_right_pic.Click
        MConstants.GLB_MSTR_CTRL.init_scope_hours_edit_ctrl.selectedProductForward()
    End Sub
    'product selection change event
    Private Sub cb_product_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_product.SelectedIndexChanged
        MConstants.GLB_MSTR_CTRL.init_scope_hours_edit_ctrl.updateView()
    End Sub
    'add or remove dependant activities
    Private Sub select_act_right_Click(sender As Object, e As EventArgs) Handles select_act_right.Click
        MConstants.GLB_MSTR_CTRL.init_scope_hours_edit_ctrl.addDependantActivities()
    End Sub
    'add or remove dependant activities
    Private Sub select_act_left_Click(sender As Object, e As EventArgs) Handles select_act_left.Click
        MConstants.GLB_MSTR_CTRL.init_scope_hours_edit_ctrl.RemoveDependantActivities()
    End Sub
    'change main activity
    Private Sub select_main_act_cb_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles select_main_act_cb.SelectionChangeCommitted
        MConstants.GLB_MSTR_CTRL.init_scope_hours_edit_ctrl.main_activity_changed()
    End Sub
    'select an item among dependant activities to edit hours
    Private Sub act_dep_selected_listb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles act_dep_selected_listb.SelectedIndexChanged
        MConstants.GLB_MSTR_CTRL.init_scope_hours_edit_ctrl.updateLabMatViews()
    End Sub

    'material main view, handle click on edit button
    Private Sub act_materials_dgrid_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles init_scope_mat_dgrid.CellContentClick
        Try
            ' Ignore clicks that are not on button cells. 
            If e.RowIndex >= 0 AndAlso init_scope_mat_dgrid.Columns(e.ColumnIndex).Name = MConstants.CONST_CONF_MAT_EDIT Then
                Try
                    Dim material As CMaterial
                    material = CType(init_scope_mat_dgrid.Rows(e.RowIndex).DataBoundItem, Equin.ApplicationFramework.ObjectView(Of CMaterial)).Object
                    MConstants.GLB_MSTR_CTRL.material_edit_ctrl.launchEditForm(material, CMaterialInitRowValidator.getInstance, material.activity_obj)
                    CGridViewRowValidationHandler.initializeGridErrorState(init_scope_mat_dgrid)
                Catch ex As Exception
                    MGeneralFuntionsViewControl.displayMessage("Error!", "Cannot launch the form to edit material, check bound has been done" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
                End Try
            Else
                Return
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub add_scope_main_datagrid_DataError(ByVal sender As Object, ByVal e As DataGridViewDataErrorEventArgs) Handles init_scope_hours_dgrid.DataError

        sender = sender
        e = e

    End Sub

    'when closing the form save quote_entries, activity and sold hours
    Private Sub FormInitScopeActivities_FormClosed(sender As Object, e As EventArgs) Handles MyBase.FormClosed
        'reset scope for unsed sap hours
        MConstants.GLB_MSTR_CTRL.ini_scope_controller.quote_entries_list.Refresh()
    End Sub
End Class