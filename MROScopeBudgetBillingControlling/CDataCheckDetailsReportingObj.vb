﻿Public Class CDataCheckDetailsReportingObj

    Public Sub New()

    End Sub

    Private _object_type As String
    Public Property object_type() As String
        Get
            Return _object_type
        End Get
        Set(ByVal value As String)
            If Not _object_type = value Then
                _object_type = value
            End If
        End Set
    End Property

    Private _object_name As String
    Public Property object_name() As String
        Get
            Return _object_name
        End Get
        Set(ByVal value As String)
            If Not _object_name = value Then
                _object_name = value
            End If
        End Set
    End Property

    Private _error_desc As String
    Public Property error_desc() As String
        Get
            Return _error_desc
        End Get
        Set(ByVal value As String)
            If Not _error_desc = value Then
                _error_desc = value
            End If
        End Set

    End Property
End Class
