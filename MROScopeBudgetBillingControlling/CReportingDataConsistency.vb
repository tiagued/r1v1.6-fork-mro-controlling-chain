﻿Imports System.Text
Imports Equin.ApplicationFramework
Imports Microsoft.Office.Interop.Excel

Public Class CReportingDataConsistency
    Private Shared instance As CReportingDataConsistency

    Public form As FormReportDataCheck

    'sum
    Private dataCheckSummaryConf As List(Of CReportConfObject)
    'sub total
    Private dataCheckCountListConf As List(Of CReportConfObject)
    'details
    Private dataCheckDetailsListConf As List(Of CReportConfObject)

    Private reportWB As Microsoft.Office.Interop.Excel.Workbook

    'list of data
    Private detail_error_list As New List(Of CDataCheckDetailsReportingObj)
    Private total_per_bo_type_list As New Dictionary(Of String, CDataCheckCountReportingObj)

    Public Shared Function getInstance() As CReportingDataConsistency
        Try
            If IsNothing(instance) Then
                instance = New CReportingDataConsistency

                'proj summ
                instance.dataCheckSummaryConf = MConstants.listOfReportConf(MConstants.REPORT_DATA_CHECK_SUMMARY_SHEET_NAME)(MConstants.REPORT_CONF_SUMMARY_LIST_NAME)
                'count per BO
                instance.dataCheckCountListConf = MConstants.listOfReportConf(MConstants.REPORT_DATA_CHECK_SUMMARY_SHEET_NAME)(MConstants.REPORT_DATA_CHECK_CNT_PER_BO_LIST_NAME)
                'error details
                instance.dataCheckDetailsListConf = MConstants.listOfReportConf(MConstants.REPORT_DATA_CHECK_DETAILS_SHEET_NAME)(MConstants.REPORT_DATA_CHECK_DETAILS_LIST_NAME)
            End If
        Catch ex As Exception
            instance = Nothing
            MGeneralFuntionsViewControl.displayMessage("Data Consistency Check Error", "An error occured while checking project data consistency." & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return instance
    End Function

    Public Sub log_text(text As String)
        Try
            form.log_bt.Text = text
        Catch ex As Exception
            Throw New Exception("An error occured while logging progress" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    'bind form controls to report object properties
    Public Sub prepareDataCheckReportView()
        Try
            form = New FormReportDataCheck

            'labor controlling ?
            form.lab_ctrl_chk.DataBindings.Clear()
            form.lab_ctrl_chk.DataBindings.Add("Checked", Me, "is_labor_ctrl_included", False, DataSourceUpdateMode.OnPropertyChanged)

            'log
            log_text("Click Run to generate report")

            form.ShowDialog()
            form.BringToFront()
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Controlling Error", "An error occured while preparing controlling reporting view." & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private _is_labor_ctrl_included As Boolean
    Public Property is_labor_ctrl_included() As Boolean
        Get
            Return _is_labor_ctrl_included
        End Get
        Set(ByVal value As Boolean)
            If Not _is_labor_ctrl_included = value Then
                _is_labor_ctrl_included = value
            End If
        End Set
    End Property

    Public Sub launch()
        Try
            'log
            log_text("Starting...")

            Dim template As String = MConstants.constantConf(MConstants.CONST_CONF_DATA_CHCK_TEMPLATE_FILE).value
            If Not System.IO.File.Exists(template) Then
                Throw New Exception("The Data Check Template File " & template & " Does not exists")
            End If

            'generate excel file
            'log
            log_text("calculating saving path")
            Dim reportName As String = MConstants.constantConf(MConstants.CONST_CONF_DATA_CHCK_REPORT_NAME).value
            Dim reportFolder As String = ""
            'replace variable in report name
            reportName = reportName.Replace(MConstants.constantConf(MConstants.CONST_CONF_PROJ_PARAM).value, MConstants.GLB_MSTR_CTRL.project_controller.project.project_def)
            reportName = reportName.Replace(MConstants.constantConf(MConstants.CONST_CONF_DATE_PARAM).value, DateTime.Now.ToString("yyyy.MM.dd hh.mm.ss"))

            reportName = MConstants.GLB_MSTR_CTRL.csFile.db_folder & "\" & reportName

            'log
            log_text("Copy and Open template to " & reportName)
            reportWB = MConstants.GLB_MSTR_CTRL.csFile.userxlAppOpenTemplate(template, reportName)

            Try
                MConstants.GLB_MSTR_CTRL.csFile.OffCalc(reportWB.Application)

                'load tabs
                'log
                log_text("Calculating data")
                loadLists()
                'log
                log_text("Filling Data check summary")
                loadDataCheckSummary()
                'log
                log_text("Filling Data check Details Data")
                loadDataCheckDetailsData()

                'clear memory
                total_per_bo_type_list.Clear()
                detail_error_list.Clear()

            Catch ex As Exception
                Throw New Exception("Error " & Chr(10) & ex.Message & ex.StackTrace)
            Finally
                MConstants.GLB_MSTR_CTRL.csFile.OnCalc(reportWB.Application)
            End Try
            'log
            log_text("Saving Report")
            MConstants.GLB_MSTR_CTRL.csFile.safeWorkbookSave(reportWB)
            reportWB.Activate()
            Dim proj_sum_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_HRS_STATEMENT_SUM_TAB_NAME).value)
            proj_sum_ws.Activate()
            'log
            log_text("Report successfully generated")
            MGeneralFuntionsViewControl.displayMessage("Data Check Success", "The Data Check report has been generated successfully :" & reportWB.FullName, MsgBoxStyle.Information)
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Data Check Error", "An error occured while preparingData Check report view." & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        Finally
            form.Close()
        End Try
    End Sub

    Public Sub loadLists()
        Try
            total_per_bo_type_list.Clear()
            detail_error_list.Clear()

            'check project
            Dim checkProj As KeyValuePair(Of Boolean, String) = Nothing
            Dim cntReptObj As CDataCheckCountReportingObj = Nothing
            Dim detailsReptObj As CDataCheckDetailsReportingObj = Nothing

            'project general data
            checkProj = MTabNavigationStory.conf_proj_check(MConstants.GLB_MSTR_CTRL)
            If Not checkProj.Key Then
                If Not total_per_bo_type_list.ContainsKey("Project") Then
                    cntReptObj = New CDataCheckCountReportingObj
                    cntReptObj.label = "Project Config"
                    total_per_bo_type_list.Add("Project", cntReptObj)
                Else
                    cntReptObj = total_per_bo_type_list("Project")
                End If
                cntReptObj.qty = cntReptObj.qty + 1

                detailsReptObj = New CDataCheckDetailsReportingObj
                detailsReptObj.object_name = MConstants.GLB_MSTR_CTRL.project_controller.project.project_list_str
                detailsReptObj.object_type = cntReptObj.label
                detailsReptObj.error_desc = checkProj.Value
                detail_error_list.Add(detailsReptObj)
            End If

            'project billing cond
            checkProj = MTabNavigationStory.conf_bill_cond_check(MConstants.GLB_MSTR_CTRL)
            If Not checkProj.Key Then
                If Not total_per_bo_type_list.ContainsKey("Project") Then
                    cntReptObj = New CDataCheckCountReportingObj
                    cntReptObj.label = "Project Config"
                    total_per_bo_type_list.Add("Project", cntReptObj)
                Else
                    cntReptObj = total_per_bo_type_list("Project")
                End If
                cntReptObj.qty = cntReptObj.qty + 1
                'log details
                detailsReptObj = New CDataCheckDetailsReportingObj
                detailsReptObj.object_name = MConstants.GLB_MSTR_CTRL.project_controller.project.project_list_str
                detailsReptObj.object_type = cntReptObj.label
                detailsReptObj.error_desc = checkProj.Value
                detail_error_list.Add(detailsReptObj)
            End If

            'project labor rate
            checkProj = MTabNavigationStory.conf_labor_rate_check(MConstants.GLB_MSTR_CTRL)
            If Not checkProj.Key Then
                If Not total_per_bo_type_list.ContainsKey("Project") Then
                    cntReptObj = New CDataCheckCountReportingObj
                    cntReptObj.label = "Project Config"
                    total_per_bo_type_list.Add("Project", cntReptObj)
                Else
                    cntReptObj = total_per_bo_type_list("Project")
                End If
                cntReptObj.qty = cntReptObj.qty + 1
                'log details
                detailsReptObj = New CDataCheckDetailsReportingObj
                detailsReptObj.object_name = MConstants.GLB_MSTR_CTRL.project_controller.project.project_list_str
                detailsReptObj.object_type = cntReptObj.label
                detailsReptObj.error_desc = checkProj.Value
                detail_error_list.Add(detailsReptObj)
            End If

            'exchange rates
            For Each exgR As CExchangeRate In MConstants.GLB_MSTR_CTRL.billing_cond_controller._exchangeRateBList.DataSource
                If exgR.calc_is_in_error_state AndAlso exgR.isEditable.Key Then
                    If Not total_per_bo_type_list.ContainsKey("CExchangeRate") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "Exchange Rate"
                        total_per_bo_type_list.Add("CExchangeRate", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CExchangeRate")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = exgR.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = exgR.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'markup
            For Each markup As CMatMarkup In MConstants.GLB_MSTR_CTRL.billing_cond_controller._matMarkupBList.DataSource
                If markup.calc_is_in_error_state AndAlso markup.isEditable.Key Then
                    If Not total_per_bo_type_list.ContainsKey("CMatMarkup") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "Material Markup"
                        total_per_bo_type_list.Add("CMatMarkup", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CMatMarkup")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = markup.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = markup.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'freight
            For Each freight As CMatFreight In MConstants.GLB_MSTR_CTRL.billing_cond_controller.freightList.DataSource
                If freight.calc_is_in_error_state AndAlso freight.isEditable.Key Then
                    If Not total_per_bo_type_list.ContainsKey("CMatFreight") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "Material Freight"
                        total_per_bo_type_list.Add("CMatFreight", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CMatFreight")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = freight.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = freight.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'eco T
            For Each ecoT As CSurcharge In MConstants.GLB_MSTR_CTRL.billing_cond_controller.ecoTaxList.DataSource
                If ecoT.calc_is_in_error_state AndAlso ecoT.isEditable.Key Then
                    If Not total_per_bo_type_list.ContainsKey("EcoTax") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "Eco Tax"
                        total_per_bo_type_list.Add("EcoTax", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("EcoTax")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = ecoT.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = ecoT.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'GUM
            For Each gum As CSurcharge In MConstants.GLB_MSTR_CTRL.billing_cond_controller.GUMList.DataSource
                If gum.calc_is_in_error_state AndAlso gum.isEditable.Key Then
                    If Not total_per_bo_type_list.ContainsKey("GUM") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "GUM"
                        total_per_bo_type_list.Add("GUM", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("GUM")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = gum.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = gum.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'Down Payment
            For Each downP As ObjectView(Of CDownPayment) In MConstants.GLB_MSTR_CTRL.billing_cond_controller.dowmPaymentList
                If downP.Object.calc_is_in_error_state Then
                    If Not total_per_bo_type_list.ContainsKey("CDownPayment") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "Down Payment"
                        total_per_bo_type_list.Add("CDownPayment", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CDownPayment")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = downP.Object.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = downP.Object.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'Discount
            For Each disc As ObjectView(Of CDiscount) In MConstants.GLB_MSTR_CTRL.billing_cond_controller.discountList
                If disc.Object.calc_is_in_error_state Then
                    If Not total_per_bo_type_list.ContainsKey("CDiscount") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "Discount"
                        total_per_bo_type_list.Add("CDiscount", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CDiscount")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = disc.Object.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = disc.Object.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'NTE
            For Each nte As ObjectView(Of CNotToExceed) In MConstants.GLB_MSTR_CTRL.billing_cond_controller.nteList
                If nte.Object.calc_is_in_error_state Then
                    If Not total_per_bo_type_list.ContainsKey("CNotToExceed") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "NTE"
                        total_per_bo_type_list.Add("CNotToExceed", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CNotToExceed")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = nte.Object.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = nte.Object.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'Labor Rate
            For Each labrate As ObjectView(Of CLaborRate) In MConstants.GLB_MSTR_CTRL.lab_rate_controller._std_labor_rate_list
                If labrate.Object.calc_is_in_error_state AndAlso labrate.Object.isEditable.Key Then
                    If Not total_per_bo_type_list.ContainsKey("CLaborRate") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "Labor Rate"
                        total_per_bo_type_list.Add("CLaborRate", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CLaborRate")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = labrate.Object.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = labrate.Object.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'Task Rate
            For Each taskRate As ObjectView(Of CTaskRate) In MConstants.GLB_MSTR_CTRL.lab_rate_controller._task_rate_list
                If taskRate.Object.calc_is_in_error_state AndAlso taskRate.Object.isEditable.Key Then
                    If Not total_per_bo_type_list.ContainsKey("CTaskRate") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "Task Rate"
                        total_per_bo_type_list.Add("CTaskRate", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CTaskRate")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = taskRate.Object.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = taskRate.Object.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'Quote
            For Each quote As ObjectView(Of CSalesQuote) In MConstants.GLB_MSTR_CTRL.ini_scope_controller.quote_list
                If quote.Object.calc_is_in_error_state Then
                    If Not total_per_bo_type_list.ContainsKey("CSalesQuote") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "Initial Quote"
                        total_per_bo_type_list.Add("CSalesQuote", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CSalesQuote")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = quote.Object.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = quote.Object.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'Sales Quote
            For Each quoteE As ObjectView(Of CSalesQuoteEntry) In MConstants.GLB_MSTR_CTRL.ini_scope_controller.quote_entries_list
                If quoteE.Object.calc_is_in_error_state Then
                    If Not total_per_bo_type_list.ContainsKey("CSalesQuoteEntry") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "Pricing Summary"
                        total_per_bo_type_list.Add("CSalesQuoteEntry", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CSalesQuoteEntry")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = quoteE.Object.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = quoteE.Object.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'Sales Quote E
            For Each quoteE As ObjectView(Of CSalesQuoteEntry) In MConstants.GLB_MSTR_CTRL.ini_scope_controller.quote_entries_list
                If quoteE.Object.calc_is_in_error_state Then
                    If Not total_per_bo_type_list.ContainsKey("CSalesQuoteEntry") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "Pricing Summary"
                        total_per_bo_type_list.Add("CSalesQuoteEntry", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CSalesQuoteEntry")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = quoteE.Object.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = quoteE.Object.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'Activity Labor Material
            For Each act As ObjectView(Of CScopeActivity) In MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList
                CGridViewRowValidationHandler.validateRowObj(act.Object, CScopeActivityRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ADD_SCOP_ACT_VIEW_DISPLAY))
                If act.Object.calc_is_in_error_state Then
                    If Not total_per_bo_type_list.ContainsKey("CScopeActivity") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "Activity"
                        total_per_bo_type_list.Add("CScopeActivity", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CScopeActivity")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = act.Object.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = act.Object.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
                'labor
                For Each soldH As CSoldHours In act.Object.sold_hour_list
                    If IsNothing(soldH.init_quote_entry_obj) Then
                        CGridViewRowValidationHandler.validateRowObj(soldH, CSoldHoursAddRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ADD_HOURS_VIEW_DISPLAY))
                    Else
                        CGridViewRowValidationHandler.validateRowObj(soldH, CSoldHoursInitRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_INIT_HOURS_VIEW_DISPLAY))
                    End If

                    If soldH.calc_is_in_error_state Then
                        If Not total_per_bo_type_list.ContainsKey("CSoldHours") Then
                            cntReptObj = New CDataCheckCountReportingObj
                            cntReptObj.label = "Activity Labor"
                            total_per_bo_type_list.Add("CSoldHours", cntReptObj)
                        Else
                            cntReptObj = total_per_bo_type_list("CSoldHours")
                        End If
                        cntReptObj.qty = cntReptObj.qty + 1
                        'log details
                        detailsReptObj = New CDataCheckDetailsReportingObj
                        detailsReptObj.object_name = act.Object.tostring & " " & soldH.tostring
                        detailsReptObj.object_type = cntReptObj.label
                        detailsReptObj.error_desc = soldH.calc_error_text
                        detail_error_list.Add(detailsReptObj)
                    End If
                Next
                'material
                For Each material As CMaterial In act.Object.material_list
                    If IsNothing(material.init_quote_entry_obj) Then
                        CGridViewRowValidationHandler.validateRowObj(material, CMaterialAddRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_MAT_EDIT_VIEW_DISPLAY))
                    Else
                        CGridViewRowValidationHandler.validateRowObj(material, CMaterialInitRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_MAT_EDIT_VIEW_DISPLAY))
                    End If
                    If material.calc_is_in_error_state Then
                        If Not total_per_bo_type_list.ContainsKey("CMaterial") Then
                            cntReptObj = New CDataCheckCountReportingObj
                            cntReptObj.label = "Activity Material"
                            total_per_bo_type_list.Add("CMaterial", cntReptObj)
                        Else
                            cntReptObj = total_per_bo_type_list("CMaterial")
                        End If
                        cntReptObj.qty = cntReptObj.qty + 1
                        'log details
                        detailsReptObj = New CDataCheckDetailsReportingObj
                        detailsReptObj.object_name = act.Object.tostring & " " & material.tostring
                        detailsReptObj.object_type = cntReptObj.label
                        detailsReptObj.error_desc = material.calc_error_text
                        detail_error_list.Add(detailsReptObj)
                    End If
                Next
            Next

            'AWQ
            For Each awq As ObjectView(Of CAWQuotation) In MConstants.GLB_MSTR_CTRL.awq_controller.awq_list
                CGridViewRowValidationHandler.validateRowObj(awq.Object, CAWQuotationRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_AWQ_VIEW_DISPLAY))
                If awq.Object.calc_is_in_error_state Then
                    If Not total_per_bo_type_list.ContainsKey("CAWQuotation") Then
                        cntReptObj = New CDataCheckCountReportingObj
                        cntReptObj.label = "AWQ"
                        total_per_bo_type_list.Add("CAWQuotation", cntReptObj)
                    Else
                        cntReptObj = total_per_bo_type_list("CAWQuotation")
                    End If
                    cntReptObj.qty = cntReptObj.qty + 1
                    'log details
                    detailsReptObj = New CDataCheckDetailsReportingObj
                    detailsReptObj.object_name = awq.Object.tostring
                    detailsReptObj.object_type = cntReptObj.label
                    detailsReptObj.error_desc = awq.Object.calc_error_text
                    detail_error_list.Add(detailsReptObj)
                End If
            Next

            'Labor Ctrl
            If _is_labor_ctrl_included Then

                Dim initLoaded As Boolean = True
                If Not MConstants.GLB_MSTR_CTRL.labor_controlling_controller.LABOR_CONTROL_VIEW_LOADABLE Then
                    MConstants.GLB_MSTR_CTRL.labor_controlling_controller.loadControllingModule()
                    initLoaded = False
                End If

                'cc transfer 
                For Each ccTransf As ObjectView(Of CCCTransfer) In MConstants.GLB_MSTR_CTRL.labor_controlling_controller._ccTransferListView
                    CGridViewRowValidationHandler.validateRowObj(ccTransf.Object, CCCTransferRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_CTRL_CONF_TRANSF_CC_VIEW_DISPLAY))
                    If ccTransf.Object.calc_is_in_error_state Then
                        If Not total_per_bo_type_list.ContainsKey("CCCTransfer") Then
                            cntReptObj = New CDataCheckCountReportingObj
                            cntReptObj.label = "Cost Center Transfer"
                            total_per_bo_type_list.Add("CCCTransfer", cntReptObj)
                        Else
                            cntReptObj = total_per_bo_type_list("CCCTransfer")
                        End If
                        cntReptObj.qty = cntReptObj.qty + 1
                        'log details
                        detailsReptObj = New CDataCheckDetailsReportingObj
                        detailsReptObj.object_name = ccTransf.Object.ToString
                        detailsReptObj.object_type = cntReptObj.label
                        detailsReptObj.error_desc = ccTransf.Object.calc_error_text
                        detail_error_list.Add(detailsReptObj)
                    End If
                Next

                For Each node As ObjectView(Of CLaborControlMainViewObj) In MConstants.GLB_MSTR_CTRL.labor_controlling_controller._laborControlCostNodeList
                    For Each ctrl_obj As CLaborControlMainViewObj In node.Object.get_structure_children
                        'get real controlling objects
                        If ctrl_obj.is_virtual_cost_node Then
                            Continue For
                        End If

                        'hours transfer
                        For Each hourTr As CHoursTransfer In ctrl_obj.hoursTransfert
                            If hourTr.hours_type = MConstants.CONST_CONF_CTRL_HRS_TRANSF_TYPE_ACTUAL Then
                                CGridViewRowValidationHandler.validateRowObj(hourTr, CActualTransferRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_CTRL_ACTUAL_TO_VIEW))
                            Else
                                CGridViewRowValidationHandler.validateRowObj(hourTr, CBudgetTransferRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_CTRL_BUDGET_TO_VIEW))
                            End If
                            If hourTr.calc_is_in_error_state Then
                                If Not total_per_bo_type_list.ContainsKey("CHoursTransfer") Then
                                    cntReptObj = New CDataCheckCountReportingObj
                                    cntReptObj.label = "Hours Transfer"
                                    total_per_bo_type_list.Add("CHoursTransfer", cntReptObj)
                                Else
                                    cntReptObj = total_per_bo_type_list("CHoursTransfer")
                                End If
                                cntReptObj.qty = cntReptObj.qty + 1
                                'log details
                                detailsReptObj = New CDataCheckDetailsReportingObj
                                detailsReptObj.object_name = hourTr.tostring
                                detailsReptObj.object_type = cntReptObj.label
                                detailsReptObj.error_desc = hourTr.calc_error_text
                                detail_error_list.Add(detailsReptObj)
                            End If
                        Next

                        'budget deviations
                        For Each devJust As CDevJust In ctrl_obj.deviationJustification
                            If devJust.type = MConstants.CONST_CONF_CTRL_JUST_OVERSHOOT Then
                                CGridViewRowValidationHandler.validateRowObj(devJust, COverhShootJustRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_CTRL_OVERSHOT_VIEW))
                            Else
                                CGridViewRowValidationHandler.validateRowObj(devJust, CGainJustRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_CTRL_GAIN_VIEW))
                            End If
                            If devJust.calc_is_in_error_state Then
                                If Not total_per_bo_type_list.ContainsKey("CDevJust") Then
                                    cntReptObj = New CDataCheckCountReportingObj
                                    cntReptObj.label = "Deviation Justification"
                                    total_per_bo_type_list.Add("CDevJust", cntReptObj)
                                Else
                                    cntReptObj = total_per_bo_type_list("CDevJust")
                                End If
                                cntReptObj.qty = cntReptObj.qty + 1
                                'log details
                                detailsReptObj = New CDataCheckDetailsReportingObj
                                detailsReptObj.object_name = devJust.tostring
                                detailsReptObj.object_type = cntReptObj.label
                                detailsReptObj.error_desc = devJust.calc_error_text
                                detail_error_list.Add(detailsReptObj)
                            End If
                        Next
                    Next
                Next

                If Not initLoaded Then
                    MConstants.GLB_MSTR_CTRL.labor_controlling_controller.unload_labor_controlling()
                End If
            End If

        Catch ex As Exception
            Throw New Exception("An error occured while loading error state data" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Sub loadDataCheckSummary()
        Try
            Dim dataChk_sum_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_DATA_CHK_SUMM_TAB_NAME).value)

            Dim value As Object
            'fill summary values
            For Each confO As CReportConfObject In dataCheckSummaryConf
                If confO.item_object = "CReportingDataConsistency" Then
                    value = CallByName(Me, confO.item_property, CallType.Get)
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    dataChk_sum_ws.Cells(confO.item_row, confO.item_column) = value
                Else
                    Throw New Exception("Summary Report, conf object type not found " & confO.item_object)
                End If
            Next

            'act list
            Dim reportArray(total_per_bo_type_list.Count - 1, dataCheckCountListConf.Count - 1) As Object
            Dim i As Integer = 0
            'build array
            For Each entry As CDataCheckCountReportingObj In total_per_bo_type_list.Values
                For Each confO As CReportConfObject In dataCheckCountListConf
                    If confO.item_object = "CDataCheckCountReportingObj" Then
                        value = CallByName(entry, confO.item_property, CallType.Get)
                    Else
                        Throw New Exception("Data Check Report, conf object type not found " & confO.item_object)
                    End If
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    reportArray(i, confO.item_column - 1) = value
                Next
                i = i + 1
            Next
            'insert
            Dim xlrg As Microsoft.Office.Interop.Excel.Range
            Dim xlrg_start As Microsoft.Office.Interop.Excel.Range
            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(dataCheckCountListConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange
            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & dataCheckCountListConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try
            'startrow + 1 to insert from the line below to propagate the good formating
            xlrg = CType(dataChk_sum_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)

            'reset range as lines have been added
            xlrg = CType(dataChk_sum_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            xlrg = dataChk_sum_ws.Range(xlrg, xlrg.Offset(total_per_bo_type_list.Count - 1, dataCheckCountListConf.Count - 1))
            xlrg.Value = reportArray
        Catch ex As Exception
            Throw New Exception("An error occured while generating Summary view of the report" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Sub loadDataCheckDetailsData()
        Try
            Dim dataChck_details_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_DATA_CHK_DETAILS_TAB_NAME).value)

            Dim value As Object

            'act list
            Dim reportArray(detail_error_list.Count - 1, dataCheckDetailsListConf.Count - 1) As Object
            Dim i As Integer = 0
            'build array
            For Each entry As CDataCheckDetailsReportingObj In detail_error_list
                For Each confO As CReportConfObject In dataCheckDetailsListConf
                    If confO.item_object = "CDataCheckDetailsReportingObj" Then
                        value = CallByName(entry, confO.item_property, CallType.Get)
                    Else
                        Throw New Exception("Data Check Report, conf object type not found " & confO.item_object)
                    End If
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    reportArray(i, confO.item_column - 1) = value
                Next
                i = i + 1
            Next
            'insert
            Dim xlrg As Microsoft.Office.Interop.Excel.Range
            Dim xlrg_start As Microsoft.Office.Interop.Excel.Range
            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(dataCheckDetailsListConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange
            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & dataCheckDetailsListConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try
            'startrow + 1 to insert from the line below to propagate the good formating
            xlrg = CType(dataChck_details_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)

            'reset range as lines have been added
            xlrg = CType(dataChck_details_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            xlrg = dataChck_details_ws.Range(xlrg, xlrg.Offset(detail_error_list.Count - 1, dataCheckDetailsListConf.Count - 1))
            xlrg.Value = reportArray

        Catch ex As Exception
            Throw New Exception("An error occured while generating details error list" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public ReadOnly Property Aircraft() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.ac_reg & "(" & MConstants.GLB_MSTR_CTRL.project_controller.project.ac_master & ")"
        End Get
    End Property

    Public ReadOnly Property Project() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.project_list_str
        End Get
    End Property

    Public ReadOnly Property ProjectDescription() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.project_desc
        End Get
    End Property

    Public ReadOnly Property getDate() As Date
        Get
            Return DateTime.Now
        End Get
    End Property

    Public ReadOnly Property AWQSpecialist() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.awq_specialist
        End Get
    End Property


    Public ReadOnly Property getProjectStartDate() As Date
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.input_date
        End Get
    End Property

    Public ReadOnly Property getProjectFinishDate() As Date
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.fcst_finish_date
        End Get
    End Property

    Public ReadOnly Property getLatestActualBookingDate() As Date
        Get
            Return MConstants.GLB_MSTR_CTRL.labor_controlling_controller.latest_actual_booking
        End Get
    End Property

    Public ReadOnly Property CustomerCompany() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.customer
        End Get
    End Property

    Public ReadOnly Property CustomerName() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.customer_rep_name
        End Get
    End Property
End Class
