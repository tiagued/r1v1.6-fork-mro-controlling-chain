﻿
Public Class CTaskRateRowValidator
    Public Shared instance As CTaskRateRowValidator
    Public Shared Function getInstance() As CTaskRateRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CTaskRateRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(taskRate As CTaskRate)
        taskRate.calc_is_in_error_state = False
    End Sub

    Public Function value(taskRate As CTaskRate) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If taskRate.is_editable Then
            If taskRate.value <= 0 Then
                err = "Task Rate Value shall be between greather than 0"
                res = False
                taskRate.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function curr(taskRate As CTaskRate) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If taskRate.is_editable Then
            If String.IsNullOrWhiteSpace(taskRate.curr) OrElse taskRate.curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value < 0 Then
                err = "Task Rate Currecny Cannot be Empty"
                res = False
                taskRate.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function
End Class
