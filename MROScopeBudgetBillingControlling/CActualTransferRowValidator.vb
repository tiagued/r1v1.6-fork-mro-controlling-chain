﻿Public Class CActualTransferRowValidator
    Public Shared instance As CActualTransferRowValidator
    Public Shared Function getInstance() As CActualTransferRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CActualTransferRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(tObj As CHoursTransfer)
        tObj.calc_is_in_error_state = False
    End Sub

    'validate hour transfer
    Public Function qty(tObj As CHoursTransfer) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If tObj.qty <= 0 Then
            err = "The transferred quantity must be greather than 0"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'validate to act
    Public Function to_act_id(tObj As CHoursTransfer) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If tObj.to_act_id <= 0 Then
            err = "No activity is selected to transfer hours"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'validate to cc
    Public Function to_cc(tObj As CHoursTransfer) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(tObj.to_cc) OrElse tObj.to_cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "No Cost Center is selected to transfer hours"
            res = False
        ElseIf tObj.from_act_id = tObj.to_act_id AndAlso tObj.to_cc = tObj.from_cc Then
            err = "Self (same activity, same cc) transfer is not allowed"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'qty available
    Public Function actual_available_to_transfer(tObj As CHoursTransfer) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If tObj.actual_available_to_transfer < 0 OrElse tObj.calc_is_ignored Then
            err = "Cannot transfer more than the available quantity (Self Actual - Already Transfered Actual)"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

End Class
