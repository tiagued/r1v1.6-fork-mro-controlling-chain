﻿Imports System.ComponentModel
Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CBillingConditionDelegate
    Public _exchangeRateBList As BindingListView(Of CExchangeRate)
    Public _tagk_list As Dictionary(Of String, Double)
    Public _matMarkupBList As BindingListView(Of CMatMarkup)
    Public _sortedMarkupList As List(Of CMatMarkup)
    Public _freightBList As BindingListView(Of CMatFreight)
    Public _ecoTaxBList As BindingListView(Of CSurcharge)
    Public _gumBList As BindingListView(Of CSurcharge)
    Public _discountList As BindingListView(Of CDiscount)
    Public _downPaymentList As BindingListView(Of CDownPayment)
    Public _nteList As BindingListView(Of CNotToExceed)
    'view use to easly filter for dictionnary list used for combo box list on act view
    Private _discountList_filtered_act As BindingListView(Of CDiscount)
    Private _nteList_filtered_act As BindingListView(Of CNotToExceed)
    'dictionray to create nte combo box list on act view
    Public _nteList_lov_act As BindingSource
    Public _nteList_lov_mat_po As BindingSource
    Public _nteList_lov_lab_cc As BindingSource

    'dictionray to create discount combo box list on act view
    Public _discountList_lov_act As BindingSource
    Public _discountList_lov_mat_po As BindingSource
    Public _discountList_lov_lab_cc As BindingSource
    Public _project_discount_list As List(Of CDiscount)

    Public mctrl As CMasterController

    Public Sub New(mc As CMasterController)
        mctrl = mc
    End Sub


    'get full lists
    Public ReadOnly Property discountList() As BindingListView(Of CDiscount)
        Get
            Return _discountList
        End Get
    End Property
    Public ReadOnly Property dowmPaymentList() As BindingListView(Of CDownPayment)
        Get
            Return _downPaymentList
        End Get
    End Property
    Public ReadOnly Property nteList() As BindingListView(Of CNotToExceed)
        Get
            Return _nteList
        End Get
    End Property
    Public ReadOnly Property freightList() As BindingListView(Of CMatFreight)
        Get
            Return _freightBList
        End Get
    End Property
    Public ReadOnly Property ecoTaxList() As BindingListView(Of CSurcharge)
        Get
            Return _ecoTaxBList
        End Get
    End Property
    Public ReadOnly Property GUMList() As BindingListView(Of CSurcharge)
        Get
            Return _gumBList
        End Get
    End Property
    'read data from exchange rate DB
    Public Sub model_read_BillingConditionDB()
        Try
            model_read_ExcahngeRateDB()
            model_read_MaterialMarkupDB()
            model_read_MaterialFreightDB()
            model_read_SurchargeDB()
            model_read_DownpaymentDB()
            model_read_DiscountDB()
            model_read_NTEDB()
        Catch ex As Exception
            Throw New Exception("Error occured when reading exchange rate data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform queries on billing conditions tables"
    End Sub

    'read data from exchange rate DB
    Public Sub model_read_ExcahngeRateDB()
        Dim listO As List(Of Object)

        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to exhange rate table..."
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_EXCHG_RATE_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_EXCHG_RATE_DB_READ), True)
            _exchangeRateBList = New BindingListView(Of CExchangeRate)(listO)
            MConstants.GLOB_EXCHG_RATE_LOADED = True
            _tagk_list = New Dictionary(Of String, Double)
        Catch ex As Exception
            Throw New Exception("Error occured when reading exchange rate data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to exchange rate table"
    End Sub

    'read data from exchange rate DB
    Public Sub model_read_MaterialMarkupDB()
        Dim listO As List(Of Object)
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to material markup table..."
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_MATERIAL_MKP_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_MATERIAL_MKP_DB_READ), True)
            MConstants.GLOB_MAT_MKP_LOADED = True
            _matMarkupBList = New BindingListView(Of CMatMarkup)(listO)

        Catch ex As Exception
            Throw New Exception("Error occured when reading material markup data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to material markup table"
    End Sub

    'read data from freight rate DB
    Public Sub model_read_MaterialFreightDB()
        Dim listO As List(Of Object)
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to freight table..."
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_FREIGHT_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_FREIGHT_DB_READ), True)
            MConstants.GLOB_MAT_FREIGHT_LOADED = True
            _freightBList = New BindingListView(Of CMatFreight)(listO)

        Catch ex As Exception
            Throw New Exception("Error occured when reading freight data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to freight table"
    End Sub

    'get cmatfrieght reference from ID
    Public Function getFreightObject(_id As String) As CMatFreight

        For Each freight As CMatFreight In _freightBList.DataSource
            If freight.id_str = _id Then
                Return freight
            End If
        Next
        MGeneralFuntionsViewControl.displayMessage("Freight Not found", "freight object with freigt id " & _id & " does not exist", MsgBoxStyle.Critical)
        Return Nothing
    End Function

    'read data from surcharge  DB
    Public Sub model_read_SurchargeDB()
        Dim listO As List(Of Object)
        Dim lisOGUM As New List(Of CSurcharge)
        Dim lisOECO As New List(Of CSurcharge)
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to surcharge table..."
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_SURCHARGE_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_SURCHARGE_DB_READ), True)
            For Each surch As CSurcharge In listO
                If surch.surcharge_type = MConstants.constantConf(MConstants.CONST_CONF_SURCHARGE_TYPE_GUM).value Then
                    lisOGUM.Add(surch)
                ElseIf surch.surcharge_type = MConstants.constantConf(MConstants.CONST_CONF_SURCHARGE_TYPE_ECO).value Then
                    lisOECO.Add(surch)
                End If
            Next
            MConstants.GLOB_SURCHARGE_LOADED = True
            _ecoTaxBList = New BindingListView(Of CSurcharge)(lisOECO)
            _gumBList = New BindingListView(Of CSurcharge)(lisOGUM)
        Catch ex As Exception
            Throw New Exception("Error occured when reading surcharge data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to surcharge table"
    End Sub

    'read data from doenpayment  DB
    Public Sub model_read_DownpaymentDB()
        Dim listO As List(Of Object)
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to downpayment table..."
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_DOWNPAYMENT_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_DOWNPAYMENT_DB_READ), True)
            _downPaymentList = New BindingListView(Of CDownPayment)(listO)

            MConstants.GLOB_DOWNPAYMENT_LOADED = True
        Catch ex As Exception
            Throw New Exception("Error occured when reading downpayment data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to downpayment table"
    End Sub

    'read data from discount  DB
    Public Sub model_read_DiscountDB()
        Dim listO As List(Of Object)
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to discount table..."
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_DISCOUNT_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_DISCOUNT_DB_READ), True)
            _discountList = New BindingListView(Of CDiscount)(listO)
            'bind discount view applicable to act with the main view and add a handler for refresh
            _discountList_filtered_act = New BindingListView(Of CDiscount)(listO)
            'one instance of lov to keep auto refresh of views
            _discountList_lov_act = New BindingSource
            _discountList_lov_mat_po = New BindingSource
            _discountList_lov_lab_cc = New BindingSource
            MConstants.GLOB_DISCOUNT_LOADED = True
            'first run
            refreshDiscountDependantViews()
        Catch ex As Exception
            Throw New Exception("Error occured when reading discount data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to discount table"
    End Sub
    'get discount reference from ID
    Public Function getDiscountObject(id As Long) As CDiscount

        For Each disc As CDiscount In _discountList.DataSource
            If disc.id = id Then
                Return disc
            End If
        Next
        MGeneralFuntionsViewControl.displayMessage("Discount not found", "discount object with ID " & id & " does not exist", MsgBoxStyle.Critical)
        Return Nothing
    End Function
    'filter discount data applicable for activity
    Public Function filterActDiscLovDelegate(ByVal disc As CDiscount)
        Return Not disc.system_status = MConstants.CONST_SYS_ROW_DELETED And disc.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_DISC_SCOPE_APP_ACT_KEY).value And disc.isRecordable
    End Function
    'filter discount data applicable for material PO
    Public Function filterMatPODiscLovDelegate(ByVal disc As CDiscount)
        Return Not disc.system_status = MConstants.CONST_SYS_ROW_DELETED And disc.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_DISC_SCOPE_APP_MAT_PO_KEY).value And disc.isRecordable
    End Function
    'filter discount data applicable for labor CC
    Public Function filterLabCCDiscLovDelegate(ByVal disc As CDiscount)
        Return Not disc.system_status = MConstants.CONST_SYS_ROW_DELETED And disc.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_DISC_SCOPE_APP_LAB_CC_KEY).value And disc.isRecordable
    End Function
    'filter discount data applicable to Project
    Public Function filterProjectDiscLovDelegate(ByVal disc As CDiscount)
        Return Not disc.system_status = MConstants.CONST_SYS_ROW_DELETED And disc.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_DISC_SCOPE_APP_PROJ_KEY).value And disc.isRecordable
    End Function

    'refresh discount depending views
    Public Sub refreshDiscountDependantViews()
        Dim listO As List(Of CDiscount)
        'activity level
        _discountList_filtered_act.RemoveFilter()
        _discountList_filtered_act.ApplyFilter(AddressOf filterActDiscLovDelegate)
        listO = MGeneralFuntionsViewControl.getDiscoutFromListView(_discountList_filtered_act)
        listO.Add(MConstants.EMPTY_DISC)
        _discountList_lov_act.DataSource = listO
        _discountList_lov_act.ResetBindings(False)

        'material line level
        _discountList_filtered_act.RemoveFilter()
        _discountList_filtered_act.ApplyFilter(AddressOf filterMatPODiscLovDelegate)
        listO = MGeneralFuntionsViewControl.getDiscoutFromListView(_discountList_filtered_act)
        listO.Add(MConstants.EMPTY_DISC)
        _discountList_lov_mat_po.DataSource = listO
        _discountList_lov_mat_po.ResetBindings(False)

        'cost center labor level
        _discountList_filtered_act.RemoveFilter()
        _discountList_filtered_act.ApplyFilter(AddressOf filterLabCCDiscLovDelegate)
        listO = MGeneralFuntionsViewControl.getDiscoutFromListView(_discountList_filtered_act)
        listO.Add(MConstants.EMPTY_DISC)
        _discountList_lov_lab_cc.DataSource = listO
        _discountList_lov_lab_cc.ResetBindings(False)

        'project level
        _discountList_filtered_act.RemoveFilter()
        _discountList_filtered_act.ApplyFilter(AddressOf filterProjectDiscLovDelegate)
        _project_discount_list = MGeneralFuntionsViewControl.getDiscoutFromListView(_discountList_filtered_act)
    End Sub
    'read data from NTE  DB
    Public Sub model_read_NTEDB()
        Dim listO As List(Of Object)
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to nte table..."
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_NTE_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_NTE_DB_READ), True)
            _nteList = New BindingListView(Of CNotToExceed)(listO)

            'add event handler to refresh dependant views
            _nteList_filtered_act = New BindingListView(Of CNotToExceed)(listO)
            'create one instance of lovs list to keep auto refresh in views
            _nteList_lov_act = New BindingSource
            _nteList_lov_mat_po = New BindingSource
            _nteList_lov_lab_cc = New BindingSource

            MConstants.GLOB_NTE_LOADED = True
            'first run refresh
            refreshNTEDependantViews()
        Catch ex As Exception
            Throw New Exception("Error occured when reading nte data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to nte table"
    End Sub
    'get nte reference from ID
    Public Function getNtetObject(id As Long) As CNotToExceed

        For Each nte As CNotToExceed In _nteList.DataSource
            If nte.id = id Then
                Return nte
            End If
        Next
        MGeneralFuntionsViewControl.displayMessage("NTE not found", "nte object with ID " & id & " does not exist", MsgBoxStyle.Critical)
        Return Nothing
    End Function
    Public Function filterActNTEDelegate(ByVal nte As CNotToExceed)
        'delegate for filtering
        Return Not nte.system_status = MConstants.CONST_SYS_ROW_DELETED And nte.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_NTE_SCOPE_APP_ACT_KEY).value And nte.isRecordable
    End Function
    Public Function filterMatPONTEDelegate(ByVal nte As CNotToExceed)
        'delegate for filtering
        Return Not nte.system_status = MConstants.CONST_SYS_ROW_DELETED And nte.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_NTE_SCOPE_APP_MAT_PO_KEY).value And nte.isRecordable
    End Function
    Public Function filterLabCCNTEDelegate(ByVal nte As CNotToExceed)
        'delegate for filtering
        Return Not nte.system_status = MConstants.CONST_SYS_ROW_DELETED And nte.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_NTE_SCOPE_APP_LAB_CC_KEY).value And nte.isRecordable
    End Function
    'refresh nte depending views
    Public Sub refreshNTEDependantViews()
        Dim listO As List(Of CNotToExceed)
        'act level
        _nteList_filtered_act.RemoveFilter()
        _nteList_filtered_act.ApplyFilter(AddressOf filterActNTEDelegate)
        listO = MGeneralFuntionsViewControl.getNTEFromListView(_nteList_filtered_act)
        listO.Add(MConstants.EMPTY_NTE)
        _nteList_lov_act.DataSource = listO
        _nteList_lov_act.ResetBindings(False)

        'mat PO level
        _nteList_filtered_act.RemoveFilter()
        _nteList_filtered_act.ApplyFilter(AddressOf filterMatPONTEDelegate)
        listO = MGeneralFuntionsViewControl.getNTEFromListView(_nteList_filtered_act)
        listO.Add(MConstants.EMPTY_NTE)
        _nteList_lov_mat_po.DataSource = listO
        _nteList_lov_mat_po.ResetBindings(False)

        'lab cc level
        _nteList_filtered_act.RemoveFilter()
        _nteList_filtered_act.ApplyFilter(AddressOf filterLabCCNTEDelegate)
        listO = MGeneralFuntionsViewControl.getNTEFromListView(_nteList_filtered_act)
        listO.Add(MConstants.EMPTY_NTE)
        _nteList_lov_lab_cc.DataSource = listO
        _nteList_lov_lab_cc.ResetBindings(False)
    End Sub


    'bind the controls items in the billing config view with the relevant data object
    Public Sub bind_billing_view()
        mctrl.statusBar.status_strip_sub_label = "Bind billing conf data to view controls..."
        Try
            bind_billing_currency_view()
            bind_billing_mat_markup_view()
            bind_billing_freight_view()
            bind_billing_gum_view()
            bind_billing_ecotax_view()
            bind_billing_downpayment_view()
            bind_billing_discount_view()
            bind_billing_nte_view()
        Catch ex As Exception
            Throw New Exception("An error occured while loading the billing condition view" & Chr(10) & ex.Message, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing conf data to view controls"
    End Sub


    'bind the controls items in the billing currency config view with the relevant data object
    Public Sub bind_billing_currency_view()
        mctrl.statusBar.status_strip_sub_label = "Bind billing currency conf data to view controls..."
        Try
            'bind currencies selection
            Dim mapProj As CViewModelMapList
            Dim mapExcg As CViewModelMapList
            'map is the same as the project view
            mapProj = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_PROJ_VIEW_DISPLAY)
            mapExcg = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_EXCHG_RATE_VIEW_DISPLAY)
            'currency combo boxe in table layout
            For Each viewcontrol As Control In mctrl.mainForm.conf_bill_curr_layount.Controls
                If TypeName(viewcontrol) = "ComboBox" Then
                    Dim cb As ComboBox
                    cb = CType(viewcontrol, ComboBox)
                    'set list of value
                    MGeneralFuntionsViewControl.setComboBoxValue(cb, MConstants.listOfValueConf(mapProj.list(cb.Name).view_list_name))
                    'set selected items
                    cb.DataBindings.Add("SelectedValue", mctrl.project_controller.project, mapProj.list(cb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                    cb.DropDownStyle = ComboBoxStyle.DropDownList
                    MViewEventHandler.addDefaultComboBoxEventHandler(cb)
                ElseIf TypeName(viewcontrol) = "DateTimePicker" Then
                    Dim datePick As DateTimePicker
                    datePick = CType(viewcontrol, DateTimePicker)
                    datePick.Format = DateTimePickerFormat.Custom

                    datePick.DataBindings.Add("Value", mctrl.project_controller.project, mapProj.list(datePick.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    datePick.DataBindings.Add("CustomFormat", mctrl.project_controller.project, mapProj.list(datePick.Name & "/CustomFormat").class_field, False, DataSourceUpdateMode.Never)
                    datePick.DataBindings.Add("Checked", mctrl.project_controller.project, mapProj.list(datePick.Name & "/Checked").class_field, False, DataSourceUpdateMode.Never)
                    'use to force databinding on none visible controls
                    MGeneralFuntionsViewControl.CreateControl(datePick)
                End If
            Next

            'do not autogenerate columns
            mctrl.mainForm.conf_bill_exchg_rate_grid.AllowUserToAddRows = False
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.conf_bill_exchg_rate_grid)

            For Each map As CViewModelMap In mapExcg.list.Values
                Dim dgcol As New DataGridViewTextBoxColumn
                'column name is the property name of binded object
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = map.view_col
                dgcol.DataPropertyName = map.class_field
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic

                'only rate is editable
                If map.view_col_sys_name.ToLower = MConstants.CONST_CONF_BILL_EXCG_RATE_RATE.ToLower Then
                    dgcol.ReadOnly = False
                    dgcol.ValueType = GetType(Double)
                    'format five digits after decimal
                    dgcol.DefaultCellStyle.Format = "N5"
                    'if date
                ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_BILL_EXCG_RATE_DATE.ToLower Then
                    dgcol.ValueType = GetType(Date)
                    dgcol.ReadOnly = True
                    'format is the default application format
                    dgcol.DefaultCellStyle.Format = MConstants.constantConf(MConstants.CONST_CONF_DATE_FORMAT).value
                Else
                    dgcol.ReadOnly = True
                End If
                mctrl.mainForm.conf_bill_exchg_rate_grid.Columns.Add(dgcol)
            Next
            'bind datasource
            mctrl.mainForm.conf_bill_exchg_rate_grid.DataSource = _exchangeRateBList
            'handle event
            Dim handler = New CViewRefreshHandler()
            handler.datagridView = mctrl.mainForm.conf_bill_exchg_rate_grid
            handler.listView = _exchangeRateBList
            handler.flexibleHeigtControl = mctrl.mainForm.conf_bill_exchg_rate_grid_p
            handler.bindingListViewList.Add(mctrl.add_scope_controller.scopeList)
            handler.bindingListViewList.Add(mctrl.material_controller.matList)
            handler.delegateList.Add(AddressOf updateTagkDependantViews)
            CViewRefreshHandler.addEntry(Of CExchangeRate)(handler)
            handler.refreshHandlerViewsHeight()

            'row validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.conf_bill_exchg_rate_grid, CExchangeRateRowValidator.getInstance, mapExcg)

        Catch ex As Exception
            Throw New Exception("An error occured while loading billing currencies view" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing currency conf data to view controls"
    End Sub
    'update tagk dependant views delegate
    Public Sub updateTagkDependantViews()
        Try
            _tagk_list.Clear()
            For Each excg As CExchangeRate In _exchangeRateBList.DataSource
                _tagk_list.Add(excg.from_curr & MConstants.SEP_DEFAULT & excg.to_curr, excg.exchange_rate)
            Next
        Catch ex As Exception
            Throw New Exception("An error occured while generating tagk list " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub
    'get Tagk value
    Public Function getTagk(fromCurr As String, toCurr As String) As Double
        Try
            If fromCurr = toCurr Then
                Return 1
            End If
            Return _tagk_list(fromCurr & MConstants.SEP_DEFAULT & toCurr)
        Catch ex As Exception
            Throw New Exception("An error occured while reading tagk value from curr " & fromCurr & " to curr " & toCurr & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
    End Function

    'update rate date when the date picker value changes
    Public Sub updateRateDate(td As Date)

        For Each obj As CExchangeRate In _exchangeRateBList.DataSource
            obj.exchange_rate_date = td
        Next
    End Sub

    'bind the controls items in the billing material markup config view with the relevant data object
    Public Sub bind_billing_mat_markup_view()
        mctrl.statusBar.status_strip_sub_label = "Bind billing material markup conf data to view controls..."
        Try
            'bind currencies selection
            Dim mapProj As CViewModelMapList
            Dim mapMatMk As CViewModelMapList
            Dim cb As ComboBox
            'map is the same as the project view
            mapProj = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_PROJ_VIEW_DISPLAY)
            mapMatMk = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_MAT_MKP_VIEW_DISPLAY)
            'currency combo boxe in table layout
            For Each viewcontrol As Control In mctrl.mainForm.conf_bill_markup_layount.Controls
                If TypeName(viewcontrol) = "ComboBox" Then
                    cb = CType(viewcontrol, ComboBox)
                    'set list of value
                    'cb.DataSource = New BindingSource(mctrl.project_controller.project.confProjView.applicable_material_markup, Nothing)
                    cb.DataSource = mctrl.project_controller.project.confProjView.applicable_material_markup
                    cb.DisplayMember = "Value"
                    cb.ValueMember = "Key"
                    'set selected items
                    cb.DataBindings.Add("SelectedValue", mctrl.project_controller.project, mapProj.list(cb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                    cb.DropDownStyle = ComboBoxStyle.DropDownList
                    MViewEventHandler.addDefaultComboBoxEventHandler(cb)
                End If
            Next

            'markup datagridview
            mctrl.mainForm.conf_bill_mat_markup_grid.AllowUserToAddRows = False
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.conf_bill_mat_markup_grid)

            For Each map As CViewModelMap In mapMatMk.list.Values
                Dim dgcol As New DataGridViewTextBoxColumn
                'column name is the property name of binded object
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = map.view_col
                dgcol.DataPropertyName = map.class_field
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic

                'only rate is editable
                If map.view_col_sys_name.ToLower = MConstants.CONST_CONF_BILL_MAT_MKP_PRICE.ToLower Then
                    dgcol.ReadOnly = True
                    dgcol.ValueType = GetType(Double)
                    'format five digits after decimal
                    dgcol.DefaultCellStyle.Format = "N2"
                    dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    'if date
                ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_BILL_MAT_MKP_PERCENT.ToLower Then
                    dgcol.ValueType = GetType(Double)
                    dgcol.ReadOnly = False
                    'format is the default application format
                    dgcol.DefaultCellStyle.Format = "00.00%"
                    dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                Else
                    dgcol.ReadOnly = True
                End If
                mctrl.mainForm.conf_bill_mat_markup_grid.Columns.Add(dgcol)
            Next
            'bind datasource and apply filter
            mctrl.mainForm.conf_bill_mat_markup_grid.DataSource = _matMarkupBList
            'handle event
            Dim handler = New CViewRefreshHandler
            handler.datagridView = mctrl.mainForm.conf_bill_mat_markup_grid
            handler.listView = _matMarkupBList
            handler.flexibleHeigtControl = mctrl.mainForm.conf_bill_mat_markup_grid_p
            handler.bindingListViewList.Add(mctrl.add_scope_controller.scopeList)
            handler.bindingListViewList.Add(mctrl.material_controller.matList)
            CViewRefreshHandler.addEntry(Of CMatMarkup)(handler)
            handler.refreshHandlerViewsHeight()

            'row validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.conf_bill_mat_markup_grid, CMatMarkupRowValidator.getInstance, mapMatMk)

            markupValueChanged()

            'handle markup cap
            'bind checkboxes line and manage group enable or disable
            Dim chk As CheckBox
            Dim grpB As GroupBox
            chk = mctrl.mainForm.chk_b_conf_bill_markup_is_line_cap
            chk.DataBindings.Add("Checked", mctrl.project_controller.project, mapProj.list(chk.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            grpB = mctrl.mainForm.conf_bill_markup_grp_cap_l_grp
            grpB.DataBindings.Add("Enabled", mctrl.project_controller.project, mapProj.list(grpB.Name & "/Enabled").class_field, False, DataSourceUpdateMode.Never)
            'bind checkboxes project
            chk = mctrl.mainForm.chk_b_conf_bill_markup_is_proj_cap
            chk.DataBindings.Add("Checked", mctrl.project_controller.project, mapProj.list(chk.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            grpB = mctrl.mainForm.conf_bill_markup_grp_cap_p_grp
            grpB.DataBindings.Add("Enabled", mctrl.project_controller.project, mapProj.list(grpB.Name & "/Enabled").class_field, False, DataSourceUpdateMode.Never)


            'bind values
            Dim tbbd As Binding
            Dim tb As TextBox
            tb = mctrl.mainForm.cb_cnf_bill_markup_cap_line_value
            tbbd = tb.DataBindings.Add("Text", mctrl.project_controller.project, mapProj.list(tb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            tbbd.FormatInfo = MConstants.CULT_INFO
            tbbd.FormatString = "N2"
            tbbd.FormattingEnabled = True
            'bind cap values
            tb = mctrl.mainForm.cb_cnf_bill_markup_cap_proj_value
            tbbd = tb.DataBindings.Add("Text", mctrl.project_controller.project, mapProj.list(tb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            tbbd.FormatInfo = MConstants.CULT_INFO
            tbbd.FormatString = "N2"
            tbbd.FormattingEnabled = True
            'bind currencies
            cb = mctrl.mainForm.cb_cnf_bill_markup_cap_line_curr
            MGeneralFuntionsViewControl.setComboBoxValue(cb, MConstants.listOfValueConf(mapProj.list(cb.Name).view_list_name))
            cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cb.DropDownStyle = ComboBoxStyle.DropDownList
            cb.DataBindings.Add("SelectedValue", mctrl.project_controller.project, mapProj.list(cb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            MViewEventHandler.addDefaultComboBoxEventHandler(cb)
            'bind checkboxes project
            cb = mctrl.mainForm.cb_cnf_bill_markup_cap_proj_curr
            MGeneralFuntionsViewControl.setComboBoxValue(cb, MConstants.listOfValueConf(mapProj.list(cb.Name).view_list_name))
            cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cb.DropDownStyle = ComboBoxStyle.DropDownList
            cb.DataBindings.Add("SelectedValue", mctrl.project_controller.project, mapProj.list(cb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            MViewEventHandler.addDefaultComboBoxEventHandler(cb)

        Catch ex As Exception
            Throw New Exception("An error occured while loading billing material markup view" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing material markup conf data to view controls"
    End Sub
    'update markup view
    Public Sub markupValueChanged()
        If MConstants.GLOB_MAT_MKP_LOADED Then
            _matMarkupBList.ApplyFilter(AddressOf filterMarkupLabel)
            refreshProjectApplicableMarkupList()
        End If
    End Sub
    Private Function filterMarkupLabel(ByVal markup As CMatMarkup)
        'delegate for filtering
        If String.IsNullOrWhiteSpace(mctrl.project_controller.project.material_markup) Or mctrl.project_controller.project.material_markup = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            Dim bds As BindingSource
            bds = mctrl.project_controller.project.confProjView.applicable_material_markup.DataSource

            For Each kvp As KeyValuePair(Of String, String) In bds.List
                If markup.label = kvp.Key Then
                    Return True
                End If
            Next
            Return False
        Else
            Return markup.label = mctrl.project_controller.project.material_markup
        End If
    End Function
    'filte only on current project markups
    Private Function filterProjectApplicableMarkupLabel(ByVal markup As CMatMarkup)
        Return markup.label = mctrl.project_controller.project.material_markup
    End Function
    Public Sub refreshProjectApplicableMarkupList()
        Dim projAppMarkupView As New BindingListView(Of CMatMarkup)(_matMarkupBList.DataSource)
        projAppMarkupView.ApplyFilter(AddressOf filterProjectApplicableMarkupLabel)
        'APPLY SORT
        projAppMarkupView.ApplySort("price ASC")
        _sortedMarkupList = MGeneralFuntionsViewControl.getMarkupFromListView(projAppMarkupView)
    End Sub
    Public Function getMarkupRate(value As Double) As CMatMarkup
        Dim previousMarkup As New CMatMarkup
        For Each mark As CMatMarkup In _sortedMarkupList
            If value < mark.price Then
                Return previousMarkup
            End If
            previousMarkup = mark
        Next
        'highest markup
        Return previousMarkup
    End Function

    'bind the controls items in the billing material markup config view with the relevant data object
    Public Sub bind_billing_freight_view()
        mctrl.statusBar.status_strip_sub_label = "Bind billing freight conf data to view controls..."
        Try
            'bind currencies selection
            Dim mapProj As CViewModelMapList
            Dim mapFreight As CViewModelMapList
            Dim cb As ComboBox
            'map is the same as the project view
            mapProj = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_PROJ_VIEW_DISPLAY)
            mapFreight = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_FREIGHT_VIEW_DISPLAY)

            'freight datagridview
            mctrl.mainForm.conf_bill_mat_freight_grid.AllowUserToAddRows = False
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.conf_bill_mat_freight_grid)

            'loop on view columns
            For Each map As CViewModelMap In mapFreight.list.Values
                If map.view_col_sys_name.ToLower = MConstants.CONST_CONF_BILL_FREIGHT_CURR.ToLower Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "value"
                    dgcol.ValueMember = "key"
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    mctrl.mainForm.conf_bill_mat_freight_grid.Columns.Add(dgcol)
                Else
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'only value and currency are editable
                    If map.view_col_sys_name.ToLower = MConstants.CONST_CONF_BILL_FREIGHT_PRICE.ToLower Then
                        dgcol.ReadOnly = False
                        dgcol.ValueType = GetType(Double)
                        'format five digits after decimal
                        dgcol.DefaultCellStyle.Format = "N2"
                        dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    Else
                        dgcol.ReadOnly = True
                    End If
                    mctrl.mainForm.conf_bill_mat_freight_grid.Columns.Add(dgcol)
                End If
            Next
            'bind datasource and apply filter
            mctrl.mainForm.conf_bill_mat_freight_grid.DataSource = _freightBList
            'handle event
            Dim handler = New CViewRefreshHandler
            handler.datagridView = mctrl.mainForm.conf_bill_mat_freight_grid
            handler.listView = _freightBList
            handler.flexibleHeigtControl = mctrl.mainForm.conf_bill_mat_freight_grid_p
            handler.bindingListViewList.Add(mctrl.add_scope_controller.scopeList)
            handler.bindingListViewList.Add(mctrl.material_controller.matList)
            CViewRefreshHandler.addEntry(Of CMatFreight)(handler)
            handler.refreshHandlerViewsHeight()

            'row validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.conf_bill_mat_freight_grid, CMatFreightRowValidator.getInstance, mapFreight)

            'handle freight rate
            Dim chk As CheckBox
            Dim tb As TextBox
            Dim tbbd As Binding

            'handle freight cap
            'bind checkboxes line and manage group enable or disable
            tb = mctrl.mainForm.lbl_cnf_bill_freight_rate_val

            tbbd = tb.DataBindings.Add("Text", mctrl.project_controller.project, mapProj.list(tb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            tbbd.FormatInfo = MConstants.CULT_INFO
            tbbd.FormatString = "P2"
            tbbd.FormattingEnabled = True

            Dim grpB As GroupBox
            chk = mctrl.mainForm.chk_b_conf_bill_freight_is_line_cap
            chk.DataBindings.Add("Checked", mctrl.project_controller.project, mapProj.list(chk.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            grpB = mctrl.mainForm.conf_bill_freight_grp_cap_l_grp
            grpB.DataBindings.Add("Enabled", mctrl.project_controller.project, mapProj.list(grpB.Name & "/Enabled").class_field, False, DataSourceUpdateMode.Never)
            'bind checkboxes project
            chk = mctrl.mainForm.chk_b_conf_bill_freight_is_proj_cap
            chk.DataBindings.Add("Checked", mctrl.project_controller.project, mapProj.list(chk.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            grpB = mctrl.mainForm.conf_bill_freight_grp_cap_p_grp
            grpB.DataBindings.Add("Enabled", mctrl.project_controller.project, mapProj.list(grpB.Name & "/Enabled").class_field, False, DataSourceUpdateMode.Never)

            'bind values
            tb = mctrl.mainForm.cb_cnf_bill_freight_cap_line_value
            tbbd = tb.DataBindings.Add("Text", mctrl.project_controller.project, mapProj.list(tb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            tbbd.FormatInfo = MConstants.CULT_INFO
            tbbd.FormatString = "N2"
            tbbd.FormattingEnabled = True
            'bind cap values
            tb = mctrl.mainForm.cb_cnf_bill_freight_cap_proj_value
            tbbd = tb.DataBindings.Add("Text", mctrl.project_controller.project, mapProj.list(tb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            tbbd.FormatInfo = MConstants.CULT_INFO
            tbbd.FormatString = "N2"
            tbbd.FormattingEnabled = True
            'bind currencies
            cb = mctrl.mainForm.cb_cnf_bill_freight_cap_line_curr
            MGeneralFuntionsViewControl.setComboBoxValue(cb, MConstants.listOfValueConf(mapProj.list(cb.Name).view_list_name))
            cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cb.DropDownStyle = ComboBoxStyle.DropDownList
            cb.DataBindings.Add("SelectedValue", mctrl.project_controller.project, mapProj.list(cb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            MViewEventHandler.addDefaultComboBoxEventHandler(cb)
            'bind checkboxes project
            cb = mctrl.mainForm.cb_cnf_bill_freight_cap_proj_curr
            MGeneralFuntionsViewControl.setComboBoxValue(cb, MConstants.listOfValueConf(mapProj.list(cb.Name).view_list_name))
            cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cb.DropDownStyle = ComboBoxStyle.DropDownList
            cb.DataBindings.Add("SelectedValue", mctrl.project_controller.project, mapProj.list(cb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            MViewEventHandler.addDefaultComboBoxEventHandler(cb)

        Catch ex As Exception
            Throw New Exception("An error occured while loading billing freight view" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing freight conf data to view controls"
    End Sub


    'bind the controls items in the billing material markup config view with the relevant data object
    Public Sub bind_billing_gum_view()
        mctrl.statusBar.status_strip_sub_label = "Bind billing gum conf data to view controls..."
        Try
            'bind currencies selection
            Dim mapProj As CViewModelMapList
            Dim mapGUM As CViewModelMapList
            Dim cb As ComboBox
            'map is the same as the project view
            mapProj = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_PROJ_VIEW_DISPLAY)
            mapGUM = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_GUM_VIEW_DISPLAY)
            'currency combo boxe in table layout
            For Each viewcontrol As Control In mctrl.mainForm.conf_bill_surch_gum_layout.Controls
                If TypeName(viewcontrol) = "ComboBox" Then
                    cb = CType(viewcontrol, ComboBox)
                    MGeneralFuntionsViewControl.setComboBoxValue(cb, MConstants.listOfValueConf(mapProj.list(cb.Name).view_list_name))
                    cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                    cb.DropDownStyle = ComboBoxStyle.DropDownList
                    cb.DataBindings.Add("SelectedValue", mctrl.project_controller.project, mapProj.list(cb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    MViewEventHandler.addDefaultComboBoxEventHandler(cb)
                End If
            Next

            'gum datagridview
            mctrl.mainForm.conf_bill_surch_gum_grid.AllowUserToAddRows = False
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.conf_bill_surch_gum_grid)

            For Each map As CViewModelMap In mapGUM.list.Values
                If map.view_col_sys_name.ToLower = MConstants.CONST_CONF_BILL_SURCH_CURR.ToLower Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "value"
                    dgcol.ValueMember = "key"
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    mctrl.mainForm.conf_bill_surch_gum_grid.Columns.Add(dgcol)
                Else
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'only value and currency are editable
                    If map.view_col_sys_name.ToLower = MConstants.CONST_CONF_BILL_SURCH_CAP.ToLower Then
                        dgcol.ReadOnly = False
                        dgcol.ValueType = GetType(Double)
                        'format five digits after decimal
                        dgcol.DefaultCellStyle.Format = "N2"
                        dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_BILL_SURCH_RATE.ToLower Then
                        dgcol.ReadOnly = False
                        dgcol.ValueType = GetType(Double)
                        'format five digits after decimal
                        dgcol.DefaultCellStyle.Format = "00.00%"
                        dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    Else
                        dgcol.ReadOnly = True
                    End If
                    mctrl.mainForm.conf_bill_surch_gum_grid.Columns.Add(dgcol)
                End If
            Next

            'bind datasource and apply filter
            mctrl.mainForm.conf_bill_surch_gum_grid.DataSource = _gumBList
            filterGUMView()
            'handle event
            Dim handler = New CViewRefreshHandler
            handler.datagridView = mctrl.mainForm.conf_bill_surch_gum_grid
            handler.listView = _gumBList
            handler.flexibleHeigtControl = mctrl.mainForm.conf_bill_surch_gum_grid_p
            handler.bindingListViewList.Add(mctrl.add_scope_controller.scopeList)
            handler.bindingListViewList.Add(mctrl.material_controller.matList)
            CViewRefreshHandler.addEntry(Of CSurcharge)(handler)
            handler.refreshHandlerViewsHeight()

            'row validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.conf_bill_surch_gum_grid, CSurchargeRowValidator.getInstance, mapGUM)

        Catch ex As Exception
            Throw New Exception("An error occured while loading billing gum view" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing gum conf data to view controls"
    End Sub
    'update markup view
    Public Sub filterGUMView()
        _gumBList.ApplyFilter(AddressOf filterSelectedGUM)
        _gumBList.Refresh()
    End Sub
    Private Function filterSelectedGUM(ByVal surch As CSurcharge)
        'delegate for filtering
        If String.IsNullOrWhiteSpace(mctrl.project_controller.project.gum) Or mctrl.project_controller.project.gum = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            Return surch.surcharge_type = MConstants.constantConf(MConstants.CONST_CONF_SURCHARGE_TYPE_GUM).value
        Else
            Return surch.label = mctrl.project_controller.project.gum And surch.surcharge_type = MConstants.constantConf(MConstants.CONST_CONF_SURCHARGE_TYPE_GUM).value
        End If
    End Function

    'bind the controls items in the billing material markup config view with the relevant data object
    Public Sub bind_billing_ecotax_view()
        mctrl.statusBar.status_strip_sub_label = "Bind billing Eco tax conf data to view controls..."
        Try
            'bind currencies selection
            Dim mapProj As CViewModelMapList
            Dim mapEco As CViewModelMapList
            Dim cb As ComboBox
            'map is the same as the project view
            mapProj = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_PROJ_VIEW_DISPLAY)
            mapEco = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_ECOTAX_VIEW_DISPLAY)
            'currency combo boxe in table layout
            For Each viewcontrol As Control In mctrl.mainForm.conf_bill_surch_eco_layout.Controls
                If TypeName(viewcontrol) = "ComboBox" Then
                    cb = CType(viewcontrol, ComboBox)
                    MGeneralFuntionsViewControl.setComboBoxValue(cb, MConstants.listOfValueConf(mapProj.list(cb.Name).view_list_name))
                    cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                    cb.DropDownStyle = ComboBoxStyle.DropDownList
                    cb.DataBindings.Add("SelectedValue", mctrl.project_controller.project, mapProj.list(cb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    MViewEventHandler.addDefaultComboBoxEventHandler(cb)
                End If
            Next

            'eco datagridview
            mctrl.mainForm.conf_bill_surch_eco_grid.AllowUserToAddRows = False
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.conf_bill_surch_eco_grid)

            For Each map As CViewModelMap In mapEco.list.Values
                If map.view_col_sys_name.ToLower = MConstants.CONST_CONF_BILL_SURCH_CURR.ToLower Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "value"
                    dgcol.ValueMember = "key"
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    mctrl.mainForm.conf_bill_surch_eco_grid.Columns.Add(dgcol)
                Else
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'only value and currency are editable
                    If map.view_col_sys_name.ToLower = MConstants.CONST_CONF_BILL_SURCH_CAP.ToLower Then
                        dgcol.ReadOnly = False
                        dgcol.ValueType = GetType(Double)
                        'format five digits after decimal
                        dgcol.DefaultCellStyle.Format = "N2"
                        dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_BILL_SURCH_RATE.ToLower Then
                        dgcol.ReadOnly = False
                        dgcol.ValueType = GetType(Double)
                        'format five digits after decimal
                        dgcol.DefaultCellStyle.Format = "00.00%"
                        dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    Else
                        dgcol.ReadOnly = True
                    End If
                    mctrl.mainForm.conf_bill_surch_eco_grid.Columns.Add(dgcol)
                End If
            Next

            'bind datasource and apply filter
            mctrl.mainForm.conf_bill_surch_eco_grid.DataSource = _ecoTaxBList
            filterECOView()
            'handle event
            Dim handler = New CViewRefreshHandler
            handler.datagridView = mctrl.mainForm.conf_bill_surch_eco_grid
            handler.listView = _ecoTaxBList
            handler.flexibleHeigtControl = mctrl.mainForm.conf_bill_surch_eco_grid_p
            handler.bindingListViewList.Add(mctrl.add_scope_controller.scopeList)
            handler.bindingListViewList.Add(mctrl.material_controller.matList)
            CViewRefreshHandler.addEntry(Of CSurcharge)(handler)
            handler.refreshHandlerViewsHeight()

            'row validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.conf_bill_surch_eco_grid, CSurchargeRowValidator.getInstance, mapEco)

        Catch ex As Exception
            Throw New Exception("An error occured while loading billing eco Taax view" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing Eco Tax conf data to view controls"
    End Sub
    'update eco view
    Public Sub filterECOView()
        _ecoTaxBList.ApplyFilter(AddressOf filterSelectedECO)
        _ecoTaxBList.Refresh()
    End Sub
    Private Function filterSelectedECO(ByVal eco As CSurcharge)
        'delegate for filtering
        If String.IsNullOrWhiteSpace(mctrl.project_controller.project.eco_tax) Or mctrl.project_controller.project.eco_tax = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            Return eco.surcharge_type = MConstants.constantConf(MConstants.CONST_CONF_SURCHARGE_TYPE_ECO).value
        Else
            Return eco.label = mctrl.project_controller.project.eco_tax And eco.surcharge_type = MConstants.constantConf(MConstants.CONST_CONF_SURCHARGE_TYPE_ECO).value
        End If
    End Function



    'bind downpayment beiew
    Public Sub bind_billing_downpayment_view()
        mctrl.statusBar.status_strip_sub_label = "Bind billing Dowmpayment conf data to view controls..."
        Try
            Dim mapDownP As CViewModelMapList

            'get discount map view
            mapDownP = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_DOWNPAYMENT_VIEW_DISPLAY)

            'discount datagridview
            mctrl.mainForm.conf_bill_down_payment_grid.AllowUserToAddRows = True
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.conf_bill_down_payment_grid)


            For Each map As CViewModelMap In mapDownP.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapDownP.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "Double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_bill_down_payment_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_DATE).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                    Else
                        dgcol.DefaultCellStyle.Format = MConstants.constantConf(MConstants.CONST_CONF_DATE_FORMAT).value
                    End If
                    dgcol.ValueType = GetType(Date)
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    MConstants.GLB_MSTR_CTRL.mainForm.conf_bill_down_payment_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapDownP.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_DOWMPAYMENT_PAYER.ToLower Then
                        bds.DataSource = MConstants.listOfPayer.Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "label"
                        dgcol.ValueMember = "payer"
                    Else
                        Throw New Exception("Error occured when binding downpayment view. List " & map.view_list_name & " does not exist in LOV")
                    End If
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_bill_down_payment_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_bill_down_payment_grid.Columns.Add(dgcol)
                End If
            Next

            'bind datasource
            mctrl.mainForm.conf_bill_down_payment_grid.DataSource = _downPaymentList
            _downPaymentList.ApplyFilter(AddressOf filterDeletedDownPaymentDelegate)

            'validator
            'row validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.conf_bill_down_payment_grid, CDownPaymentRowValidator.getInstance, mapDownP)
            dhler.validateAddDelegate = AddressOf downpayment_view_ValidateAdd
            dhler.cancelAddDelegate = AddressOf downpayment_view_CancelAdd
        Catch ex As Exception
            Throw New Exception("An error occured while loading billing Downpayment view" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing Downpayment conf data to view controls"
    End Sub

    'filter deleted
    Public Function filterDeletedDownPaymentDelegate(downP As CDownPayment) As Boolean
        Return downP.system_status <> MConstants.CONST_SYS_ROW_DELETED
    End Function

    'validate new
    Public Sub downpayment_view_ValidateAdd(downP As ObjectView(Of CDownPayment), rowIndex As Integer)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(downP.Object) Then
                'if new item
                downP.Object.setID()
                If Not _downPaymentList.DataSource.Contains(downP.Object) Then
                    _downPaymentList.EndNew(rowIndex)
                End If
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new downpayment entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'cancel new
    Public Function downpayment_view_CancelAdd(downP As ObjectView(Of CDownPayment), rowIndex As Integer) As Boolean
        'return true to block leaving and return false to allow leaving
        Dim res As Boolean
        Try
            'check if new
            If downP.Object.id > 0 Then
                res = True
            Else
                If _downPaymentList.DataSource.Contains(downP.Object) Then
                    res = True
                Else
                    downP.CancelEdit()
                    _downPaymentList.CancelNew(rowIndex)
                    res = False
                End If
            End If

        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating downpayment entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return res
    End Function




    'bind the controls items in the billing material markup config view with the relevant data object
    Public Sub bind_billing_discount_view()
        mctrl.statusBar.status_strip_sub_label = "Bind billing Discount conf data to view controls..."
        Try
            Dim mapDisc As CViewModelMapList

            'get discount map view
            mapDisc = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_DISCOUNT_VIEW_DISPLAY)

            'discount datagridview
            mctrl.mainForm.conf_bill_discount_grid.AllowUserToAddRows = True
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.conf_bill_discount_grid)


            For Each map As CViewModelMap In mapDisc.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapDisc.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "Double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_bill_discount_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapDisc.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_SOLD_HRS_PAYER.ToLower Then
                        bds.DataSource = MConstants.listOfPayer.Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "label"
                        dgcol.ValueMember = "payer"
                    Else
                        Throw New Exception("Error occured when binding Discount view in additional scope tab. List " & map.view_list_name & " does not exist in LOV")
                    End If
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_bill_discount_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_bill_discount_grid.Columns.Add(dgcol)
                End If
            Next

            'bind datasource
            mctrl.mainForm.conf_bill_discount_grid.DataSource = _discountList

            filterDiscountView()

            'handle event
            Dim handler = New CViewRefreshHandler
            handler.datagridView = mctrl.mainForm.conf_bill_discount_grid
            handler.listView = _discountList
            handler.delegateList.Add(AddressOf refreshDiscountDependantViews)
            handler.flexibleHeigtControl = mctrl.mainForm.conf_bill_discount_grid_p
            handler.bindingListViewList.Add(mctrl.add_scope_controller.scopeList)
            handler.bindingListViewList.Add(mctrl.material_controller.matList)
            CViewRefreshHandler.addEntry(Of CDiscount)(handler)
            handler.refreshHandlerViewsHeight()

            'validator
            'row validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.conf_bill_discount_grid, CDiscountRowValidator.getInstance, mapDisc)
            dhler.validateAddDelegate = AddressOf discount_view_ValidateAdd
            dhler.cancelAddDelegate = AddressOf discount_view_CancelAdd
        Catch ex As Exception
            Throw New Exception("An error occured while loading billing Discount view" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing Discount conf data to view controls"
    End Sub
    'validate new
    Public Sub discount_view_ValidateAdd(discV As ObjectView(Of CDiscount), rowIndex As Integer)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(discV.Object) Then
                'if new item
                discV.Object.setID()
                refreshDiscountDependantViews()
                If Not _discountList.DataSource.Contains(discV.Object) Then
                    _discountList.EndNew(rowIndex)
                End If
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new discount entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'cancel new
    Public Function discount_view_CancelAdd(discV As ObjectView(Of CDiscount), rowIndex As Integer) As Boolean
        'return true to block leaving and return false to allow leaving
        Dim res As Boolean
        Try
            'check if new
            If discV.Object.id > 0 Then
                res = True
            Else
                If _discountList.DataSource.Contains(discV.Object) Then
                    res = True
                Else
                    discV.CancelEdit()
                    _discountList.CancelNew(rowIndex)
                    res = False
                End If
            End If

        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating discount entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return res
    End Function
    'update discount view
    Public Sub filterDiscountView()
        _discountList.ApplyFilter(AddressOf filterDeletedDiscount)
    End Sub
    Private Function filterDeletedDiscount(ByVal disc As CDiscount)
        'delegate for filtering
        Return Not disc.system_status = MConstants.CONST_SYS_ROW_DELETED
    End Function


    'bind the controls items in the billing material nte config view with the relevant data object
    Public Sub bind_billing_nte_view()
        mctrl.statusBar.status_strip_sub_label = "Bind billing nte conf data to view controls..."
        Try
            Dim mapNTE As CViewModelMapList

            'get discount map view
            mapNTE = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_NTE_VIEW_DISPLAY)

            'discount datagridview
            mctrl.mainForm.conf_bill_nte_grid.AllowUserToAddRows = True
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.conf_bill_nte_grid)

            For Each map As CViewModelMap In mapNTE.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapNTE.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "Double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_bill_nte_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapNTE.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_SOLD_HRS_PAYER.ToLower Then
                        bds.DataSource = MConstants.listOfPayer.Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "label"
                        dgcol.ValueMember = "payer"
                    Else
                        Throw New Exception("Error occured when binding NTE view in additional scope tab. List " & map.view_list_name & " does not exist in LOV")
                    End If
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_bill_nte_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_bill_nte_grid.Columns.Add(dgcol)
                End If
            Next

            'bind datasource
            mctrl.mainForm.conf_bill_nte_grid.DataSource = _nteList
            filterNTEView()
            'handle event
            Dim handler = New CViewRefreshHandler
            handler.datagridView = mctrl.mainForm.conf_bill_nte_grid
            handler.listView = _nteList
            handler.delegateList.Add(AddressOf refreshNTEDependantViews)
            handler.flexibleHeigtControl = mctrl.mainForm.conf_bill_nte_grid_p
            handler.bindingListViewList.Add(mctrl.add_scope_controller.scopeList)
            handler.bindingListViewList.Add(mctrl.material_controller.matList)
            CViewRefreshHandler.addEntry(Of CNotToExceed)(handler)
            handler.refreshHandlerViewsHeight()

            'validator
            'row validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.conf_bill_nte_grid, CNotToExceedRowValidator.getInstance, mapNTE)
            dhler.validateAddDelegate = AddressOf nte_view_ValidateAdd
        Catch ex As Exception
            Throw New Exception("An error occured while loading billing nte view" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing nte conf data to view controls"
    End Sub

    'validate new
    Public Sub nte_view_ValidateAdd(nte As ObjectView(Of CNotToExceed), rowIndex As Integer)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(nte.Object) Then
                'if new item
                nte.Object.setID()
                refreshNTEDependantViews()
                If Not _nteList.DataSource.Contains(nte.Object) Then
                    _nteList.EndNew(rowIndex)
                End If
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new discount entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'update discount view
    Public Sub filterNTEView()
        _nteList.ApplyFilter(AddressOf filterDeletedNTE)
    End Sub
    Private Function filterDeletedNTE(ByVal nte As CNotToExceed)
        'delegate for filtering
        Return Not nte.system_status = MConstants.CONST_SYS_ROW_DELETED
    End Function

    'perform delete
    Public Sub deleteObjectDiscount(disc As CDiscount)
        Try
            If _discountList.DataSource.Contains(disc) Then
                _discountList.DataSource.Remove(disc)
            End If
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(disc)
        Catch ex As Exception
        End Try
    End Sub

    'perform delete
    Public Sub deleteObjectNTE(nte As CNotToExceed)
        Try
            If _nteList.DataSource.Contains(nte) Then
                _nteList.DataSource.Remove(nte)
            End If
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(nte)
        Catch ex As Exception
        End Try
    End Sub

    'perform delete
    Public Sub deleteObjectDownPayment(dp As CDownPayment)
        Try
            If _downPaymentList.DataSource.Contains(dp) Then
                _downPaymentList.DataSource.Remove(dp)
            End If
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(dp)
        Catch ex As Exception
        End Try
    End Sub


End Class
