﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CDevJust
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    Public Shared currentMaxID As Integer
    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_DEV_JUST_LOADED Then
            _act_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
            _act_object = MConstants.EMPTY_ACT
            _cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _type = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _approved_on = MConstants.APP_NULL_DATE
            _scope = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _reason = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        End If
    End Sub

    Public Sub setID()
        If MConstants.GLOB_DEV_JUST_LOADED Then
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
        End If
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return True
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_DEV_JUST_DB_READ
    End Function

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Return New KeyValuePair(Of Boolean, String)(True, "")
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res = False
        Dim mess = ""

        Try
            If Not IsNothing(_ctrl_obj) AndAlso _ctrl_obj.deviationJustification.Contains(Me) Then
                _ctrl_obj.deviationJustification.Remove(Me)
                _ctrl_obj.update()
            End If
            Me.system_status = MConstants.CONST_SYS_ROW_DELETED
            MConstants.GLB_MSTR_CTRL.labor_controlling_controller.deleteObjectDevJust(Me)
            res = True
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.ToString & Chr(10) & ex.Message & Chr(10) & ex.StackTrace
        End Try

        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_DEV_JUST_LOADED Then
                    CDevJust.currentMaxID = Math.Max(CDevJust.currentMaxID, _id)
                End If
            End If
        End Set
    End Property



    Private _act_id As Long
    Public Property act_id() As Long
        Get
            Return _act_id
        End Get
        Set(ByVal value As Long)
            If Not _act_id = value Then
                _act_id = value
                Me.act_object = MConstants.GLB_MSTR_CTRL.add_scope_controller.getActivityObject(_act_id)
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _act_object As CScopeActivity
    Public Property act_object() As CScopeActivity
        Get
            Return _act_object
        End Get
        Set(ByVal value As CScopeActivity)
            If Not Object.Equals(value, _act_object) Then
                _act_object = value
                If IsNothing(value) Then
                    _act_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _act_id = value.id
                End If
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _cc As String
    Public Property cc() As String
        Get
            Return _cc
        End Get
        Set(ByVal value As String)
            If Not _cc = value Then
                _cc = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _qty As Double
    Public Property qty() As Double
        Get
            Return _qty
        End Get
        Set(ByVal value As Double)
            If Not _qty = value Then
                _qty = value
                NotifyPropertyChanged()
                update_ctrl_obj()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _type As String
    Public Property type() As String
        Get
            Return _type
        End Get
        Set(ByVal value As String)
            If Not _type = value Then
                _type = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _reason As String
    Public Property reason() As String
        Get
            Return _reason
        End Get
        Set(ByVal value As String)
            If Not _reason = value Then
                _reason = value
                update_ctrl_obj()
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            If Not _description = value Then
                _description = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _scope As String
    Public Property scope() As String
        Get
            Return _scope
        End Get
        Set(ByVal value As String)
            If Not _scope = value Then
                _scope = value
                update_ctrl_obj()
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public ReadOnly Property scope_str As String
        Get
            If MConstants.listOfValueConf(CONST_CONF_LOV_LAB_CTRL_DEV_JUST_SCOPE_LIST).ContainsKey(_scope) Then
                Return MConstants.listOfValueConf(CONST_CONF_LOV_LAB_CTRL_DEV_JUST_SCOPE_LIST)(_scope).value
            Else
                Return ""
            End If
        End Get
    End Property

    Private _claimed_by As String
    Public Property claimed_by() As String
        Get
            Return _claimed_by
        End Get
        Set(ByVal value As String)
            If Not _claimed_by = value Then
                _claimed_by = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _approved_by As String
    Public Property approved_by() As String
        Get
            Return _approved_by
        End Get
        Set(ByVal value As String)
            If Not _approved_by = value Then
                _approved_by = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _approved_on As Date
    Public Property approved_on() As Date
        Get
            Return _approved_on
        End Get
        Set(ByVal value As Date)
            If Not _approved_on = value Then
                _approved_on = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_opening_eac As Boolean
    Public Property is_opening_eac() As Boolean
        Get
            Return _is_opening_eac
        End Get
        Set(ByVal value As Boolean)
            If Not _is_opening_eac = value Then
                _is_opening_eac = value
                update_ctrl_obj()
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _ctrl_obj As CLaborControlMainViewObj
    Public Property ctrl_obj() As CLaborControlMainViewObj
        Get
            Return _ctrl_obj
        End Get
        Set(ByVal value As CLaborControlMainViewObj)
            _ctrl_obj = value
        End Set
    End Property

    'update calculations
    Public Sub update_ctrl_obj()
        If Not IsNothing(_ctrl_obj) Then
            _ctrl_obj.update()
        End If
    End Sub

    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not value = _calc_error_text Then
                _calc_error_text = value
            End If
        End Set
    End Property

    'text to display any error
    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
        End Set
    End Property


    'to string
    Public Overrides Function tostring() As String
        Return " Act: " & If(IsNothing(_act_object), _act_id, _act_object.tostring) & ",  CC: " & _cc & ", scope: " & _scope & ", qty: " & _qty
    End Function
End Class
