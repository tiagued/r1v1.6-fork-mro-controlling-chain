﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CAWQActivityTransferViewObject
    Implements INotifyPropertyChanged

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Private _labor As CSoldHours
    Public Property labor() As CSoldHours
        Get
            Return _labor
        End Get
        Set(ByVal value As CSoldHours)
            _labor = value
            'simulate refresh
            Me.labor_str = ""
        End Set
    End Property

    Private _material As CMaterial
    Public Property material() As CMaterial
        Get
            Return _material
        End Get
        Set(ByVal value As CMaterial)
            _material = value
            'simulate refresh
            Me.material_str = ""
        End Set
    End Property

    'in case the labor or material from fake activity is added as new lab/mat in the transferred activity
    Private _activity As CScopeActivity
    Public Property activity() As CScopeActivity
        Get
            Return _activity
        End Get
        Set(ByVal value As CScopeActivity)
            _activity = value
            'simulate refresh
            Me.activity_str = ""
        End Set
    End Property

    Private _linked_view_object As CAWQActivityTransferViewObject
    Public Property linked_view_object() As CAWQActivityTransferViewObject
        Get
            Return _linked_view_object
        End Get
        Set(ByVal value As CAWQActivityTransferViewObject)
            _linked_view_object = value
            'simulate refresh
            Me.linked_view_object_str = ""
        End Set
    End Property

    Public Property material_str() As String
        Get
            Dim res As String = ""
            If Not IsNothing(_material) Then
                res = _material.calc_material_info
                If Not IsNothing(_material.activity_obj) Then
                    If _material.activity_obj.is_fake_activity Then
                        'mat to transfer
                        If Not IsNothing(_material.awq_obj) Then
                            res = _material.awq_obj.reference_revision & " " & res
                        End If
                    Else
                        'mat that could be replaced
                        res = _material.activity_obj.getMatNetWorkActivity & " " & res
                    End If
                End If
            ElseIf Not IsNothing(_activity) Then
                res = _activity.tostring
            End If
            Return res
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    Public Property labor_str() As String
        Get
            Dim res As String = ""
            If Not IsNothing(_labor) Then
                res = _labor.cc & " " & _labor.sold_hrs & " hr(s) " & _labor.calc_price_before_disc.ToString("N2") & " " & MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr).value
                If Not IsNothing(_labor.act_object) Then
                    If _labor.act_object.is_fake_activity Then
                        'lab to transfer
                        If Not IsNothing(_labor.awq_obj) Then
                            res = _labor.awq_obj.reference_revision & " " & res
                        End If
                    Else
                        'lab that could be replaced
                        res = _labor.act_object.getMatNetWorkActivity & " " & res
                    End If
                End If
            ElseIf Not IsNothing(_activity) Then
                res = _activity.tostring
            End If
            Return res
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    Public Property activity_str() As String
        Get
            Dim res As String = ""
            If Not IsNothing(_activity) Then
                res = _activity.tostring
            End If
            Return res
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property


    Public Property linked_view_object_str() As String
        Get
            Return If(Not IsNothing(_linked_view_object), _linked_view_object.ToString, "")
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    Public Overrides Function ToString() As String
        Dim res As String = ""
        If Not IsNothing(_labor) Then
            res = Me.labor_str
        ElseIf Not IsNothing(_material) Then
            res = Me.material_str
        ElseIf Not IsNothing(_activity) Then
            res = Me.activity_str
        End If
        Return res
    End Function
End Class
