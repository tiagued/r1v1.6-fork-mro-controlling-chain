﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormReportAWQForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.cancel_bt = New System.Windows.Forms.Button()
        Me.run_bt = New System.Windows.Forms.Button()
        Me.report_payer_grp = New System.Windows.Forms.GroupBox()
        Me.cb_mat_curr = New System.Windows.Forms.ComboBox()
        Me.cb_lab_curr = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cb_payer = New System.Windows.Forms.ComboBox()
        Me.payer_lbl = New System.Windows.Forms.Label()
        Me.logs_grp = New System.Windows.Forms.GroupBox()
        Me.log_bt = New System.Windows.Forms.TextBox()
        Me.report_payer_grp.SuspendLayout()
        Me.logs_grp.SuspendLayout()
        Me.SuspendLayout()
        '
        'cancel_bt
        '
        Me.cancel_bt.Location = New System.Drawing.Point(346, 386)
        Me.cancel_bt.Name = "cancel_bt"
        Me.cancel_bt.Size = New System.Drawing.Size(92, 23)
        Me.cancel_bt.TabIndex = 4
        Me.cancel_bt.Text = "Cancel"
        Me.cancel_bt.UseVisualStyleBackColor = True
        '
        'run_bt
        '
        Me.run_bt.Location = New System.Drawing.Point(111, 386)
        Me.run_bt.Name = "run_bt"
        Me.run_bt.Size = New System.Drawing.Size(92, 23)
        Me.run_bt.TabIndex = 3
        Me.run_bt.Text = "Run"
        Me.run_bt.UseVisualStyleBackColor = True
        '
        'report_payer_grp
        '
        Me.report_payer_grp.Controls.Add(Me.cb_mat_curr)
        Me.report_payer_grp.Controls.Add(Me.cb_lab_curr)
        Me.report_payer_grp.Controls.Add(Me.Label2)
        Me.report_payer_grp.Controls.Add(Me.Label1)
        Me.report_payer_grp.Controls.Add(Me.cb_payer)
        Me.report_payer_grp.Controls.Add(Me.payer_lbl)
        Me.report_payer_grp.Location = New System.Drawing.Point(3, 15)
        Me.report_payer_grp.Name = "report_payer_grp"
        Me.report_payer_grp.Size = New System.Drawing.Size(578, 58)
        Me.report_payer_grp.TabIndex = 6
        Me.report_payer_grp.TabStop = False
        Me.report_payer_grp.Text = "Report Payer"
        '
        'cb_mat_curr
        '
        Me.cb_mat_curr.FormattingEnabled = True
        Me.cb_mat_curr.Location = New System.Drawing.Point(491, 22)
        Me.cb_mat_curr.Margin = New System.Windows.Forms.Padding(2)
        Me.cb_mat_curr.Name = "cb_mat_curr"
        Me.cb_mat_curr.Size = New System.Drawing.Size(65, 21)
        Me.cb_mat_curr.TabIndex = 9
        '
        'cb_lab_curr
        '
        Me.cb_lab_curr.FormattingEnabled = True
        Me.cb_lab_curr.Location = New System.Drawing.Point(343, 22)
        Me.cb_lab_curr.Margin = New System.Windows.Forms.Padding(2)
        Me.cb_lab_curr.Name = "cb_lab_curr"
        Me.cb_lab_curr.Size = New System.Drawing.Size(67, 21)
        Me.cb_lab_curr.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(433, 24)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Mat Curr. :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(277, 24)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Lab Curr. :"
        '
        'cb_payer
        '
        Me.cb_payer.FormattingEnabled = True
        Me.cb_payer.Location = New System.Drawing.Point(81, 22)
        Me.cb_payer.Margin = New System.Windows.Forms.Padding(2)
        Me.cb_payer.Name = "cb_payer"
        Me.cb_payer.Size = New System.Drawing.Size(171, 21)
        Me.cb_payer.TabIndex = 1
        '
        'payer_lbl
        '
        Me.payer_lbl.AutoSize = True
        Me.payer_lbl.Location = New System.Drawing.Point(7, 24)
        Me.payer_lbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.payer_lbl.Name = "payer_lbl"
        Me.payer_lbl.Size = New System.Drawing.Size(73, 13)
        Me.payer_lbl.TabIndex = 0
        Me.payer_lbl.Text = "Select Payer :"
        '
        'logs_grp
        '
        Me.logs_grp.Controls.Add(Me.log_bt)
        Me.logs_grp.Location = New System.Drawing.Point(3, 328)
        Me.logs_grp.Name = "logs_grp"
        Me.logs_grp.Size = New System.Drawing.Size(578, 43)
        Me.logs_grp.TabIndex = 8
        Me.logs_grp.TabStop = False
        Me.logs_grp.Text = "Logs"
        '
        'log_bt
        '
        Me.log_bt.Enabled = False
        Me.log_bt.Location = New System.Drawing.Point(5, 19)
        Me.log_bt.Name = "log_bt"
        Me.log_bt.Size = New System.Drawing.Size(566, 20)
        Me.log_bt.TabIndex = 0
        '
        'FormReportAWQForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(589, 421)
        Me.Controls.Add(Me.logs_grp)
        Me.Controls.Add(Me.report_payer_grp)
        Me.Controls.Add(Me.cancel_bt)
        Me.Controls.Add(Me.run_bt)
        Me.Name = "FormReportAWQForm"
        Me.Text = "AWQ Form"
        Me.report_payer_grp.ResumeLayout(False)
        Me.report_payer_grp.PerformLayout()
        Me.logs_grp.ResumeLayout(False)
        Me.logs_grp.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents cancel_bt As Button
    Friend WithEvents run_bt As Button
    Friend WithEvents report_payer_grp As GroupBox
    Friend WithEvents cb_payer As ComboBox
    Friend WithEvents payer_lbl As Label
    Friend WithEvents cb_mat_curr As ComboBox
    Friend WithEvents cb_lab_curr As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents logs_grp As GroupBox
    Friend WithEvents log_bt As TextBox
End Class
