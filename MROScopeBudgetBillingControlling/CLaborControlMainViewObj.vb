﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CLaborControlMainViewObj
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable

    Public Sub New(Optional _is_transfer_obj As Boolean = False)
        If Not _is_transfer_obj Then
            'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
            actualHours = New List(Of CActualHours)
            hoursTransfert = New List(Of CHoursTransfer)
            deviationJustification = New List(Of CDevJust)

            calculation_children = New List(Of CLaborControlMainViewObj)
            calculation_parents = New List(Of CLaborControlMainViewObj)

            structure_children = New List(Of CLaborControlMainViewObj)
            structure_children_keys = New List(Of String)

            overshoot_per_reason = New Dictionary(Of String, Double)
            saving_per_reason = New Dictionary(Of String, Double)

            transfer_scope_list = New Dictionary(Of String, String)

            _sent_item = New CLaborControlMainViewObj(True)
            _received_item = New CLaborControlMainViewObj(True)
        Else
            Dim a = 2
        End If
    End Sub


    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = False
        Dim mess As String = ""
        res = False
        mess = "Cannot be modified"
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res = False
        Dim mess = "It is not allowed to delete Labor Controlling Entries"
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function is_ctrl_obj_editable() As Boolean
        Return Not (Object.Equals(MConstants.LABOR_CTRL_ALL_CC, _cc_object) OrElse _is_virtual_cost_node OrElse structure_children.Count > 0)
    End Function

    Public ReadOnly Property activity_key As String
        Get
            Return _act_obj.proj & " " & _act_obj.act & " " & _act_obj.notification
        End Get
    End Property

    Private _act_obj As CScopeActivity
    Public Property act_obj() As CScopeActivity
        Get
            Return _act_obj
        End Get
        Set(ByVal value As CScopeActivity)
            _act_obj = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _cc_object As CCostCenter
    Public Property cc_object() As CCostCenter
        Get
            Return _cc_object
        End Get
        Set(ByVal value As CCostCenter)
            _cc_object = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _is_virtual_cost_node As Boolean
    Public Property is_virtual_cost_node() As Boolean
        Get
            Return _is_virtual_cost_node
        End Get
        Set(ByVal value As Boolean)
            _is_virtual_cost_node = value
        End Set
    End Property

    Public ReadOnly Property act_network As String
        Get
            Return _act_obj.proj
        End Get
    End Property

    Public ReadOnly Property act_number As String
        Get
            Return _act_obj.act
        End Get
    End Property

    Public ReadOnly Property act_jc As String
        Get
            Return _act_obj.job_card
        End Get
    End Property

    Public ReadOnly Property act_desc As String
        Get
            Return _act_obj.act_desc
        End Get
    End Property

    Public ReadOnly Property act_status As String
        Get
            Return _act_obj.status
        End Get
    End Property

    Public ReadOnly Property act_notif As String
        Get
            Return _act_obj.notification
        End Get
    End Property

    Public ReadOnly Property cc As String
        Get
            Return _cc_object.cc
        End Get
    End Property

    Public ReadOnly Property cc_desc As String
        Get
            Return _cc_object.friendly_description
        End Get
    End Property

    'item sent
    Private _sent_item As CLaborControlMainViewObj
    Public ReadOnly Property sent_item As CLaborControlMainViewObj
        Get
            Return _sent_item
        End Get
    End Property
    'received items
    Private _received_item As CLaborControlMainViewObj
    Public ReadOnly Property received_item As CLaborControlMainViewObj
        Get
            Return _received_item
        End Get
    End Property

    'Activity CC Budget As Sold
    Private _budget_init_quoted_qty As Double
    Public Property budget_init_quoted_qty() As Double
        Get
            Return _budget_init_quoted_qty
        End Get
        Set(ByVal value As Double)
            _budget_init_quoted_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _budget_init_adj_qty As Double
    Public Property budget_init_adj_qty() As Double
        Get
            Return _budget_init_adj_qty
        End Get
        Set(ByVal value As Double)
            _budget_init_adj_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Public Property budget_init_qty() As Double
        Get
            Return _budget_init_adj_qty + _budget_init_quoted_qty
        End Get
        Set(value As Double)
        End Set
    End Property

    Private _tr_budget_init_qty As Double
    Public Property tr_budget_init_qty() As Double
        Get
            Return _tr_budget_init_qty
        End Get
        Set(value As Double)
            _tr_budget_init_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _budget_add_below_thr_qty As Double
    Public Property budget_add_below_thr_qty() As Double
        Get
            Return _budget_add_below_thr_qty
        End Get
        Set(ByVal value As Double)
            _budget_add_below_thr_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_budget_add_below_thr_qty As Double
    Public Property tr_budget_add_below_thr_qty As Double
        Get
            Return _tr_budget_add_below_thr_qty
        End Get
        Set(value As Double)
            _tr_budget_add_below_thr_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'awq opend not sent
    Private _budget_add_awq_inwork_qty As Double
    Public Property budget_add_awq_inwork_qty() As Double
        Get
            Return _budget_add_awq_inwork_qty
        End Get
        Set(ByVal value As Double)
            _budget_add_awq_inwork_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_budget_add_awq_inwork_qty As Double
    Public Property tr_budget_add_awq_inwork_qty As Double
        Get
            Return _tr_budget_add_awq_inwork_qty
        End Get
        Set(value As Double)
            _tr_budget_add_awq_inwork_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'awq waiting customer
    Private _budget_add_awq_sent_qty As Double
    Public Property budget_add_awq_sent_qty() As Double
        Get
            Return _budget_add_awq_sent_qty
        End Get
        Set(ByVal value As Double)
            _budget_add_awq_sent_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_budget_add_awq_sent_qty As Double
    Public Property tr_budget_add_awq_sent_qty As Double
        Get
            Return _tr_budget_add_awq_sent_qty
        End Get
        Set(value As Double)
            _tr_budget_add_awq_sent_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'awq approved by pm
    Private _budget_add_awq_pm_go_qty As Double
    Public Property budget_add_awq_pm_go_qty() As Double
        Get
            Return _budget_add_awq_pm_go_qty
        End Get
        Set(ByVal value As Double)
            _budget_add_awq_pm_go_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_budget_add_awq_pm_go_qty As Double
    Public Property tr_budget_add_awq_pm_go_qty As Double
        Get
            Return _tr_budget_add_awq_pm_go_qty
        End Get
        Set(value As Double)
            _tr_budget_add_awq_pm_go_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'awq approved by cust
    Private _budget_add_awq_approved_qty As Double
    Public Property budget_add_awq_approved_qty() As Double
        Get
            Return _budget_add_awq_approved_qty
        End Get
        Set(ByVal value As Double)
            _budget_add_awq_approved_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_budget_add_awq_approved_qty As Double
    Public Property tr_budget_add_awq_approved_qty As Double
        Get
            Return _tr_budget_add_awq_approved_qty
        End Get
        Set(value As Double)
            _tr_budget_add_awq_approved_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'foc budget
    Private _budget_foc_init_qty As Double
    Public Property budget_foc_init_qty() As Double
        Get
            Return _budget_foc_init_qty
        End Get
        Set(ByVal value As Double)
            _budget_foc_init_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_budget_foc_init_qty As Double
    Public Property tr_budget_foc_init_qty As Double
        Get
            Return _tr_budget_foc_init_qty
        End Get
        Set(value As Double)
            _tr_budget_foc_init_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _budget_foc_add_blw_qty As Double
    Public Property budget_foc_add_blw_qty() As Double
        Get
            Return _budget_foc_add_blw_qty
        End Get
        Set(ByVal value As Double)
            _budget_foc_add_blw_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_budget_foc_add_blw_qty As Double
    Public Property tr_budget_foc_add_blw_qty As Double
        Get
            Return _tr_budget_foc_add_blw_qty
        End Get
        Set(value As Double)
            _tr_budget_foc_add_blw_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _budget_foc_add_appr_qty As Double
    Public Property budget_foc_add_appr_qty() As Double
        Get
            Return _budget_foc_add_appr_qty
        End Get
        Set(ByVal value As Double)
            _budget_foc_add_appr_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_budget_foc_add_appr_qty As Double
    Public Property tr_budget_foc_add_appr_qty As Double
        Get
            Return _tr_budget_foc_add_appr_qty
        End Get
        Set(value As Double)
            _tr_budget_foc_add_appr_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _budget_foc_add_pending_qty As Double
    Public Property budget_foc_add_pending_qty() As Double
        Get
            Return _budget_foc_add_pending_qty
        End Get
        Set(ByVal value As Double)
            _budget_foc_add_pending_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_budget_foc_add_pending_qty As Double
    Public Property tr_budget_foc_add_pending_qty As Double
        Get
            Return _tr_budget_foc_add_pending_qty
        End Get
        Set(value As Double)
            _tr_budget_foc_add_pending_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _budget_foc_add_inwork_qty As Double
    Public Property budget_foc_add_inwork_qty() As Double
        Get
            Return _budget_foc_add_inwork_qty
        End Get
        Set(ByVal value As Double)
            _budget_foc_add_inwork_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_budget_foc_add_inwork_qty As Double
    Public Property tr_budget_foc_add_inwork_qty As Double
        Get
            Return _tr_budget_foc_add_inwork_qty
        End Get
        Set(value As Double)
            _tr_budget_foc_add_inwork_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'add of foc
    Private _tr_budget_foc_add_qty As Double
    Public Property tr_budget_foc_add_qty() As Double
        Get
            Return _tr_budget_foc_add_qty
        End Get
        Set(value As Double)
            _tr_budget_foc_add_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'total of foc
    Private _budget_foc_total_qty As Double
    Public Property budget_foc_total_qty As Double
        Get
            Return _budget_foc_total_qty
        End Get
        Set(value As Double)
            _budget_foc_total_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_budget_foc_total_qty As Double
    Public Property tr_budget_foc_total_qty() As Double
        Get
            Return _tr_budget_foc_total_qty
        End Get
        Set(value As Double)
            _tr_budget_foc_total_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'total of foc approved
    Private _tr_budget_foc_total_approved_qty As Double
    Public Property tr_budget_foc_total_approved_qty() As Double
        Get
            Return _tr_budget_foc_total_approved_qty
        End Get
        Set(value As Double)
            _tr_budget_foc_total_approved_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'ODI
    Private _budget_odi_qty As Double
    Public Property budget_odi_qty() As Double
        Get
            Return _budget_odi_qty
        End Get
        Set(ByVal value As Double)
            _budget_odi_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'TBA
    Private _budget_tba_qty As Double
    Public Property budget_tba_qty() As Double
        Get
            Return _budget_tba_qty
        End Get
        Set(ByVal value As Double)
            _budget_tba_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'Planned
    Private _budget_plan_qty As Double
    Public Property budget_plan_qty() As Double
        Get
            Return _budget_plan_qty
        End Get
        Set(ByVal value As Double)
            _budget_plan_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'Risk Reserve
    Private _budget_risk_reserve_approved_qty As Double
    Public Property budget_risk_reserve_approved_qty() As Double
        Get
            Return _budget_risk_reserve_approved_qty
        End Get
        Set(ByVal value As Double)
            _budget_risk_reserve_approved_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _budget_risk_reserve_pending_qty As Double
    Public Property budget_risk_reserve_pending_qty() As Double
        Get
            Return _budget_risk_reserve_pending_qty
        End Get
        Set(ByVal value As Double)
            _budget_risk_reserve_pending_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_budget_odi_qty As Double
    Public Property tr_budget_odi_qty As Double
        Get
            Return _tr_budget_odi_qty
        End Get
        Set(value As Double)
            _tr_budget_odi_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'total budget$
    Private _tr_budget_total_qty As Double
    Public Property tr_budget_total_qty() As Double
        Get
            Return _tr_budget_total_qty
        End Get
        Set(value As Double)
            _tr_budget_total_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'total budget approved
    Private _tr_budget_total_approved_qty As Double
    Public Property tr_budget_total_approved_qty() As Double
        Get
            Return _tr_budget_total_approved_qty
        End Get
        Set(value As Double)
            _tr_budget_total_approved_qty = value
            NotifyPropertyChanged()
        End Set
    End Property
    'total self budget
    Private _self_budget_total_qty As Double
    Public Property self_budget_total_qty() As Double
        Get
            Return _self_budget_total_qty
        End Get
        Set(value As Double)
            _self_budget_total_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'Budget Sent
    Private _tr_sent_budget_total_approved_qty As Double
    Public Property tr_sent_budget_total_approved_qty() As Double
        Get
            Return _tr_sent_budget_total_approved_qty
        End Get
        Set(value As Double)
            _tr_sent_budget_total_approved_qty = value
            NotifyPropertyChanged()
        End Set
    End Property
    'Budget received
    Private _tr_received_budget_total_approved_qty As Double
    Public Property tr_received_budget_total_approved_qty() As Double
        Get
            Return _tr_received_budget_total_approved_qty
        End Get
        Set(value As Double)
            _tr_received_budget_total_approved_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'actuals total as booked
    Private _actual_booked_qty As Double
    Public Property actual_booked_qty() As Double
        Get
            Return _actual_booked_qty
        End Get
        Set(ByVal value As Double)
            _actual_booked_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _tr_actual_booked_qty As Double
    Public Property tr_actual_booked_qty As Double
        Get
            Return _tr_actual_booked_qty
        End Get
        Set(value As Double)
            _tr_actual_booked_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'Actual Sent
    Private _tr_sent_actual_booked_qty As Double
    Public Property tr_sent_actual_booked_qty() As Double
        Get
            Return _tr_sent_actual_booked_qty
        End Get
        Set(value As Double)
            _tr_sent_actual_booked_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'Actual received
    Private _tr_received_actual_booked_qty As Double
    Public Property tr_received_actual_booked_qty() As Double
        Get
            Return _tr_received_actual_booked_qty
        End Get
        Set(value As Double)
            _tr_received_actual_booked_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'actuals total as booked
    Private _actual_init_qty As Double
    Public Property actual_init_qty() As Double
        Get
            Return _actual_init_qty
        End Get
        Set(ByVal value As Double)
            _actual_init_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _actual_init_foc_qty As Double
    Public Property actual_init_foc_qty() As Double
        Get
            Return _actual_init_foc_qty
        End Get
        Set(ByVal value As Double)
            _actual_init_foc_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _actual_odi_qty As Double
    Public Property actual_odi_qty() As Double
        Get
            Return _actual_odi_qty
        End Get
        Set(ByVal value As Double)
            _actual_odi_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _actual_add_foc_qty As Double
    Public Property actual_add_foc_qty() As Double
        Get
            Return _actual_add_foc_qty
        End Get
        Set(ByVal value As Double)
            _actual_add_foc_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _actual_add_blw_thr_qty As Double
    Public Property actual_add_blw_thr_qty() As Double
        Get
            Return _actual_add_blw_thr_qty
        End Get
        Set(ByVal value As Double)
            _actual_add_blw_thr_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _actual_add_appr_qty As Double
    Public Property actual_add_appr_qty() As Double
        Get
            Return _actual_add_appr_qty
        End Get
        Set(ByVal value As Double)
            _actual_add_appr_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _actual_add_sent_qty As Double
    Public Property actual_add_sent_qty() As Double
        Get
            Return _actual_add_sent_qty
        End Get
        Set(ByVal value As Double)
            _actual_add_sent_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _actual_add_inwork_qty As Double
    Public Property actual_add_inwork_qty() As Double
        Get
            Return _actual_add_inwork_qty
        End Get
        Set(ByVal value As Double)
            _actual_add_inwork_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    'Overshoot Deviation Justification
    Private _overshoot_just_init_qty As Double
    Public Property overshoot_just_init_qty() As Double
        Get
            Return _overshoot_just_init_qty
        End Get
        Set(ByVal value As Double)
            _overshoot_just_init_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _overshoot_just_init_foc_qty As Double
    Public Property overshoot_just_init_foc_qty() As Double
        Get
            Return _overshoot_just_init_foc_qty
        End Get
        Set(ByVal value As Double)
            _overshoot_just_init_foc_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _overshoot_just_odi_qty As Double
    Public Property overshoot_just_odi_qty() As Double
        Get
            Return _overshoot_just_odi_qty
        End Get
        Set(ByVal value As Double)
            _overshoot_just_odi_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _overshoot_just_add_foc_qty As Double
    Public Property overshoot_just_add_foc_qty() As Double
        Get
            Return _overshoot_just_add_foc_qty
        End Get
        Set(ByVal value As Double)
            _overshoot_just_add_foc_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _overshoot_just_add_blw_thr_qty As Double
    Public Property overshoot_just_add_blw_thr_qty() As Double
        Get
            Return _overshoot_just_add_blw_thr_qty
        End Get
        Set(ByVal value As Double)
            _overshoot_just_add_blw_thr_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _overshoot_just_add_appr_qty As Double
    Public Property overshoot_just_add_appr_qty() As Double
        Get
            Return _overshoot_just_add_appr_qty
        End Get
        Set(ByVal value As Double)
            _overshoot_just_add_appr_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _overshoot_just_add_sent_qty As Double
    Public Property overshoot_just_add_sent_qty() As Double
        Get
            Return _overshoot_just_add_sent_qty
        End Get
        Set(ByVal value As Double)
            _overshoot_just_add_sent_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _overshoot_just_add_inwork_qty As Double
    Public Property overshoot_just_add_inwork_qty() As Double
        Get
            Return _overshoot_just_add_inwork_qty
        End Get
        Set(ByVal value As Double)
            _overshoot_just_add_inwork_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Public Property overshoot_just_total_qty As Double
        Get
            Return Me.overshoot_just_init_qty + Me.overshoot_just_init_foc_qty + Me.overshoot_just_odi_qty + Me.overshoot_just_add_foc_qty + Me.overshoot_just_add_blw_thr_qty + Me.overshoot_just_add_appr_qty + Me.overshoot_just_add_sent_qty + Me.overshoot_just_add_inwork_qty
        End Get
        Set(value As Double)
        End Set
    End Property

    'Gain Deviation Justification
    Private _gain_just_init_qty As Double
    Public Property gain_just_init_qty() As Double
        Get
            Return _gain_just_init_qty
        End Get
        Set(ByVal value As Double)
            _gain_just_init_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _gain_just_init_foc_qty As Double
    Public Property gain_just_init_foc_qty() As Double
        Get
            Return _gain_just_init_foc_qty
        End Get
        Set(ByVal value As Double)
            _gain_just_init_foc_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _gain_just_odi_qty As Double
    Public Property gain_just_odi_qty() As Double
        Get
            Return _gain_just_odi_qty
        End Get
        Set(ByVal value As Double)
            _gain_just_odi_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _gain_just_add_foc_qty As Double
    Public Property gain_just_add_foc_qty() As Double
        Get
            Return _gain_just_add_foc_qty
        End Get
        Set(ByVal value As Double)
            _gain_just_add_foc_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _gain_just_add_blw_thr_qty As Double
    Public Property gain_just_add_blw_thr_qty() As Double
        Get
            Return _gain_just_add_blw_thr_qty
        End Get
        Set(ByVal value As Double)
            _gain_just_add_blw_thr_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _gain_just_add_appr_qty As Double
    Public Property gain_just_add_appr_qty() As Double
        Get
            Return _gain_just_add_appr_qty
        End Get
        Set(ByVal value As Double)
            _gain_just_add_appr_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _gain_just_add_sent_qty As Double
    Public Property gain_just_add_sent_qty() As Double
        Get
            Return _gain_just_add_sent_qty
        End Get
        Set(ByVal value As Double)
            _gain_just_add_sent_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _gain_just_add_inwork_qty As Double
    Public Property gain_just_add_inwork_qty() As Double
        Get
            Return _gain_just_add_inwork_qty
        End Get
        Set(ByVal value As Double)
            _gain_just_add_inwork_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Public Property gain_just_total_qty As Double
        Get
            Return Me.gain_just_init_qty + Me.gain_just_init_foc_qty + Me.gain_just_odi_qty + Me.gain_just_add_foc_qty + Me.gain_just_add_blw_thr_qty + Me.gain_just_add_appr_qty + Me.gain_just_add_sent_qty + Me.gain_just_add_inwork_qty
        End Get
        Set(value As Double)
        End Set
    End Property

    'deviations
    Private _deviation_appr_bef_just_qty As Double
    Public Property deviation_appr_bef_just_qty As Double
        Get
            Return _deviation_appr_bef_just_qty
        End Get
        Set(value As Double)
            _deviation_appr_bef_just_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _deviation_bef_just_qty As Double
    Public Property deviation_bef_just_qty As Double
        Get
            Return _deviation_bef_just_qty
        End Get
        Set(value As Double)
            _deviation_bef_just_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _deviation_appr_after_just_qty As Double
    Public Property deviation_appr_after_just_qty As Double
        Get
            Return _deviation_appr_after_just_qty
        End Get
        Set(value As Double)
            _deviation_appr_after_just_qty = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _deviation_after_just_qty As Double
    Public Property deviation_after_just_qty As Double
        Get
            Return _deviation_after_just_qty
        End Get
        Set(value As Double)
            _deviation_after_just_qty = value
            NotifyPropertyChanged()
        End Set
    End Property


    Public overshoot_per_reason As Dictionary(Of String, Double)
    Public saving_per_reason As Dictionary(Of String, Double)

    'lists
    Public actualHours As List(Of CActualHours)
    Public hoursTransfert As List(Of CHoursTransfer)
    Public deviationJustification As List(Of CDevJust)

    Public calculation_children As List(Of CLaborControlMainViewObj)
    Public calculation_parents As List(Of CLaborControlMainViewObj)

    Private structure_children As List(Of CLaborControlMainViewObj)
    Private structure_children_keys As List(Of String)
    Public transfer_scope_list As Dictionary(Of String, String)

    Public Function get_structure_children() As List(Of CLaborControlMainViewObj)
        Return structure_children
    End Function

    Public Function structure_children_exists(key As String)
        Return structure_children_keys.Contains(key)
    End Function

    Public Sub add_structure_children(key As String, obj As CLaborControlMainViewObj)
        If Not structure_children_keys.Contains(key) Then
            structure_children.Add(obj)
            structure_children_keys.Add(key)
        End If
    End Sub
    Public Sub remove_structure_children(key As String, obj As CLaborControlMainViewObj)
        If structure_children_keys.Contains(key) Then
            structure_children_keys.Remove(key)
        End If
        If structure_children.Contains(obj) Then
            structure_children.Remove(obj)
        End If
    End Sub

    Public Function isZero() As Boolean
        Return Me.tr_budget_total_qty = 0 AndAlso Me.tr_actual_booked_qty = 0 AndAlso Me.gain_just_total_qty = 0 AndAlso Me.overshoot_just_total_qty = 0
    End Function

    Public Sub setToZero()
        Me.budget_init_quoted_qty = 0
        Me.budget_init_adj_qty = 0
        Me.budget_add_below_thr_qty = 0
        Me.budget_add_awq_inwork_qty = 0
        Me.budget_add_awq_sent_qty = 0
        Me.budget_add_awq_pm_go_qty = 0
        Me.budget_add_awq_approved_qty = 0
        Me.budget_foc_init_qty = 0
        Me.budget_foc_add_blw_qty = 0
        Me.budget_foc_add_appr_qty = 0
        Me.budget_foc_add_pending_qty = 0
        Me.budget_foc_add_inwork_qty = 0
        Me.budget_odi_qty = 0
        Me.budget_tba_qty = 0
        Me.budget_plan_qty = 0
        Me.budget_risk_reserve_approved_qty = 0
        Me.budget_risk_reserve_pending_qty = 0
        Me.actual_booked_qty = 0
        Me.actual_init_qty = 0
        Me.actual_init_foc_qty = 0
        Me.actual_odi_qty = 0
        Me.actual_add_foc_qty = 0
        Me.actual_add_blw_thr_qty = 0
        Me.actual_add_appr_qty = 0
        Me.actual_add_sent_qty = 0
        Me.actual_add_inwork_qty = 0
        Me.overshoot_just_init_qty = 0
        Me.overshoot_just_init_foc_qty = 0
        Me.overshoot_just_odi_qty = 0
        Me.overshoot_just_add_foc_qty = 0
        Me.overshoot_just_add_blw_thr_qty = 0
        Me.overshoot_just_add_appr_qty = 0
        Me.overshoot_just_add_sent_qty = 0
        Me.overshoot_just_add_inwork_qty = 0
        Me.gain_just_init_qty = 0
        Me.gain_just_init_foc_qty = 0
        Me.gain_just_odi_qty = 0
        Me.gain_just_add_foc_qty = 0
        Me.gain_just_add_blw_thr_qty = 0
        Me.gain_just_add_appr_qty = 0
        Me.gain_just_add_sent_qty = 0
        Me.gain_just_add_inwork_qty = 0

        'calculated
        Me.tr_budget_init_qty = 0
        Me.tr_budget_add_below_thr_qty = 0
        Me.tr_budget_add_awq_inwork_qty = 0
        Me.tr_budget_add_awq_sent_qty = 0
        Me.tr_budget_add_awq_pm_go_qty = 0
        Me.tr_budget_add_awq_approved_qty = 0
        Me.tr_budget_foc_init_qty = 0
        Me.tr_budget_foc_add_blw_qty = 0
        Me.tr_budget_foc_add_appr_qty = 0
        Me.tr_budget_foc_add_pending_qty = 0
        Me.tr_budget_foc_add_inwork_qty = 0
        Me.tr_budget_foc_add_qty = 0
        Me.budget_foc_total_qty = 0
        Me.tr_budget_foc_total_qty = 0
        Me.tr_budget_foc_total_approved_qty = 0
        Me.tr_budget_odi_qty = 0
        Me.tr_budget_total_qty = 0
        Me.tr_budget_total_approved_qty = 0
        Me.self_budget_total_qty = 0
        Me.tr_actual_booked_qty = 0
        Me.deviation_appr_bef_just_qty = 0
        Me.deviation_bef_just_qty = 0
        Me.deviation_appr_after_just_qty = 0
        Me.deviation_after_just_qty = 0
        Me.tr_sent_budget_total_approved_qty = 0
        Me.tr_sent_actual_booked_qty = 0
        Me.tr_received_budget_total_approved_qty = 0
        Me.tr_received_actual_booked_qty = 0
    End Sub

    'calculate
    Public Sub update(Optional calcul_parent As Boolean = True)
        Try
            If Not Me.is_ctrl_obj_editable Then
                Exit Sub
            End If
            Me.setToZero()
            _sent_item.setToZero()
            _received_item.setToZero()

            overshoot_per_reason.Clear()
            saving_per_reason.Clear()
            transfer_scope_list.Clear()
            _calc_is_in_error_state = False
            _calc_error_text = ""

            Dim transferCCList As Dictionary(Of String, CCostCenter) = MConstants.GLB_MSTR_CTRL.labor_controlling_controller.ccTransferList
            Dim isActFOC As Boolean = False
            isActFOC = act_obj.labor_sold_mark = MConstants.constantConf(MConstants.CONST_CONF_LOV_RED_MARK_FOC_KEY).value OrElse
                 act_obj.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_NOT_ADD_V_KEY).value OrElse
                 act_obj.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_CANC_FWD_KEY).value OrElse
                 act_obj.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_WAST_KEY).value
            'fill budgets

            For Each soldH As CSoldHours In act_obj.sold_hour_list

                Dim occ As CCostCenter
                If transferCCList.ContainsKey(soldH.cc) Then
                    occ = transferCCList(soldH.cc)
                Else
                    occ = soldH.cc_obj
                End If
                If Not occ.cc = cc_object.cc Then
                    Continue For
                End If
                'get plan hours no matter it is reported or not
                Me.budget_plan_qty = Me.budget_plan_qty + soldH.plan_hrs
                'report TBA
                If Not soldH.is_reported Then
                    Me.budget_tba_qty = Me.budget_tba_qty + soldH.sold_hrs
                    Continue For
                End If

                If Not String.IsNullOrWhiteSpace(soldH.red_mark) AndAlso soldH.red_mark = MConstants.constantConf(MConstants.CONST_CONF_LOV_RED_MARK_FOC_KEY).value Then
                    If Not IsNothing(soldH.init_quote_entry_obj) Then
                        If _act_obj.is_odi Then
                            Me.budget_odi_qty = Me.budget_odi_qty + soldH.sold_hrs + soldH.sold_hrs_adj
                        Else
                            Me.budget_foc_init_qty = Me.budget_foc_init_qty + soldH.sold_hrs + soldH.sold_hrs_adj
                        End If
                    Else
                        If _act_obj.is_odi Then
                            Me.budget_odi_qty = Me.budget_odi_qty + soldH.sold_hrs
                        Else
                            If IsNothing(soldH.awq_obj) OrElse isActFOC Then
                                Me.budget_foc_add_blw_qty = Me.budget_foc_add_blw_qty + soldH.sold_hrs
                            ElseIf IsNothing(soldH.remove_awq_obj) Then
                                'according to awq status
                                If soldH.awq_obj.is_in_work Then
                                    Me.budget_foc_add_inwork_qty = Me.budget_foc_add_inwork_qty + soldH.sold_hrs
                                    If soldH.awq_obj.is_PM_GO Then
                                        Me.budget_add_awq_pm_go_qty = Me.budget_add_awq_pm_go_qty + soldH.sold_hrs
                                    End If
                                ElseIf soldH.awq_obj.is_open_and_sent_to_customer Then
                                    Me.budget_foc_add_pending_qty = Me.budget_foc_add_pending_qty + soldH.sold_hrs
                                    If soldH.awq_obj.is_PM_GO Then
                                        Me.budget_add_awq_pm_go_qty = Me.budget_add_awq_pm_go_qty + soldH.sold_hrs
                                    End If
                                ElseIf soldH.awq_obj.is_approved_closed Then
                                    Me.budget_foc_add_appr_qty = Me.budget_foc_add_appr_qty + soldH.sold_hrs
                                    'removed AWQ
                                    If Not IsNothing(soldH.remove_awq_obj) Then
                                        If soldH.remove_awq_obj.is_in_work Then
                                            Me.budget_foc_add_inwork_qty = Me.budget_foc_add_inwork_qty - soldH.sold_hrs
                                        ElseIf soldH.remove_awq_obj.is_open_and_sent_to_customer Then
                                            Me.budget_foc_add_pending_qty = Me.budget_foc_add_pending_qty - soldH.sold_hrs
                                        ElseIf soldH.remove_awq_obj.is_approved_closed Then
                                            Me.budget_foc_add_appr_qty = Me.budget_foc_add_appr_qty - soldH.sold_hrs
                                        End If
                                    End If
                                Else
                                    _calc_error_text = _calc_error_text & Chr(10) & "AWQ " & soldH.awq_obj.reference_revision & "could not be categorized"
                                End If
                            End If
                        End If
                    End If
                ElseIf Not String.IsNullOrWhiteSpace(soldH.red_mark) AndAlso soldH.red_mark = MConstants.constantConf(MConstants.CONST_CONF_LOV_RED_MARK_RISK_RESV_KEY).value Then
                    If Not IsNothing(soldH.init_quote_entry_obj) Then
                        Me.budget_risk_reserve_approved_qty = Me.budget_risk_reserve_approved_qty + soldH.sold_hrs
                    Else
                        If IsNothing(soldH.awq_obj) Then
                            Me.budget_risk_reserve_approved_qty = Me.budget_risk_reserve_approved_qty + soldH.sold_hrs
                        ElseIf IsNothing(soldH.remove_awq_obj) Then
                            'according to awq status
                            If soldH.awq_obj.is_in_work Then

                            ElseIf soldH.awq_obj.is_open_and_sent_to_customer Then
                                Me.budget_risk_reserve_pending_qty = Me.budget_risk_reserve_pending_qty + soldH.sold_hrs
                            ElseIf soldH.awq_obj.is_approved_closed Then
                                Me.budget_risk_reserve_approved_qty = Me.budget_risk_reserve_approved_qty + soldH.sold_hrs
                            Else
                                _calc_error_text = _calc_error_text & Chr(10) & "AWQ " & soldH.awq_obj.reference_revision & "could not be categorized"
                            End If
                        End If
                    End If
                ElseIf Not IsNothing(soldH.init_quote_entry_obj) Then
                    If _act_obj.is_odi Then
                        Me.budget_odi_qty = Me.budget_odi_qty + soldH.sold_hrs + soldH.sold_hrs_adj
                    Else
                        Me.budget_init_quoted_qty = Me.budget_init_quoted_qty + soldH.sold_hrs
                        Me.budget_init_adj_qty = Me.budget_init_adj_qty + soldH.sold_hrs_adj
                    End If
                Else
                    If _act_obj.is_odi Then
                        Me.budget_odi_qty = Me.budget_odi_qty + soldH.sold_hrs
                    ElseIf IsNothing(soldH.awq_obj) Then
                        If isActFOC Then
                            Me.budget_foc_add_blw_qty = Me.budget_foc_add_blw_qty + soldH.sold_hrs
                        Else
                            Me.budget_add_below_thr_qty = Me.budget_add_below_thr_qty + soldH.sold_hrs
                        End If
                    Else
                        'according to awq status
                        If soldH.awq_obj.is_in_work Then
                            Me.budget_add_awq_inwork_qty = Me.budget_add_awq_inwork_qty + soldH.sold_hrs
                            If soldH.awq_obj.is_PM_GO Then
                                Me.budget_add_awq_pm_go_qty = Me.budget_add_awq_pm_go_qty + soldH.sold_hrs
                            End If
                        ElseIf soldH.awq_obj.is_open_and_sent_to_customer Then
                            Me.budget_add_awq_sent_qty = Me.budget_add_awq_sent_qty + soldH.sold_hrs
                            If soldH.awq_obj.is_PM_GO Then
                                Me.budget_add_awq_pm_go_qty = Me.budget_add_awq_pm_go_qty + soldH.sold_hrs
                            End If
                        ElseIf soldH.awq_obj.is_approved_closed Then
                            Me.budget_add_awq_approved_qty = Me.budget_add_awq_approved_qty + soldH.sold_hrs
                            'removed AWQ
                            If Not IsNothing(soldH.remove_awq_obj) Then
                                If soldH.remove_awq_obj.is_in_work Then
                                    Me.budget_add_awq_inwork_qty = Me.budget_add_awq_inwork_qty - soldH.sold_hrs
                                ElseIf soldH.remove_awq_obj.is_open_and_sent_to_customer Then
                                    Me.budget_add_awq_sent_qty = Me.budget_add_awq_sent_qty - soldH.sold_hrs
                                ElseIf soldH.remove_awq_obj.is_approved_closed Then
                                    Me.budget_add_awq_approved_qty = Me.budget_add_awq_approved_qty - soldH.sold_hrs
                                End If
                            End If
                        Else
                            _calc_error_text = _calc_error_text & Chr(10) & "AWQ " & soldH.awq_obj.reference_revision & "could not be categorized"
                        End If
                    End If
                End If
            Next

            'actual hours
            For Each ActH As CActualHours In actualHours
                Me.actual_booked_qty = Me.actual_booked_qty + ActH.qty
                If MConstants.GLB_MSTR_CTRL.labor_controlling_controller.latest_actual_booking < ActH.posting_date Then
                    MConstants.GLB_MSTR_CTRL.labor_controlling_controller.latest_actual_booking = ActH.posting_date
                End If
            Next

            'budget transfers
            For Each Htrans As CHoursTransfer In hoursTransfert
                If MConstants.GLB_MSTR_CTRL.labor_controlling_controller.getTransferedCC(Htrans.to_cc).cc = _cc_object.cc AndAlso Htrans.to_act_id = _act_obj.id Then
                    If Htrans.hours_type = MConstants.CONST_CONF_CTRL_HRS_TRANSF_TYPE_ACTUAL Then
                        'When updating transferHour object, from object shall always be updated before to object, to calculate if item is ignored or not
                        If Not Htrans.calc_is_ignored Then
                            _received_item.actual_booked_qty = _received_item.actual_booked_qty + Htrans.qty
                        End If
                    Else
                        'When updating transferHour object, from object shall always be updated before to object, to calculate if item is ignored or not
                        If Not Htrans.calc_is_ignored Then
                            If Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ODI Then
                                _received_item.budget_odi_qty = _received_item.budget_odi_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_INIT Then
                                _received_item.budget_init_quoted_qty = _received_item.budget_init_quoted_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_BLW_THR Then
                                _received_item.budget_add_below_thr_qty = _received_item.budget_add_below_thr_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_APPR Then
                                _received_item.budget_add_awq_approved_qty = _received_item.budget_add_awq_approved_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_PEND Then
                                _received_item.budget_add_awq_sent_qty = _received_item.budget_add_awq_sent_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_INWK Then
                                _received_item.budget_add_awq_inwork_qty = _received_item.budget_add_awq_inwork_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_INIT Then
                                _received_item.budget_foc_init_qty = _received_item.budget_foc_init_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_BLW_THR Then
                                _received_item.budget_foc_add_blw_qty = _received_item.budget_foc_add_blw_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_APPR Then
                                _received_item.budget_foc_add_appr_qty = _received_item.budget_foc_add_appr_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_PEND Then
                                _received_item.budget_foc_add_pending_qty = _received_item.budget_foc_add_pending_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_INWK Then
                                _received_item.budget_foc_add_inwork_qty = _received_item.budget_foc_add_inwork_qty + Htrans.qty
                            Else
                                _calc_error_text = _calc_error_text & Chr(10) & "Transfer " & Htrans.tostring & " scope is unknown. Act " & _act_obj.getActKey & ", CC" & _cc_object.cc
                                _calc_is_in_error_state = True
                            End If
                        End If
                    End If
                ElseIf MConstants.GLB_MSTR_CTRL.labor_controlling_controller.getTransferedCC(Htrans.from_cc).cc = _cc_object.cc AndAlso Htrans.from_act_id = _act_obj.id Then
                    If Htrans.hours_type = MConstants.CONST_CONF_CTRL_HRS_TRANSF_TYPE_ACTUAL Then
                        If Htrans.actual_available_to_transfer >= Htrans.qty Then
                            Htrans.calc_is_ignored = False
                            _sent_item.actual_booked_qty = _sent_item.actual_booked_qty + Htrans.qty
                        Else
                            Htrans.calc_is_ignored = True
                        End If
                    Else
                        If Htrans.budget_available_to_transfer >= Htrans.qty Then
                            Htrans.calc_is_ignored = False
                            If Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ODI Then
                                _sent_item.budget_odi_qty = _sent_item.budget_odi_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_INIT Then
                                _sent_item.budget_init_quoted_qty = _sent_item.budget_init_quoted_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_BLW_THR Then
                                _sent_item.budget_add_below_thr_qty = _sent_item.budget_add_below_thr_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_APPR Then
                                _sent_item.budget_add_awq_approved_qty = _sent_item.budget_add_awq_approved_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_PEND Then
                                _sent_item.budget_add_awq_sent_qty = _sent_item.budget_add_awq_sent_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_INWK Then
                                _sent_item.budget_add_awq_inwork_qty = _sent_item.budget_add_awq_inwork_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_INIT Then
                                _sent_item.budget_foc_init_qty = _sent_item.budget_foc_init_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_BLW_THR Then
                                _sent_item.budget_foc_add_blw_qty = _sent_item.budget_foc_add_blw_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_APPR Then
                                _sent_item.budget_foc_add_appr_qty = _sent_item.budget_foc_add_appr_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_PEND Then
                                _sent_item.budget_foc_add_pending_qty = _sent_item.budget_foc_add_pending_qty + Htrans.qty
                            ElseIf Htrans.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_INWK Then
                                _sent_item.budget_foc_add_inwork_qty = _sent_item.budget_foc_add_inwork_qty + Htrans.qty
                            Else
                                _calc_error_text = _calc_error_text & Chr(10) & "Transfer " & Htrans.tostring & " scope is unknown. Act " & _act_obj.getActKey & ", CC" & _cc_object.cc
                                _calc_is_in_error_state = True
                            End If
                        Else
                            Htrans.calc_is_ignored = True
                        End If
                    End If
                Else
                    _calc_error_text = _calc_error_text & Chr(10) & "Transfer " & Htrans.tostring & " not match with labor controling entry " & _act_obj.getActKey & ", CC" & _cc_object.cc
                    _calc_is_in_error_state = True
                End If
            Next

            'actual per scope
            Dim actual_to_share = Me.tr_actual_booked_qty
            'init
            If actual_to_share > 0 AndAlso actual_to_share <= Me.tr_budget_init_qty Then
                _actual_init_qty = actual_to_share
                actual_to_share = 0
            Else
                _actual_init_qty = Me.tr_budget_init_qty
                actual_to_share = actual_to_share - Me.tr_budget_init_qty
            End If
            'init foc
            If actual_to_share > 0 AndAlso actual_to_share <= Me.tr_budget_foc_init_qty Then
                _actual_init_foc_qty = actual_to_share
                actual_to_share = 0
            Else
                _actual_init_foc_qty = Me.tr_budget_foc_init_qty
                actual_to_share = actual_to_share - Me.tr_budget_foc_init_qty
            End If
            'odi
            If actual_to_share > 0 AndAlso actual_to_share <= Me.tr_budget_odi_qty Then
                _actual_odi_qty = actual_to_share
                actual_to_share = 0
            Else
                _actual_odi_qty = Me.tr_budget_odi_qty
                actual_to_share = actual_to_share - Me.tr_budget_odi_qty
            End If
            'add foc
            Dim total_add_foc As Double = Me.tr_budget_foc_add_appr_qty + Me.tr_budget_foc_add_pending_qty + Me.tr_budget_foc_add_inwork_qty + Me.tr_budget_foc_add_blw_qty
            If actual_to_share > 0 AndAlso actual_to_share <= total_add_foc Then
                _actual_add_foc_qty = actual_to_share
                actual_to_share = 0
            Else
                _actual_add_foc_qty = total_add_foc
                actual_to_share = actual_to_share - total_add_foc
            End If
            'below thr
            If actual_to_share > 0 AndAlso actual_to_share <= Me.tr_budget_add_below_thr_qty Then
                _actual_add_blw_thr_qty = actual_to_share
                actual_to_share = 0
            Else
                _actual_add_blw_thr_qty = Me.tr_budget_add_below_thr_qty
                actual_to_share = actual_to_share - Me.tr_budget_add_below_thr_qty
            End If
            'awq pending
            If actual_to_share > 0 AndAlso actual_to_share <= Me.tr_budget_add_awq_sent_qty Then
                _actual_add_sent_qty = actual_to_share
                actual_to_share = 0
            Else
                _actual_add_sent_qty = Me.tr_budget_add_awq_sent_qty
                actual_to_share = actual_to_share - Me.tr_budget_add_awq_sent_qty
            End If
            'awq inwork
            If actual_to_share > 0 AndAlso actual_to_share <= Me.tr_budget_add_awq_inwork_qty Then
                _actual_add_inwork_qty = actual_to_share
                actual_to_share = 0
            Else
                _actual_add_inwork_qty = Me.tr_budget_add_awq_inwork_qty
                actual_to_share = actual_to_share - Me.tr_budget_add_awq_inwork_qty
            End If

            'justifications
            For Each just As CDevJust In deviationJustification
                Dim just_reason = just.reason
                If just.is_opening_eac Then
                    just_reason = MConstants.CONST_CONF_CTRL_JUST_OPENING_EAC_PREFIX & just.reason
                End If
                If just.type = MConstants.CONST_CONF_CTRL_JUST_OVERSHOOT Then
                    If Not overshoot_per_reason.ContainsKey(just_reason) Then
                        overshoot_per_reason.Add(just_reason, 0)
                    End If
                    overshoot_per_reason(just_reason) = overshoot_per_reason(just_reason) + just.qty
                    'scope
                    If just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ODI Then
                        _overshoot_just_odi_qty = _overshoot_just_odi_qty + just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_INIT Then
                        _overshoot_just_init_qty = _overshoot_just_init_qty + just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_BLW_THR Then
                        _overshoot_just_add_blw_thr_qty = _overshoot_just_add_blw_thr_qty + just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_APPR Then
                        _overshoot_just_add_appr_qty = _overshoot_just_add_appr_qty + just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_PEND Then
                        _overshoot_just_add_sent_qty = _overshoot_just_add_sent_qty + just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_INWK Then
                        _overshoot_just_add_inwork_qty = _overshoot_just_add_inwork_qty + just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_INIT Then
                        _overshoot_just_init_foc_qty = _overshoot_just_init_foc_qty + just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_BLW_THR Then
                        _overshoot_just_add_foc_qty = _overshoot_just_add_foc_qty + just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_APPR Then
                        _overshoot_just_add_foc_qty = _overshoot_just_add_foc_qty + just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_PEND Then
                        _overshoot_just_add_foc_qty = _overshoot_just_add_foc_qty + just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_INWK Then
                        _overshoot_just_add_foc_qty = _overshoot_just_add_foc_qty + just.qty
                    Else
                        _calc_error_text = _calc_error_text & Chr(10) & "Deviation Justification scope unknown " & just.tostring
                        _calc_is_in_error_state = True
                    End If
                Else
                    If Not saving_per_reason.ContainsKey(just_reason) Then
                        saving_per_reason.Add(just_reason, 0)
                    End If
                    saving_per_reason(just_reason) = saving_per_reason(just_reason) + just.qty
                    'scope
                    If just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ODI Then
                        _gain_just_odi_qty = _gain_just_odi_qty - just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_INIT Then
                        _gain_just_init_qty = _gain_just_init_qty - just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_BLW_THR Then
                        _gain_just_add_blw_thr_qty = _gain_just_add_blw_thr_qty - just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_APPR Then
                        _gain_just_add_appr_qty = _gain_just_add_appr_qty - just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_PEND Then
                        _gain_just_add_sent_qty = _gain_just_add_sent_qty - just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_INWK Then
                        _gain_just_add_inwork_qty = _gain_just_add_inwork_qty - just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_INIT Then
                        _gain_just_init_foc_qty = _gain_just_init_foc_qty - just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_BLW_THR Then
                        _gain_just_add_foc_qty = _gain_just_add_foc_qty - just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_APPR Then
                        _gain_just_add_foc_qty = _gain_just_add_foc_qty - just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_PEND Then
                        _gain_just_add_foc_qty = _gain_just_add_foc_qty - just.qty
                    ElseIf just.scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_INWK Then
                        _gain_just_add_foc_qty = _gain_just_add_foc_qty - just.qty
                    Else
                        _calc_error_text = _calc_error_text & Chr(10) & "Deviation Justification scope unknown " & just.tostring
                        _calc_is_in_error_state = True
                    End If
                End If
            Next


            If Me.budget_init_qty > 0 Then
                transfer_scope_list.Add(MConstants.CONST_CONF_LAB_CTRL_SCOPE_INIT, MConstants.listOfValueConf(CONST_CONF_LOV_LAB_CTRL_DEV_JUST_SCOPE_LIST)(CONST_CONF_LAB_CTRL_SCOPE_INIT).value)
            End If
            If Me.budget_add_below_thr_qty > 0 Then
                transfer_scope_list.Add(MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_BLW_THR, MConstants.listOfValueConf(CONST_CONF_LOV_LAB_CTRL_DEV_JUST_SCOPE_LIST)(CONST_CONF_LAB_CTRL_SCOPE_ADD_BLW_THR).value)
            End If
            If Me.budget_add_awq_approved_qty > 0 Then
                transfer_scope_list.Add(MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_APPR, MConstants.listOfValueConf(CONST_CONF_LOV_LAB_CTRL_DEV_JUST_SCOPE_LIST)(CONST_CONF_LAB_CTRL_SCOPE_ADD_APPR).value)
            End If
            If Me.budget_odi_qty > 0 Then
                transfer_scope_list.Add(MConstants.CONST_CONF_LAB_CTRL_SCOPE_ODI, MConstants.listOfValueConf(CONST_CONF_LOV_LAB_CTRL_DEV_JUST_SCOPE_LIST)(CONST_CONF_LAB_CTRL_SCOPE_ODI).value)
            End If

            If Me.budget_foc_init_qty > 0 Then
                transfer_scope_list.Add(MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_INIT, MConstants.listOfValueConf(CONST_CONF_LOV_LAB_CTRL_DEV_JUST_SCOPE_LIST)(CONST_CONF_LAB_CTRL_SCOPE_FOC_INIT).value)
            End If
            If Me.budget_foc_add_blw_qty > 0 Then
                transfer_scope_list.Add(MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_BLW_THR, MConstants.listOfValueConf(CONST_CONF_LOV_LAB_CTRL_DEV_JUST_SCOPE_LIST)(CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_BLW_THR).value)
            End If
            If Me.budget_foc_add_appr_qty > 0 Then
                transfer_scope_list.Add(MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_APPR, MConstants.listOfValueConf(CONST_CONF_LOV_LAB_CTRL_DEV_JUST_SCOPE_LIST)(CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_APPR).value)
            End If

            'CALCULATED
            Me.tr_budget_init_qty = Me.budget_init_qty - _sent_item.budget_init_qty + _received_item.budget_init_qty
            Me.tr_budget_add_below_thr_qty = Me.budget_add_below_thr_qty - _sent_item.budget_add_below_thr_qty + _received_item.budget_add_below_thr_qty
            Me.tr_budget_add_awq_inwork_qty = Me.budget_add_awq_inwork_qty - _sent_item.budget_add_awq_inwork_qty + _received_item.budget_add_awq_inwork_qty
            Me.tr_budget_add_awq_sent_qty = Me.budget_add_awq_sent_qty - _sent_item.budget_add_awq_sent_qty + _received_item.budget_add_awq_sent_qty
            Me.tr_budget_add_awq_pm_go_qty = Me.budget_add_awq_pm_go_qty - _sent_item.budget_add_awq_pm_go_qty + _received_item.budget_add_awq_pm_go_qty
            Me.tr_budget_add_awq_approved_qty = Me.budget_add_awq_approved_qty - _sent_item.budget_add_awq_approved_qty + _received_item.budget_add_awq_approved_qty
            Me.tr_budget_foc_init_qty = Me.budget_foc_init_qty - _sent_item.budget_foc_init_qty + _received_item.budget_foc_init_qty
            Me.tr_budget_foc_add_blw_qty = Me.budget_foc_add_blw_qty - _sent_item.budget_foc_add_blw_qty + _received_item.budget_foc_add_blw_qty
            Me.tr_budget_foc_add_appr_qty = Me.budget_foc_add_appr_qty - _sent_item.budget_foc_add_appr_qty + _received_item.budget_foc_add_appr_qty
            Me.tr_budget_foc_add_pending_qty = Me.budget_foc_add_pending_qty - _sent_item.budget_foc_add_pending_qty + _received_item.budget_foc_add_pending_qty
            Me.tr_budget_foc_add_inwork_qty = Me.budget_foc_add_inwork_qty - _sent_item.budget_foc_add_inwork_qty + _received_item.budget_foc_add_inwork_qty
            Me.tr_budget_foc_add_qty = Me.tr_budget_foc_add_inwork_qty + Me.tr_budget_foc_add_pending_qty + Me.tr_budget_foc_add_blw_qty + Me.tr_budget_foc_add_appr_qty
            Me.budget_foc_total_qty = Me.budget_foc_add_inwork_qty + Me.budget_foc_add_pending_qty + Me.budget_foc_add_blw_qty + Me.budget_foc_add_appr_qty + Me.budget_foc_init_qty
            Me.tr_budget_foc_total_qty = Me.tr_budget_foc_add_inwork_qty + Me.tr_budget_foc_add_pending_qty + Me.tr_budget_foc_add_blw_qty + Me.tr_budget_foc_add_appr_qty + Me.tr_budget_foc_init_qty
            Me.tr_budget_foc_total_approved_qty = Me.tr_budget_foc_add_blw_qty + Me.tr_budget_foc_add_appr_qty + Me.tr_budget_foc_init_qty
            Me.tr_budget_odi_qty = Me.budget_odi_qty - _sent_item.budget_odi_qty + _received_item.budget_odi_qty
            Me.tr_budget_total_approved_qty = Me.tr_budget_foc_total_approved_qty + Me.tr_budget_init_qty + Me.tr_budget_add_awq_approved_qty + Me.tr_budget_add_below_thr_qty + Me.tr_budget_odi_qty
            Me.tr_budget_total_qty = Me.tr_budget_total_approved_qty + Me.tr_budget_add_awq_sent_qty + Me.tr_budget_add_awq_inwork_qty + Me.tr_budget_foc_total_qty - Me.tr_budget_foc_total_approved_qty
            Me.self_budget_total_qty = getSelfBudget()
            Me.tr_actual_booked_qty = Me.actual_booked_qty - _sent_item.actual_booked_qty + _received_item.actual_booked_qty
            Me.deviation_appr_bef_just_qty = Me.tr_actual_booked_qty - Me.tr_budget_total_approved_qty
            Me.deviation_bef_just_qty = Me.tr_actual_booked_qty - Me.tr_budget_total_qty
            Me.deviation_appr_after_just_qty = Me.tr_actual_booked_qty - (Me.tr_budget_total_approved_qty + Me.overshoot_just_total_qty + Me.gain_just_total_qty)
            Me.deviation_after_just_qty = Me.tr_actual_booked_qty - (Me.tr_budget_total_qty + Me.overshoot_just_total_qty + Me.gain_just_total_qty)
            Me.tr_sent_budget_total_approved_qty = _sent_item.getSelfBudget
            Me.tr_received_budget_total_approved_qty = _received_item.getSelfBudget
            Me.tr_sent_actual_booked_qty = _sent_item.actual_booked_qty
            Me.tr_received_actual_booked_qty = _received_item.actual_booked_qty

            If calcul_parent Then
                updateParent()
            End If
        Catch ex As Exception
            _calc_is_in_error_state = True
            _calc_error_text = _calc_error_text & Chr(10) & "Calculation Error:" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace
        End Try
    End Sub

    'get self budget (used for sent and received items)
    Public Function getSelfBudget() As Double
        Return Me.budget_foc_total_qty + Me.budget_init_qty + Me.budget_add_awq_approved_qty + Me.budget_add_awq_sent_qty + Me.budget_add_awq_inwork_qty + Me.budget_add_below_thr_qty + Me.budget_odi_qty
    End Function

    'update parents
    Public Sub updateParent()
        Try
            'parent calcul
            For Each parent As CLaborControlMainViewObj In calculation_parents
                parent.updateFromChildren()
            Next
        Catch ex As Exception
            _calc_is_in_error_state = True
            _calc_error_text = _calc_error_text & Chr(10) & "Calculation Error While updating parents:" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace
        End Try
    End Sub

    'calculate
    Public Sub updateFromChildren()
        Try
            If Me.is_ctrl_obj_editable Then
                Exit Sub
            End If
            Me.setToZero()
            _received_item.setToZero()
            _received_item.setToZero()

            overshoot_per_reason.Clear()
            saving_per_reason.Clear()
            transfer_scope_list.Clear()
            _calc_is_in_error_state = False
            _calc_error_text = ""

            For Each child As CLaborControlMainViewObj In calculation_children
                Me.budget_init_quoted_qty = Me.budget_init_quoted_qty + child.budget_init_quoted_qty
                Me.budget_init_adj_qty = Me.budget_init_adj_qty + child.budget_init_adj_qty
                Me.budget_add_below_thr_qty = Me.budget_add_below_thr_qty + child.budget_add_below_thr_qty
                Me.budget_add_awq_inwork_qty = Me.budget_add_awq_inwork_qty + child.budget_add_awq_inwork_qty
                Me.budget_add_awq_sent_qty = Me.budget_add_awq_sent_qty + child.budget_add_awq_sent_qty
                Me.budget_add_awq_pm_go_qty = Me.budget_add_awq_pm_go_qty + child.budget_add_awq_pm_go_qty
                Me.budget_add_awq_approved_qty = Me.budget_add_awq_approved_qty + child.budget_add_awq_approved_qty
                Me.budget_foc_init_qty = Me.budget_foc_init_qty + child.budget_foc_init_qty
                Me.budget_foc_add_blw_qty = Me.budget_foc_add_blw_qty + child.budget_foc_add_blw_qty
                Me.budget_foc_add_appr_qty = Me.budget_foc_add_appr_qty + child.budget_foc_add_appr_qty
                Me.budget_foc_add_pending_qty = Me.budget_foc_add_pending_qty + child.budget_foc_add_pending_qty
                Me.budget_foc_add_inwork_qty = Me.budget_foc_add_inwork_qty + child.budget_foc_add_inwork_qty
                Me.budget_odi_qty = Me.budget_odi_qty + child.budget_odi_qty
                Me.budget_tba_qty = Me.budget_tba_qty + child.budget_tba_qty
                Me.budget_plan_qty = Me.budget_plan_qty + child.budget_plan_qty
                Me.budget_risk_reserve_approved_qty = Me.budget_risk_reserve_approved_qty + child.budget_risk_reserve_approved_qty
                Me.budget_risk_reserve_pending_qty = Me.budget_risk_reserve_pending_qty + child.budget_risk_reserve_pending_qty
                Me.actual_booked_qty = Me.actual_booked_qty + child.actual_booked_qty
                Me.actual_init_qty = Me.actual_init_qty + child.actual_init_qty
                Me.actual_init_foc_qty = Me.actual_init_foc_qty + child.actual_init_foc_qty
                Me.actual_odi_qty = Me.actual_odi_qty + child.actual_odi_qty
                Me.actual_add_foc_qty = Me.actual_add_foc_qty + child.actual_add_foc_qty
                Me.actual_add_blw_thr_qty = Me.actual_add_blw_thr_qty + child.actual_add_blw_thr_qty
                Me.actual_add_appr_qty = Me.actual_add_appr_qty + child.actual_add_appr_qty
                Me.actual_add_sent_qty = Me.actual_add_sent_qty + child.actual_add_sent_qty
                Me.actual_add_inwork_qty = Me.actual_add_inwork_qty + child.actual_add_inwork_qty
                Me.overshoot_just_init_qty = Me.overshoot_just_init_qty + child.overshoot_just_init_qty
                Me.overshoot_just_init_foc_qty = Me.overshoot_just_init_foc_qty + child.overshoot_just_init_foc_qty
                Me.overshoot_just_odi_qty = Me.overshoot_just_odi_qty + child.overshoot_just_odi_qty
                Me.overshoot_just_add_foc_qty = Me.overshoot_just_add_foc_qty + child.overshoot_just_add_foc_qty
                Me.overshoot_just_add_blw_thr_qty = Me.overshoot_just_add_blw_thr_qty + child.overshoot_just_add_blw_thr_qty
                Me.overshoot_just_add_appr_qty = Me.overshoot_just_add_appr_qty + child.overshoot_just_add_appr_qty
                Me.overshoot_just_add_sent_qty = Me.overshoot_just_add_sent_qty + child.overshoot_just_add_sent_qty
                Me.overshoot_just_add_inwork_qty = Me.overshoot_just_add_inwork_qty + child.overshoot_just_add_inwork_qty
                Me.gain_just_init_foc_qty = Me.gain_just_init_foc_qty + child.gain_just_init_foc_qty
                Me.gain_just_odi_qty = Me.gain_just_odi_qty + child.gain_just_odi_qty
                Me.gain_just_add_foc_qty = Me.gain_just_add_foc_qty + child.gain_just_add_foc_qty
                Me.gain_just_add_blw_thr_qty = Me.gain_just_add_blw_thr_qty + child.gain_just_add_blw_thr_qty
                Me.gain_just_add_appr_qty = Me.gain_just_add_appr_qty + child.gain_just_add_appr_qty
                Me.gain_just_add_sent_qty = Me.gain_just_add_sent_qty + child.gain_just_add_sent_qty
                Me.gain_just_add_inwork_qty = Me.gain_just_add_inwork_qty + child.gain_just_add_inwork_qty
                'CALCULATED
                Me.tr_budget_init_qty = Me.tr_budget_init_qty + child.tr_budget_init_qty
                Me.tr_budget_add_below_thr_qty = Me.tr_budget_add_below_thr_qty + child.tr_budget_add_below_thr_qty
                Me.tr_budget_add_awq_inwork_qty = Me.tr_budget_add_awq_inwork_qty + child.tr_budget_add_awq_inwork_qty
                Me.tr_budget_add_awq_sent_qty = Me.tr_budget_add_awq_sent_qty + child.tr_budget_add_awq_sent_qty
                Me.tr_budget_add_awq_pm_go_qty = Me.tr_budget_add_awq_pm_go_qty + child.tr_budget_add_awq_pm_go_qty
                Me.tr_budget_add_awq_approved_qty = Me.tr_budget_add_awq_approved_qty + child.tr_budget_add_awq_approved_qty
                Me.tr_budget_foc_init_qty = Me.tr_budget_foc_init_qty + child.tr_budget_foc_init_qty
                Me.tr_budget_foc_add_blw_qty = Me.tr_budget_foc_add_blw_qty + child.tr_budget_foc_add_blw_qty
                Me.tr_budget_foc_add_appr_qty = Me.tr_budget_foc_add_appr_qty + child.tr_budget_foc_add_appr_qty
                Me.tr_budget_foc_add_pending_qty = Me.tr_budget_foc_add_pending_qty + child.tr_budget_foc_add_pending_qty
                Me.tr_budget_foc_add_inwork_qty = Me.tr_budget_foc_add_inwork_qty + child.tr_budget_foc_add_inwork_qty
                Me.tr_budget_foc_add_qty = Me.tr_budget_foc_add_qty + child.tr_budget_foc_add_qty
                Me.budget_foc_total_qty = Me.budget_foc_total_qty + child.budget_foc_total_qty
                Me.tr_budget_foc_total_qty = Me.tr_budget_foc_total_qty + child.tr_budget_foc_total_qty
                Me.tr_budget_foc_total_approved_qty = Me.tr_budget_foc_total_approved_qty + child.tr_budget_foc_total_approved_qty
                Me.tr_budget_odi_qty = Me.tr_budget_odi_qty + child.tr_budget_odi_qty
                Me.tr_budget_total_qty = Me.tr_budget_total_qty + child.tr_budget_total_qty
                Me.tr_budget_total_approved_qty = Me.tr_budget_total_approved_qty + child.tr_budget_total_approved_qty
                Me.self_budget_total_qty = Me.self_budget_total_qty + child.self_budget_total_qty
                Me.tr_actual_booked_qty = Me.tr_actual_booked_qty + child.tr_actual_booked_qty
                Me.deviation_appr_bef_just_qty = Me.deviation_appr_bef_just_qty + child.deviation_appr_bef_just_qty
                Me.deviation_bef_just_qty = Me.deviation_bef_just_qty + child.deviation_bef_just_qty
                Me.deviation_appr_after_just_qty = Me.deviation_appr_after_just_qty + child.deviation_appr_after_just_qty
                Me.deviation_after_just_qty = Me.deviation_after_just_qty + child.deviation_after_just_qty
                Me.tr_sent_budget_total_approved_qty = Me.tr_sent_budget_total_approved_qty + child.tr_sent_budget_total_approved_qty
                Me.tr_received_budget_total_approved_qty = Me.tr_received_budget_total_approved_qty + child.tr_received_budget_total_approved_qty
                Me.tr_sent_actual_booked_qty = Me.tr_sent_actual_booked_qty + child.tr_sent_actual_booked_qty
                Me.tr_received_actual_booked_qty = Me.tr_received_actual_booked_qty + child.tr_received_actual_booked_qty

                For Each just_cause As String In child.saving_per_reason.Keys
                    If Not saving_per_reason.ContainsKey(just_cause) Then
                        saving_per_reason.Add(just_cause, 0)
                    End If
                    saving_per_reason(just_cause) = saving_per_reason(just_cause) + child.saving_per_reason(just_cause)
                Next
                For Each just_cause As String In child.overshoot_per_reason.Keys
                    If Not overshoot_per_reason.ContainsKey(just_cause) Then
                        overshoot_per_reason.Add(just_cause, 0)
                    End If
                    overshoot_per_reason(just_cause) = overshoot_per_reason(just_cause) + child.overshoot_per_reason(just_cause)
                Next
            Next

        Catch ex As Exception
            _calc_is_in_error_state = True
            _calc_error_text = _calc_error_text & Chr(10) & "Calculation Error:" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace
        End Try
    End Sub


    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    'text to display any error
    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
        End Set
    End Property

    Public ReadOnly Property uniqueID() As String
        Get
            Return getKey()
        End Get
    End Property

    Public Function getKey() As String
        Return _act_obj.id & MConstants.SEP_DEFAULT & _cc_object.cc
    End Function
    Public ReadOnly Property view_str() As String
        Get
            Return Me.tostring()
        End Get
    End Property

    'to string
    Public Overrides Function tostring() As String
        If Not IsNothing(_act_obj) AndAlso Not IsNothing(_cc_object) Then
            Return _act_obj.proj & "-" & _act_obj.act & " JC-" & _act_obj.job_card & " CC-" & _cc_object.cc & " **" & _act_obj.act_desc & " **" & _cc_object.description
        Else
            Return ""
        End If
    End Function

End Class
