﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices
Imports System.Text
Imports Equin.ApplicationFramework

Public Class CProjectCalculator
    Implements INotifyPropertyChanged

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Sub New()
        _listOfCalculations = New Dictionary(Of String, CScopeAtivityCalcEntry)
        'subtotal or total before discount and surcharge
        _sub_total = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_SUBTOTAL).value)
        _listOfCalculations.Add(_sub_total.entry_id, _sub_total)
        'subtotal dimensions : init, add appr, add pending ...
        _sub_total_scope_init = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_INIT).value)
        _listOfCalculations.Add(_sub_total_scope_init.entry_id, _sub_total_scope_init)
        _sub_total_scope_init_adj = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_INIT_ADJ).value)
        _listOfCalculations.Add(_sub_total_scope_init_adj.entry_id, _sub_total_scope_init_adj)
        _sub_total_scope_init_total = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_INIT_TOTAL).value)
        _listOfCalculations.Add(_sub_total_scope_init_total.entry_id, _sub_total_scope_init_total)
        _sub_total_scope_add_below_thr = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_ADD_BELOW_THR).value)
        _listOfCalculations.Add(_sub_total_scope_add_below_thr.entry_id, _sub_total_scope_add_below_thr)
        _sub_total_scope_add_appr = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_ADD_APPR).value)
        _listOfCalculations.Add(_sub_total_scope_add_appr.entry_id, _sub_total_scope_add_appr)
        _sub_total_scope_add_blwthr_and_appr = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_ADD_BLWTHR_AND_APPR).value)
        _listOfCalculations.Add(_sub_total_scope_add_blwthr_and_appr.entry_id, _sub_total_scope_add_blwthr_and_appr)
        _sub_total_scope_add_pending = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_ADD_PENDING).value)
        _listOfCalculations.Add(_sub_total_scope_add_pending.entry_id, _sub_total_scope_add_pending)
        _sub_total_scope_add_total = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_ADD_TOTAL).value)
        _listOfCalculations.Add(_sub_total_scope_add_total.entry_id, _sub_total_scope_add_total)
        'total
        _total_after_disc_surch = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_TOTAL).value)
        _listOfCalculations.Add(_total_after_disc_surch.entry_id, _total_after_disc_surch)
        'ecotax
        _eco_tax = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_ECOTAX).value)
        _listOfCalculations.Add(_eco_tax.entry_id, _eco_tax)
        'gum
        _gum = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_GUM).value)
        _listOfCalculations.Add(_gum.entry_id, _gum)
        'surcharge total
        _surcharge_total = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_SURCH_TOTAL).value)
        _listOfCalculations.Add(_surcharge_total.entry_id, _surcharge_total)
        'discount_total total
        _discount_total = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_DISC_TOTAL).value)
        _listOfCalculations.Add(_discount_total.entry_id, _discount_total)
        'NTE total discount
        _nte_total = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_NTE_TOTAL).value)
        _listOfCalculations.Add(_nte_total.entry_id, _nte_total)
        'NTE total gross
        _nte_gross_total = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_NTE_GROSS_TOTAL).value)
        _listOfCalculations.Add(_nte_gross_total.entry_id, _nte_gross_total)
        'NTE total chargeable
        _nte_chargeable_total = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_NTE_CHARG_TOTAL).value)
        _listOfCalculations.Add(_nte_chargeable_total.entry_id, _nte_chargeable_total)

        'subtotal after surcharge
        _sub_total_after_surcharge_bef_disc = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_SUBTOTAL_AFT_SUR).value)
        _listOfCalculations.Add(_sub_total_after_surcharge_bef_disc.entry_id, _sub_total_after_surcharge_bef_disc)
        'dp
        _dp_total = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_DP_TOTAL).value)
        _listOfCalculations.Add(_dp_total.entry_id, _dp_total)
        _dp_unpaid_not_due = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_DP_UNPAID_NOT_DUE).value)
        _listOfCalculations.Add(_dp_unpaid_not_due.entry_id, _dp_unpaid_not_due)
        _dp_un_paid = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_DP_UNPAID).value)
        _listOfCalculations.Add(_dp_un_paid.entry_id, _dp_un_paid)
        _dp_paid = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_DP_PAID).value)
        _listOfCalculations.Add(_dp_paid.entry_id, _dp_paid)
        'ooutstanding
        _outstanding = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_OUTSTANDING).value)
        _listOfCalculations.Add(_outstanding.entry_id, _outstanding)
        'AWQ sent pending
        _sent_awq_pending = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_AWQ_SUM_SENT_AWQ_PENDING).value)
        _listOfCalculations.Add(_sent_awq_pending.entry_id, _sent_awq_pending)
        'AWQ sent approved
        _sent_awq_approved = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_AWQ_SUM_SENT_AWQ_APPROVED).value)
        _listOfCalculations.Add(_sent_awq_approved.entry_id, _sent_awq_approved)
        'AWQ sent pending & Approved
        _sent_awq_approved_pending = New CScopeAtivityCalcEntry(MConstants.constantConf(MConstants.CONST_CONF_PROJ_AWQ_SUM_SENT_AWQ_PENDING_AND_APP).value)
        _listOfCalculations.Add(_sent_awq_approved_pending.entry_id, _sent_awq_approved_pending)

        'discount 
        _listOfDiscount = New Dictionary(Of Long, CScopeAtivityCalcEntry)
        'nte
        _listOfGrossNTE = New Dictionary(Of Long, CScopeAtivityCalcEntry)
        _listNTE = New Dictionary(Of Long, CScopeAtivityCalcEntry)
        'list of DP
        _listDP = New Dictionary(Of String, CScopeAtivityCalcEntry)
    End Sub

    Public Sub clear()
        For Each entry As CScopeAtivityCalcEntry In _listOfCalculations.Values
            entry.setEntryToZero()
        Next
        'discount 
        _listOfDiscount.Clear()
        'nte
        _listOfGrossNTE.Clear()
        _listNTE.Clear()
        'dp
        _listDP.Clear()
    End Sub
    Public Sub update(payer As String, calc_args as CProjCalculatorArgs)
        Try
            clear()

            'initialize errors
            _calc_error_text = ""
            Dim project As CProjectModel = MConstants.GLB_MSTR_CTRL.project_controller.project

            'init ecotax and gum objects
            Dim ecotaxObj As CSurcharge = Nothing
            Dim gumObj As CSurcharge = Nothing

            If MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CUST_REPORT_SURCH_APPL_LIST).ContainsKey(payer) Then
                'surcharges are applicable to payer
                For Each surc As CSurcharge In MConstants.GLB_MSTR_CTRL.billing_cond_controller.ecoTaxList.DataSource
                    If surc.label = project.eco_tax Then
                        ecotaxObj = surc
                    End If
                Next
                For Each surc As CSurcharge In MConstants.GLB_MSTR_CTRL.billing_cond_controller.GUMList.DataSource
                    If surc.label = project.gum Then
                        gumObj = surc
                    End If
                Next
                If IsNothing(gumObj) OrElse IsNothing(ecotaxObj) Then
                    Throw New Exception("GUM and ECOTAX must not be empty")
                End If

                'load surcharge dynamics labels
                Dim sb As New StringBuilder()
                sb.AppendFormat(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_ECOTAX_TEXT).value, ecotaxObj.rate.ToString("P2"), ecotaxObj.cap_value.ToString("N2") & MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(ecotaxObj.curr).value)
                _eco_tax.label = sb.ToString
                sb.Clear()
                sb.AppendFormat(MConstants.constantConf(MConstants.CONST_CONF_PROJ_SUM_GUM_TEXT).value, gumObj.rate.ToString("P2"), gumObj.cap_value.ToString("N2") & MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(gumObj.curr).value)
                _gum.label = sb.ToString
            Else
                'load surcharge dynamics labels
                _eco_tax.label = "Ecotax Not : Applicable"
                _gum.label = "General Material Surcharge : Not Applicable"
            End If

            Dim total_act_after_disc As CScopeAtivityCalcEntry = CScopeAtivityCalcEntry.getEmpty

            'handle cancelled quotes entries. if quote entry has been cancelled, put values and adjusted values in sum for init and adj init scopes
            For Each quoteE As CSalesQuoteEntry In MConstants.GLB_MSTR_CTRL.ini_scope_controller.quote_entries_list.DataSource
                If quoteE.isInitCancelled AndAlso Not IsNothing(quoteE.quote_obj) AndAlso quoteE.quote_obj.payer_obj.payer = payer Then
                    'subtotal init
                    sub_total_scope_init.addValuesFrom(quoteE.getInitCancelledEntry)
                    'subtotal init adj
                    sub_total_scope_init_adj.addValuesFrom(quoteE.getInitAdjCancelledEntry)
                End If
            Next

            'item to collect NTEN with no notes

            Dim nten_with_no_note As New CScopeAtivityCalcEntry("")
            'get all default NTE if any
            Dim def_lab_ntey As CNotToExceed = Nothing
            Dim def_lab_nten As CNotToExceed = Nothing
            Dim default_mat_nte_list As New Dictionary(Of String, CNotToExceed)

            Dim is_any_default_mat_nte As Boolean = False
            Dim is_any_default_lab_nte As Boolean = False
            For Each nteObjView As ObjectView(Of CNotToExceed) In MConstants.GLB_MSTR_CTRL.billing_cond_controller.nteList
                If nteObjView.Object.is_default Then
                    'lab
                    If MUtils.isLaborApplicableLabMat(nteObjView.Object.lab_mat_applicability) Then
                        If nteObjView.Object.isNTEYes Then
                            def_lab_ntey = nteObjView.Object
                        Else
                            def_lab_nten = nteObjView.Object
                        End If
                        is_any_default_lab_nte = True
                    End If
                    'mat
                    If MUtils.isMaterialApplicableLabMat(nteObjView.Object.lab_mat_applicability) Then
                        MUtils.addKeyValIfNotExistsOrReplace(Of CNotToExceed)(default_mat_nte_list, nteObjView.Object.nte_type & "_" & MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_KEY).value, nteObjView.Object)
                        is_any_default_mat_nte = True
                    End If
                    'rep
                    If MUtils.isRepairApplicableLabMat(nteObjView.Object.lab_mat_applicability) Then
                        MUtils.addKeyValIfNotExistsOrReplace(Of CNotToExceed)(default_mat_nte_list, nteObjView.Object.nte_type & "_" & MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_REP_KEY).value, nteObjView.Object)
                        is_any_default_mat_nte = True
                    End If
                    'frgh
                    If MUtils.isFreightApplicableLabMat(nteObjView.Object.lab_mat_applicability) Then
                        MUtils.addKeyValIfNotExistsOrReplace(Of CNotToExceed)(default_mat_nte_list, nteObjView.Object.nte_type & "_" & MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_FREIGHT_KEY).value, nteObjView.Object)
                        is_any_default_mat_nte = True
                    End If
                    'serv
                    If MUtils.isServApplicableLabMat(nteObjView.Object.lab_mat_applicability) Then
                        MUtils.addKeyValIfNotExistsOrReplace(Of CNotToExceed)(default_mat_nte_list, nteObjView.Object.nte_type & "_" & MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_SERV_KEY).value, nteObjView.Object)
                        is_any_default_mat_nte = True
                    End If
                End If
            Next
            'get list of unique mat nte
            Dim unique_default_nte_list As New List(Of CNotToExceed)
            For Each _uniq_nte As CNotToExceed In default_mat_nte_list.Values
                If Not unique_default_nte_list.Contains(_uniq_nte) Then
                    unique_default_nte_list.Add(_uniq_nte)
                End If
            Next

            For Each act As CScopeActivity In MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource
                Dim _calc_entry As CScopeAtivityCalcEntry
                Dim _act_approved_awq As CScopeAtivityCalcEntry = Nothing
                Dim _act_pending_awq As CScopeAtivityCalcEntry = Nothing
                Dim actComment As String = ""
                If Not String.IsNullOrWhiteSpace(act.comment) Then
                    actComment = act.comment & Chr(10)
                End If
                'ignore if cost collector
                If act.is_cost_collector Then
                    Continue For
                End If

                'ignore if hidden
                If act.is_hidden Then
                    _calc_entry = act.calculator.getEntry(payer, CScopeActivityCalculator.TOTAL, CScopeActivityCalculator.BEFORE_DISC_SUFF)
                    If IsNothing(_calc_entry) OrElse _calc_entry.is_zero Then
                        Continue For
                    End If
                End If

                If act.is_fake_activity AndAlso Not act.isActLinkedToAnyNonCancelledAWQ Then
                    Continue For
                End If

                'gross subtotal
                _calc_entry = act.calculator.getEntry(payer, CScopeActivityCalculator.TOTAL, CScopeActivityCalculator.BEFORE_DISC_SUFF)
                If Not IsNothing(_calc_entry) Then
                    sub_total.addValuesFrom(_calc_entry)
                Else
                    calc_error_text = calc_error_text & Chr(10) & "Activity " & act.tostring & " cannot find total before discount calculation"
                End If
                'net subtotal
                _calc_entry = act.calculator.getEntry(payer, CScopeActivityCalculator.TOTAL, CScopeActivityCalculator.AFTER_DISC_SUFF)
                If Not IsNothing(_calc_entry) Then
                    total_act_after_disc.addValuesFrom(_calc_entry)
                Else
                    calc_error_text = calc_error_text & Chr(10) & "Activity " & act.tostring & " cannot find total after discount calculation"
                End If

                'init scope
                For Each quoActLk As CSaleQuoteEntryToActLink In act.quote_entry_link_list
                    If Not IsNothing(quoActLk.init_quote_entry_obj) Then
                        If Not Object.Equals(quoActLk.init_quote_entry_obj.main_activity_obj, act) AndAlso act.is_hidden Then
                            Continue For
                        End If
                        'subtotal init
                        _calc_entry = act.calculator.getEntry(payer, quoActLk.init_quote_entry_obj.calc_entry_scope, CScopeActivityCalculator.BEFORE_DISC_SUFF)
                        If Not IsNothing(_calc_entry) Then
                            sub_total_scope_init.addValuesFrom(_calc_entry)
                            sub_total_scope_init_total.addValuesFrom(_calc_entry)
                            'add to list to report
                            If Not IsNothing(calc_args.project_details_scope_list) Then
                                'comment
                                _calc_entry.comment = actComment & quoActLk.init_quote_entry_obj.comment
                                calc_args.project_details_scope_list.Add(_calc_entry)
                            End If
                        End If

                        'subtotal init adj
                        _calc_entry = act.calculator.getEntry(payer, quoActLk.init_quote_entry_obj.calc_entry_scope_adj, CScopeActivityCalculator.BEFORE_DISC_SUFF)
                        If Not IsNothing(_calc_entry) Then
                            sub_total_scope_init_adj.addValuesFrom(_calc_entry)
                            sub_total_scope_init_total.addValuesFrom(_calc_entry)
                            'add to list to report
                            If Not IsNothing(calc_args.project_details_scope_list) Then
                                'comment
                                _calc_entry.comment = actComment & quoActLk.init_quote_entry_obj.adj_comment
                                calc_args.project_details_scope_list.Add(_calc_entry)
                            End If
                        End If
                    End If
                Next
                'subtotal add below threshold
                Dim is_add_scope_below_thr As Boolean = False
                _calc_entry = act.calculator.getEntry(payer, CScopeActivityCalculator.ADD, CScopeActivityCalculator.BEFORE_DISC_SUFF)
                If Not IsNothing(_calc_entry) Then
                    sub_total_scope_add_below_thr.addValuesFrom(_calc_entry)
                    sub_total_scope_add_total.addValuesFrom(_calc_entry)
                    sub_total_scope_add_blwthr_and_appr.addValuesFrom(_calc_entry)
                    is_add_scope_below_thr = True
                    'add to list to report
                    If Not IsNothing(calc_args.project_details_scope_list) Then
                        If _calc_entry.is_zero Then
                            'if add and no material or hours, set scope label as additional, not below threshold
                            _calc_entry.init_add_label = MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD_REPORT_NO_PRICE).value
                        End If
                        'comment
                        _calc_entry.comment = act.comment
                        calc_args.project_details_scope_list.Add(_calc_entry)
                    End If
                End If
                'add with awq
                If Not IsNothing(act.awq_master_obj) Then
                    If Object.Equals(act.awq_master_obj.main_activity_obj, act) Then
                        'to track if act is linked to an awq that has not been sent or is no more open
                        Dim has_sent_open_or_closed As Boolean = False
                        Dim is_latest_awq_in_work As Boolean = False
                        For Each awq As CAWQuotation In act.awq_master_obj.revision_list
                            Dim listO As List(Of CScopeAtivityCalcEntry) = CAWQuotationCalucaltor.getAWQPayerSummary(awq, payer)
                            If Not IsNothing(listO) Then
                                _calc_entry = listO(2)
                                If awq.is_approved_closed Then
                                    has_sent_open_or_closed = True
                                    sub_total_scope_add_appr.addValuesFrom(_calc_entry)
                                    sub_total_scope_add_total.addValuesFrom(_calc_entry)
                                    sub_total_scope_add_blwthr_and_appr.addValuesFrom(_calc_entry)
                                    'add to list to report
                                    If Not IsNothing(calc_args.project_details_scope_list) Then
                                        'can be many awq approved
                                        If IsNothing(_act_approved_awq) Then
                                            _act_approved_awq = CScopeAtivityCalcEntry.getEmpty
                                            _act_approved_awq.act_obj = act
                                            _act_approved_awq.init_add = CScopeActivityCalculator.ADD_AWQ
                                            _act_approved_awq.entry_scope = awq.reference_revision
                                            _act_approved_awq.entry_scope_label = awq.reference_revision
                                            'comment
                                            _act_approved_awq.comment = actComment & awq.comments

                                            'copy values and properties
                                            _act_approved_awq.addValuesFrom(_calc_entry)
                                            _act_approved_awq.customer_status = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK)(MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_APPROVED_KEY).value).value
                                            _act_approved_awq.business_object = awq
                                            calc_args.project_details_scope_list.Add(_act_approved_awq)
                                        Else
                                            'latest awq rev
                                            _act_approved_awq.entry_scope_label = awq.reference_revision
                                            _act_approved_awq.addValuesFrom(_calc_entry)
                                        End If
                                    End If
                                ElseIf awq.is_open_and_sent_to_customer Then
                                    has_sent_open_or_closed = True
                                    'pending
                                    sub_total_scope_add_pending.addValuesFrom(_calc_entry)
                                    sub_total_scope_add_total.addValuesFrom(_calc_entry)
                                    'add to list to report
                                    If Not IsNothing(calc_args.project_details_scope_list) Then
                                        _act_pending_awq = CScopeAtivityCalcEntry.getEmpty
                                        _act_pending_awq.act_obj = act
                                        _act_pending_awq.init_add = CScopeActivityCalculator.ADD_AWQ
                                        _act_pending_awq.entry_scope = awq.reference_revision
                                        _act_pending_awq.entry_scope_label = awq.reference_revision
                                        'comment
                                        _act_pending_awq.comment = actComment & awq.comments
                                        _act_pending_awq.addValuesFrom(_calc_entry)
                                        _act_pending_awq.customer_status = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK)(MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_PENDING_KEY).value).value
                                        _act_pending_awq.business_object = awq

                                        calc_args.project_details_scope_list.Add(_act_pending_awq)
                                    End If
                                Else
                                    'ignore cancelled or revised awq
                                    If awq.is_latest_rev_calc AndAlso awq.is_in_work Then
                                        is_latest_awq_in_work = True
                                    End If
                                End If
                            End If
                        Next

                        'add mat info default text

                        If Not IsNothing(_act_approved_awq) AndAlso Not act.is_small_mat Then
                            If String.IsNullOrWhiteSpace(_calc_entry.mat_info_calc) Then
                                _act_approved_awq.mat_info_calc = MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_LINE_START).value & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_SMALL_MAT_TXT).value
                            Else
                                Dim mat_info_def = MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_LINE_START).value & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_SMALL_MAT_TXT).value
                                If _act_approved_awq.mat_info_calc.Contains(mat_info_def) Then
                                    _act_approved_awq.mat_info_calc = _act_approved_awq.mat_info_calc.Replace(Chr(10) & mat_info_def, "")
                                    _act_approved_awq.mat_info_calc = _act_approved_awq.mat_info_calc.Replace(mat_info_def, "")
                                End If
                                If String.IsNullOrWhiteSpace(_act_approved_awq.mat_info_calc) Then
                                    _act_approved_awq.mat_info_calc = mat_info_def
                                Else
                                    _act_approved_awq.mat_info_calc = _act_approved_awq.mat_info_calc & Chr(10) & mat_info_def
                                End If
                            End If
                        End If
                        If Not IsNothing(_act_pending_awq) AndAlso Not act.is_small_mat Then
                            If String.IsNullOrWhiteSpace(_calc_entry.mat_info_calc) Then
                                _act_pending_awq.mat_info_calc = MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_LINE_START).value & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_SMALL_MAT_TXT).value
                            Else
                                Dim mat_info_def = MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_LINE_START).value & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_SMALL_MAT_TXT).value
                                If _act_pending_awq.mat_info_calc.Contains(mat_info_def) Then
                                    _act_pending_awq.mat_info_calc = _act_pending_awq.mat_info_calc.Replace(Chr(10) & mat_info_def, "")
                                    _act_pending_awq.mat_info_calc = _act_pending_awq.mat_info_calc.Replace(mat_info_def, "")
                                End If
                                If String.IsNullOrWhiteSpace(_act_pending_awq.mat_info_calc) Then
                                    _act_pending_awq.mat_info_calc = mat_info_def
                                Else
                                    _act_pending_awq.mat_info_calc = _act_pending_awq.mat_info_calc & Chr(10) & mat_info_def
                                End If
                            End If
                        End If

                        If Not IsNothing(calc_args.project_details_scope_list) Then
                            If is_latest_awq_in_work AndAlso Not has_sent_open_or_closed AndAlso Not is_add_scope_below_thr Then
                                'check if payer is in any labor or mat
                                Dim payer_found As Boolean = False
                                For Each _payer As CPayer In act.calculator.calc_payer_list
                                    If _payer.payer = payer Then
                                        payer_found = True
                                    End If
                                Next
                                If payer_found Then
                                    'case of an AWQ that has not been sent or is cancelled. If cancelled user must set it as cancelled in Red Mark
                                    'consider it only if there is no ADD scope
                                    _calc_entry = CScopeAtivityCalcEntry.getEmpty
                                    _calc_entry.act_obj = act
                                    _calc_entry.comment = act.comment
                                    _calc_entry.init_add = CScopeActivityCalculator.ADD
                                    _calc_entry.init_add_label = MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD_REPORT_NO_PRICE).value
                                    _calc_entry.labor_sold_calc_str = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_PRICE_RED_MARK)(MConstants.constantConf(MConstants.CONST_CONF_LOV_RED_MARK_TBA_KEY).value).value
                                    _calc_entry.mat_info_calc = MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_LINE_START).value & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_SMALL_MAT_TXT).value
                                    calc_args.project_details_scope_list.Add(_calc_entry)
                                End If
                            End If
                        End If
                    Else
                        If Not IsNothing(calc_args.project_details_scope_list) Then
                            'if dependant activity that is not hidden, report only if there is an entry for this payer
                            If Not act.is_hidden Then
                                _calc_entry = Nothing
                                Dim reportReferToQuote As Boolean = False
                                Dim referToAWQ As CAWQuotation = Nothing
                                For Each _awq As CAWQuotation In act.awq_master_obj.revision_list
                                    If _awq.is_approved_closed OrElse _awq.is_open_and_sent_to_customer Then
                                        Dim listO As List(Of CScopeAtivityCalcEntry) = CAWQuotationCalucaltor.getAWQPayerSummary(_awq, payer)
                                        If Not IsNothing(listO) Then
                                            reportReferToQuote = True
                                            referToAWQ = _awq
                                        End If
                                    End If
                                Next
                                If reportReferToQuote Then
                                    'consider it only if there is no ADD scope
                                    _calc_entry = CScopeAtivityCalcEntry.getEmpty
                                    _calc_entry.act_obj = act
                                    _calc_entry.comment = act.comment
                                    _calc_entry.entry_scope_label = referToAWQ.reference_revision
                                    'put ADD to get NA ont customer feedback
                                    _calc_entry.init_add = CScopeActivityCalculator.ADD_AWQ
                                    _calc_entry.labor_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                                    _calc_entry.mat_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                                    _calc_entry.rep_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                                    _calc_entry.freight_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                                    _calc_entry.serv_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                                    _calc_entry.mat_info_calc = MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_LINE_START).value & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_SMALL_MAT_TXT).value
                                    'customer feedback
                                    If referToAWQ.is_approved_closed Then
                                        _calc_entry.customer_status = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK)(MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_APPROVED_KEY).value).value
                                    Else
                                        _calc_entry.customer_status = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK)(MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_PENDING_KEY).value).value
                                    End If
                                    _calc_entry.business_object = referToAWQ
                                    calc_args.project_details_scope_list.Add(_calc_entry)
                                Else
                                    'AWQ is inwork but not sent
                                    _calc_entry = CScopeAtivityCalcEntry.getEmpty
                                    _calc_entry.act_obj = act
                                    _calc_entry.comment = act.comment
                                    _calc_entry.init_add = CScopeActivityCalculator.ADD
                                    _calc_entry.init_add_label = MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD_REPORT_NO_PRICE).value
                                    _calc_entry.labor_sold_calc_str = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_PRICE_RED_MARK)(MConstants.constantConf(MConstants.CONST_CONF_LOV_RED_MARK_TBA_KEY).value).value
                                    _calc_entry.mat_info_calc = MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_LINE_START).value & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_SMALL_MAT_TXT).value
                                    calc_args.project_details_scope_list.Add(_calc_entry)
                                End If
                            End If
                        End If
                    End If
                End If

                'activity discount
                _calc_entry = act.calculator.getEntry(payer, CScopeActivityCalculator.TOTAL, CScopeActivityCalculator.ACT_DISC_SUFF)
                If Not IsNothing(_calc_entry) AndAlso Not IsNothing(act.discount_obj) AndAlso act.discount_obj.id <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value AndAlso act.discount_obj.payer_obj.payer = payer Then
                    If Not _listOfDiscount.ContainsKey(act.discount_obj.id) Then
                        _listOfDiscount.Add(act.discount_obj.id, New CScopeAtivityCalcEntry(act.discount_obj.id))
                        _listOfDiscount(act.discount_obj.id).label = act.discount_obj.description
                    End If
                    _listOfDiscount(act.discount_obj.id).addValuesFrom(_calc_entry)
                End If

                'activity NTE
                If payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value AndAlso act.fix_labor > 0 _
                    AndAlso Not (String.IsNullOrWhiteSpace(act.labor_sold_mark) OrElse act.labor_sold_mark = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value) Then
                    If (Not IsNothing(act.nte_obj) AndAlso act.nte_obj.id > 0) OrElse (act.isWarrCodeNTE AndAlso Not IsNothing(def_lab_ntey)) OrElse (act.isWarrCodeNTEExclusion AndAlso Not IsNothing(def_lab_nten)) Then
                        Dim nteObj As CNotToExceed = If(IsNothing(act.nte_obj) OrElse act.nte_obj.id <= 0, If(act.isWarrCodeNTE, def_lab_ntey, def_lab_nten), act.nte_obj)
                        If Not _listOfGrossNTE.ContainsKey(nteObj.id) Then
                            _listOfGrossNTE.Add(nteObj.id, New CScopeAtivityCalcEntry(nteObj.id))
                            _listOfGrossNTE(nteObj.id).label = nteObj.description
                        End If
                        Dim nte_lab = New CScopeAtivityCalcEntry("")
                        nte_lab.labor_sold_calc = act.fix_labor
                        _listOfGrossNTE(nteObj.id).addValuesFrom(nte_lab)
                        'log NTE Detalils
                        addActivityToNTEDetails(calc_args, act, nteObj, nte_lab)
                    ElseIf act.isWarrCodeNTEExclusion Then
                        'if no applicable NTE and that act is NTEN, add as nten with no note
                        Dim nte_lab = New CScopeAtivityCalcEntry("")
                        nte_lab.labor_sold_calc = act.fix_labor
                        nten_with_no_note.addValuesFrom(nte_lab)
                        'log NTE Detalils
                        addActivityToNTEDetails(calc_args, act, MConstants.NTEN_NO_NOTE, nte_lab)
                    End If

                End If

                'discount and nte
                For Each soldH As CSoldHours In act.sold_hour_list
                    'discount
                    If Not IsNothing(soldH.discount_obj) AndAlso soldH.discount_obj.id > 0 AndAlso soldH.payer_obj.payer = payer AndAlso soldH.discount_obj.payer_obj.payer = payer _
                        AndAlso soldH.system_status <> MConstants.CONST_SYS_ROW_DELETED AndAlso soldH.cust_release_disc_nte_reportable Then
                        If Not _listOfDiscount.ContainsKey(soldH.discount_obj.id) Then
                            _listOfDiscount.Add(soldH.discount_obj.id, New CScopeAtivityCalcEntry(soldH.discount_obj.id))
                            _listOfDiscount(soldH.discount_obj.id).label = soldH.discount_obj.description
                        End If
                        _listOfDiscount(soldH.discount_obj.id).labor_sold_calc = _listOfDiscount(soldH.discount_obj.id).labor_sold_calc + soldH.calc_discount
                    End If
                    'nte
                    If ((act.isWarrCodeNTE AndAlso Not IsNothing(def_lab_ntey)) OrElse (act.isWarrCodeNTEExclusion AndAlso Not IsNothing(def_lab_nten)) _
                            OrElse (Not IsNothing(soldH.nte_obj) AndAlso soldH.nte_obj.id > 0) OrElse (Not IsNothing(act.nte_obj) AndAlso act.nte_obj.id > 0)) _
                       AndAlso IsNothing(soldH.init_quote_entry_obj) AndAlso soldH.cust_release_disc_nte_reportable Then
                        Dim nteObj As CNotToExceed = If(IsNothing(soldH.nte_obj) OrElse soldH.nte_obj.id <= 0, act.nte_obj, soldH.nte_obj)
                        If IsNothing(nteObj) OrElse nteObj.id <= 0 Then
                            nteObj = If(act.isWarrCodeNTE, def_lab_ntey, def_lab_nten)
                        End If
                        If soldH.payer_obj.payer = payer AndAlso nteObj.payer = payer AndAlso nteObj.system_status <> MConstants.CONST_SYS_ROW_DELETED Then
                            If Not _listOfGrossNTE.ContainsKey(nteObj.id) Then
                                _listOfGrossNTE.Add(nteObj.id, New CScopeAtivityCalcEntry(nteObj.id))
                                _listOfGrossNTE(nteObj.id).label = nteObj.description
                            End If
                            Dim lab_nte_entry = nteObj.getNTEAsCalEntry(soldH)
                            _listOfGrossNTE(nteObj.id).addValuesFrom(lab_nte_entry)
                            'log NTE Detalils
                            addActivityToNTEDetails(calc_args, act, nteObj, lab_nte_entry)
                        End If
                    ElseIf act.isWarrCodeNTEExclusion AndAlso soldH.payer_obj.payer = payer AndAlso payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value _
                        AndAlso IsNothing(soldH.init_quote_entry_obj) AndAlso soldH.cust_release_disc_nte_reportable Then
                        Dim lab_nte_entry = MConstants.NTEN_NO_NOTE.getNTEAsCalEntry(soldH)
                        nten_with_no_note.addValuesFrom(lab_nte_entry)
                        'log NTE Detalils
                        addActivityToNTEDetails(calc_args, act, MConstants.NTEN_NO_NOTE, lab_nte_entry)
                    End If
                Next

                For Each mat As CMaterial In act.material_list
                    Dim discOrNteEntry As CScopeAtivityCalcEntry
                    'discount
                    If Not IsNothing(mat.discount_obj) AndAlso mat.discount_obj.id > 0 AndAlso mat.payer_obj.payer = payer AndAlso mat.discount_obj.payer = payer _
                                        AndAlso mat.system_status <> MConstants.CONST_SYS_ROW_DELETED AndAlso mat.cust_release_disc_nte_reportable Then
                        If Not _listOfDiscount.ContainsKey(mat.discount_obj.id) Then
                            _listOfDiscount.Add(mat.discount_obj.id, New CScopeAtivityCalcEntry(mat.discount_obj.id))
                            _listOfDiscount(mat.discount_obj.id).label = mat.discount_obj.description
                        End If
                        discOrNteEntry = _listOfDiscount(mat.discount_obj.id)
                        If mat.activity_obj.freight_sold_mark = MConstants.CONST_CONF_LOV_EMPTY_VAL OrElse String.IsNullOrWhiteSpace(mat.activity_obj.freight_sold_mark) Then
                            discOrNteEntry.freight_sold_calc = discOrNteEntry.freight_sold_calc + mat.calc_total_freight_disc_in_freight_curr
                        End If
                        If mat.activity_obj.serv_sold_mark = MConstants.CONST_CONF_LOV_EMPTY_VAL OrElse String.IsNullOrWhiteSpace(mat.activity_obj.serv_sold_mark) Then
                            discOrNteEntry.serv_sold_calc = discOrNteEntry.serv_sold_calc + mat.calc_handling_zcus_disc_in_mat_curr
                        End If

                        If mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_MAT_KEY).value Then
                            If mat.activity_obj.mat_sold_mark = MConstants.CONST_CONF_LOV_EMPTY_VAL OrElse String.IsNullOrWhiteSpace(mat.activity_obj.mat_sold_mark) Then
                                discOrNteEntry.mat_sold_calc = discOrNteEntry.mat_sold_calc + mat.calc_discount_total_material_in_mat_curr
                            End If
                        ElseIf mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_REP_KEY).value Then
                            If mat.activity_obj.rep_sold_mark = MConstants.CONST_CONF_LOV_EMPTY_VAL OrElse String.IsNullOrWhiteSpace(mat.activity_obj.rep_sold_mark) Then
                                discOrNteEntry.rep_sold_calc = discOrNteEntry.rep_sold_calc + mat.calc_discount_total_material_in_mat_curr
                            End If
                        ElseIf mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_FREIGHT_KEY).value Then
                            If mat.activity_obj.freight_sold_mark = MConstants.CONST_CONF_LOV_EMPTY_VAL OrElse String.IsNullOrWhiteSpace(mat.activity_obj.freight_sold_mark) Then
                                discOrNteEntry.freight_sold_calc = discOrNteEntry.freight_sold_calc + mat.calc_discount_total_material_in_mat_curr
                            End If
                        ElseIf mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_3RD_P_KEY).value Then
                            If mat.activity_obj.serv_sold_mark = MConstants.CONST_CONF_LOV_EMPTY_VAL OrElse String.IsNullOrWhiteSpace(mat.activity_obj.serv_sold_mark) Then
                                discOrNteEntry.serv_sold_calc = discOrNteEntry.serv_sold_calc + mat.calc_discount_total_material_in_mat_curr
                            End If
                        End If
                    End If

                    'NTE
                    If mat.payer_obj.payer = payer AndAlso IsNothing(mat.init_quote_entry_obj) AndAlso mat.cust_release_disc_nte_reportable Then
                        Dim mat_excluded_nte_entry As CScopeAtivityCalcEntry = MConstants.NTEN_NO_NOTE.getNTEAsCalEntry(mat)
                        If ((is_any_default_mat_nte) OrElse (Not IsNothing(mat.nte_obj) AndAlso mat.nte_obj.id > 0) OrElse (Not IsNothing(act.nte_obj) AndAlso act.nte_obj.id > 0)) Then

                            Dim nteObj As CNotToExceed = If(IsNothing(mat.nte_obj) OrElse mat.nte_obj.id <= 0, act.nte_obj, mat.nte_obj)
                            'could be several default nte for mat (freight, one for serv, ..)
                            Dim list_mat_nte As List(Of CNotToExceed) = Nothing
                            'in case of default NTE
                            If nteObj.id <= 0 Then
                                list_mat_nte = unique_default_nte_list
                            Else
                                list_mat_nte = New List(Of CNotToExceed)
                                list_mat_nte.Add(nteObj)
                            End If
                            'browse nte
                            For Each _mat_nte As CNotToExceed In list_mat_nte

                                'if default NTE check matching between NTE types between NTE obj and act
                                If _mat_nte.is_default AndAlso Not ((_mat_nte.isNTEYes AndAlso act.isWarrCodeNTE) OrElse (_mat_nte.isNTEExclusion AndAlso act.isWarrCodeNTEExclusion)) Then
                                    Continue For
                                End If

                                If Not IsNothing(_mat_nte) AndAlso _mat_nte.payer = payer AndAlso _mat_nte.system_status <> MConstants.CONST_SYS_ROW_DELETED Then
                                    If Not _listOfGrossNTE.ContainsKey(_mat_nte.id) Then
                                        _listOfGrossNTE.Add(_mat_nte.id, New CScopeAtivityCalcEntry(_mat_nte.id))
                                        _listOfGrossNTE(_mat_nte.id).label = _mat_nte.description
                                    End If

                                    discOrNteEntry = _listOfGrossNTE(_mat_nte.id)
                                    'get nte entry and set to 0 applicable material type values
                                    Dim this_nte_enty As CScopeAtivityCalcEntry = _mat_nte.getNTEAsCalEntry(mat, mat_excluded_nte_entry)

                                    If Not (mat.activity_obj.mat_sold_mark = MConstants.CONST_CONF_LOV_EMPTY_VAL OrElse String.IsNullOrWhiteSpace(mat.activity_obj.mat_sold_mark)) Then
                                        this_nte_enty.mat_sold_calc = 0
                                    End If
                                    If Not (mat.activity_obj.rep_sold_mark = MConstants.CONST_CONF_LOV_EMPTY_VAL OrElse String.IsNullOrWhiteSpace(mat.activity_obj.rep_sold_mark)) Then
                                        this_nte_enty.rep_sold_calc = 0
                                    End If
                                    If Not (mat.activity_obj.freight_sold_mark = MConstants.CONST_CONF_LOV_EMPTY_VAL OrElse String.IsNullOrWhiteSpace(mat.activity_obj.freight_sold_mark)) Then
                                        this_nte_enty.freight_sold_calc = 0
                                    End If
                                    If Not (mat.activity_obj.serv_sold_mark = MConstants.CONST_CONF_LOV_EMPTY_VAL OrElse String.IsNullOrWhiteSpace(mat.activity_obj.serv_sold_mark)) Then
                                        this_nte_enty.serv_sold_calc = 0
                                    End If

                                    discOrNteEntry.addValuesFrom(this_nte_enty)
                                    'log NTE Detalils
                                    addActivityToNTEDetails(calc_args, act, _mat_nte, this_nte_enty)
                                End If
                            Next

                        End If
                        If act.isWarrCodeNTEExclusion AndAlso Not mat_excluded_nte_entry.is_zero Then
                            nten_with_no_note.addValuesFrom(mat_excluded_nte_entry)
                            'log NTE Detalils
                            addActivityToNTEDetails(calc_args, act, MConstants.NTEN_NO_NOTE, mat_excluded_nte_entry)
                        End If
                    End If
                Next
            Next

            If Not IsNothing(gumObj) AndAlso Not IsNothing(ecotaxObj) Then
                'surcharges
                _gum.mat_sold_calc = gumObj.getSurchargeValue(MCalc.toMatCurr(sub_total.labor_sold_calc, project.lab_curr), project.mat_curr)
                _eco_tax.mat_sold_calc = ecotaxObj.getSurchargeValue(sub_total.mat_sold_calc + toMatCurr(sub_total.rep_sold_calc, project.repair_curr), project.mat_curr)
                _surcharge_total.addValuesFrom(_gum)
                _surcharge_total.addValuesFrom(_eco_tax)
            End If

            'project discount
            For Each disc As CDiscount In MConstants.GLB_MSTR_CTRL.billing_cond_controller._project_discount_list
                If payer <> disc.payer_obj.payer Then
                    Continue For
                End If

                Dim discEntry As CScopeAtivityCalcEntry
                If Not _listOfDiscount.ContainsKey(disc.id) Then
                    _listOfDiscount.Add(disc.id, New CScopeAtivityCalcEntry(disc.id))
                    _listOfDiscount(disc.id).label = disc.description
                End If
                discEntry = _listOfDiscount(disc.id)

                If MUtils.isLaborApplicableLabMat(disc.lab_mat_applicability) Then
                    discEntry.labor_sold_calc = -1 * disc.getDiscountValue(total_act_after_disc.labor_sold_calc, project.lab_curr)
                ElseIf MUtils.isMaterialApplicableLabMat(disc.lab_mat_applicability) Then
                    discEntry.mat_sold_calc = -1 * disc.getDiscountValue(total_act_after_disc.mat_sold_calc, project.mat_curr)
                ElseIf MUtils.isRepairApplicableLabMat(disc.lab_mat_applicability) Then
                    discEntry.rep_sold_calc = -1 * disc.getDiscountValue(total_act_after_disc.rep_sold_calc, project.repair_curr)
                ElseIf MUtils.isFreightApplicableLabMat(disc.lab_mat_applicability) Then
                    discEntry.freight_sold_calc = -1 * disc.getDiscountValue(total_act_after_disc.freight_sold_calc, project.freight_curr)
                ElseIf MUtils.isServApplicableLabMat(disc.lab_mat_applicability) Then
                    discEntry.serv_sold_calc = -1 * disc.getDiscountValue(total_act_after_disc.serv_sold_calc, project.third_party_curr)
                End If

                'remove discount value for next discounts
                total_act_after_disc.addValuesFrom(discEntry)
            Next

            'remove unused and apply cap
            For Each _entry_id As Long In _listOfDiscount.Keys.ToArray
                If _listOfDiscount(_entry_id).is_zero Then
                    _listOfDiscount.Remove(_entry_id)
                Else
                    Dim disc As CDiscount = MConstants.GLB_MSTR_CTRL.billing_cond_controller.getDiscountObject(_entry_id)
                    If disc.cap_value > 0 AndAlso Not String.IsNullOrWhiteSpace(disc.cap_value_curr) AndAlso disc.cap_value_curr <> MConstants.CONST_CONF_LOV_EMPTY_VAL Then
                        Dim cap_value As Double = 0
                        Dim disc_entry = _listOfDiscount(_entry_id)

                        If disc.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_KEY).value Then
                            'labor
                            cap_value = MCalc.toLabCurr(disc.cap_value, disc.cap_value_curr)
                            If -disc_entry.labor_sold_calc > cap_value Then
                                disc_entry.labor_sold_calc = -cap_value
                            End If
                        ElseIf disc.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_KEY).value Then
                            'material
                            cap_value = MCalc.toMatCurr(disc.cap_value, disc.cap_value_curr)
                            If -disc_entry.mat_sold_calc > cap_value Then
                                disc_entry.mat_sold_calc = -cap_value
                            End If
                        ElseIf disc.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_REP_KEY).value Then
                            'rep
                            cap_value = MCalc.toMatCurr(disc.cap_value, disc.cap_value_curr)
                            If -disc_entry.rep_sold_calc > cap_value Then
                                disc_entry.rep_sold_calc = -cap_value
                            End If
                        ElseIf disc.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_FREIGHT_KEY).value Then
                            'freight
                            cap_value = MCalc.toFreightCurr(disc.cap_value, disc.cap_value_curr)
                            If -disc_entry.freight_sold_calc > cap_value Then
                                disc_entry.freight_sold_calc = -cap_value
                            End If
                        ElseIf disc.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_SERV_KEY).value Then
                            'serv
                            cap_value = MCalc.toServCurr(disc.cap_value, disc.cap_value_curr)
                            If -disc_entry.serv_sold_calc > cap_value Then
                                disc_entry.serv_sold_calc = -cap_value
                            End If
                        Else
                            'mix of lab mat rep frgh serv
                            Dim capValueInitialized As Boolean = False
                            Dim capCurrentCurrency As String = ""
                            'labor
                            If MUtils.isLaborApplicableLabMat(disc.lab_mat_applicability) Then
                                If Not capValueInitialized Then
                                    cap_value = MCalc.toLabCurr(disc.cap_value, disc.cap_value_curr)
                                    capValueInitialized = True
                                Else
                                    cap_value = MCalc.toLabCurr(cap_value, capCurrentCurrency)
                                End If
                                capCurrentCurrency = MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr

                                If -disc_entry.labor_sold_calc > cap_value Then
                                    disc_entry.labor_sold_calc = -cap_value
                                    cap_value = 0
                                Else
                                    cap_value = cap_value + disc_entry.labor_sold_calc
                                End If
                            End If

                            'material
                            If MUtils.isMaterialApplicableLabMat(disc.lab_mat_applicability) Then
                                If Not capValueInitialized Then
                                    cap_value = MCalc.toMatCurr(disc.cap_value, disc.cap_value_curr)
                                    capValueInitialized = True
                                Else
                                    cap_value = MCalc.toMatCurr(cap_value, capCurrentCurrency)
                                End If
                                capCurrentCurrency = MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr

                                If -disc_entry.mat_sold_calc > cap_value Then
                                    disc_entry.mat_sold_calc = -cap_value
                                    cap_value = 0
                                Else
                                    cap_value = cap_value + disc_entry.mat_sold_calc
                                End If
                            End If

                            'rep
                            If MUtils.isRepairApplicableLabMat(disc.lab_mat_applicability) Then
                                If Not capValueInitialized Then
                                    cap_value = MCalc.toMatCurr(disc.cap_value, disc.cap_value_curr)
                                    capValueInitialized = True
                                Else
                                    cap_value = MCalc.toMatCurr(cap_value, capCurrentCurrency)
                                End If
                                capCurrentCurrency = MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr

                                If -disc_entry.rep_sold_calc > cap_value Then
                                    disc_entry.rep_sold_calc = -cap_value
                                    cap_value = 0
                                Else
                                    cap_value = cap_value + disc_entry.rep_sold_calc
                                End If
                            End If

                            'freight
                            If MUtils.isFreightApplicableLabMat(disc.lab_mat_applicability) Then
                                If Not capValueInitialized Then
                                    cap_value = MCalc.toFreightCurr(disc.cap_value, disc.cap_value_curr)
                                    capValueInitialized = True
                                Else
                                    cap_value = MCalc.toFreightCurr(cap_value, capCurrentCurrency)
                                End If
                                capCurrentCurrency = MConstants.GLB_MSTR_CTRL.project_controller.project.freight_curr

                                If -disc_entry.freight_sold_calc > cap_value Then
                                    disc_entry.freight_sold_calc = -cap_value
                                    cap_value = 0
                                Else
                                    cap_value = cap_value + disc_entry.freight_sold_calc
                                End If
                            End If

                            'serv
                            If MUtils.isServApplicableLabMat(disc.lab_mat_applicability) Then
                                If Not capValueInitialized Then
                                    cap_value = MCalc.toServCurr(disc.cap_value, disc.cap_value_curr)
                                    capValueInitialized = True
                                Else
                                    cap_value = MCalc.toServCurr(cap_value, capCurrentCurrency)
                                End If
                                capCurrentCurrency = MConstants.GLB_MSTR_CTRL.project_controller.project.third_party_curr

                                If -disc_entry.serv_sold_calc > cap_value Then
                                    disc_entry.serv_sold_calc = -cap_value
                                    cap_value = 0
                                Else
                                    cap_value = cap_value + disc_entry.labor_sold_calc
                                End If
                            End If
                        End If
                    End If
                End If
            Next

            'remove unused
            For Each _entry_id As Long In _listOfGrossNTE.Keys.ToArray
                If _listOfGrossNTE(_entry_id).is_zero Then
                    _listOfGrossNTE.Remove(_entry_id)
                End If
            Next

            'NTE
            For Each _nteObj As CNotToExceed In MConstants.GLB_MSTR_CTRL.billing_cond_controller.nteList.DataSource
                If _nteObj.system_status <> MConstants.CONST_SYS_ROW_DELETED AndAlso _nteObj.payer = payer Then
                    If _listOfGrossNTE.ContainsKey(_nteObj.id) Then
                        Dim gross_entry = _listOfGrossNTE(_nteObj.id)
                        Dim net_entry As New CScopeAtivityCalcEntry(_nteObj.id)
                        Dim nteObjEntry As New CScopeAtivityCalcEntry(_nteObj.id)

                        Dim nte_value_remaining As Double = 0
                        Dim is_nte_value_consumed As Boolean = False
                        Dim nte_value_remaining_curr As String = ""

                        'LABOR
                        'app lab and labmat (lab is priority)
                        If MUtils.isLaborApplicableLabMat(_nteObj.lab_mat_applicability) Then
                            If Not is_nte_value_consumed Then
                                nteObjEntry.labor_sold_calc = MCalc.toLabCurr(_nteObj.nte_value, _nteObj.value_curr)
                                is_nte_value_consumed = True
                            Else
                                nteObjEntry.labor_sold_calc = MCalc.toLabCurr(nte_value_remaining, nte_value_remaining_curr)
                            End If
                            nte_value_remaining_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr

                            If _nteObj.isNTEYes Then
                                'nte yes
                                If gross_entry.labor_sold_calc > nteObjEntry.labor_sold_calc Then
                                    net_entry.labor_sold_calc = net_entry.labor_sold_calc + nteObjEntry.labor_sold_calc - gross_entry.labor_sold_calc
                                    nte_value_remaining = 0
                                Else
                                    'if lab mat transfer remaing to mat
                                    nte_value_remaining = nteObjEntry.labor_sold_calc - gross_entry.labor_sold_calc
                                End If
                            Else
                                'nte exclusion
                                If gross_entry.labor_sold_calc <= nteObjEntry.labor_sold_calc Then
                                    net_entry.labor_sold_calc = net_entry.labor_sold_calc - gross_entry.labor_sold_calc
                                    'if lab mat transfer remaing to mat
                                    nte_value_remaining = nteObjEntry.labor_sold_calc - gross_entry.labor_sold_calc
                                Else
                                    net_entry.labor_sold_calc = net_entry.labor_sold_calc - nteObjEntry.labor_sold_calc
                                    nte_value_remaining = 0
                                End If
                            End If
                        End If

                        'MATERIAL
                        If MUtils.isMaterialApplicableLabMat(_nteObj.lab_mat_applicability) Then
                            'app mat and freight (mat is priority)
                            If Not is_nte_value_consumed Then
                                nteObjEntry.mat_sold_calc = MCalc.toMatCurr(_nteObj.nte_value, _nteObj.value_curr)
                                is_nte_value_consumed = True
                            Else
                                nteObjEntry.mat_sold_calc = MCalc.toMatCurr(nte_value_remaining, nte_value_remaining_curr)
                            End If
                            nte_value_remaining_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr

                            If _nteObj.isNTEYes Then
                                'nte yes
                                If gross_entry.mat_sold_calc > nteObjEntry.mat_sold_calc Then
                                    net_entry.mat_sold_calc = net_entry.mat_sold_calc + nteObjEntry.mat_sold_calc - gross_entry.mat_sold_calc
                                    nte_value_remaining = 0
                                Else
                                    'if lab mat or all mat transfer remaing to rep
                                    nte_value_remaining = nteObjEntry.mat_sold_calc - gross_entry.mat_sold_calc
                                End If
                            Else
                                'nte exclusion
                                If gross_entry.mat_sold_calc <= nteObjEntry.mat_sold_calc Then
                                    net_entry.mat_sold_calc = net_entry.mat_sold_calc - gross_entry.mat_sold_calc
                                    'if lab mat transfer remaing to mat
                                    nte_value_remaining = nteObjEntry.mat_sold_calc - gross_entry.mat_sold_calc
                                Else
                                    net_entry.mat_sold_calc = net_entry.mat_sold_calc - nteObjEntry.mat_sold_calc
                                    nte_value_remaining = 0
                                End If
                            End If
                        End If

                        'REPAIR
                        If MUtils.isRepairApplicableLabMat(_nteObj.lab_mat_applicability) Then
                            If Not is_nte_value_consumed Then
                                nteObjEntry.rep_sold_calc = MCalc.toMatCurr(_nteObj.nte_value, _nteObj.value_curr)
                                is_nte_value_consumed = True
                            Else
                                nteObjEntry.rep_sold_calc = MCalc.toMatCurr(nte_value_remaining, nte_value_remaining_curr)
                            End If
                            nte_value_remaining_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr

                            If _nteObj.isNTEYes Then
                                'nte yes
                                If gross_entry.rep_sold_calc > nteObjEntry.rep_sold_calc Then
                                    net_entry.rep_sold_calc = net_entry.rep_sold_calc + nteObjEntry.rep_sold_calc - gross_entry.rep_sold_calc
                                    nte_value_remaining = 0
                                Else
                                    'if lab mat or all mat transfer remaing to freight
                                    nte_value_remaining = nteObjEntry.rep_sold_calc - gross_entry.rep_sold_calc
                                End If
                            Else
                                'nte exclusion
                                If gross_entry.rep_sold_calc <= nteObjEntry.rep_sold_calc Then
                                    net_entry.rep_sold_calc = net_entry.rep_sold_calc - gross_entry.rep_sold_calc
                                    'if lab mat transfer remaing to mat
                                    nte_value_remaining = nteObjEntry.rep_sold_calc - gross_entry.rep_sold_calc
                                Else
                                    net_entry.rep_sold_calc = net_entry.rep_sold_calc - nteObjEntry.rep_sold_calc
                                    nte_value_remaining = 0
                                End If
                            End If
                        End If

                        'FREIGHT
                        If MUtils.isFreightApplicableLabMat(_nteObj.lab_mat_applicability) Then
                            If Not is_nte_value_consumed Then
                                nteObjEntry.freight_sold_calc = MCalc.toFreightCurr(_nteObj.nte_value, _nteObj.value_curr)
                                is_nte_value_consumed = True
                            Else
                                nteObjEntry.freight_sold_calc = MCalc.toFreightCurr(nte_value_remaining, nte_value_remaining_curr)
                            End If
                            nte_value_remaining_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.freight_curr

                            If _nteObj.isNTEYes Then
                                'nte yes
                                If gross_entry.freight_sold_calc > nteObjEntry.freight_sold_calc Then
                                    net_entry.freight_sold_calc = net_entry.freight_sold_calc + nteObjEntry.freight_sold_calc - gross_entry.freight_sold_calc
                                    nte_value_remaining = 0
                                Else
                                    'if lab mat or all mat transfer remaing to freight
                                    nte_value_remaining = nteObjEntry.freight_sold_calc - gross_entry.freight_sold_calc
                                End If
                            Else
                                'nte exclusion
                                If gross_entry.freight_sold_calc <= nteObjEntry.freight_sold_calc Then
                                    net_entry.freight_sold_calc = net_entry.freight_sold_calc - gross_entry.freight_sold_calc
                                    'if lab mat transfer remaing to mat
                                    nte_value_remaining = nteObjEntry.freight_sold_calc - gross_entry.freight_sold_calc
                                Else
                                    net_entry.freight_sold_calc = net_entry.freight_sold_calc - nteObjEntry.freight_sold_calc
                                    nte_value_remaining = 0
                                End If
                            End If
                        End If

                        If MUtils.isServApplicableLabMat(_nteObj.lab_mat_applicability) Then
                            If Not is_nte_value_consumed Then
                                nteObjEntry.serv_sold_calc = MCalc.toServCurr(_nteObj.nte_value, _nteObj.value_curr)
                                is_nte_value_consumed = True
                            Else
                                nteObjEntry.serv_sold_calc = MCalc.toServCurr(nte_value_remaining, nte_value_remaining_curr)
                            End If
                            nte_value_remaining_curr = MConstants.GLB_MSTR_CTRL.project_controller.project.third_party_curr

                            If _nteObj.isNTEYes Then
                                'nte yes
                                If gross_entry.serv_sold_calc > nteObjEntry.serv_sold_calc Then
                                    net_entry.serv_sold_calc = net_entry.serv_sold_calc + nteObjEntry.serv_sold_calc - gross_entry.serv_sold_calc
                                    nte_value_remaining = 0
                                Else
                                    nte_value_remaining = nteObjEntry.serv_sold_calc - gross_entry.serv_sold_calc
                                End If
                            Else
                                'nte exclusion
                                If gross_entry.serv_sold_calc <= nteObjEntry.serv_sold_calc Then
                                    net_entry.serv_sold_calc = net_entry.serv_sold_calc - gross_entry.serv_sold_calc
                                    nte_value_remaining = nteObjEntry.serv_sold_calc - gross_entry.serv_sold_calc
                                Else
                                    net_entry.serv_sold_calc = net_entry.serv_sold_calc - nteObjEntry.serv_sold_calc
                                    nte_value_remaining = 0
                                End If
                            End If
                        End If

                        'add entry
                        net_entry.label = gross_entry.label
                        'use for label NTE Value
                        net_entry.init_add_label = "Threshold : " & _nteObj.nte_value & " " & _nteObj.value_curr_view & " On " & _nteObj.lab_mat_applicability_view
                        _listNTE.Add(_nteObj.id, net_entry)
                    Else
                        'not used NTE's
                        Dim net_entry As New CScopeAtivityCalcEntry(_nteObj.id)
                        net_entry.label = _nteObj.description
                        'use for label NTE Value
                        net_entry.init_add_label = "Threshold : " & _nteObj.nte_value & " " & _nteObj.value_curr_view & " On " & _nteObj.lab_mat_applicability_view
                        _listNTE.Add(_nteObj.id, net_entry)
                    End If

                End If
            Next

            'generate NTE details NTE Obj list
            mergeNTEDetails(calc_args, nten_with_no_note)

            'discount total
            For Each disEntry As CScopeAtivityCalcEntry In _listOfDiscount.Values
                _discount_total.addValuesFrom(disEntry)
            Next
            'NTE Total
            For Each nteEntry As CScopeAtivityCalcEntry In _listOfGrossNTE.Values
                _nte_gross_total.addValuesFrom(nteEntry)
                _nte_chargeable_total.addValuesFrom(nteEntry)
            Next
            'nten no note
            _nte_gross_total.addValuesFrom(nten_with_no_note)
            _nte_chargeable_total.addValuesFrom(nten_with_no_note)

            For Each nteEntry As CScopeAtivityCalcEntry In _listNTE.Values
                _nte_total.addValuesFrom(nteEntry)
                _nte_chargeable_total.addValuesFrom(nteEntry)
            Next

            'surcharge
            'add surcharge
            _sub_total_after_surcharge_bef_disc.addValuesFrom(_sub_total)
            _sub_total_after_surcharge_bef_disc.addValuesFrom(_gum)
            _sub_total_after_surcharge_bef_disc.addValuesFrom(_eco_tax)
            'add discount
            _total_after_disc_surch.addValuesFrom(_sub_total_after_surcharge_bef_disc)
            _total_after_disc_surch.addValuesFrom(_discount_total)
            _total_after_disc_surch.addValuesFrom(_nte_total)

            'down payments
            For Each dpView As ObjectView(Of CDownPayment) In MConstants.GLB_MSTR_CTRL.billing_cond_controller.dowmPaymentList
                Dim dp As CDownPayment = dpView.Object
                Dim dp_entry As CScopeAtivityCalcEntry

                If payer <> dp.payer_obj.payer Then
                    Continue For
                End If

                If Not _listDP.ContainsKey(dp.number) Then
                    dp_entry = CScopeAtivityCalcEntry.getEmpty
                    dp_entry.business_object = dp
                    _listDP.Add(dp.number, dp_entry)
                Else
                    dp_entry = _listDP(dp.number)
                End If

                'item
                If dp.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_LABMAT_APP_MAT_KEY).value Then
                    dp_entry.mat_sold_calc = dp_entry.mat_sold_calc + dp.dp_value
                Else
                    dp_entry.labor_sold_calc = dp_entry.labor_sold_calc + dp.dp_value
                End If

                'summary
                If dp.payment_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_PAYMENT_STAT_PAID_KEY).value Then
                    If dp.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_LABMAT_APP_MAT_KEY).value Then
                        _dp_paid.mat_sold_calc = _dp_paid.mat_sold_calc + dp.dp_value
                    Else
                        _dp_paid.labor_sold_calc = _dp_paid.labor_sold_calc + dp.dp_value
                    End If
                ElseIf dp.payment_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_PAYMENT_STAT_UNPAID_KEY).value AndAlso dp.due_date <= Date.Now Then
                    If dp.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_LABMAT_APP_MAT_KEY).value Then
                        _dp_un_paid.mat_sold_calc = _dp_un_paid.mat_sold_calc + dp.dp_value
                    Else
                        _dp_un_paid.labor_sold_calc = _dp_un_paid.labor_sold_calc + dp.dp_value
                    End If
                ElseIf dp.payment_status <> MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_PAYMENT_STAT_CANCEL_KEY).value Then
                    If dp.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_LABMAT_APP_MAT_KEY).value Then
                        _dp_unpaid_not_due.mat_sold_calc = _dp_unpaid_not_due.mat_sold_calc + dp.dp_value
                    Else
                        _dp_unpaid_not_due.labor_sold_calc = _dp_unpaid_not_due.labor_sold_calc + dp.dp_value
                    End If
                End If
            Next

            'dp total
            dp_total.addValuesFrom(_dp_paid)
            dp_total.addValuesFrom(_dp_un_paid)
            dp_total.addValuesFrom(_dp_unpaid_not_due)

            'out standing
            _outstanding.addValuesFrom(_dp_un_paid)
            _outstanding.addValuesFrom(_dp_unpaid_not_due)

        Catch ex As Exception
            _calc_error_text = "An error occured while updating project summary data" & Chr(10) & ex.Message & ex.StackTrace
        End Try
    End Sub

    Public Sub updateAWQ(payer As String, _result_awq_list As List(Of CAWQuotationReportingObject))
        'clear
        _sent_awq_pending.setEntryToZero()
        _sent_awq_approved.setEntryToZero()
        _sent_awq_approved_pending.setEntryToZero()
        _result_awq_list.Clear()

        Dim sent_awq_list As New List(Of CAWQuotation)

        'AWQ Total
        'net value before project discount
        Dim latestSentAWQDict As New Dictionary(Of String, CAWQuotation)
        Dim latestSentAWQList As New List(Of CAWQuotation)

        For Each awq_view As ObjectView(Of CAWQuotation) In MConstants.GLB_MSTR_CTRL.awq_controller.awq_list
            Dim _awq = awq_view.Object
            'test if this payer is applicable for this AWQ revision ( this has an incidence on latest rev. Lates rev is not the same depending on the payer
            Dim listO As List(Of CScopeAtivityCalcEntry) = CAWQuotationCalucaltor.getAWQPayerSummary(_awq, payer)
            If _awq.has_been_sent AndAlso ((_awq.is_cancelled_or_superseded AndAlso _awq.is_cancelled_awq_applicable(payer)) OrElse (Not IsNothing(listO))) Then
                'to be reported
                If Not sent_awq_list.Contains(_awq) Then
                    sent_awq_list.Add(_awq)
                End If

                'to be consider in total
                If Not latestSentAWQDict.ContainsKey(_awq.reference) Then
                    latestSentAWQDict.Add(_awq.reference, _awq)
                Else
                    If latestSentAWQDict(_awq.reference).revision < _awq.revision Then
                        latestSentAWQDict(_awq.reference) = _awq
                    End If
                End If
            End If
        Next

        latestSentAWQList = latestSentAWQDict.Values.ToList

        'TBD Payer for AWQ
        For Each _awq As CAWQuotation In sent_awq_list
            Dim previousAWQ_After_Disc As New CScopeAtivityCalcEntry("")
            Dim thisAWQ_After_Disc As New CScopeAtivityCalcEntry("")

            Dim listO As List(Of CScopeAtivityCalcEntry) = CAWQuotationCalucaltor.getAWQPayerSummary(_awq, payer)

            If IsNothing(listO) Then
                'ignore those items for OEM
                If _awq.is_cancelled_or_superseded AndAlso _awq.is_cancelled_awq_applicable(payer) Then
                    'if item has been cancelled or superseded, material and labor are removed so no more calc entry.
                    'As we want to show them in report, we add them. Limit: same awq will appear in both customer and oem report. Use OEM feedback field ?

                    Dim repO As New CAWQuotationReportingObject(_awq)
                    If latestSentAWQList.Contains(_awq) Then
                        repO.is_latest_sent = True
                    End If
                    _result_awq_list.Add(repO)
                Else
                    'if opened or closed awq with not entry, means that the payer is not part of this awq
                    Continue For
                End If
            Else
                'if entry found, add to total
                'get value before discount
                listO(0).copyValuesTo(previousAWQ_After_Disc)
                listO(2).copyValuesTo(thisAWQ_After_Disc)
                'add discount
                previousAWQ_After_Disc.addValuesFrom(listO(1))
                thisAWQ_After_Disc.addValuesFrom(listO(3))

                Dim repO As New CAWQuotationReportingObject(_awq)
                _result_awq_list.Add(repO)
                If _awq.is_approved_closed Then
                    repO.lab_sold_approved_calc = previousAWQ_After_Disc.labor_sold_calc + thisAWQ_After_Disc.labor_sold_calc
                    repO.mat_sold_approved_calc = previousAWQ_After_Disc.all_mat_calc + thisAWQ_After_Disc.all_mat_calc
                Else
                    repO.lab_sold_approved_calc = previousAWQ_After_Disc.labor_sold_calc
                    repO.mat_sold_approved_calc = previousAWQ_After_Disc.all_mat_calc

                    repO.lab_sold_pending_calc = thisAWQ_After_Disc.labor_sold_calc
                    repO.mat_sold_pending_calc = thisAWQ_After_Disc.all_mat_calc
                End If

                If latestSentAWQList.Contains(_awq) Then
                    repO.is_latest_sent = True
                    If _awq.is_approved_closed Then
                        _sent_awq_approved.addValuesFrom(previousAWQ_After_Disc)
                        _sent_awq_approved.addValuesFrom(thisAWQ_After_Disc)
                    Else
                        _sent_awq_approved.addValuesFrom(previousAWQ_After_Disc)
                        _sent_awq_pending.addValuesFrom(thisAWQ_After_Disc)
                    End If
                    _sent_awq_approved_pending.addValuesFrom(previousAWQ_After_Disc)
                    _sent_awq_approved_pending.addValuesFrom(thisAWQ_After_Disc)
                End If
            End If
        Next
    End Sub

    Public Function getlistOfCalcItem(item As String, item_prop As String) As Object
        Dim res As Object = Nothing
        Try
            If _listOfCalculations.ContainsKey(item) Then
                Dim itemObject As Object = _listOfCalculations(item)
                res = CallByName(itemObject, item_prop, CallType.Get)
            Else
                Throw New Exception("Project Calculator List of calulcations does not contains item " & item)
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while retrieving property " & item_prop & " of calculator item " & item)
        End Try
        Return res
    End Function

    Private _listOfCalculations As Dictionary(Of String, CScopeAtivityCalcEntry)

    'add nte details act objects
    Public Sub addActivityToNTEDetails(cal_arg As CProjCalculatorArgs, act As CScopeActivity, nte As CNotToExceed, entry As CScopeAtivityCalcEntry)
        Try
            If Not IsNothing(cal_arg.nteDetailsActivitiesList) Then
                Dim current_entry As CScopeAtivityCalcEntry = Nothing
                If Not cal_arg.nteDetailsActivitiesList.ContainsKey(act.id & "_" & nte.id) Then
                    current_entry = New CScopeAtivityCalcEntry(act.id & "_" & nte.id)
                    current_entry.act_obj = act
                    current_entry.label = nte.description
                    cal_arg.nteDetailsActivitiesList.Add(act.id & "_" & nte.id, current_entry)
                Else
                    current_entry = cal_arg.nteDetailsActivitiesList(act.id & "_" & nte.id)
                End If
                current_entry.addValuesFrom(entry)
            End If
        Catch ex As Exception
            Throw New Exception("An error occued while adding activity to nte details activity list" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    'merge gross and net NTE Lists for NTE DEtails report
    Public Sub mergeNTEDetails(cal_arg As CProjCalculatorArgs, nten_with_no_note As CScopeAtivityCalcEntry)
        Try
            If Not IsNothing(cal_arg.nteDetailsNTEObjList) Then
                For Each key As Long In _listNTE.Keys
                    Dim net_entry As CScopeAtivityCalcEntry = _listNTE(key)
                    Dim gross_entry As CScopeAtivityCalcEntry = net_entry
                    If _listOfGrossNTE.ContainsKey(key) Then
                        gross_entry = _listOfGrossNTE(key)
                    End If
                    'entry
                    Dim reportO As New CNTEDetailsReportObj
                    reportO.label = net_entry.init_add_label
                    reportO.description = net_entry.label
                    reportO.setGrossValues(gross_entry)
                    reportO.setDiscountValues(net_entry)
                    cal_arg.nteDetailsNTEObjList.Add(reportO)
                Next
                'add NTE Exclusion with  no notes
                Dim reportONTEN As New CNTEDetailsReportObj
                reportONTEN.label = MConstants.NTEN_NO_NOTE.label
                reportONTEN.description = MConstants.NTEN_NO_NOTE.description
                reportONTEN.setGrossValues(nten_with_no_note)
                reportONTEN.setDiscountValues(New CScopeAtivityCalcEntry(""))
                cal_arg.nteDetailsNTEObjList.Add(reportONTEN)
            End If
        Catch ex As Exception
            Throw New Exception("An error occued while adding activity to nte details activity list" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Private _listOfDiscount As Dictionary(Of Long, CScopeAtivityCalcEntry)
    Public ReadOnly Property listOfDiscount As Dictionary(Of Long, CScopeAtivityCalcEntry)
        Get
            Return _listOfDiscount
        End Get
    End Property

    Private _listNTE As Dictionary(Of Long, CScopeAtivityCalcEntry)
    Private _listOfGrossNTE As Dictionary(Of Long, CScopeAtivityCalcEntry)
    Public ReadOnly Property listOfNTE As Dictionary(Of Long, CScopeAtivityCalcEntry)
        Get
            Return _listNTE
        End Get
    End Property

    Private _listDP As Dictionary(Of String, CScopeAtivityCalcEntry)
    Public ReadOnly Property listDP As Dictionary(Of String, CScopeAtivityCalcEntry)
        Get
            Return _listDP
        End Get
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    'subtotal or total before discount and surcharge
    Private _sub_total As CScopeAtivityCalcEntry
    Public Property sub_total() As CScopeAtivityCalcEntry
        Get
            Return _sub_total
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sub_total, value) Then
                _sub_total = value
            End If
        End Set
    End Property

    Private _sub_total_scope_init As CScopeAtivityCalcEntry
    Public Property sub_total_scope_init() As CScopeAtivityCalcEntry
        Get
            Return _sub_total_scope_init
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sub_total_scope_init, value) Then
                _sub_total_scope_init = value
            End If
        End Set
    End Property

    Private _sub_total_scope_init_adj As CScopeAtivityCalcEntry
    Public Property sub_total_scope_init_adj() As CScopeAtivityCalcEntry
        Get
            Return _sub_total_scope_init_adj
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sub_total_scope_init_adj, value) Then
                _sub_total_scope_init_adj = value
            End If
        End Set
    End Property

    Private _sub_total_scope_init_total As CScopeAtivityCalcEntry
    Public Property sub_total_scope_init_total() As CScopeAtivityCalcEntry
        Get
            Return _sub_total_scope_init_total
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sub_total_scope_init_total, value) Then
                _sub_total_scope_init_total = value
            End If
        End Set
    End Property

    Private _sub_total_scope_add_below_thr As CScopeAtivityCalcEntry
    Public Property sub_total_scope_add_below_thr() As CScopeAtivityCalcEntry
        Get
            Return _sub_total_scope_add_below_thr
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sub_total_scope_add_below_thr, value) Then
                _sub_total_scope_add_below_thr = value
            End If
        End Set
    End Property

    Private _sub_total_scope_add_appr As CScopeAtivityCalcEntry
    Public Property sub_total_scope_add_appr() As CScopeAtivityCalcEntry
        Get
            Return _sub_total_scope_add_appr
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sub_total_scope_add_appr, value) Then
                _sub_total_scope_add_appr = value
            End If
        End Set
    End Property

    Private _sub_total_scope_add_blwthr_and_appr As CScopeAtivityCalcEntry
    Public Property sub_total_scope_add_blwthr_and_appr() As CScopeAtivityCalcEntry
        Get
            Return _sub_total_scope_add_blwthr_and_appr
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sub_total_scope_add_blwthr_and_appr, value) Then
                _sub_total_scope_add_blwthr_and_appr = value
            End If
        End Set
    End Property

    Private _sub_total_scope_add_pending As CScopeAtivityCalcEntry
    Public Property sub_total_scope_add_pending() As CScopeAtivityCalcEntry
        Get
            Return _sub_total_scope_add_pending
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sub_total_scope_add_pending, value) Then
                _sub_total_scope_add_pending = value
            End If
        End Set
    End Property

    Private _sub_total_scope_add_total As CScopeAtivityCalcEntry
    Public Property sub_total_scope_add_total() As CScopeAtivityCalcEntry
        Get
            Return _sub_total_scope_add_total
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sub_total_scope_add_total, value) Then
                _sub_total_scope_add_total = value
            End If
        End Set
    End Property

    'total = subtotal + surcharge - discount
    Private _total_after_disc_surch As CScopeAtivityCalcEntry
    Public Property total_after_disc_surch() As CScopeAtivityCalcEntry
        Get
            Return _total_after_disc_surch
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_total_after_disc_surch, value) Then
                _total_after_disc_surch = value
            End If
        End Set
    End Property

    Private _eco_tax As CScopeAtivityCalcEntry
    Public Property eco_tax() As CScopeAtivityCalcEntry
        Get
            Return _eco_tax
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_eco_tax, value) Then
                _eco_tax = value
            End If
        End Set
    End Property

    Private _gum As CScopeAtivityCalcEntry
    Public Property gum() As CScopeAtivityCalcEntry
        Get
            Return _gum
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_gum, value) Then
                _gum = value
            End If
        End Set
    End Property

    Private _surcharge_total As CScopeAtivityCalcEntry
    Public Property surcharge_total() As CScopeAtivityCalcEntry
        Get
            Return _surcharge_total
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_surcharge_total, value) Then
                _surcharge_total = value
            End If
        End Set
    End Property

    Private _discount_total As CScopeAtivityCalcEntry
    Public Property discount_total() As CScopeAtivityCalcEntry
        Get
            Return _discount_total
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_discount_total, value) Then
                _discount_total = value
            End If
        End Set
    End Property

    Private _nte_total As CScopeAtivityCalcEntry
    Public Property nte_total() As CScopeAtivityCalcEntry
        Get
            Return _nte_total
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_nte_total, value) Then
                _nte_total = value
            End If
        End Set
    End Property

    Private _nte_gross_total As CScopeAtivityCalcEntry
    Public Property nte_gross_total() As CScopeAtivityCalcEntry
        Get
            Return _nte_gross_total
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_nte_gross_total, value) Then
                _nte_gross_total = value
            End If
        End Set
    End Property

    Private _nte_chargeable_total As CScopeAtivityCalcEntry
    Public Property nte_chargeable_total() As CScopeAtivityCalcEntry
        Get
            Return _nte_chargeable_total
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_nte_chargeable_total, value) Then
                _nte_chargeable_total = value
            End If
        End Set
    End Property

    Private _sub_total_after_surcharge_bef_disc As CScopeAtivityCalcEntry
    Public Property sub_total_after_surcharge_bef_disc() As CScopeAtivityCalcEntry
        Get
            Return _sub_total_after_surcharge_bef_disc
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sub_total_after_surcharge_bef_disc, value) Then
                _sub_total_after_surcharge_bef_disc = value
            End If
        End Set
    End Property

    Private _dp_paid As CScopeAtivityCalcEntry
    Public Property dp_paid() As CScopeAtivityCalcEntry
        Get
            Return _dp_paid
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_dp_paid, value) Then
                _dp_paid = value
            End If
        End Set
    End Property

    Private _dp_un_paid As CScopeAtivityCalcEntry
    Public Property dp_un_paid() As CScopeAtivityCalcEntry
        Get
            Return _dp_un_paid
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_dp_un_paid, value) Then
                _dp_un_paid = value
            End If
        End Set
    End Property

    Private _dp_unpaid_not_due As CScopeAtivityCalcEntry
    Public Property dp_unpaid_not_due() As CScopeAtivityCalcEntry
        Get
            Return _dp_unpaid_not_due
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_dp_unpaid_not_due, value) Then
                _dp_unpaid_not_due = value
            End If
        End Set
    End Property

    Private _dp_total As CScopeAtivityCalcEntry
    Public Property dp_total() As CScopeAtivityCalcEntry
        Get
            Return _dp_total
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_dp_total, value) Then
                _dp_total = value
            End If
        End Set
    End Property

    Private _outstanding As CScopeAtivityCalcEntry
    Public Property outstanding() As CScopeAtivityCalcEntry
        Get
            Return _outstanding
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_outstanding, value) Then
                _outstanding = value
            End If
        End Set
    End Property

    'awq report
    Private _sent_awq_pending As CScopeAtivityCalcEntry
    Public Property sent_awq_pending() As CScopeAtivityCalcEntry
        Get
            Return _sent_awq_pending
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sent_awq_pending, value) Then
                _sent_awq_pending = value
            End If
        End Set
    End Property
    Private _sent_awq_approved As CScopeAtivityCalcEntry
    Public Property sent_awq_approved() As CScopeAtivityCalcEntry
        Get
            Return _sent_awq_approved
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sent_awq_approved, value) Then
                _sent_awq_approved = value
            End If
        End Set
    End Property
    Private _sent_awq_approved_pending As CScopeAtivityCalcEntry
    Public Property sent_awq_approved_pending() As CScopeAtivityCalcEntry
        Get
            Return _sent_awq_approved_pending
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            If Object.Equals(_sent_awq_approved_pending, value) Then
                _sent_awq_approved_pending = value
            End If
        End Set
    End Property
End Class
