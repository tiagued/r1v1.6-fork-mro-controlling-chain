﻿Module MTabNavigationStory
    Private confTabChangeValidation As KeyValuePair(Of Boolean, String)
    Private mainTabChangeValidation As KeyValuePair(Of Boolean, String)

    Public Sub addDefaultNavigationStoryHandlers()
        'when selecting a page in the config tabcontrol
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_ctrl_config.Selecting, AddressOf tab_ctrl_config_Selecting
        'when leaving a page of the config control
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_ctrl_config_page_billing.Leave, AddressOf tab_ctrl_config_page_billing_Leave
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_ctrl_config_page_proj.Leave, AddressOf tab_ctrl_config_page_proj_Leave
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_ctrl_config_page_rates.Leave, AddressOf tab_ctrl_config_page_rates_Leave
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_ctrl_config_page_editable_lov.Leave, AddressOf config_tab_ctrl_default_no_validation_Leave

        'add handler when leaving a page in application main tabcontrol
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_ctrl_main.Selecting, AddressOf tab_ctrl_main_Selecting
        'when leaving a page of the main tab control
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_main_page_config.Leave, AddressOf tab_ctrl_main_page_config_Leave
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_main_page_init_scope.Leave, AddressOf tab_ctrl_main_page_default_no_validation_Leave
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_main_page_add_scope.Leave, AddressOf tab_ctrl_main_page_default_no_validation_Leave
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_main_page_materials.Leave, AddressOf tab_ctrl_main_page_default_no_validation_Leave
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_main_page_awq.Leave, AddressOf tab_ctrl_main_page_default_no_validation_Leave
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_main_proj_ctrl.Leave, AddressOf tab_ctrl_main_page_default_no_validation_Leave
        AddHandler MConstants.GLB_MSTR_CTRL.mainForm.tab_main_proj_ctrl.Leave, AddressOf tab_ctrl_main_page_labor_controlling_Leave
    End Sub

    'changing selected page in main tab control 
    Private Sub tab_ctrl_main_Selecting(sender As Object, e As TabControlCancelEventArgs)
        If Not mainTabChangeValidation.Key Then
            e.Cancel = True
            MsgBox(mainTabChangeValidation.Value, MsgBoxStyle.Critical)
        End If
        'initialize
        mainTabChangeValidation = New KeyValuePair(Of Boolean, String)(False, "")
    End Sub

    'validate when leave config page 
    Private Sub tab_ctrl_main_page_config_Leave(sender As Object, e As EventArgs)
        mainTabChangeValidation = MTabNavigationStory.conf_all_pages_check(MConstants.GLB_MSTR_CTRL)
    End Sub

    'default leave with no validation
    Private Sub tab_ctrl_main_page_labor_controlling_Leave(sender As Object, e As EventArgs)
        mainTabChangeValidation = New KeyValuePair(Of Boolean, String)(True, "")
        'unload
        MConstants.GLB_MSTR_CTRL.labor_controlling_controller.unload_labor_controlling()
    End Sub

    'leaving labor controlling tab, unload items
    Private Sub tab_ctrl_main_page_default_no_validation_Leave(sender As Object, e As EventArgs)
        mainTabChangeValidation = New KeyValuePair(Of Boolean, String)(True, "")
    End Sub

    'changing selected page in config tab control config
    Private Sub tab_ctrl_config_Selecting(sender As Object, e As TabControlCancelEventArgs)
        If Not confTabChangeValidation.Key Then
            e.Cancel = True
            MsgBox(confTabChangeValidation.Value, MsgBoxStyle.Critical)
        End If
        'initialize
        confTabChangeValidation = New KeyValuePair(Of Boolean, String)(False, "")
    End Sub

    'validate billing page leave
    Private Sub tab_ctrl_config_page_billing_Leave(sender As Object, e As EventArgs)
        confTabChangeValidation = MTabNavigationStory.conf_bill_cond_check(MConstants.GLB_MSTR_CTRL)
        If confTabChangeValidation.Key Then
            'write to DB if ok
        End If
    End Sub

    'validate config project page leave 
    Private Sub tab_ctrl_config_page_proj_Leave(sender As Object, e As EventArgs)
        confTabChangeValidation = MTabNavigationStory.conf_proj_check(MConstants.GLB_MSTR_CTRL)
        If confTabChangeValidation.Key Then
            'write to DB if ok
        End If
    End Sub

    'validate config labor rate page leave 
    Private Sub tab_ctrl_config_page_rates_Leave(sender As Object, e As EventArgs)
        confTabChangeValidation = MTabNavigationStory.conf_labor_rate_check(MConstants.GLB_MSTR_CTRL)
        If confTabChangeValidation.Key Then
            'write to DB if ok
        End If
    End Sub

    'default leave with no validation config
    Private Sub config_tab_ctrl_default_no_validation_Leave(sender As Object, e As EventArgs)
        confTabChangeValidation = New KeyValuePair(Of Boolean, String)(True, "")
    End Sub

    'check all properties are filled on project layout
    Public Function conf_proj_check(mctrl As CMasterController) As KeyValuePair(Of Boolean, String)
        Dim isOK As Boolean
        Dim errMess As String
        Dim proj As CProjectModel

        isOK = True
        errMess = "Please enter a value in the following fields:"
        proj = mctrl.project_controller.project

        If String.IsNullOrEmpty(proj.ac_group) Then
            isOK = False
            errMess = errMess & Chr(10) & "Aircraft Group"
        End If

        If proj.input_date = MConstants.APP_NULL_DATE OrElse Not proj.curr_input_date_picker_checked Then
            isOK = False
            errMess = errMess & Chr(10) & "CURRENT INPUT DATE"
        End If

        If proj.fcst_finish_date = MConstants.APP_NULL_DATE OrElse Not proj.end_fcst_date_picker_checked Then
            isOK = False
            errMess = errMess & Chr(10) & "CURRENT CRS FCST DATE"
        End If

        If String.IsNullOrEmpty(proj.customer) Then
            isOK = False
            errMess = errMess & Chr(10) & "Customer"
        End If

        If String.IsNullOrEmpty(proj.customer_email) Then
            isOK = False
            errMess = errMess & Chr(10) & "Customer email"
        End If

        If String.IsNullOrEmpty(proj.billing_group) Then
            isOK = False
            errMess = errMess & Chr(10) & "Billing Group"
        End If

        Return New KeyValuePair(Of Boolean, String)(isOK, errMess)
    End Function


    'check all properties are filled on billing cond layout
    Public Function conf_bill_cond_check(mctrl As CMasterController) As KeyValuePair(Of Boolean, String)
        Dim isOK As Boolean
        Dim errMess As String
        Dim proj As CProjectModel

        isOK = True
        errMess = "Please enter a value in the following fields:"
        proj = mctrl.project_controller.project

        If String.IsNullOrEmpty(proj.lab_curr) OrElse proj.lab_curr.ToLower.Equals(MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value.ToLower) Then
            isOK = False
            errMess = errMess & Chr(10) & "Labor Currency"
        End If
        If String.IsNullOrEmpty(proj.mat_curr) OrElse proj.mat_curr.ToLower.Equals(MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value.ToLower) Then
            isOK = False
            errMess = errMess & Chr(10) & "Material Currency"
        End If
        If proj.tagk_date = MConstants.APP_NULL_DATE OrElse Not proj.tagk_date_format_checked Then
            isOK = False
            errMess = errMess & Chr(10) & "TAGK Date"
        End If
        'material markup
        If proj.material_markup = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            isOK = False
            errMess = errMess & Chr(10) & "Markup"
        End If
        'ecotax
        If proj.eco_tax = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            isOK = False
            errMess = errMess & Chr(10) & "EcoTax"
        End If
        'gum
        If proj.gum = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            isOK = False
            errMess = errMess & Chr(10) & "GUM"
        End If
        'check tagk value
        If isOK Then
            errMess = ""
            For Each val As Double In mctrl.billing_cond_controller._tagk_list.Values
                If val = 0 Then
                    errMess = Chr(10) & "One or more TAGK Rate are equals to 0 !"
                    isOK = False
                End If
            Next
        End If
        Return New KeyValuePair(Of Boolean, String)(isOK, errMess)
    End Function

    'check all properties are filled on labor rale
    Public Function conf_labor_rate_check(mctrl As CMasterController) As KeyValuePair(Of Boolean, String)
        Dim isOK As Boolean
        Dim errMess As String
        Dim proj As CProjectModel

        isOK = True
        errMess = "Please enter a value in the following fields:"
        proj = mctrl.project_controller.project

        If String.IsNullOrEmpty(proj.labor_rate_std) OrElse proj.labor_rate_std = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            isOK = False
            errMess = errMess & Chr(10) & "Labor Catalog"
        End If

        Return New KeyValuePair(Of Boolean, String)(isOK, errMess)
    End Function

    'check all properties are in all config pages
    Public Function conf_all_pages_check(mctrl As CMasterController) As KeyValuePair(Of Boolean, String)
        Dim isOK As Boolean
        Dim errMess As String
        Dim proj As CProjectModel
        Dim res As KeyValuePair(Of Boolean, String)
        isOK = True
        errMess = "Please edit mandatory values in the following config pages :"
        proj = mctrl.project_controller.project

        'check project data page
        res = conf_proj_check(mctrl)
        If Not res.Key Then
            isOK = False
            errMess = errMess & Chr(10) & Chr(10) & "Project Tab, " & res.Value
        End If
        'check billing condition data page
        res = conf_bill_cond_check(mctrl)
        If Not res.Key Then
            isOK = False
            errMess = errMess & Chr(10) & Chr(10) & "Billing Conditions Tab, " & res.Value
        End If
        'check labor rate data page
        res = conf_labor_rate_check(mctrl)
        If Not res.Key Then
            isOK = False
            errMess = errMess & Chr(10) & Chr(10) & "Labor Rates Tab, " & res.Value
        End If

        Return New KeyValuePair(Of Boolean, String)(isOK, errMess)
    End Function
End Module
