﻿Public Class CAWQuotationRowValidator
    Public Shared instance As CAWQuotationRowValidator
    Public Shared Function getInstance() As CAWQuotationRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CAWQuotationRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(awq As CAWQuotation)
        awq.calc_is_in_error_state = False
    End Sub

    'check calculation ran without error
    Public Function errorText(awq As CAWQuotation) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not String.IsNullOrWhiteSpace(awq.calc_error_text_thrown) Then
            err = awq.calc_error_text_thrown
            res = False
        Else
            res = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'main activity compulsory
    Public Function main_activity_obj(awq As CAWQuotation) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If awq.is_archived Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        Dim checked_awq As CAWQuotation
        If Not IsNothing(awq.master_obj) Then
            checked_awq = awq.master_obj
        Else
            checked_awq = awq
        End If

        If IsNothing(checked_awq.main_activity_obj) OrElse checked_awq.main_activity_obj.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value Then
            err = "Main activity cannot be empty"
            res = False
            awq.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'at least one activity to be mapped
    Public Function dependantActList(awq As CAWQuotation) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If awq.is_archived Then
            Return New KeyValuePair(Of Boolean, String)(True, "")

        End If
        Dim checked_awq As CAWQuotation
        If Not IsNothing(awq.master_obj) Then
            checked_awq = awq.master_obj
        Else
            checked_awq = awq
        End If

        If checked_awq.dependantActList.Count <= 0 Then
            err = "At least one activity shall be mapped to the AWQ"
            res = False
            awq.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function description(awq As CAWQuotation) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If awq.is_archived Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If String.IsNullOrWhiteSpace(awq.description) Then
            err = "AWQ description cannot be empty"
            res = False
            awq.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function offer_validity(awq As CAWQuotation) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If awq.is_archived Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value _
            OrElse awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value Then
            If Not (IsDate(awq.offer_validity) AndAlso awq.offer_validity > MConstants.APP_NULL_DATE AndAlso awq.offer_validity < Date.MaxValue) Then
                err = "AWQ Offer Validity date cannot be empty"
                res = False
                awq.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)

    End Function

    Public Function ground_time_unit(awq As CAWQuotation) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If awq.is_archived Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If awq.ground_time > 0 AndAlso (String.IsNullOrWhiteSpace(awq.ground_time_unit) Or awq.ground_time_unit = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value) Then
            err = "Ground time unit cannot be empty"
            res = False
            awq.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function


    Public Function cust_sent_date(awq As CAWQuotation) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value _
            OrElse awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value Then
            If Not (IsDate(awq.cust_sent_date) AndAlso awq.cust_sent_date > MConstants.APP_NULL_DATE And awq.cust_sent_date < Date.MaxValue) Then
                err = "AWQ Customer Sent Date cannot be empty"
                res = False
                awq.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function cust_feedback_due_date(awq As CAWQuotation) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value OrElse awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value Then
            If Not (IsDate(awq.cust_feedback_due_date) AndAlso awq.cust_feedback_due_date > MConstants.APP_NULL_DATE And awq.cust_feedback_due_date < Date.MaxValue) Then
                err = "AWQ Customer Feedback Due Date cannot be empty"
                res = False
                awq.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function cust_feedback_actual_date(awq As CAWQuotation) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value Then
            If Not (IsDate(awq.cust_feedback_actual_date) AndAlso awq.cust_feedback_actual_date > MConstants.APP_NULL_DATE And awq.cust_feedback_actual_date < Date.MaxValue) Then
                err = "AWQ Customer Feedback Actual Date cannot be empty if status is closed"
                res = False
                awq.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function cust_feedback(awq As CAWQuotation) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value AndAlso awq.cust_feedback <> MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK_APPROVED_KEY).value Then
            err = "AWQ Customer Feedback Value must be approved When the AWQ Status is closed"
            res = False
            awq.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function
    Public Function issue_date(awq As CAWQuotation) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_QUOTE_READY_KEY).value OrElse
            awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_WAIT_PM_APP_KEY).value OrElse
            awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value OrElse
             awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value Then
            If Not (IsDate(awq.issue_date) AndAlso awq.issue_date > MConstants.APP_NULL_DATE And awq.issue_date < Date.MaxValue) Then
                err = "AWQ Issue Date cannot be empty"
                res = False
                awq.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function


    Public Function internal_status(awq As CAWQuotation) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If String.IsNullOrWhiteSpace(awq.internal_status) Or awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "AWQ Internal Status cannot be empty"
            res = False
            awq.calc_is_in_error_state = True
        End If

        'if status is set to release or cancel, for user to use the buttons
        If awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CANCEL_KEY).value And awq.internal_status <> awq.system_status Then
            err = "Please use the CANCEL button to complete AWQ cancelation"
            res = False
            awq.calc_is_in_error_state = True
        End If

        'if status is set to release or cancel, force user to use the buttons
        If awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value And awq.internal_status <> awq.system_status Then
            err = "Please use the Release button to complete AWQ release process"
            res = False
            awq.calc_is_in_error_state = True
        End If

        'if status is set to release or cancel or replaced, for user to use the buttons
        If awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_SUPERS_REVISED_KEY).value And awq.internal_status <> awq.system_status Then
            err = "Please use the New Revision button to complete AWQ new revision process"
            res = False
            awq.calc_is_in_error_state = True
        End If

        'if status is set to release or cancel or replaced, for user to use the buttons
        If awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value And awq.internal_status <> awq.system_status Then
            err = "Please use the Close button to complete AWQ closure process"
            res = False
            awq.calc_is_in_error_state = True
        End If

        'if status is set to release or cancel, for user to use the buttons
        If awq.internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_ARCHIVED_KEY).value And awq.internal_status <> awq.system_status Then
            err = "Please use the ARCHIVE button to complete AWQ archiving"
            res = False
            awq.calc_is_in_error_state = True
        End If

        Return New KeyValuePair(Of Boolean, String)(res, err)

    End Function
End Class