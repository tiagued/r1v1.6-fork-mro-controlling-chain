﻿Imports System.Data.OleDb

Module MGeneralFunctionsDB
    Public Function formatValue(val As Object, type As String) As Object
        Dim res As Object
        If type.ToLower() = "string" Then
            Dim typeval As String
            Try
                If IsDBNull(val) Then
                    typeval = ""
                Else
                    typeval = CStr(val)
                    If String.IsNullOrEmpty(CStr(typeval)) Then
                        typeval = ""
                    End If
                End If

            Catch ex As Exception
                Throw New Exception("Value [" & val & "] cannot be casted to " & type)
            End Try
            res = Trim(typeval)
        ElseIf type.ToLower() = "double" Then
            Dim typeval As Double = 0
            Try
                If IsDBNull(val) Then
                    typeval = 0
                ElseIf Not Double.TryParse(val, typeval) Then
                    typeval = 0
                End If

            Catch ex As Exception
                Throw New Exception("Value [" & val & "] cannot be casted to " & type)
            End Try
            res = typeval
        ElseIf type.ToLower() = "long" Or type.ToLower() = "int64" Then
            Dim typeval As Long = 0
            Try
                If IsDBNull(val) Then
                    typeval = 0
                ElseIf Not Long.TryParse(val, typeval) Then
                    typeval = 0
                End If

            Catch ex As Exception
                Throw New Exception("Value [" & val & "] cannot be casted to " & type)
            End Try
            res = typeval
        ElseIf type.ToLower() = "datetime" Or type.ToLower() = "date" Then
            Dim typeval As Date = MConstants.APP_NULL_DATE
            Try
                If IsDBNull(val) Then
                    typeval = MConstants.APP_NULL_DATE
                ElseIf IsNumeric(val) Then
                    Try
                        typeval = DateTime.FromOADate(val)
                    Catch ex As Exception
                        Date.TryParse(val, typeval)
                    End Try
                ElseIf Not Date.TryParse(val, typeval) Then
                    typeval = MConstants.APP_NULL_DATE
                Else
                    typeval = CDate(val)
                End If

            Catch ex As Exception
                typeval = MConstants.APP_NULL_DATE
            End Try
            res = typeval
        ElseIf type.ToLower() = "boolean" Then
            Dim typeval As Boolean
            Try
                If IsDBNull(val) Then
                    typeval = False
                ElseIf Not Boolean.TryParse(val, typeval) Then
                    Dim _int As Integer
                    If Integer.TryParse(val, _int) Then
                        typeval = Convert.ToBoolean(_int)
                    Else
                        typeval = False
                    End If
                End If

            Catch ex As Exception
                Throw New Exception("Value [" & val & "] cannot be casted to " & type)
            End Try
            res = typeval
        ElseIf type.ToLower().StartsWith("int") Then
            Dim typeval As Integer
            Try
                If IsDBNull(val) Then
                    typeval = 0
                ElseIf Not Integer.TryParse(val, typeval) Then
                    typeval = 0
                End If

            Catch ex As Exception
                Throw New Exception("Value [" & val & "] cannot be casted to " & type)
            End Try
            res = typeval
        Else
            res = val
        End If
        Return res
    End Function



    Public Function getValueOledbType(type As String) As OleDbType
        Dim oledbType As OleDbType
        If type.ToLower() = "string" Then
            oledbType = OleDbType.LongVarChar
        ElseIf type.ToLower() = "double" Then
            oledbType = OleDbType.Double
        ElseIf type.ToLower() = "long" Or type.ToLower() = "int64" Then
            oledbType = OleDbType.Integer
        ElseIf type.ToLower() = "date" Or type.ToLower() = "datetime" Then
            oledbType = OleDbType.Date
        ElseIf type.ToLower() = "boolean" Then
            oledbType = OleDbType.Boolean
        ElseIf type.ToLower().StartsWith("int32") OrElse type.ToLower().StartsWith("int") Then
            oledbType = OleDbType.Integer
        Else
            Throw New Exception("Type " & type & ", no oledb equivalent found !")
        End If

        Return oledbType
    End Function

    Public Function formatValueAsString(val As Object) As String
        Dim res As String = ""
        If TypeOf val Is String Then
            Dim typeval As String
            If IsDBNull(val) Then
                typeval = ""
            Else
                res = val
            End If
        ElseIf TypeOf val Is Double Then
            res = val.ToString
        ElseIf TypeOf val Is Integer OrElse TypeOf val Is Long Then
            res = val.ToString
        ElseIf TypeOf val Is Date Then
            Dim myDate As Date = val
            res = myDate.ToString("dd/MM/yyyy")
        ElseIf TypeOf val Is DateTime Then
            Dim myDate As DateTime = val
            res = myDate.ToString("dd/MM/yyyy HH:mm:ss")
        ElseIf TypeOf val Is Boolean Then
            res = val.ToString
        Else
            res = val.ToString
        End If
        Return res
    End Function
End Module
