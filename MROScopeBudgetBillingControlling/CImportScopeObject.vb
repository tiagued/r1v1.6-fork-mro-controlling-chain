﻿Public Class CImportScopeObject

    Public Sub New()
        _cc_list = New Dictionary(Of String, CImportScopeObject)
        correctiveActionList = New List(Of String)
    End Sub

    Public correctiveActionList As List(Of String)

    Private _ac_reg As String
    Public Property ac_reg() As String
        Get
            Return _ac_reg
        End Get
        Set(ByVal value As String)
            _ac_reg = value
        End Set
    End Property


    Private _ac_master As String
    Public Property ac_master() As String
        Get
            Return _ac_master
        End Get
        Set(ByVal value As String)
            _ac_master = value
        End Set
    End Property

    Private _manufacturer As String
    Public Property manufacturer() As String
        Get
            Return _manufacturer
        End Get
        Set(ByVal value As String)
            _manufacturer = value
        End Set
    End Property

    Private _network As String
    Public Property network() As String
        Get
            Return _network
        End Get
        Set(ByVal value As String)
            _network = value
        End Set
    End Property

    Private _network_description As String
    Public Property network_description() As String
        Get
            Return _network_description
        End Get
        Set(ByVal value As String)
            _network_description = value
        End Set
    End Property

    Private _network_activity As String
    Public Property network_activity() As String
        Get
            Return _network_activity
        End Get
        Set(ByVal value As String)
            _network_activity = value
        End Set
    End Property

    Private _notif As String
    Public Property notif() As String
        Get
            Return _notif
        End Get
        Set(ByVal value As String)
            _notif = value
        End Set
    End Property


    Private _job_card As String
    Public Property job_card() As String
        Get
            Return _job_card
        End Get
        Set(ByVal value As String)
            If value.Length < 4 Then
                Dim valInt As Integer
                If Integer.TryParse(value, valInt) Then
                    value = valInt.ToString("D4")
                End If
            End If
            _job_card = value
        End Set
    End Property

    Private _notif_type As String
    Public Property notif_type() As String
        Get
            Return _notif_type
        End Get
        Set(ByVal value As String)
            _notif_type = value
        End Set
    End Property

    Private _user_stat As String
    Public Property user_stat() As String
        Get
            Return Trim(_user_stat)
        End Get
        Set(ByVal value As String)
            _user_stat = value
        End Set
    End Property

    Private _notif_desc As String
    Public Property notif_desc() As String
        Get
            Return _notif_desc
        End Get
        Set(ByVal value As String)
            _notif_desc = value
        End Set
    End Property

    Private _notif_text As String
    Public Property notif_text() As String
        Get
            Return _notif_text
        End Get
        Set(ByVal value As String)
            _notif_text = value
        End Set
    End Property

    Private _check_type As String
    Public Property check_type() As String
        Get
            Return _check_type
        End Get
        Set(ByVal value As String)
            _check_type = value
        End Set
    End Property

    Private _warr_code As String
    Public Property warr_code() As String
        Get
            Return _warr_code
        End Get
        Set(ByVal value As String)
            _warr_code = value
        End Set
    End Property

    Private _warr_code_desc As String
    Public Property warr_code_desc() As String
        Get
            Return _warr_code_desc
        End Get
        Set(ByVal value As String)
            _warr_code_desc = value
        End Set
    End Property

    Private _operator_code As String
    Public Property operator_code() As String
        Get
            Return _operator_code
        End Get
        Set(ByVal value As String)
            _operator_code = value
        End Set
    End Property

    Private _operation_number As String
    Public Property operation_number() As String
        Get
            Return _operation_number
        End Get
        Set(ByVal value As String)
            _operation_number = value
        End Set
    End Property

    Private _panel As String
    Public Property panel() As String
        Get
            Return _panel
        End Get
        Set(ByVal value As String)
            _panel = value
        End Set
    End Property

    Private _operation_short_text As String
    Public Property operation_short_text() As String
        Get
            Return _operation_short_text
        End Get
        Set(ByVal value As String)
            _operation_short_text = value
        End Set
    End Property

    Private _operation_text As String
    Public Property operation_text() As String
        Get
            Return _operation_text
        End Get
        Set(ByVal value As String)
            _operation_text = value
        End Set
    End Property

    Private _work_center As String
    Public Property work_center() As String
        Get
            Return If(String.IsNullOrWhiteSpace(_work_center), MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value, _work_center)
        End Get
        Set(ByVal value As String)
            _work_center = value
        End Set
    End Property

    Private _work_center_desc As String
    Public Property work_center_desc() As String
        Get
            Return _work_center_desc
        End Get
        Set(ByVal value As String)
            _work_center_desc = value
        End Set
    End Property

    Private _work As Double
    Public Property work() As Double
        Get
            Return _work
        End Get
        Set(ByVal value As Double)
            _work = value
        End Set
    End Property

    'sum of cost center hours
    Private _calculated_planned_hrs As Double
    Public Property calculated_planned_hrs() As Double
        Get
            Return _calculated_planned_hrs
        End Get
        Set(ByVal value As Double)
            _calculated_planned_hrs = value
        End Set
    End Property
    'concatenation of operations description
    Private _calculated_operation_desc As String
    Public Property calculated_operation_desc() As String
        Get
            Return _calculated_operation_desc
        End Get
        Set(ByVal value As String)
            _calculated_operation_desc = value
        End Set
    End Property

    Private _cc_list As Dictionary(Of String, CImportScopeObject)
    Public ReadOnly Property cc_list() As Dictionary(Of String, CImportScopeObject)
        Get
            Return _cc_list
        End Get
    End Property

    Public Function getActivity() As CScopeActivity
        Dim act As New CScopeActivity()
        act.proj = Trim(_network)
        act.act = Trim(_network_activity)
        act.notification = Trim(_notif)
        act.job_card = Trim(_job_card)
        act.status = Trim(_user_stat)
        act.category = Trim(_notif_type)
        act.act_desc = Trim(_notif_desc)
        act.act_long_desc = Trim(_notif_text)
        act.check_type = Trim(_check_type)
        act.warr_code = Trim(_warr_code)
        act.operator_code = Trim(_operator_code)
        act.corrective_action = String.Join(Chr(10), correctiveActionList)
        Return act
    End Function

    Public Function getCostCollectorActivity() As CScopeActivity
        Dim act As New CScopeActivity()
        act.proj = _network
        act.act = _network_activity
        act.notification = _notif
        act.act_desc = _notif_desc
        act.status = "" '_user_stat
        act.job_card = "0000"
        act.category = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        act.act_long_desc = _notif_desc
        act.check_type = ""
        act.warr_code = ""
        act.operator_code = ""
        act.corrective_action = ""
        act.is_cost_collector = True
        Dim firstDigit As Integer = 0
        If Not String.IsNullOrWhiteSpace(_network_activity) AndAlso Integer.TryParse(Left(_network_activity, 1), firstDigit) Then
            If firstDigit > 7 Then
                act.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            ElseIf firstDigit = 0 Then
                act.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_NOT_ADD_V_KEY).value
            End If
        ElseIf IsNumeric(_network_activity) AndAlso CLng(_network_activity) < 1000 Then
            act.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_NOT_ADD_V_KEY).value
        End If

        Return act
    End Function

    Public Function getSoldHour(act As CScopeActivity) As CSoldHours
        Dim sold As New CSoldHours()

        sold.act_object = act
        sold.operation_desc = _calculated_operation_desc
        sold.plan_cc = _work_center
        sold.billing_work_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        'if work center in cc list
        If MConstants.listOfCC.ContainsKey(_work_center) Then
            sold.cc = _work_center
        Else
            sold.cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        End If

        sold.plan_hrs = _calculated_planned_hrs
        sold.is_sap_import = True
        Return sold
    End Function

    Public Function getOperationDesc() As String
        Return "Op:" & _operation_number & MConstants.SEP_COMMA & "Desc:" & _operation_short_text & MConstants.SEP_COMMA & "Panel:" & _panel & MConstants.SEP_COMMA & "Hours:" & _work
    End Function

    Public Function getCorrectiveAction() As String
        Return _operation_number & " " & operation_text
    End Function
    Public Function getActKey() As String
        Return Trim(_network) & MConstants.SEP_DEFAULT & Trim(_network_activity) & MConstants.SEP_DEFAULT & Trim(_notif)
    End Function
End Class