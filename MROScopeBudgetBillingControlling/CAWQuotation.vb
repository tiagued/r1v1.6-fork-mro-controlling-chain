﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CAWQuotation
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    'used to created new ids
    Public Shared currentMaxID As Integer

    Public Sub New()
        _revision_list = New List(Of CAWQuotation)
        _ground_time_unit = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        _prioriry = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        _airworthiness = MConstants.constantConf(MConstants.CONST_CONF_AWQ_AIRWORTHY_LOV_NO_IMP_KEY).value
        _internal_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_COST_IN_PROGR_KEY).value
        _cust_feedback = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        _oem_feedback = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        _pricing = MConstants.constantConf(MConstants.CONST_CONF_AWQ_PRICING_LOV_FIX_KEY).value
        _weight_and_balance = MConstants.constantConf(MConstants.CONST_CONF_AWQ_WEIGHT_BAL_DEFAULT).value
        _payment_terms = MConstants.constantConf(MConstants.CONST_CONF_AWQ_PAYMENT_COND_DEFAULT).value
        _dependantActList = New List(Of CScopeActivity)

        'init date
        _offer_validity = MConstants.APP_NULL_DATE
        _due_date = MConstants.APP_NULL_DATE
        _issue_date = MConstants.APP_NULL_DATE
        _cust_sent_date = MConstants.APP_NULL_DATE
        _cust_feedback_due_date = MConstants.APP_NULL_DATE
        _cust_feedback_actual_date = MConstants.APP_NULL_DATE

        _sold_hour_list = New List(Of CSoldHours)
        _material_list = New List(Of CMaterial)
        _sold_hour_removed_list = New List(Of CSoldHours)
        _material_removed_list = New List(Of CMaterial)

        calculator = New CAWQuotationCalucaltor(Me)
    End Sub

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        If Not IsNothing(MConstants.GLB_MSTR_CTRL.notifHelper) Then
            MConstants.GLB_MSTR_CTRL.notifHelper.OnPropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
        End If
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Sub setID()
        MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
    End Sub

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = True
        Dim mess As String = ""
        If Not Me.is_in_work Then
            res = False
            mess = "Cannot be modify cause it's been closed"
        End If
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return _id > 0
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
        'master id
        If IsNothing(_master_obj) OrElse _master_id <= 0 Then
            'first rev
            Me.master_id = t_id
            'set data on activity
            For Each act As CScopeActivity In _dependantActList
                act.awq_master_id = id
            Next
        End If
        'Me.setReference() done before call of setid

        For Each soldHr As CSoldHours In _sold_hour_list
            soldHr.awq_id = id
        Next
        For Each mat As CMaterial In _material_list
            mat.awq_id = id
        Next
        For Each soldHr As CSoldHours In _sold_hour_removed_list
            soldHr.remove_awq_id = id
        Next
        For Each mat As CMaterial In _material_removed_list
            mat.remove_awq_id = id
        Next

    End Sub

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        If Not _calc_is_in_error_state AndAlso Not String.IsNullOrWhiteSpace(_calc_error_text_thrown) Then
            Return New KeyValuePair(Of Boolean, String)(True, _calc_error_text_thrown)
        Else
            Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
        End If
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_AWQ_DB_READ
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res = False
        Dim mess = "It is not allowed to delete AWQ"
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Shared Function getEmptyAWQ() As CAWQuotation
        Dim currState As Boolean
        currState = MConstants.GLOB_AWQ_LOADED
        MConstants.GLOB_AWQ_LOADED = False
        Dim empty As New CAWQuotation
        empty.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
        'if proj nothing it will cause issue in combobox selection
        empty.reference = " "
        MConstants.GLOB_AWQ_LOADED = currState
        Return empty
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_AWQ_LOADED Then
                    CAWQuotation.currentMaxID = Math.Max(CAWQuotation.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _main_activity_id As Long
    Public Property main_activity_id() As Long
        Get
            Return _main_activity_id
        End Get
        Set(ByVal value As Long)
            If Not _main_activity_id = value Then
                _main_activity_id = value
                If GLOB_AWQ_LOADED Then
                    'retrieve only for none empty
                    If _main_activity_id > 0 Then
                        _main_activity_obj = MConstants.GLB_MSTR_CTRL.add_scope_controller.getActivityObject(_main_activity_id)
                    Else
                        _main_activity_obj = Nothing
                    End If
                End If
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _main_activity_obj As CScopeActivity
    Public Property main_activity_obj() As CScopeActivity
        Get
            Return _main_activity_obj
        End Get
        Set(ByVal value As CScopeActivity)
            If Not Object.Equals(value, _main_activity_obj) Then
                _main_activity_obj = value
                'set ID
                If IsNothing(_main_activity_obj) Then
                    _main_activity_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _main_activity_id = _main_activity_obj.id
                End If
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _master_id As Long
    Public Property master_id() As Long
        Get
            Return _master_id
        End Get
        Set(ByVal value As Long)
            If Not _master_id = value Then
                _master_id = value
                If GLOB_AWQ_LOADED Then
                    'retrieve only for none empty
                    If _master_id > 0 Then
                        If Not (Not IsNothing(_master_obj) AndAlso _master_obj.id = _master_id) Then
                            _master_obj = MConstants.GLB_MSTR_CTRL.awq_controller.getAWQObject(_master_id)
                        End If
                    Else
                        _master_obj = Nothing
                    End If
                End If
                'NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _master_obj As CAWQuotation
    Public Property master_obj() As CAWQuotation
        Get
            Return _master_obj
        End Get
        Set(ByVal value As CAWQuotation)
            If Not Object.Equals(_master_obj, value) Then
                _master_obj = value
                'set ID
                If IsNothing(_master_obj) Then
                    Me.master_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    Me.master_id = _master_obj.id
                End If
                'NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _revision_list As List(Of CAWQuotation)

    'use function, not property to avoid stack overflow exception when reset bindingview
    Public Function revision_list() As List(Of CAWQuotation)
        Return _revision_list
    End Function
    Public Sub setReference()
        Dim master_awq As CAWQuotation
        If Not IsNothing(_master_obj) Then
            master_awq = _master_obj
        Else
            master_awq = Me
        End If
        Me.reference = master_awq.main_activity_obj.proj & "-" & master_awq.main_activity_obj.act & "-JC" & master_awq.main_activity_obj.job_card
    End Sub

    Private _reference As String
    Public Property reference() As String
        Get
            Return _reference
        End Get
        Set(ByVal value As String)
            If Not _reference = value Then
                _reference = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _temp_reference As String
    Public Property temp_reference() As String
        Get
            Return _temp_reference
        End Get
        Set(ByVal value As String)
            If Not _temp_reference = value Then
                _temp_reference = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public ReadOnly Property revision_str As String
        Get
            Return MConstants.constantConf(MConstants.CONST_CONF_AWQ_REVISION_PREFIX).value & (_revision).ToString("00")
        End Get
    End Property

    Public ReadOnly Property reference_revision As String
        Get
            Return _reference & "-" & Me.revision_str
        End Get
    End Property

    Public ReadOnly Property reference_revision_and_temp_ref As String
        Get
            If Not String.IsNullOrWhiteSpace(_temp_reference) Then
                Return Me.reference_revision & Chr(10) & "(" & _temp_reference & "-" & Me.revision_str & ")"
            Else
                Return Me.reference_revision
            End If
        End Get
    End Property

    Private _revision As Integer
    Public Property revision() As Integer
        Get
            Return _revision
        End Get
        Set(ByVal value As Integer)
            If Not _revision = value Then
                _revision = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_pm_go_ahead As Boolean
    Public Property is_pm_go_ahead() As Boolean
        Get
            Return _is_pm_go_ahead
        End Get
        Set(ByVal value As Boolean)
            If Not _is_pm_go_ahead = value Then
                _is_pm_go_ahead = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'ON MASTER
    Public Function getPreviouApprovedRevs() As List(Of String)
        Dim res As New List(Of String)
        If _revision = 0 OrElse IsNothing(_master_obj) Then
            Return res
        End If

        For Each awq As CAWQuotation In _master_obj.revision_list
            If awq.revision < _revision And awq.system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value Then
                If Not res.Contains(awq.reference_revision) Then
                    res.Add(awq.reference_revision)
                End If
            End If
        Next
        Return res
    End Function

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            If Not _description = value Then
                _description = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _comments As String
    Public Property comments() As String
        Get
            Return _comments
        End Get
        Set(ByVal value As String)
            If Not _comments = value Then
                _comments = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _offer_validity As Date
    Public Property offer_validity() As Date
        Get
            Return _offer_validity
        End Get
        Set(ByVal value As Date)
            If Not _offer_validity = value Then
                _offer_validity = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _ground_time As Long
    Public Property ground_time() As Long
        Get
            Return _ground_time
        End Get
        Set(ByVal value As Long)
            If Not _ground_time = value Then
                _ground_time = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _ground_time_unit As String
    Public Property ground_time_unit() As String
        Get
            Return _ground_time_unit
        End Get
        Set(ByVal value As String)
            If Not _ground_time_unit = value Then
                _ground_time_unit = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _airworthiness As String
    Public Property airworthiness() As String
        Get
            Return _airworthiness
        End Get
        Set(ByVal value As String)
            If Not _airworthiness = value Then
                _airworthiness = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public ReadOnly Property airworthiness_view As String
        Get
            If Not String.IsNullOrWhiteSpace(_airworthiness) Then
                If MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_AWQ_AIRW_STATUS).ContainsKey(_airworthiness) Then
                    Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_AWQ_AIRW_STATUS)(_airworthiness).value
                Else
                    Return _airworthiness
                End If
            Else
                Return ""
            End If
        End Get
    End Property

    Private _weight_and_balance As String
    Public Property weight_and_balance() As String
        Get
            Return _weight_and_balance
        End Get
        Set(ByVal value As String)
            If Not _weight_and_balance = value Then
                _weight_and_balance = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _prioriry As String
    Public Property prioriry() As String
        Get
            Return _prioriry
        End Get
        Set(ByVal value As String)
            If Not _prioriry = value Then
                _prioriry = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _due_date As Date
    Public Property due_date() As Date
        Get
            Return _due_date
        End Get
        Set(ByVal value As Date)
            If Not _due_date = value Then
                _due_date = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _issue_date As Date
    Public Property issue_date() As Date
        Get
            Return _issue_date
        End Get
        Set(ByVal value As Date)
            If Not _issue_date = value Then
                _issue_date = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _internal_status As String
    Public Property internal_status() As String
        Get
            Return _internal_status
        End Get
        Set(ByVal value As String)
            If Not _internal_status = value Then
                _internal_status = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _cust_sent_date As Date
    Public Property cust_sent_date() As Date
        Get
            Return _cust_sent_date
        End Get
        Set(ByVal value As Date)
            If Not _cust_sent_date = value Then
                _cust_sent_date = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _cust_feedback_due_date As Date
    Public Property cust_feedback_due_date() As Date
        Get
            Return _cust_feedback_due_date
        End Get
        Set(ByVal value As Date)
            If Not _cust_feedback_due_date = value Then
                _cust_feedback_due_date = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _cust_feedback As String
    Public Property cust_feedback() As String
        Get
            Return _cust_feedback
        End Get
        Set(ByVal value As String)
            If Not _cust_feedback = value Then
                _cust_feedback = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _oem_feedback As String
    Public Property oem_feedback() As String
        Get
            Return _oem_feedback
        End Get
        Set(ByVal value As String)
            If Not _oem_feedback = value Then
                _oem_feedback = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Function is_cancelled_awq_applicable(my_payer As String)
        'cust feedback
        If String.IsNullOrWhiteSpace(_oem_feedback) OrElse MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value = _oem_feedback Then
            Return my_payer = MConstants.listOfPayer(MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value).payer
        Else
            'oem feedback
            Return my_payer <> MConstants.listOfPayer(MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value).payer
        End If
    End Function
    Public ReadOnly Property cust_feedback_str() As String
        Get
            If String.IsNullOrWhiteSpace(_cust_feedback) OrElse _cust_feedback = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
                Return ""
            Else
                Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_AWQ_CUST_FBK)(_cust_feedback).value
            End If
        End Get
    End Property

    Private _cust_feedback_actual_date As Date
    Public Property cust_feedback_actual_date() As Date
        Get
            Return _cust_feedback_actual_date
        End Get
        Set(ByVal value As Date)
            If Not _cust_feedback_actual_date = value Then
                _cust_feedback_actual_date = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _cust_comment As String
    Public Property cust_comment() As String
        Get
            Return _cust_comment
        End Get
        Set(ByVal value As String)
            If Not _cust_comment = value Then
                _cust_comment = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _pricing As String
    Public Property pricing() As String
        Get
            Return _pricing
        End Get
        Set(ByVal value As String)
            If Not _pricing = value Then
                _pricing = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _payment_terms As String
    Public Property payment_terms() As String
        Get
            Return _payment_terms
        End Get
        Set(ByVal value As String)
            If Not _payment_terms = value Then
                _payment_terms = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _latest_REV As CAWQuotation
    Public Property latest_REV() As CAWQuotation
        Get
            Return _latest_REV
        End Get
        Set(ByVal value As CAWQuotation)
            _latest_REV = value
            NotifyPropertyChanged()
        End Set
    End Property

    Private _is_latest_rev_calc As Boolean
    Public Property is_latest_rev_calc() As Boolean
        Get
            Return IsNothing(master_obj) OrElse master_obj.latest_REV.Equals(Me)
        End Get
        Set(ByVal value As Boolean)
            NotifyPropertyChanged()
        End Set
    End Property

    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _dp_labor As String
    Public Property dp_labor() As String
        Get
            Return _dp_labor
        End Get
        Set(ByVal value As String)
            If Not _dp_labor = value Then
                _dp_labor = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _dp_material As String
    Public Property dp_material() As String
        Get
            Return _dp_material
        End Get
        Set(ByVal value As String)
            If Not _dp_material = value Then
                _dp_material = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property
    'to string
    Public Overrides Function tostring() As String
        Return Me.reference_revision & " " & _description
    End Function

    'text to check if there are any error
    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_is_in_error_state = value Then
                _calc_is_in_error_state = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Public ReadOnly Property dependant_activities_str() As String
        Get
            Dim res As String = ""
            If Not IsNothing(_master_obj) Then
                For Each act As CScopeActivity In _master_obj.dependantActList
                    If res = "" Then
                        res = act.proj & "-" & act.act
                    Else
                        res = res & ", " & act.proj & "-" & act.act
                    End If
                Next
            End If
            Return res
        End Get
    End Property

    Private _dependantActList As List(Of CScopeActivity)
    Public Function dependantActList() As List(Of CScopeActivity)
        Return _dependantActList
    End Function

    Private _sold_hour_list As List(Of CSoldHours)
    Public Function sold_hour_list() As List(Of CSoldHours)
        Return _sold_hour_list
    End Function

    Private _material_list As List(Of CMaterial)
    Public Function material_list() As List(Of CMaterial)
        Return _material_list
    End Function

    Private _sold_hour_removed_list As List(Of CSoldHours)
    Public Function sold_hour_removed_list() As List(Of CSoldHours)
        Return _sold_hour_removed_list
    End Function

    Private _material_removed_list As List(Of CMaterial)
    Public Function material_removed_list() As List(Of CMaterial)
        Return _material_removed_list
    End Function

    Public Sub clearAWQBindedItems()
        Dim deleteRes As KeyValuePair(Of Boolean, String) = Nothing
        For Each soldH As CSoldHours In _sold_hour_list
            soldH.awq_obj = Nothing
            'delete if not from SAP and initialize to 0 if from SAP
            If soldH.is_sap_import Then
                soldH.initializeSAPImport()
            Else
                deleteRes = soldH.deleteItem()
            End If
        Next
        _sold_hour_list.Clear()
        For Each soldH As CSoldHours In _sold_hour_removed_list
            soldH.remove_awq_obj = Nothing
        Next
        _sold_hour_removed_list.Clear()
        For Each mat As CMaterial In _material_list
            mat.awq_obj = Nothing
            If mat.is_sap_import Then
                mat.initializeSAPImport()
            Else
                deleteRes = mat.deleteItem()
            End If
        Next
        _material_list.Clear()
        For Each mat As CMaterial In _material_removed_list
            mat.remove_awq_obj = Nothing
        Next
        _material_removed_list.Clear()
    End Sub

    Public Sub replaceAWQBindedItems(newRev As CAWQuotation)
        For Each soldH As CSoldHours In _sold_hour_list
            soldH.awq_obj = newRev
            newRev.sold_hour_list.Add(soldH)
        Next
        _sold_hour_list.Clear()
        For Each soldH As CSoldHours In _sold_hour_removed_list
            soldH.remove_awq_obj = newRev
            newRev.sold_hour_removed_list.Add(soldH)
        Next
        _sold_hour_removed_list.Clear()
        For Each mat As CMaterial In _material_list
            mat.awq_obj = newRev
            newRev.material_list.Add(mat)
        Next
        _material_list.Clear()
        For Each mat As CMaterial In _material_removed_list
            mat.remove_awq_obj = newRev
            newRev.material_removed_list.Add(mat)
        Next
        _material_removed_list.Clear()
    End Sub

    'for reporting
    Public ReadOnly Property act_category As String
        Get
            Return If(IsNothing(_master_obj) OrElse IsNothing(_master_obj.main_activity_obj), "", _master_obj.main_activity_obj.category_view)
        End Get
    End Property
    Public ReadOnly Property ground_time_str As String
        Get
            Return If(_ground_time = 0, MConstants.constantConf(MConstants.CONST_CONF_AWQ_GROUND_TIME_DEFAULT).value, _ground_time & " " & _ground_time_unit)
        End Get
    End Property

    Private _hours_sold_calc As Double
    Public Property hours_sold_calc() As Double
        Get
            Return _hours_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _hours_sold_calc Then
                _hours_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _labor_sold_calc As Double
    Public Property labor_sold_calc() As Double
        Get
            Return _labor_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _labor_sold_calc Then
                _labor_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Public ReadOnly Property labor_sold_after_disc_calc() As Double
        Get
            Return _labor_sold_calc + _lab_sold_disc_calc
        End Get
    End Property

    Private _mat_sold_calc As Double
    Public Property mat_sold_calc() As Double
        Get
            Return _mat_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _mat_sold_calc Then
                _mat_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _rep_sold_calc As Double
    Public Property rep_sold_calc() As Double
        Get
            Return _rep_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _rep_sold_calc Then
                _rep_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _serv_sold_calc As Double
    Public Property serv_sold_calc() As Double
        Get
            Return _serv_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _serv_sold_calc Then
                _serv_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _freight_sold_calc As Double
    Public Property freight_sold_calc() As Double
        Get
            Return _freight_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _freight_sold_calc Then
                _freight_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _total_mat_sold_calc As Double
    Public Property total_mat_sold_calc() As Double
        Get
            Return _total_mat_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _total_mat_sold_calc Then
                _total_mat_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Public ReadOnly Property total_mat_sold_aft_disc_calc() As Double
        Get
            Return _total_mat_sold_calc + _total_mat_sold_disc_calc
        End Get
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not _calc_error_text = value Then
                _calc_error_text = value
            End If
        End Set
    End Property

    Private _calc_error_text_thrown As String
    Public Property calc_error_text_thrown() As String
        Get
            Return _calc_error_text_thrown
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    Private _hours_sold_approved_calc As Double
    Public Property hours_sold_approved_calc() As Double
        Get
            Return _hours_sold_approved_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _hours_sold_approved_calc Then
                _hours_sold_approved_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _labor_sold_approved_calc As Double
    Public Property labor_sold_approved_calc() As Double
        Get
            Return _labor_sold_approved_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _labor_sold_approved_calc Then
                _labor_sold_approved_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Public ReadOnly Property labor_sold_after_disc_approved_calc() As Double
        Get
            Return _labor_sold_approved_calc + _lab_sold_disc_approved_calc
        End Get
    End Property

    Private _mat_sold_approved_calc As Double
    Public Property mat_sold_approved_calc() As Double
        Get
            Return _mat_sold_approved_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _mat_sold_approved_calc Then
                _mat_sold_approved_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _rep_sold_approved_calc As Double
    Public Property rep_sold_approved_calc() As Double
        Get
            Return _rep_sold_approved_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _rep_sold_approved_calc Then
                _rep_sold_approved_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _serv_sold_approved_calc As Double
    Public Property serv_sold_approved_calc() As Double
        Get
            Return _serv_sold_approved_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _serv_sold_approved_calc Then
                _serv_sold_approved_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _freight_sold_approved_calc As Double
    Public Property freight_sold_approved_calc() As Double
        Get
            Return _freight_sold_approved_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _freight_sold_approved_calc Then
                _freight_sold_approved_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _total_mat_sold_approved_calc As Double
    Public Property total_mat_sold_approved_calc() As Double
        Get
            Return _total_mat_sold_approved_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _total_mat_sold_approved_calc Then
                _total_mat_sold_approved_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Public ReadOnly Property total_mat_sold_aft_disc_approved_calc() As Double
        Get
            Return _total_mat_sold_approved_calc + _total_mat_sold_disc_approved_calc
        End Get
    End Property

    Public ReadOnly Property getApprPendingLaborAftDisc() As Double
        Get
            Return Me.labor_sold_after_disc_calc + Me.labor_sold_after_disc_approved_calc
        End Get
    End Property

    Public ReadOnly Property getApprPendingTotalMatAftDisc() As Double
        Get
            Return Me.total_mat_sold_aft_disc_calc + Me.total_mat_sold_aft_disc_approved_calc
        End Get
    End Property

    'discounts
    Private _lab_sold_disc_approved_calc As Double
    Public Property lab_sold_disc_approved_calc() As Double
        Get
            Return _lab_sold_disc_approved_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _lab_sold_disc_approved_calc Then
                _lab_sold_disc_approved_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _lab_sold_disc_calc As Double
    Public Property lab_sold_disc_calc() As Double
        Get
            Return _lab_sold_disc_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _lab_sold_disc_calc Then
                _lab_sold_disc_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _total_mat_sold_disc_approved_calc As Double
    Public Property total_mat_sold_disc_approved_calc() As Double
        Get
            Return _total_mat_sold_disc_approved_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _total_mat_sold_disc_approved_calc Then
                _total_mat_sold_disc_approved_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _total_mat_sold_disc_calc As Double
    Public Property total_mat_sold_disc_calc() As Double
        Get
            Return _total_mat_sold_disc_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _total_mat_sold_disc_calc Then
                _total_mat_sold_disc_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    'is any entry of this payer found in this awq ?
    Public Function isPayerCalledInAWQ(payer As CPayer)
        Return Not IsNothing(CAWQuotationCalucaltor.getAWQPayerSummary(Me, payer.payer))
    End Function

    Public ReadOnly Property getMainActLongTextCorrAct() As String
        Get
            Dim masterAWQ As CAWQuotation
            If IsNothing(_master_obj) Then
                masterAWQ = Me
            Else
                masterAWQ = _master_obj
            End If
            If Not IsNothing(masterAWQ.main_activity_obj) Then
                If String.IsNullOrWhiteSpace(masterAWQ.main_activity_obj.act_long_desc) Then
                    Return masterAWQ.main_activity_obj.corrective_action
                Else
                    Return masterAWQ.main_activity_obj.act_long_desc & Chr(10) & masterAWQ.main_activity_obj.corrective_action
                End If
            Else
                Return ""
            End If
        End Get
    End Property

    Public Function is_in_work() As Boolean
        Return Not (_system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CANCEL_KEY).value OrElse _system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value _
            OrElse _system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value OrElse _system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_SUPERS_REVISED_KEY).value _
            OrElse _system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_ARCHIVED_KEY).value)
    End Function
    'has been approved by customer ?
    Public Function is_approved_closed() As Boolean
        Return _system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY).value
    End Function
    'sent to customer but not yet feedback
    Public Function is_open_and_sent_to_customer() As Boolean
        Return _system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY).value
    End Function

    'cancelled or replace
    Public Function is_cancelled_or_superseded() As Boolean

        Return _system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_CANCEL_KEY).value _
                        OrElse _system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_SUPERS_REVISED_KEY).value _
                        OrElse _system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_ARCHIVED_KEY).value
    End Function

    'cancelled or replace
    Public Function is_archived() As Boolean
        Return _system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_AWQ_STAT_ARCHIVED_KEY).value
    End Function

    'sent
    Public Function has_been_sent() As Boolean
        Return Me.is_approved_closed OrElse Me.is_open_and_sent_to_customer OrElse (_cust_sent_date > MConstants.APP_NULL_DATE AndAlso Not Me.is_in_work)
    End Function
    'PM go ahead
    Public Function is_PM_GO() As Boolean
        Return _is_pm_go_ahead
    End Function

    'check if an activity is used in the current AWQ(any revision)
    Public Function isActUsed(act As CScopeActivity)
        Dim res As Boolean = False
        If Not IsNothing(_master_obj) Then
            For Each soldH In act.sold_hour_list
                If Not IsNothing(soldH.awq_obj) AndAlso Not IsNothing(soldH.awq_obj.master_obj) AndAlso soldH.awq_obj.master_obj.Equals(_master_obj) Then
                    Return True
                End If
            Next
            For Each mat In act.material_list
                If Not IsNothing(mat.awq_obj) AndAlso Not IsNothing(mat.awq_obj.master_obj) AndAlso mat.awq_obj.master_obj.Equals(_master_obj) Then
                    Return True
                End If
            Next
        End If
        Return res
    End Function
    'set automatically dates when releasing to customer
    Public Sub setDateAtRelease()
        Try
            If _issue_date = MConstants.APP_NULL_DATE Then
                _issue_date = Now
            End If
            If _cust_sent_date = MConstants.APP_NULL_DATE Then
                _cust_sent_date = Now
            End If
            If _offer_validity = MConstants.APP_NULL_DATE Or _offer_validity < _cust_sent_date Then
                _offer_validity = DateAndTime.DateAdd(DateInterval.Weekday, CDbl(MConstants.constantConf(MConstants.AWR_VALIDITY_DATE_BUFFER).value), _cust_sent_date)
            End If
            If _cust_feedback_due_date = MConstants.APP_NULL_DATE Or _cust_feedback_due_date < _cust_sent_date Then
                _cust_feedback_due_date = DateAndTime.DateAdd(DateInterval.Weekday, CDbl(MConstants.constantConf(MConstants.AWR_FDBCK_DUE_DATE_BUFFER).value), _cust_sent_date)
            End If
        Catch ex As Exception
        End Try
    End Sub
    'set automatically dates when closing awr
    Public Sub setDateAtClosure()
        Try
            setDateAtRelease()
            If _cust_feedback_actual_date = MConstants.APP_NULL_DATE Then
                _cust_feedback_actual_date = Now
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public calculator As CAWQuotationCalucaltor
    'refresh all calculated values of activity only
    'fillLabList use when calculating for generating AWQ form details
    Public Sub updateCal(Optional _payer As String = "", Optional fillLabMatList As Boolean = False)

        Try
            'done it only in runtime, not when reading DB
            If MConstants.GLOB_AWQ_LOADED And Not MConstants.GLOB_SAP_IMPOT_RUNNING Then
                _calc_error_text_thrown = ""

                calculator.update(_payer, fillLabMatList)

                Me.hours_sold_approved_calc = calculator.previousAWQ_Before_Disc.hours_sold_calc
                Me.labor_sold_approved_calc = calculator.previousAWQ_Before_Disc.labor_sold_calc
                Me.mat_sold_approved_calc = calculator.previousAWQ_Before_Disc.mat_sold_calc
                Me.freight_sold_approved_calc = calculator.previousAWQ_Before_Disc.freight_sold_calc
                Me.rep_sold_approved_calc = calculator.previousAWQ_Before_Disc.rep_sold_calc
                Me.serv_sold_approved_calc = calculator.previousAWQ_Before_Disc.serv_sold_calc
                Me.total_mat_sold_approved_calc = Me.mat_sold_approved_calc + Me.freight_sold_approved_calc + Me.rep_sold_approved_calc + Me.serv_sold_approved_calc
                'approved discount
                Me.lab_sold_disc_approved_calc = calculator.previousAWQ_Disc.labor_sold_calc
                Me.total_mat_sold_disc_approved_calc = calculator.previousAWQ_Disc.mat_sold_calc + calculator.previousAWQ_Disc.freight_sold_calc + calculator.previousAWQ_Disc.rep_sold_calc + calculator.previousAWQ_Disc.serv_sold_calc

                Me.hours_sold_calc = calculator.thisAWQ_Bef_Disc.hours_sold_calc
                Me.labor_sold_calc = calculator.thisAWQ_Bef_Disc.labor_sold_calc
                Me.mat_sold_calc = calculator.thisAWQ_Bef_Disc.mat_sold_calc
                Me.freight_sold_calc = calculator.thisAWQ_Bef_Disc.freight_sold_calc
                Me.rep_sold_calc = calculator.thisAWQ_Bef_Disc.rep_sold_calc
                Me.serv_sold_calc = calculator.thisAWQ_Bef_Disc.serv_sold_calc
                Me.total_mat_sold_calc = Me.mat_sold_calc + Me.freight_sold_calc + Me.rep_sold_calc + Me.serv_sold_calc
                'approved discount
                Me.lab_sold_disc_calc = calculator.thisAWQ_Disc.labor_sold_calc
                Me.total_mat_sold_disc_calc = calculator.thisAWQ_Disc.mat_sold_calc + calculator.thisAWQ_Disc.freight_sold_calc + calculator.thisAWQ_Disc.rep_sold_calc + calculator.thisAWQ_Disc.serv_sold_calc

            End If
        Catch ex As Exception
            _calc_error_text_thrown = "Calculation Error:" & Chr(10) & ex.Message
        End Try
    End Sub

End Class
