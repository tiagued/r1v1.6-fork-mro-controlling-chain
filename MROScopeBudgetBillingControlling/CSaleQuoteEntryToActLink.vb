﻿
Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CSaleQuoteEntryToActLink
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    'used to created new ids
    Public Shared currentMaxID As Integer

    Public Sub New()
        Dim i = 1
    End Sub

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(False, "")
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
    End Sub

    Public Sub New(act_arg As CScopeActivity, arg_qEntry As CSalesQuoteEntry)
        _act_obj = act_arg
        _act_id = act_arg.id
        _init_quote_entry_obj = arg_qEntry
        _init_quote_entry_id = arg_qEntry.id
        If Not IsNothing(act_arg) AndAlso Not act_arg.quote_entry_link_list.Contains(Me) Then
            act_arg.quote_entry_link_list.Add(Me)
        End If
        If Not IsNothing(arg_qEntry) AndAlso Not arg_qEntry.quote_entry_link_list.Contains(Me) Then
            arg_qEntry.quote_entry_link_list.Add(Me)
        End If
        Me.setID()
    End Sub

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        If Not IsNothing(MConstants.GLB_MSTR_CTRL.notifHelper) Then
            MConstants.GLB_MSTR_CTRL.notifHelper.OnPropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
        End If
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_QUOTE_ENTRIES_TO_ACT_LINK_DB_READ
    End Function

    Public Sub setID()
        MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return _id > 0
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub


    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem

        Dim res = False
        Dim mess = "Quote Entry - Activity Link Deleted"

        Try
            MConstants.GLB_MSTR_CTRL.ini_scope_controller.deleteQuoteEntryToActLink(Me)
            res = True
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.tostring & Chr(10) & ex.Message & Chr(10) & ex.StackTrace
        End Try

        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_AWQ_LOADED Then
                    CSaleQuoteEntryToActLink.currentMaxID = Math.Max(CSaleQuoteEntryToActLink.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _act_id As Long
    Public Property act_id() As Long
        Get
            Return _act_id
        End Get
        Set(ByVal value As Long)
            If Not _act_id = value Then
                _act_id = value
                If GLOB_SALEENTRY_TO_ACT_LK_LOADED Then
                    'retrieve only for none empty
                    If _act_id > 0 Then
                        _act_obj = MConstants.GLB_MSTR_CTRL.add_scope_controller.getActivityObject(_act_id)
                    Else
                        _act_obj = Nothing
                    End If
                End If
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _act_obj As CScopeActivity
    Public Property act_obj() As CScopeActivity
        Get
            Return _act_obj
        End Get
        Set(ByVal value As CScopeActivity)
            If Not Object.Equals(value, _act_obj) Then
                _act_obj = value
                'set ID
                If IsNothing(_act_obj) Then
                    _act_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _act_id = _act_obj.id
                End If
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _init_quote_entry_id As Long
    Public Property init_quote_entry_id() As Long
        Get
            Return _init_quote_entry_id
        End Get
        Set(ByVal value As Long)
            If Not _init_quote_entry_id = value Then
                _init_quote_entry_id = value
                If GLOB_SALEENTRY_TO_ACT_LK_LOADED Then
                    'retrieve only for none empty
                    If _init_quote_entry_id > 0 Then
                        _init_quote_entry_obj = MConstants.GLB_MSTR_CTRL.ini_scope_controller.getQuoteEntryObject(_init_quote_entry_id)
                    Else
                        _init_quote_entry_obj = Nothing
                    End If
                End If
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _init_quote_entry_obj As CSalesQuoteEntry
    Public Property init_quote_entry_obj() As CSalesQuoteEntry
        Get
            Return _init_quote_entry_obj
        End Get
        Set(ByVal value As CSalesQuoteEntry)
            If Not Object.Equals(value, _init_quote_entry_obj) Then
                _init_quote_entry_obj = value
                'set ID
                If IsNothing(_init_quote_entry_obj) Then
                    _init_quote_entry_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _init_quote_entry_id = _init_quote_entry_obj.id
                End If
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    'to string
    Public Overrides Function tostring() As String
        Dim res As String = "Sales Quote Entry : "
        If Not IsNothing(_init_quote_entry_obj) Then
            res = res & _init_quote_entry_obj.tostring
        Else
            res = res & "no quote entry linked"
        End If

        res = res & "Activity : "
        If Not IsNothing(_act_obj) Then
            res = res & _act_obj.tostring
        Else
            res = res & "no actitity linked"
        End If
        Return res
    End Function
End Class
