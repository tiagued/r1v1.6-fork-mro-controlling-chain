﻿Public Class CViewModelMapList

    Private _func As String
    Public Property func() As String
        Get
            Return _func
        End Get
        Set(ByVal value As String)
            _func = value
        End Set
    End Property

    Private _list_key As String
    Public Property list_key() As String
        Get
            Return _list_key
        End Get
        Set(ByVal value As String)
            _list_key = value
        End Set
    End Property

    Private _list As Dictionary(Of String, CViewModelMap)
    Public ReadOnly Property list() As Dictionary(Of String, CViewModelMap)
        Get
            Return _list
        End Get

    End Property

    Private _table_name As String
    Public Property table_name() As String
        Get
            Return _table_name
        End Get
        Set(ByVal value As String)
            _table_name = value
        End Set
    End Property

    Private _class_name As String
    Public Property class_name() As String
        Get
            Return _class_name
        End Get
        Set(ByVal value As String)
            _class_name = value
            _class = Type.GetType(System.Reflection.Assembly.GetExecutingAssembly.GetName.Name & "." & _class_name)
        End Set
    End Property

    Private _class_field As String
    Public Property class_field() As String
        Get
            Return _class_field
        End Get
        Set(ByVal value As String)
            _class_field = value
        End Set
    End Property

    Private _class As Type
    Public ReadOnly Property object_class() As Type
        Get
            Return _class
        End Get
    End Property

    Public Sub New(function_name As String, key As String, table As String, className As String)
        _func = function_name
        _list_key = key
        _table_name = table
        Me.class_name = className
        _list = New Dictionary(Of String, CViewModelMap)
        If IsNothing(_class) Then
            Throw New Exception("An Error occured while loading the model view mapping configuration. Please check the config. Cannot find the type " & Me.class_name)
        End If
    End Sub

    Public Function getDefaultSelect() As String
        Return "SELECT * FROM [" & _table_name & "] ORDER BY ID ASC"
    End Function

    'for imports
    Public Function getExcelDefaultSelect() As String
        Return "SELECT * FROM [" & _table_name & "$]"
    End Function

    Private updateQuery As String
    Public Function getDefaultUpdate() As String

        If Not IsNothing(updateQuery) Then
            Return updateQuery
        End If

        Dim query As String
        Dim where As String
        Dim first As Boolean
        'update on table_q which is the query obj => work around from microsoft 
        'https://support.office.com/en-us/article/access-error-query-is-corrupt-fad205a5-9fd4-49f1-be83-f21636caedec
        query = "UPDATE [" & table_name & "_q] SET"
        where = " WHERE [" & table_name & "_q].[id] = @id;"

        first = True
        For Each map As CViewModelMap In list.Values
            If map.db_col = "id" Then
                Continue For
            Else
                If first Then
                    query = query & " [" & map.db_col & "] = @" & map.db_col
                    first = False
                Else
                    query = query & ", [" & map.db_col & "] = @" & map.db_col
                End If
            End If
        Next
        query = query & where
        Return query
    End Function

    Public Function getDefaulInsert() As String
        Dim query As String
        Dim values As String
        Dim first As Boolean
        query = "INSERT INTO [" & table_name & "] ("
        values = " VALUES ("
        first = True

        For Each map As CViewModelMap In list.Values
            If map.db_col = "id" Then
                Continue For
            End If

            If first Then
                query = query & "[" & map.db_col & "]"
                values = values & "@" & map.db_col
                first = False
            Else
                query = query & ", [" & map.db_col & "]"
                values = values & ", @" & map.db_col
            End If
        Next
        query = query & ")"
        values = values & ")"
        query = query & values
        Return query
    End Function

    'get the list of mapping attributes with classField as the key in the dictionnary
    Public Function getListClassFieldAsKey() As Dictionary(Of String, CViewModelMap)
        Dim res As New Dictionary(Of String, CViewModelMap)

        For Each map As CViewModelMap In _list.Values
            If Not res.ContainsKey(map.class_field) Then
                res.Add(map.class_field, map)
            End If
        Next
        Return res
    End Function
End Class
