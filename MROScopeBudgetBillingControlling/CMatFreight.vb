﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CMatFreight
    Implements IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewListable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = True
        Dim mess As String = ""
        If Not _is_editable Then
            res = False
            mess = "Cannot be modified"
        End If
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res = False
        Dim mess = "It is not allowed to delete Material Freight objects"
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function isEmptyItem() As Boolean Implements IGenericInterfaces.IDataGridViewListable.isEmptyItem
        Return _id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_FREIGHT_DB_READ
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
            End If
        End Set
    End Property

    Private _id_str As String
    Public Property id_str() As String
        Get
            Return _id_str
        End Get
        Set(ByVal value As String)
            If Not _id_str = value Then
                _id_str = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return True
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_is_in_error_state = value Then
                _calc_is_in_error_state = value
            End If
        End Set
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not value = _calc_error_text Then
                _calc_error_text = value
            End If
        End Set
    End Property

    Private _label As String
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            If Not _label = value Then
                _label = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _freight_type As String
    Public Property freight_type() As String
        Get
            'return the user interface value
            Return _freight_type
        End Get
        Set(ByVal value As String)
            If Not _freight_type = value Then
                _freight_type = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _curr As String
    Public Property curr() As String
        Get
            Return _curr
        End Get
        Set(ByVal value As String)
            If Not _curr = value Then
                _curr = value
                If MConstants.GLOB_MAT_FREIGHT_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _price As Double
    Public Property price() As Double
        Get
            Return _price
        End Get
        Set(ByVal value As Double)
            If Not _price = value Then
                _price = value
                If MConstants.GLOB_MAT_FREIGHT_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public ReadOnly Property freight_to_string As String
        Get
            Return _price & " " & MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(_curr).value
        End Get
    End Property

    Private _is_editable As Boolean
    Public Property is_editable() As Boolean
        Get
            Return _is_editable
        End Get
        Set(ByVal value As Boolean)
            _is_editable = value
            'add item to be updated
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
        End Set
    End Property

    'to string
    Public Overrides Function tostring() As String
        Return "label: " & _label & "freight type: " & _freight_type & ", price: " & _price & " currency " & _curr
    End Function

End Class
