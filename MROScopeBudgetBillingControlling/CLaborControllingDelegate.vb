﻿Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CLaborControllingDelegate

    Private _actualHoursList As List(Of CActualHours)
    Private _laborTransferList As List(Of CHoursTransfer)
    Private _ccTransferDict As Dictionary(Of String, CCostCenter)
    Private _ccTransferList As List(Of CCCTransfer)
    Public _ccTransferListView As BindingListView(Of CCCTransfer)
    Private _deviationJustList As List(Of CDevJust)

    Private _laborControlViewDict As Dictionary(Of String, CLaborControlMainViewObj)
    Private _laborControlList As List(Of CLaborControlMainViewObj)
    Public _laborControlCostNodeList As BindingListView(Of CLaborControlMainViewObj)
    Private _cc_not_in_tool As Dictionary(Of String, CCostCenter)

    Public latest_actual_booking As Date

    Public Sub New()
        latest_actual_booking = MConstants.APP_NULL_DATE
        'initialize the lists only once, to keep the references to the dridview data source
        _actualHoursList = New List(Of CActualHours)
        _laborTransferList = New List(Of CHoursTransfer)
        _ccTransferDict = New Dictionary(Of String, CCostCenter)
        _ccTransferList = New List(Of CCCTransfer)
        _ccTransferListView = New BindingListView(Of CCCTransfer)(_ccTransferList)
        _deviationJustList = New List(Of CDevJust)

        _laborControlViewDict = New Dictionary(Of String, CLaborControlMainViewObj)
        _laborControlList = New List(Of CLaborControlMainViewObj)
        _laborControlCostNodeList = New BindingListView(Of CLaborControlMainViewObj)(_laborControlList)

        _cc_not_in_tool = New Dictionary(Of String, CCostCenter)
    End Sub
    'view to be loaded ?
    Public LABOR_CONTROL_VIEW_LOADABLE As Boolean

    'get actial list
    Public ReadOnly Property actualHoursList() As List(Of CActualHours)
        Get
            Return _actualHoursList
        End Get
    End Property
    'get hours transfer list
    Public ReadOnly Property laborTransferList() As List(Of CHoursTransfer)
        Get
            Return _laborTransferList
        End Get
    End Property
    'get cc transfer list
    Public ReadOnly Property ccTransferList() As Dictionary(Of String, CCostCenter)
        Get
            Return _ccTransferDict
        End Get
    End Property

    'get whole list
    Public ReadOnly Property laborControlCostNodeList() As BindingListView(Of CLaborControlMainViewObj)
        Get
            Return _laborControlCostNodeList
        End Get
    End Property

    'get dev just list
    Public ReadOnly Property deviationJustList() As List(Of CDevJust)
        Get
            Return _deviationJustList
        End Get
    End Property
    'read data from activity and hours DB
    Public Sub model_read_labor_control_DB()
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "perform queries on control labor tables..."
        Try
            'use GLOB_APP_LOADED to block DB update while reading database info. add_item_to_be_updated ignore if false
            Dim app_load_state = MConstants.GLOB_APP_LOADED

            MConstants.GLOB_APP_LOADED = False
            _laborControlViewDict.Clear()
            _laborControlList.Clear()

            model_read_actual_hours()
            model_read_labor_transfer()
            model_read_dev_just()
            MConstants.GLOB_APP_LOADED = app_load_state
        Catch ex As Exception
            Throw New Exception("Error occured when reading labor control data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End perform queries on control labor tables"
    End Sub

    'onload all views for memory
    Public Sub unload_labor_controlling()
        'save
        MConstants.GLB_MSTR_CTRL.save_controller.startAsync(CSaveWorkerArgs.getAutomaticUpdateDeleteAll)

        LABOR_CONTROL_VIEW_LOADABLE = False
        'clear lists
        _laborControlList.Clear()
        _laborControlViewDict.Clear()
        actualHoursList.Clear()
        laborTransferList.Clear()
        deviationJustList.Clear()
        _cc_not_in_tool.Clear()
        MConstants.GLOB_ACTUAL_HRS_LOADED = False
        MConstants.GLOB_TRANSFER_HRS_LOADED = False
        MConstants.GLOB_DEV_JUST_LOADED = False

        MConstants.GLB_MSTR_CTRL.labor_controlling_controller.laborControlCostNodeList.Refresh()
    End Sub

    'load controlling module
    Public Sub loadControllingModule()
        Try
            model_read_labor_control_DB()
            'clear lists
            _laborControlList.Clear()
            _laborControlViewDict.Clear()
            latest_actual_booking = MConstants.APP_NULL_DATE

            'link labor controlling
            'load first set of the model
            For Each act As CScopeActivity In MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource
                MConstants.GLB_MSTR_CTRL.labor_controlling_controller.loadActCtrl(act)
            Next

            For Each obj As CActualHours In MConstants.GLB_MSTR_CTRL.labor_controlling_controller.actualHoursList
                MConstants.GLB_MSTR_CTRL.labor_controlling_controller.dispatch_actual_hours(obj)
            Next
            MConstants.GLOB_ACTUAL_HRS_LOADED = True

            For Each obj As CHoursTransfer In MConstants.GLB_MSTR_CTRL.labor_controlling_controller.laborTransferList
                MConstants.GLB_MSTR_CTRL.labor_controlling_controller.dispatch_labor_hours_transfers(obj)
            Next
            MConstants.GLOB_TRANSFER_HRS_LOADED = True

            For Each obj As CDevJust In MConstants.GLB_MSTR_CTRL.labor_controlling_controller.deviationJustList
                MConstants.GLB_MSTR_CTRL.labor_controlling_controller.dispatch_deviation_just(obj)
            Next
            MConstants.GLOB_DEV_JUST_LOADED = True

            For Each ctrlO As CLaborControlMainViewObj In MConstants.GLB_MSTR_CTRL.labor_controlling_controller.laborControlCostNodeList.DataSource
                For Each child As CLaborControlMainViewObj In ctrlO.get_structure_children
                    child.update()
                Next
            Next
            MConstants.GLB_MSTR_CTRL.labor_controlling_controller.laborControlCostNodeList.Refresh()
        Catch ex As Exception
            Throw New Exception("Error occured while loading the control module" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Sub loadActCtrl(act As CScopeActivity)
        Try
            For Each soldH As CSoldHours In act.sold_hour_list
                If soldH.sold_hrs > 0 Then
                    loadActCCCtrl(act.id, act, soldH.cc_obj)
                ElseIf soldH.plan_hrs > 0 Then
                    'load plan hours of dependant activity, on the main act=> must be done on the activity itself
                    Dim initScopeMain As CScopeActivity = getInitCostNode(act)
                    If Not IsNothing(initScopeMain) Then
                        loadActCCCtrl(act.id, act, soldH.cc_obj)
                    ElseIf Not IsNothing(act.awq_master_obj) AndAlso Not IsNothing(act.awq_master_obj.main_activity_obj) Then
                        loadActCCCtrl(act.id, act, soldH.cc_obj)
                    End If
                End If
            Next
        Catch ex As Exception
            Throw New Exception("Adding Activity Item to controlling structure " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub
    'remove empty cost cntrol element
    Public Sub unLoadActCCCtrl(ctrlObj As CLaborControlMainViewObj)

        Try
            If IsNothing(ctrlObj) OrElse IsNothing(ctrlObj.act_obj) OrElse IsNothing(ctrlObj.cc_object) Then
                Exit Sub
            End If

            'ignore empty
            If Object.Equals(ctrlObj.act_obj, MConstants.EMPTY_ACT) OrElse ctrlObj.cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
                Exit Sub
            End If

            Dim costNode As CLaborControlMainViewObj = getActCostNode(ctrlObj.act_obj, MConstants.LABOR_CTRL_ALL_CC, Nothing)
            Dim costNodeKey As String = getControllingObjKey(costNode.act_obj.id, costNode.cc_object, ctrlObj.is_virtual_cost_node)
            Dim key_str As String = getControllingObjKey(ctrlObj.act_obj.id, ctrlObj.cc_object, ctrlObj.is_virtual_cost_node)

            'remove from structure children
            If costNode.structure_children_exists(key_str) Then
                costNode.remove_structure_children(key_str, ctrlObj)
            End If
            'delete cost node from parents of item to be deleted
            If ctrlObj.calculation_parents.Contains(costNode) Then
                ctrlObj.calculation_parents.Remove(costNode)
            End If
            'other parents
            For Each parent As CLaborControlMainViewObj In ctrlObj.calculation_parents
                If parent.calculation_children.Contains(ctrlObj) Then
                    parent.calculation_children.Remove(ctrlObj)
                    'if no other items on parent, delete it
                    If parent.calculation_children.Count = 0 Then
                        Dim parent_key_str As String = getControllingObjKey(parent.act_obj.id, parent.cc_object, parent.is_virtual_cost_node)
                        If costNode.structure_children_exists(parent_key_str) Then
                            costNode.remove_structure_children(parent_key_str, parent)
                        End If
                    Else
                        parent.updateFromChildren()
                    End If
                End If
            Next

            'cost node to be removed ?
            If costNode.get_structure_children.Count = 0 AndAlso _laborControlViewDict.ContainsKey(costNodeKey) Then
                _laborControlViewDict.Remove(costNodeKey)
                If _laborControlList.Contains(costNode) Then
                    _laborControlList.Remove(costNode)
                End If
            Else
                costNode.updateFromChildren()
            End If

        Catch ex As Exception
            Throw New Exception("Error occured when unloading ctrl object loadActCCCtrl, act id " & If(Not IsNothing(ctrlObj), ctrlObj.tostring, "") & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Function loadActCCCtrl(act_id As Long, act As CScopeActivity, _cc As CCostCenter) As CLaborControlMainViewObj

        Try
            If IsNothing(act) Then
                act = MConstants.GLB_MSTR_CTRL.add_scope_controller.getActivityObject(act_id)
            End If

            'ignore empty
            If Object.Equals(act, MConstants.EMPTY_ACT) OrElse _cc.cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
                Return Nothing
            End If

            Dim costNode As CLaborControlMainViewObj = getActCostNode(act, MConstants.LABOR_CTRL_ALL_CC, Nothing)
            'add all cc for this act. for below threshold is the same as the cost nodes
            Dim actAllCCObj As CLaborControlMainViewObj = getOrAddControlEntry(act.id, MConstants.LABOR_CTRL_ALL_CC, act, costNode, False, False)

            Dim cc As CCostCenter
            If _ccTransferDict.ContainsKey(_cc.cc) Then
                cc = _ccTransferDict(_cc.cc)
            Else
                cc = _cc
            End If

            'create or get this cc at cost node leveln (cost node level is the activity it self for below thr)
            Dim costNodeCC As CLaborControlMainViewObj = getActCostNode(act, cc, costNode)

            'then add the current cc for this activity if not exists. for blow threshold already exists
            Dim actCCObj As CLaborControlMainViewObj = getOrAddControlEntry(act.id, cc, act, costNode, False, False)
            buildCalcLinks(costNode, actCCObj)
            buildCalcLinks(actAllCCObj, actCCObj)
            buildCalcLinks(costNodeCC, actCCObj)

            Return actCCObj
        Catch ex As Exception
            Throw New Exception("Error occured when loading ctrl object loadActCCCtrl, act id " & act_id & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Function
    'read data from actual hours DB
    Public Sub model_read_actual_hours()
        Dim listO As List(Of Object)
        Try
            _actualHoursList.Clear()
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "perform query to actual hours table..."
            listO = MConstants.GLB_MSTR_CTRL.csDB.performSelectObject(MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ACTUAL_LABOR_DB_READ).getDefaultSelect, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ACTUAL_LABOR_DB_READ), True)
            For Each obj As CActualHours In listO
                _actualHoursList.Add(obj)
            Next
        Catch ex As Exception
            Throw New Exception("Error occured when reading actual hours data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End perform query to actual hours table"
    End Sub

    'dispatch actual hour object
    Public Function dispatch_actual_hours(actHr As CActualHours) As CLaborControlMainViewObj
        Try
            Dim cc As CCostCenter = getTransferedCC(actHr.cc)
            'get act cc entry or create it if not exist
            Dim ctrlOView As CLaborControlMainViewObj = loadActCCCtrl(actHr.act_id, actHr.act_obj, cc)
            'add actual hour to list
            ctrlOView.actualHours.Add(actHr)
            Return ctrlOView
        Catch ex As Exception
            Throw New Exception("Adding Actual Item to controlling structure " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Function

    'get transferred CC
    Public Function getTransferedCC(cc_str As String) As CCostCenter
        Dim cc As CCostCenter
        If Not String.IsNullOrWhiteSpace(cc_str) Then
            If _ccTransferDict.ContainsKey(cc_str) Then
                cc = _ccTransferDict(cc_str)
            Else
                If MConstants.listOfCC.ContainsKey(cc_str) Then
                    cc = MConstants.listOfCC(cc_str)
                Else
                    If Not _cc_not_in_tool.ContainsKey(cc_str) Then
                        Dim cc_obj = New CCostCenter
                        cc_obj.cc = cc_str
                        cc_obj.description = "Cost Center Not Handle By the Tool"
                        cc_obj.friendly_description = "Cost Center Not Handle By the Tool"
                        _cc_not_in_tool.Add(cc_str, cc_obj)
                    End If
                    cc = _cc_not_in_tool(cc_str)
                End If
            End If
        Else
            'no cc
            If Not _cc_not_in_tool.ContainsKey("cc_empty") Then
                Dim cc_obj = New CCostCenter
                cc_obj.cc = "no cc"
                cc_obj.description = "Cost Center field is empty"
                cc_obj.friendly_description = "Cost Center field is empty"
                _cc_not_in_tool.Add("cc_empty", cc_obj)
            End If
            cc = _cc_not_in_tool("cc_empty")
        End If
        Return cc
    End Function

    'create fake id for cost nodes as they are virtuals
    Public Function getControllingObjKey(act_id As Long, cc As CCostCenter, isCostNode As Boolean) As String
        If Not isCostNode Then
            Return act_id & MConstants.SEP_DEFAULT & cc.cc
        Else
            Return act_id & "_cost_node" & MConstants.SEP_DEFAULT & cc.cc
        End If
    End Function

    'build calculation links
    Public Sub buildCalcLinks(parent As CLaborControlMainViewObj, child As CLaborControlMainViewObj)
        'add this to all cc children
        If Not parent.calculation_children.Contains(child) Then
            parent.calculation_children.Add(child)
        End If
        'reverse the link
        If Not child.calculation_parents.Contains(parent) Then
            child.calculation_parents.Add(parent)
        End If
    End Sub
    'get init cost node
    Public Function getInitCostNode(act As CScopeActivity) As CScopeActivity
        Try
            Dim initMainAct As CScopeActivity = Nothing
            Dim isActMainInit As Boolean = False
            If act.quote_entry_link_list.Count > 0 Then
                'ensure that all cost centers of this activity will be linked to the same activity no matter the product summary, Take the first product sum main activity
                For Each lk As CSaleQuoteEntryToActLink In act.quote_entry_link_list
                    If Not IsNothing(lk.init_quote_entry_obj) AndAlso Not IsNothing(lk.init_quote_entry_obj.main_activity_obj) Then
                        If IsNothing(initMainAct) Then
                            initMainAct = lk.init_quote_entry_obj.main_activity_obj
                        End If
                        If Object.Equals(lk.init_quote_entry_obj.main_activity_obj, act) Then
                            isActMainInit = True
                        End If
                    End If
                Next
            End If
            If isActMainInit Then
                Return act
            Else
                Return initMainAct
            End If
        Catch ex As Exception
            Throw New Exception("Error when getting cost node for init scope" & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Function
    'get act cost node all cc
    Public Function getActCostNode(act As CScopeActivity, cc As CCostCenter, parent As CLaborControlMainViewObj) As CLaborControlMainViewObj
        Dim costNode As CLaborControlMainViewObj
        Dim isCostNode As Boolean = True
        'when cc is not all
        If Not IsNothing(parent) Then
            isCostNode = False
        End If

        Try
            Dim initMainAct As CScopeActivity = getInitCostNode(act)

            'get node
            If act.quote_entry_link_list.Count > 0 AndAlso Not IsNothing(initMainAct) Then
                'create or get cost node ALL cc the parent of all other ctrling objects
                costNode = getOrAddControlEntry(initMainAct.id, cc, initMainAct, parent, isCostNode, True)
            ElseIf Not IsNothing(act.awq_master_obj) AndAlso Not IsNothing(act.awq_master_obj.main_activity_obj) Then
                'create or get cost node ALL cc the parent of all other ctrling objects
                costNode = getOrAddControlEntry(act.awq_master_obj.main_activity_obj.id, cc, act.awq_master_obj.main_activity_obj, parent, isCostNode, True)
            Else
                'create or get cost node ALL cc the parent of all other ctrling objects
                costNode = getOrAddControlEntry(act.id, cc, act, parent, isCostNode, False)
            End If
            Return costNode
        Catch ex As Exception
            Throw New Exception("Error when getting cost node " & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Function

    'add Act-CC to list
    'isVirtualCostNode for init scope or awq scopes
    Public Function getOrAddControlEntry(act_id As Long, cc As CCostCenter, act_obj As CScopeActivity, topNode As CLaborControlMainViewObj, isCostNode As Boolean, isVirtualCostNode As Boolean) As CLaborControlMainViewObj
        Try
            Dim ctrlOView As CLaborControlMainViewObj
            Dim key_str As String = getControllingObjKey(act_id, cc, isVirtualCostNode)

            'test if entry already exists
            If Not _laborControlViewDict.ContainsKey(key_str) Then

                If IsNothing(act_obj) Then
                    act_obj = MConstants.GLB_MSTR_CTRL.add_scope_controller.getActivityObject(act_id)
                End If

                'item to create
                ctrlOView = New CLaborControlMainViewObj
                ctrlOView.cc_object = cc
                ctrlOView.act_obj = act_obj

                If isVirtualCostNode Then
                    ctrlOView.is_virtual_cost_node = True
                End If

                _laborControlViewDict.Add(key_str, ctrlOView)

                If Not isCostNode Then
                    If Not topNode.structure_children_exists(key_str) Then
                        topNode.add_structure_children(key_str, ctrlOView)
                    End If
                Else
                    'main obj to be displayed in overview view
                    _laborControlList.Add(ctrlOView)
                End If
            Else
                'entry exist
                ctrlOView = _laborControlViewDict(key_str)
            End If
            Return ctrlOView
        Catch ex As Exception
            Throw New Exception("An error occured while creating a new entry in the labor control view act id " & act_id & " cc " & cc.cc & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Function

    'read data from act DB
    Private Sub model_read_labor_transfer()
        Dim listO As List(Of Object)
        Try
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "perform query to hours transfer table..."
            listO = MConstants.GLB_MSTR_CTRL.csDB.performSelectObject(MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_HOURS_TRANSFER_DB_READ).getDefaultSelect, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_HOURS_TRANSFER_DB_READ), True)

            _laborTransferList.Clear()

            For Each obj As CHoursTransfer In listO
                _laborTransferList.Add(obj)
            Next

        Catch ex As Exception
            Throw New Exception("Error occured when reading hours transfer data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End perform query to hours transfer table"
    End Sub

    'dispatch labor transferts object
    Public Function dispatch_labor_hours_transfers(obj As CHoursTransfer) As List(Of CLaborControlMainViewObj)
        Try
            Dim res As New List(Of CLaborControlMainViewObj)
            'add it on from object
            Dim cc_from As CCostCenter = getTransferedCC(obj.from_cc)
            Dim ctrlOView_from As CLaborControlMainViewObj = loadActCCCtrl(obj.from_act_id, Nothing, cc_from)

            'add it on to object
            Dim cc_to As CCostCenter = getTransferedCC(obj.to_cc)
            Dim ctrlOView_to As CLaborControlMainViewObj = loadActCCCtrl(obj.to_act_id, Nothing, cc_to)

            ctrlOView_from.hoursTransfert.Add(obj)
            ctrlOView_to.hoursTransfert.Add(obj)

            'set link to controlling object for update reasons
            obj.from_ctrl_obj = ctrlOView_from
            obj.to_ctrl_obj = ctrlOView_to

            res.Add(ctrlOView_from)
            res.Add(ctrlOView_to)
            Return res
        Catch ex As Exception
            Throw New Exception("Adding Hours Transfers Item to controlling structure " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Function

    'read data from cc transfer DB
    Public Sub model_read_cc_transfer()
        Dim listO As List(Of Object)
        Try
            _ccTransferList.Clear()
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "perform query to cc transfer table..."
            listO = MConstants.GLB_MSTR_CTRL.csDB.performSelectObject(MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_CC_TRANSFER_DB_READ).getDefaultSelect, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_CC_TRANSFER_DB_READ), True)
            For Each obj As CCCTransfer In listO
                _ccTransferList.Add(obj)
            Next
            If Not IsNothing(_ccTransferListView) Then
                _ccTransferListView.Refresh()
            End If
            _ccTransferDict.Clear()
            'build CC transfer dictionnary
            refreshAfterCCTransferUpdate()
            MConstants.GLOB_TRANSFER_CC_LOADED = True
        Catch ex As Exception
            Throw New Exception("Error occured when reading cc transfer data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End perform query to cc transfer table"
    End Sub

    Public Sub refreshAfterCCTransferUpdate()

        _ccTransferDict.Clear()
        _ccTransferListView.Refresh()

        For Each obj As CCCTransfer In _ccTransferList
            If obj.calc_is_in_error_state Then
                Continue For
            End If
            If IsNothing(obj.to_cc) OrElse IsNothing(obj.from_cc) Then
                Continue For
            End If

            If Not _ccTransferDict.ContainsKey(obj.from_cc) Then
                Dim cco As CCostCenter
                If MConstants.listOfCC.ContainsKey(obj.to_cc) Then
                    cco = MConstants.listOfCC(obj.to_cc)
                Else
                    cco = New CCostCenter
                    cco.cc = obj.to_cc
                    cco.description = "Cost Center Not Handle By the Tool"
                    cco.friendly_description = "Cost Center Not Handle By the Tool"
                End If
                _ccTransferDict.Add(obj.from_cc, cco)
            End If
            'reload controlling view
            If MConstants.GLOB_APP_LOADED Then
                loadControllingModule()
            End If
        Next
    End Sub
    'read data from deviation justification DB
    Private Sub model_read_dev_just()
        Dim listO As List(Of Object)
        Try
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "perform query to deviation justification table..."
            listO = MConstants.GLB_MSTR_CTRL.csDB.performSelectObject(MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_DEV_JUST_DB_READ).getDefaultSelect, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_DEV_JUST_DB_READ), True)
            _deviationJustList.Clear()

            For Each obj As CDevJust In listO
                _deviationJustList.Add(obj)
            Next
        Catch ex As Exception
            Throw New Exception("Error occured when reading deviation justification data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End perform query to deviation justification table"
    End Sub

    'dispatch deviation justifications object
    Public Function dispatch_deviation_just(actHr As CDevJust) As CLaborControlMainViewObj
        Try
            Dim cc As CCostCenter = getTransferedCC(actHr.cc)
            'get act cc entry or create it if not exist
            Dim ctrlOView As CLaborControlMainViewObj = loadActCCCtrl(actHr.act_id, actHr.act_object, cc)
            'add actual hour to list
            ctrlOView.deviationJustification.Add(actHr)
            actHr.ctrl_obj = ctrlOView
            Return ctrlOView
        Catch ex As Exception
            Throw New Exception("Adding Deviation Justification Item to controlling structure " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Function

    'load controlling data model
    Public Sub loadLaborControllingModelData()
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "build Labor controlling relationships..."
        Try

        Catch ex As Exception
            Throw New Exception("Error occured when building Labor controlling relationships" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End build Labor controlling relationships"
    End Sub

    Public Sub bind_controlling_view()
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "bind controlling main views..."
        Try

            bind_ctrl_conf_transfer_cc_view()
            bind_controlling_main_view(MConstants.GLB_MSTR_CTRL.mainForm.labor_ctrl_main_dgrid)
            'to load labor controlling when dgrid is shown
            MViewEventHandler.laborControlLoadHandlerWhenVisible(MConstants.GLB_MSTR_CTRL.mainForm.labor_ctrl_main_dgrid)

            'bind datasource
            _laborControlCostNodeList.ApplySort("act_network, act_number, act_jc, cc")
            MConstants.GLB_MSTR_CTRL.mainForm.labor_ctrl_main_dgrid.DataSource = _laborControlCostNodeList

        Catch ex As Exception
            Throw New Exception("Error occured when binding controlling main views" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End bind controlling main views"
    End Sub


    'bind the controls items in the initial scope view view with the relevant data object
    Private Sub bind_ctrl_conf_transfer_cc_view()
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Bind labor controling conf CC transfer view..."
        Try
            Dim mapTrCC As CViewModelMapList

            'get activity map view
            mapTrCC = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_CTRL_CONF_TRANSF_CC_VIEW_DISPLAY)

            MViewEventHandler.addDefaultDatagridEventHandler(MConstants.GLB_MSTR_CTRL.mainForm.proj_ctrl_transfer_cc_dgrid)
            MConstants.GLB_MSTR_CTRL.mainForm.proj_ctrl_transfer_cc_dgrid.AllowUserToAddRows = True
            MConstants.GLB_MSTR_CTRL.mainForm.proj_ctrl_transfer_cc_dgrid.ReadOnly = False
            For Each map As CViewModelMap In mapTrCC.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapTrCC.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "Double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    MConstants.GLB_MSTR_CTRL.mainForm.proj_ctrl_transfer_cc_dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn

                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapTrCC.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    Else
                        bds.DataSource = MConstants.listOfCC.Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "cc_desc_view"
                        dgcol.ValueMember = "cc"
                    End If
                    dgcol.ReadOnly = Not map.col_editable
                    MConstants.GLB_MSTR_CTRL.mainForm.proj_ctrl_transfer_cc_dgrid.Columns.Add(dgcol)
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    MConstants.GLB_MSTR_CTRL.mainForm.proj_ctrl_transfer_cc_dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_BUTTON).value Then
                    Dim dgcol As New DataGridViewButtonColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Text = map.view_col
                    dgcol.UseColumnTextForButtonValue = True
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    MConstants.GLB_MSTR_CTRL.mainForm.proj_ctrl_transfer_cc_dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_IMAGE).value Then
                    Dim dgcol As New DataGridViewImageColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Image = My.Resources.ResourceManager.GetObject(map.view_col)
                    MConstants.GLB_MSTR_CTRL.mainForm.proj_ctrl_transfer_cc_dgrid.Columns.Add(dgcol)
                End If
            Next

            'bind datasource
            MConstants.GLB_MSTR_CTRL.mainForm.proj_ctrl_transfer_cc_dgrid.DataSource = _ccTransferListView
            MGeneralFuntionsViewControl.resizeDataGridWidth(MConstants.GLB_MSTR_CTRL.mainForm.proj_ctrl_transfer_cc_dgrid)
            'validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(MConstants.GLB_MSTR_CTRL.mainForm.proj_ctrl_transfer_cc_dgrid, CCCTransferRowValidator.getInstance, mapTrCC)
            dhler.validateAddDelegate = AddressOf cc_transfer_ValidateAdd

        Catch ex As Exception
            Throw New Exception("An error occured while loading labor controling conf CC transfer view" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End Bind labor controling conf CC transfer view"
    End Sub

    'validate new cc transfer item
    Public Sub cc_transfer_ValidateAdd(ccT As ObjectView(Of CCCTransfer), rowIndex As Integer)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(ccT.Object) Then
                'if new item
                ccT.Object.setID()
                If Not _ccTransferListView.DataSource.Contains(ccT.Object) Then
                    _ccTransferListView.EndNew(rowIndex)
                End If
                refreshAfterCCTransferUpdate()
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new CC Transfer" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub



    'bind the controls items in the initial scope view view with the relevant data object
    Public Sub bind_controlling_main_view(dgrid As DataGridView)
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Bind main controlling view..."
        Try
            Dim mapCtrl As CViewModelMapList

            'get  map view
            mapCtrl = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_CTRL_MAIN_VIEW_DISPLAY)

            'labor controlling datagridview
            dgrid.AllowUserToAddRows = False

            MViewEventHandler.addDefaultDatagridEventHandler(dgrid)

            For Each map As CViewModelMap In mapCtrl.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapCtrl.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "Double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn

                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapCtrl.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    Else
                        Throw New Exception("Error occured when binding activity view in additional scope tab. List " & map.view_list_name & " does not exist in LOV")
                    End If
                    dgcol.ReadOnly = Not map.col_editable
                    dgrid.Columns.Add(dgcol)
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_BUTTON).value Then
                    Dim dgcol As New DataGridViewButtonColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Text = map.view_col
                    dgcol.UseColumnTextForButtonValue = True
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_IMAGE).value Then
                    Dim dgcol As New DataGridViewImageColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Image = My.Resources.ResourceManager.GetObject(map.view_col)
                    dgrid.Columns.Add(dgcol)
                End If
            Next

            'handle frozen col
            Dim i As Integer = 1
            While i <= CInt(MConstants.constantConf(MConstants.CONST_CONF_ACT_VIEW_FREEZE_FIRST_COL_CNT).value)
                dgrid.Columns(i - 1).Frozen = True
                i = i + 1
            End While

        Catch ex As Exception
            Throw New Exception("An error occured while loading activities view" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End Bind main controlling view"
    End Sub

    'update controlling object view
    Public Sub updateCtrlObjView(ctrl_obj As CLaborControlMainViewObj, form As Form)
        Try
            Dim objMap As CViewModelMapList

            'get view model
            objMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_CTRL_OBJ_VIEW)

            'Bind current lab controlling object to the view
            Dim tbbd As Binding
            For Each map As CViewModelMap In objMap.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim textBox As TextBox
                    textBox = CType(form.Controls.Find(map.view_control, True).FirstOrDefault, TextBox)
                    textBox.DataBindings.Clear()
                    tbbd = textBox.DataBindings.Add("Text", ctrl_obj, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    'format double
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        tbbd.FormatInfo = MConstants.CULT_INFO
                        tbbd.FormatString = map.col_format
                        tbbd.FormattingEnabled = True
                    End If
                    textBox.Enabled = False
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim chk As CheckBox
                    chk = CType(form.Controls.Find(map.view_control, True).FirstOrDefault, CheckBox)
                    chk.DataBindings.Clear()
                    chk.DataBindings.Add("Checked", ctrl_obj, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    chk.Enabled = False
                End If
            Next

        Catch ex As Exception
            Throw New Exception("An error occured while loading data of labor controlling object " & ctrl_obj.tostring & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    'perform delete
    Public Sub deleteObjectHoursTransfer(hrTransf As CHoursTransfer)
        Try
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(hrTransf)
        Catch ex As Exception
        End Try
    End Sub

    'perform delete
    Public Sub deleteObjectDevJust(devJust As CDevJust)
        Try
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(devJust)
        Catch ex As Exception
        End Try
    End Sub

    'perform delete
    Public Sub deleteObjectCCTransfer(ccTransf As CCCTransfer)
        Try
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(ccTransf)
            If _ccTransferListView.DataSource.Contains(ccTransf) Then
                _ccTransferListView.DataSource.Remove(ccTransf)
            End If
            refreshAfterCCTransferUpdate()
        Catch ex As Exception
        End Try
    End Sub

End Class



