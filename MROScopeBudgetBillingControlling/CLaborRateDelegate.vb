﻿Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CLaborRateDelegate
    Public mctrl As CMasterController
    Public _std_labor_rate_list As BindingListView(Of CLaborRate)
    Public _nte_labor_rate_list As BindingListView(Of CLaborRate)
    Public _oem_labor_rate_list As BindingListView(Of CLaborRate)
    Public _task_rate_list As BindingListView(Of CTaskRate)
    Public _task_rate_lov As BindingSource
    Public _labor_rate_dict As Dictionary(Of String, CLaborRate)

    Public Sub New(_ctrl As CMasterController)
        mctrl = _ctrl
    End Sub

    'read data from labor rate DBs
    Public Sub model_read_labor_ratingDB()
        mctrl.statusBar.status_strip_sub_label = "perform queries rating tables..."
        Try
            model_read_labor_rating_conf()
            model_read_task_rating_conf()
        Catch ex As Exception
            Throw New Exception("Error occured when reading rate conf data from the DB" & Chr(10) & ex.Message, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform queries on rating tables"
    End Sub

    'read data from labor rate DB
    Public Sub model_read_labor_rating_conf()
        Dim listO As List(Of Object)
        Dim listOstd As New List(Of CLaborRate)
        Dim listOnte As New List(Of CLaborRate)
        Dim prop As New CConfProperty

        Dim listOoem As New List(Of CLaborRate)
        _labor_rate_dict = New Dictionary(Of String, CLaborRate)
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to labor rate table..."
            'dynamic creation of std rate lov
            MConstants.listOfValueConf.Add(MConstants.CONST_CONF_LOV_LABRATE_STD, New Dictionary(Of String, CConfProperty))
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_RATE_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_RATE_DB_READ), True)
            For Each lr As CLaborRate In listO
                'put items in different lists to handle several views depending on rate type
                If lr.rate_type.ToLower = MConstants.constantConf(MConstants.CONST_CONF_LAB_RATE_TYPE_STD).value.ToLower Then
                    listOstd.Add(lr)
                    'add as lov
                    If Not MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_LABRATE_STD).ContainsKey(lr.labor_rate) Then
                        prop = New CConfProperty
                        prop.key = lr.labor_rate
                        prop.value = lr.label
                        prop.list = MConstants.CONST_CONF_LOV_LABRATE_STD
                        prop.lov_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_TYPE_LOV).value
                        MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_LABRATE_STD).Add(prop.key, prop)

                    End If
                ElseIf lr.rate_type.ToLower = MConstants.constantConf(MConstants.CONST_CONF_LAB_RATE_TYPE_NTE).value.ToLower Then
                    listOnte.Add(lr)
                ElseIf lr.rate_type.ToLower = MConstants.constantConf(MConstants.CONST_CONF_LAB_RATE_TYPE_OEM).value.ToLower Then
                    listOoem.Add(lr)
                End If
                'dictionnary to speed up access to labor rate conditions
                If _labor_rate_dict.ContainsKey(lr.getLabRateKey) Then
                    Throw New Exception("The following labor rate key is not unique, please review labor rate configuration " & lr.getLabRateKey)
                Else
                    _labor_rate_dict.Add(lr.getLabRateKey, lr)
                End If
            Next
            'add empty key
            prop = New CConfProperty
            prop.key = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            prop.value = " "
            prop.list = MConstants.CONST_CONF_LOV_LABRATE_STD
            prop.lov_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_TYPE_LOV).value
            MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_LABRATE_STD).Add(prop.key, prop)

            _std_labor_rate_list = New BindingListView(Of CLaborRate)(listOstd)
            _nte_labor_rate_list = New BindingListView(Of CLaborRate)(listOnte)
            _oem_labor_rate_list = New BindingListView(Of CLaborRate)(listOoem)

            GLOB_LABOR_RATE_LOADED = True
        Catch ex As Exception
            Throw New Exception("Error occured when reading labor rate data from the DB" & Chr(10) & ex.Message, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to labor rate table"
    End Sub

    'read data from labor rate DB
    Public Sub model_read_task_rating_conf()
        Dim listO As List(Of Object)

        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to task rate table..."
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_TASKRATE_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_TASKRATE_DB_READ), True)
            _task_rate_list = New BindingListView(Of CTaskRate)(listO)
            _task_rate_lov = New BindingSource
            GLOB_TASK_RATE_LOADED = True

        Catch ex As Exception
            Throw New Exception("Error occured when reading task rate data from the DB" & Chr(10) & ex.Message, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to task rate table"
    End Sub


    Public Sub bind_lab_rate_views()
        mctrl.statusBar.status_strip_sub_label = "bind labor rating views..."
        Try
            bind_labor_rate_std_view()
            bind_labor_rate_nte_view()
            bind_labor_rate_oem_view()
            bind_task_rate_view
        Catch ex As Exception
            Throw New Exception("Error occured when binding labor rate views" & Chr(10) & ex.Message, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End bind labor rating views"
    End Sub

    Private Sub bind_labor_rate_std_view()
        mctrl.statusBar.status_strip_sub_label = "Bind billing labor rate standard conf data to view controls..."
        Try
            'bind currencies selection
            Dim mapProj As CViewModelMapList
            Dim mapLabRateStd As CViewModelMapList
            Dim bds As BindingSource

            'map is the same as the project view
            mapProj = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_PROJ_VIEW_DISPLAY)
            mapLabRateStd = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_RATE_STD_VIEW_DISPLAY)

            'choice combo box
            Dim cb As ComboBox = mctrl.mainForm.lbl_conf_labrate_std_choice_val
            MGeneralFuntionsViewControl.setComboBoxValue(cb, MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_LABRATE_STD))
            'set selected items
            cb.DataBindings.Add("SelectedValue", mctrl.project_controller.project, mapProj.list(cb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cb.DropDownStyle = ComboBoxStyle.DropDownList
            MViewEventHandler.addDefaultComboBoxEventHandler(cb)

            'std labrate datagridview
            'do not autogenerate columns
            mctrl.mainForm.conf_labrate_labrate_std_grid.AllowUserToAddRows = False
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.conf_labrate_labrate_std_grid)

            For Each map As CViewModelMap In mapLabRateStd.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapLabRateStd.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_labrate_labrate_std_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapLabRateStd.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    bds = New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    Else
                        Throw New Exception("Error occured when binding task rate view. List " & map.view_list_name & " does not exist in LOV")
                    End If
                    dgcol.ReadOnly = Not map.col_editable
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    mctrl.mainForm.conf_labrate_labrate_std_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_labrate_labrate_std_grid.Columns.Add(dgcol)
                End If
            Next
            'bind datasource and apply filter
            mctrl.mainForm.conf_labrate_labrate_std_grid.DataSource = _std_labor_rate_list
            filterLaborRateStd()
            'set width
            MGeneralFuntionsViewControl.resizeDataGridWidth(mctrl.mainForm.conf_labrate_labrate_std_grid)
            'handle event
            Dim handler = New CViewRefreshHandler
            handler.datagridView = mctrl.mainForm.conf_labrate_labrate_std_grid
            handler.listView = _std_labor_rate_list
            handler.flexibleHeigtControl = mctrl.mainForm.conf_labrate_labrate_std_grid
            CViewRefreshHandler.addEntry(Of CLaborRate)(handler)
            handler.refreshHandlerViewsHeight()

            'row validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.conf_labrate_labrate_std_grid, CLaborRateRowValidator.getInstance, mapLabRateStd)
        Catch ex As Exception
            Throw New Exception("An error occured while loading labor rate view", ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing labor rate standard conf data to view controls"
    End Sub

    Public Sub filterLaborRateStd()
        _std_labor_rate_list.ApplyFilter(AddressOf filterSelectedLaborRate)
    End Sub

    Private Function filterSelectedLaborRate(lr As CLaborRate)
        If String.IsNullOrWhiteSpace(mctrl.project_controller.project.labor_rate_std) Or mctrl.project_controller.project.labor_rate_std = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            Return True
        Else
            Return lr.labor_rate = mctrl.project_controller.project.labor_rate_std
        End If
    End Function
    'bind oem view
    Private Sub bind_labor_rate_oem_view()
        mctrl.statusBar.status_strip_sub_label = "Bind billing labor rate oem conf data to view controls..."
        Try

            Dim mapLabRateOem As CViewModelMapList
            Dim bds As BindingSource

            'map is the same as the project view
            mapLabRateOem = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_RATE_OEM_VIEW_DISPLAY)

            mctrl.mainForm.conf_labrate_labrate_oem_grid.AllowUserToAddRows = False

            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.conf_labrate_labrate_oem_grid)

            For Each map As CViewModelMap In mapLabRateOem.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapLabRateOem.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_labrate_labrate_oem_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapLabRateOem.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    bds = New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    Else
                        Throw New Exception("Error occured when binding task rate view. List " & map.view_list_name & " does not exist in LOV")
                    End If
                    dgcol.ReadOnly = Not map.col_editable
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    mctrl.mainForm.conf_labrate_labrate_oem_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_labrate_labrate_oem_grid.Columns.Add(dgcol)
                End If
            Next
            'bind datasource and apply filter
            mctrl.mainForm.conf_labrate_labrate_oem_grid.DataSource = _oem_labor_rate_list
            'set width
            MGeneralFuntionsViewControl.resizeDataGridWidth(mctrl.mainForm.conf_labrate_labrate_oem_grid)
            'handle event
            Dim handler = New CViewRefreshHandler
            handler.datagridView = mctrl.mainForm.conf_labrate_labrate_oem_grid
            handler.listView = _oem_labor_rate_list
            handler.flexibleHeigtControl = mctrl.mainForm.conf_labrate_labrate_oem_grid
            CViewRefreshHandler.addEntry(Of CLaborRate)(handler)
            handler.refreshHandlerViewsHeight()

        Catch ex As Exception
            Throw New Exception("An error occured while loading oem labor rate view", ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing labor rate oem conf data to view controls"
    End Sub
    'load view nte
    Private Sub bind_labor_rate_nte_view()
        mctrl.statusBar.status_strip_sub_label = "Bind billing labor rate standard conf data to view controls..."
        Try
            Dim mapLabRatente As CViewModelMapList
            Dim bds As BindingSource

            mapLabRatente = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_RATE_NTE_VIEW_DISPLAY)

            'nte labrate datagridview
            mctrl.mainForm.conf_labrate_labrate_nte_grid.AllowUserToAddRows = False
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.conf_labrate_labrate_nte_grid)

            For Each map As CViewModelMap In mapLabRatente.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapLabRatente.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_labrate_labrate_nte_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapLabRatente.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    bds = New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    Else
                        Throw New Exception("Error occured when binding task rate view. List " & map.view_list_name & " does not exist in LOV")
                    End If
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_labrate_labrate_nte_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_labrate_labrate_nte_grid.Columns.Add(dgcol)
                End If
            Next
            'bind datasource and apply filter
            mctrl.mainForm.conf_labrate_labrate_nte_grid.DataSource = _nte_labor_rate_list
            filterLaborRateStd()
            'set width
            MGeneralFuntionsViewControl.resizeDataGridWidth(mctrl.mainForm.conf_labrate_labrate_nte_grid)
            'handle event
            Dim handler = New CViewRefreshHandler
            handler.datagridView = mctrl.mainForm.conf_labrate_labrate_nte_grid
            handler.listView = _nte_labor_rate_list
            handler.flexibleHeigtControl = mctrl.mainForm.conf_labrate_labrate_nte_grid
            CViewRefreshHandler.addEntry(Of CLaborRate)(handler)
            handler.refreshHandlerViewsHeight()

            'row validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.conf_labrate_labrate_nte_grid, CLaborRateRowValidator.getInstance, mapLabRatente)
        Catch ex As Exception
            Throw New Exception("An error occured while loading labor rate view", ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing labor rate standard conf data to view controls"
    End Sub

    'bind oem view
    Private Sub bind_task_rate_view()
        mctrl.statusBar.status_strip_sub_label = "Bind task rate conf data to view controls..."
        Try

            Dim mapTaskRate As CViewModelMapList
            Dim bds As BindingSource
            'map is the same as the project view
            mapTaskRate = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_TASK_RATE_VIEW_DISPLAY)

            mctrl.mainForm.conf_labrate_task_rate_grid.AllowUserToAddRows = True
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.conf_labrate_task_rate_grid)

            For Each map As CViewModelMap In mapTaskRate.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapTaskRate.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_labrate_task_rate_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapTaskRate.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    bds = New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    Else
                        Throw New Exception("Error occured when binding task rate view. List " & map.view_list_name & " does not exist in LOV")
                    End If
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_labrate_task_rate_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.conf_labrate_task_rate_grid.Columns.Add(dgcol)
                End If
            Next

            'bind datasource and apply filter
            _task_rate_list.ApplyFilter(AddressOf filterDeletedTaskRateDelegate)
            mctrl.mainForm.conf_labrate_task_rate_grid.DataSource = _task_rate_list
            'set width
            MGeneralFuntionsViewControl.resizeDataGridWidth(mctrl.mainForm.conf_labrate_task_rate_grid)
            'handle event
            Dim handler = New CViewRefreshHandler
            handler.datagridView = mctrl.mainForm.conf_labrate_task_rate_grid
            handler.listView = _task_rate_list
            handler.delegateList.Add(AddressOf refreshTaskRateDependantViews)
            handler.flexibleHeigtControl = mctrl.mainForm.conf_labrate_task_rate_grid
            handler.bindingListViewList.Add(mctrl.add_scope_controller.scopeList)
            CViewRefreshHandler.addEntry(Of CTaskRate)(handler)
            handler.refreshHandlerViewsHeight()

            'row validator
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.conf_labrate_task_rate_grid, CTaskRateRowValidator.getInstance, mapTaskRate)
            dhler.validateAddDelegate = AddressOf taskRate_view_ValidateAdd

        Catch ex As Exception
            Throw New Exception("An error occured while loading labor task view", ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind billing task rate conf data to view controls"
    End Sub

    'validate new
    Public Sub taskRate_view_ValidateAdd(taskR As ObjectView(Of CTaskRate), rowIndex As Integer)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(taskR.Object) Then
                'if new item
                taskR.Object.setID()
                refreshTaskRateDependantViews()
                If Not _task_rate_list.DataSource.Contains(taskR.Object) Then
                    _task_rate_list.EndNew(rowIndex)
                End If
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new discount entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'get task rate object
    Public Function getTaskRateObject(id As Long) As CTaskRate

        For Each taskR As CTaskRate In _task_rate_list.DataSource
            If taskR.id = id Then
                Return taskR
            End If
        Next
        Throw New Exception("task Rate object with ID " & id & " does not exist")
    End Function
    'take none deleted items
    Public Function filterDeletedTaskRateDelegate(taskRate As CTaskRate) As Boolean
        Return taskRate.system_status <> MConstants.CONST_SYS_ROW_DELETED
    End Function
    'refresh dependant views
    Public Sub refreshTaskRateDependantViews()
        Dim listO As List(Of CTaskRate)
        listO = MGeneralFuntionsViewControl.getTaskRateFromListView(_task_rate_list)
        listO.Add(MConstants.EMPTY_TASKRATE)
        _task_rate_lov.DataSource = listO
        _task_rate_lov.ResetBindings(False)
    End Sub

    'perform delete
    Public Sub deleteObjectTaskRate(taskrate As CTaskRate)
        Try
            If _task_rate_list.DataSource.Contains(taskrate) Then
                _task_rate_list.DataSource.Remove(taskrate)
            End If
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(taskrate)
        Catch ex As Exception
        End Try
    End Sub

End Class
