﻿Public Class CLaborCtrlReportingObj

    Public Sub New(ctrl_o As CLaborControlMainViewObj)
        _activity_key = ctrl_o.activity_key
        _proj = ctrl_o.act_obj.proj
        _activity = ctrl_o.act_obj.act
        _activity_desc = ctrl_o.act_obj.act_desc
        _jc = ctrl_o.act_obj.job_card
        _notification = ctrl_o.act_obj.notification
        _wty_code = ctrl_o.act_obj.warr_code
        _status = ctrl_o.act_obj.status_view
        _category = ctrl_o.act_obj.category_view
        _cc = ctrl_o.cc_object.cc
        _cc_desc = ctrl_o.cc_object.friendly_description
        _cc_group = ctrl_o.cc_object.grouping
        _cc_sap_id = ctrl_o.cc_object.sap_id
    End Sub

    Public Sub setCC(ctrl_o As CLaborControlMainViewObj)
        _cc = ctrl_o.cc_object.cc
        _cc_desc = ctrl_o.cc_object.friendly_description
        _cc_group = ctrl_o.cc_object.grouping
        _cc_sap_id = ctrl_o.cc_object.sap_id
    End Sub

    Private _activity_key As String
    Public Property activity_key() As String
        Get
            Return _activity_key
        End Get
        Set(ByVal value As String)
            If Not _activity_key = value Then
                _activity_key = value
            End If
        End Set
    End Property

    Private _proj As String
    Public Property proj() As String
        Get
            Return _proj
        End Get
        Set(ByVal value As String)
            If Not _proj = value Then
                _proj = value
            End If
        End Set
    End Property

    Private _activity As String
    Public Property activity() As String
        Get
            Return _activity
        End Get
        Set(ByVal value As String)
            If Not _activity = value Then
                _activity = value
            End If
        End Set
    End Property

    Private _activity_desc As String
    Public Property activity_desc() As String
        Get
            Return _activity_desc
        End Get
        Set(ByVal value As String)
            If Not _activity_desc = value Then
                _activity_desc = value
            End If
        End Set
    End Property

    Private _jc As String
    Public Property jc() As String
        Get
            Return _jc
        End Get
        Set(ByVal value As String)
            If Not _jc = value Then
                _jc = value
            End If
        End Set
    End Property

    Private _notification As String
    Public Property notification() As String
        Get
            Return _notification
        End Get
        Set(ByVal value As String)
            If Not _notification = value Then
                _notification = value
            End If
        End Set
    End Property

    Private _cost_node As String
    Public Property cost_node() As String
        Get
            Return _cost_node
        End Get
        Set(ByVal value As String)
            If Not _cost_node = value Then
                _cost_node = value
            End If
        End Set
    End Property

    Private _wty_code As String
    Public Property wty_code() As String
        Get
            Return _wty_code
        End Get
        Set(ByVal value As String)
            If Not _wty_code = value Then
                _wty_code = value
            End If
        End Set
    End Property

    Private _status As String
    Public Property status() As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            If Not _status = value Then
                _status = value
            End If
        End Set
    End Property

    Private _category As String
    Public Property category() As String
        Get
            Return _category
        End Get
        Set(ByVal value As String)
            If Not _category = value Then
                _category = value
            End If
        End Set
    End Property

    Private _cc As String
    Public Property cc() As String
        Get
            Return _cc
        End Get
        Set(ByVal value As String)
            If Not _cc = value Then
                _cc = value
            End If
        End Set
    End Property

    Private _cc_desc As String
    Public Property cc_desc() As String
        Get
            Return _cc_desc
        End Get
        Set(ByVal value As String)
            If Not _cc_desc = value Then
                _cc_desc = value
            End If
        End Set
    End Property

    Private _cc_group As String
    Public Property cc_group() As String
        Get
            Return _cc_group
        End Get
        Set(ByVal value As String)
            If Not _cc_group = value Then
                _cc_group = value
            End If
        End Set
    End Property

    Private _cc_sap_id As String
    Public Property cc_sap_id() As String
        Get
            Return _cc_sap_id
        End Get
        Set(ByVal value As String)
            If Not _cc_sap_id = value Then
                _cc_sap_id = value
            End If
        End Set
    End Property

    Private _hours_type As String
    Public Property hours_type() As String
        Get
            Return _hours_type
        End Get
        Set(ByVal value As String)
            If Not _hours_type = value Then
                _hours_type = value
            End If
        End Set
    End Property

    Private _qty As Double
    Public Property qty() As Double
        Get
            Return _qty
        End Get
        Set(ByVal value As Double)
            If Not _qty = value Then
                _qty = value
            End If
        End Set
    End Property
End Class
