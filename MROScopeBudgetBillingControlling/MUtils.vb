﻿Module MUtils

    'add key to dictionnary only if does not exists
    Public Sub addKeyValIfNotExistsOrReplace(Of T)(list As Dictionary(Of String, T), key As String, val As T)
        If Not list.ContainsKey(key) Then
            list.Add(key, val)
        Else
            list(key) = val
        End If
    End Sub

    'check if lab mat app is labor
    Public Function isLaborApplicableLabMat(appl As String)
        Dim res As Boolean = False
        Try
            If String.IsNullOrWhiteSpace(appl) Then
                res = False
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_SERV_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_KEY).value Then
                res = True
            Else
                res = False
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while testing if labmat applicability is labor" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        Return res
    End Function

    'check if lab mat app is material
    Public Function isMaterialApplicableLabMat(appl As String)
        Dim res As Boolean = False
        Try
            If String.IsNullOrWhiteSpace(appl) Then
                res = False
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_SERV_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_REP_FRGH_SERV_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_REP_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_REP_FRGH_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_KEY).value Then
                res = True
            Else
                res = False
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while testing if labmat applicability is material" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        Return res
    End Function


    'check if lab mat app is repair
    Public Function isRepairApplicableLabMat(appl As String)
        Dim res As Boolean = False
        Try
            If String.IsNullOrWhiteSpace(appl) Then
                res = False
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_REP_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_SERV_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_REP_FRGH_SERV_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_REP_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_REP_FRGH_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_KEY).value Then
                res = True
            Else
                res = False
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while testing if labmat applicability is Repair" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        Return res
    End Function


    'check if lab mat app is freight
    Public Function isFreightApplicableLabMat(appl As String)
        Dim res As Boolean = False
        Try
            If String.IsNullOrWhiteSpace(appl) Then
                res = False
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_SERV_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_FREIGHT_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_REP_FRGH_SERV_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_REP_FRGH_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_KEY).value Then
                res = True
            Else
                res = False
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while testing if labmat applicability is Freight" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        Return res
    End Function


    'check if lab mat app is serv
    Public Function isServApplicableLabMat(appl As String)
        Dim res As Boolean = False
        Try
            If String.IsNullOrWhiteSpace(appl) Then
                res = False
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_SERV_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_SERV_KEY).value Then
                res = True
            ElseIf appl = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_REP_FRGH_SERV_KEY).value Then
                res = True
            Else
                res = False
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while testing if labmat applicability is Service" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        Return res

    End Function
End Module
