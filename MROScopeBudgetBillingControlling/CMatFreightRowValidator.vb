﻿
Public Class CMatFreightRowValidator
    Public Shared instance As CMatFreightRowValidator
    Public Shared Function getInstance() As CMatFreightRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CMatFreightRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(freight As CMatFreight)
        freight.calc_is_in_error_state = False
    End Sub

    Public Function percent(freight As CMatFreight) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If freight.is_editable Then
            If freight.price < 0 Then
                err = "Freight Price Shall be Positive"
                res = False
                freight.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function curr(freight As CMatFreight) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If freight.is_editable Then
            If String.IsNullOrWhiteSpace(freight.curr) OrElse freight.curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value < 0 Then
                err = "Freight Currecny Cannot be Empty"
                res = False
                freight.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function
End Class
