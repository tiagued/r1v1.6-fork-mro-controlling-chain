﻿Imports Equin.ApplicationFramework

Public Class CScopeActivityCalculator

    'to be replaced by a fake awr object
    Public Shared INIT As String = "0¬$INIT"
    Public Shared INIT_ADJ As String = "1¬$INIT_ADJ"
    'asci sorting for the view. numbers before letter
    Public Shared ADD As String = "X¬$ADD"
    Public Shared ADD_AWQ As String = "Y¬$ADD_AWQ"
    Public Shared TOTAL As String = "Z¬$TOTAL"
    Public Shared BEFORE_DISC_SUFF As String = "A_BEFOR_DISC"
    Public Shared DISC_SUFF As String = "B_DISC"
    Public Shared ACT_DISC_SUFF As String = "C_ACT_DISC"
    Public Shared AFTER_DISC_SUFF As String = "D_AFTER_DISC"
    Public Shared EMPTY_LINE As String = "EMPTY_LINE"

    Public Sub New(act As CScopeActivity)
        _act = act
        _calc_tree_structure = New Dictionary(Of String, Dictionary(Of String, List(Of CScopeAtivityCalcEntry)))
        _calc_payer_list = New List(Of CPayer)
        _calc_scope_list = New List(Of CConfProperty)
        _calc_scope_list_dic = New Dictionary(Of String, String)

        'add empty
        _calc_payer_list.Add(MConstants.EMPTY_PAYER)
        _calc_scope_list.Add(MConstants.EMPTY_CONFPROP)
        _calc_scope_list_dic.Add(MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value, "")
        'flat organization from _calc_tree_structure
        _calc_entries_list = New List(Of CScopeAtivityCalcEntry)
        _calc_entries_view = New BindingListView(Of CScopeAtivityCalcEntry)(_calc_entries_list)
        _calc_entries_view.ApplySort("entry_scope ASC, calc_type ASC ")

        _calc_entry_type_list = New Dictionary(Of String, String)
        _calc_entry_type_list.Add(BEFORE_DISC_SUFF, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_BEF_DISC).value)
        _calc_entry_type_list.Add(DISC_SUFF, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_DISC).value)
        _calc_entry_type_list.Add(AFTER_DISC_SUFF, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_AFT_DISC).value)
        _calc_entry_type_list.Add(MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value, " ")

        _view_current_payer = MConstants.EMPTY_PAYER
        _view_current_entry_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        _view_current_scope = MConstants.EMPTY_CONFPROP
    End Sub


    Private _act As CScopeActivity
    Public Property act() As CScopeActivity
        Get
            Return _act
        End Get
        Set(ByVal value As CScopeActivity)
            _act = value
        End Set
    End Property

    Private _fillMatLabList As Boolean
    Public Property fillMatLabList() As Boolean
        Get
            Return _fillMatLabList
        End Get
        Set(ByVal value As Boolean)
            _fillMatLabList = value
        End Set
    End Property
    'list of payers
    Private _calc_payer_list As List(Of CPayer)

    Public ReadOnly Property calc_payer_list() As List(Of CPayer)
        Get
            Return _calc_payer_list
        End Get
    End Property

    'use in pair with list just to ease search of uniqueness
    Private _calc_scope_list_dic As Dictionary(Of String, String)
    'list of scopes
    Private _calc_scope_list As List(Of CConfProperty)
    Public ReadOnly Property calc_scope_list() As List(Of CConfProperty)
        Get
            Return _calc_scope_list
        End Get
    End Property

    'list of scopes
    Private _calc_entry_type_list As Dictionary(Of String, String)
    Public ReadOnly Property calc_entry_type_list() As Dictionary(Of String, String)
        Get
            Return _calc_entry_type_list
        End Get
    End Property

    'list of calucation entries
    Private _calc_entries_view As BindingListView(Of CScopeAtivityCalcEntry)
    Public ReadOnly Property calc_entries_view() As BindingListView(Of CScopeAtivityCalcEntry)
        Get
            Return _calc_entries_view
        End Get
    End Property

    'selected payer to filter on
    Private _view_current_payer As CPayer
    Public Property view_current_payer() As CPayer
        Get
            Return _view_current_payer
        End Get
        Set(value As CPayer)
            _view_current_payer = value
            filterView()
        End Set
    End Property

    'selected scope to filter on
    Private _view_current_scope As CConfProperty
    Public Property view_current_scope() As CConfProperty
        Get
            Return _view_current_scope
        End Get
        Set(value As CConfProperty)
            _view_current_scope = value
            filterView()
        End Set
    End Property

    'selected entry type to filter on
    Private _view_current_entry_type As String
    Public Property view_current_entry_type() As String
        Get
            Return _view_current_entry_type
        End Get
        Set(value As String)
            If Not _view_current_entry_type = value Then
                _view_current_entry_type = value
                filterView()
            End If
        End Set
    End Property

    'list used in the view
    Private _calc_entries_list As List(Of CScopeAtivityCalcEntry)

    Private _empty_entry As CScopeAtivityCalcEntry
    Public Property emptry_entry() As CScopeAtivityCalcEntry
        Get
            Return _empty_entry
        End Get
        Set(ByVal value As CScopeAtivityCalcEntry)
            _empty_entry = value
        End Set
    End Property

    Private _calc_tree_structure As Dictionary(Of String, Dictionary(Of String, List(Of CScopeAtivityCalcEntry)))


    Public Sub update(Optional fillScopeMatLabList As Boolean = False)
        Try
            _fillMatLabList = fillScopeMatLabList

            Dim customer As CPayer = MConstants.listOfPayer(MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value)
            'clear cb lists
            _calc_payer_list.Clear()
            _calc_scope_list.Clear()
            _calc_scope_list_dic.Clear()
            _calc_payer_list.Add(MConstants.EMPTY_PAYER)
            _calc_scope_list.Add(MConstants.EMPTY_CONFPROP)
            _calc_scope_list_dic.Add(MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value, "")


            'lists to handle OEM text
            Dim laborPayers As New Dictionary(Of String, List(Of CPayer))
            Dim matPayers As New Dictionary(Of String, List(Of CPayer))
            Dim repPayers As New Dictionary(Of String, List(Of CPayer))
            Dim freightPayers As New Dictionary(Of String, List(Of CPayer))
            Dim servPayers As New Dictionary(Of String, List(Of CPayer))

            'temporary, musb be reused for performance matters
            _calc_tree_structure = New Dictionary(Of String, Dictionary(Of String, List(Of CScopeAtivityCalcEntry)))

            'for view
            _calc_entries_list.Clear()

            'add total for all activities
            addCalcGroup(TOTAL, TOTAL, customer, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_TOTAL).value)

            'is markup
            'if labor mark
            Dim isLaborRedMark As Boolean = act.labor_sold_mark <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value _
                AndAlso Not String.IsNullOrWhiteSpace(act.labor_sold_mark)
            'if mat mark
            Dim isMatRedMark As Boolean = act.mat_sold_mark <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value _
                AndAlso Not String.IsNullOrWhiteSpace(act.mat_sold_mark)
            'if rep mark
            Dim isRepRedMark As Boolean = act.rep_sold_mark <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value _
                AndAlso Not String.IsNullOrWhiteSpace(act.rep_sold_mark)
            'if Freight mark
            Dim isFreightRedMark As Boolean = act.freight_sold_mark <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value _
                AndAlso Not String.IsNullOrWhiteSpace(act.freight_sold_mark)
            'if labor mark
            Dim isServRedMark As Boolean = act.serv_sold_mark <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value _
                AndAlso Not String.IsNullOrWhiteSpace(act.serv_sold_mark)

            Dim isActLinkedToAWQ As Boolean = Not IsNothing(act.awq_master_obj)
            'create calc entry for all approved or sent awq, will be used to add potential cost collectors values during reporting as cost collectors are not reported
            If Not IsNothing(act.awq_master_obj) AndAlso act.awq_master_obj.Equals(act) Then
                For Each my_awq As CAWQuotation In act.awq_master_obj.revision_list
                    If my_awq.is_approved_closed OrElse my_awq.is_open_and_sent_to_customer Then

                    End If
                Next
            End If

            'use tô display default data for items with no material and/or labor
            _empty_entry = New CScopeAtivityCalcEntry(EMPTY_LINE)
            _empty_entry.act_obj = act

            Dim isActInit As Boolean = False
            'init price, material and labor
            If act.quote_entry_link_list.Count > 0 Then
                isActInit = True
                For Each quoActLk As CSaleQuoteEntryToActLink In act.quote_entry_link_list
                    If Not IsNothing(quoActLk.init_quote_entry_obj) Then
                        'total
                        addCalcGroup(TOTAL, TOTAL, quoActLk.init_quote_entry_obj.quote_obj.payer_obj, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_TOTAL).value)
                        'add calc object if not exist. A calculation group is a set of three lines: before discount, discount and after discount
                        addCalcGroup(INIT, quoActLk.init_quote_entry_obj.calc_entry_scope, quoActLk.init_quote_entry_obj.quote_obj.payer_obj, quoActLk.init_quote_entry_obj.calc_entry_scope_label)


                        'before discount
                        Dim beforeDisc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(quoActLk.init_quote_entry_obj.calc_entry_scope, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                        Dim disc As CScopeAtivityCalcEntry = getDiscountEntry(quoActLk.init_quote_entry_obj.calc_entry_scope, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                        Dim afterDisc As CScopeAtivityCalcEntry = getAfterDiscountEntry(quoActLk.init_quote_entry_obj.calc_entry_scope, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)

                        'total 
                        Dim total_befor_disc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(TOTAL, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                        Dim total_disc As CScopeAtivityCalcEntry = getDiscountEntry(TOTAL, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                        Dim total_after_disc As CScopeAtivityCalcEntry = getAfterDiscountEntry(TOTAL, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)

                        'is adjusted
                        Dim is_adj As Boolean = quoActLk.init_quote_entry_obj.labor_price_adj < 0 OrElse quoActLk.init_quote_entry_obj.mat_price_adj < 0 OrElse quoActLk.init_quote_entry_obj.repair_price_adj < 0 _
                                OrElse quoActLk.init_quote_entry_obj.freight_price_adj < 0 OrElse quoActLk.init_quote_entry_obj.third_party_price_adj < 0

                        If Not IsNothing(quoActLk.init_quote_entry_obj.main_activity_obj) AndAlso quoActLk.init_quote_entry_obj.main_activity_obj.Equals(act) Then

                            ' init payer for oem warr text
                            addWarrPayer(quoActLk.init_quote_entry_obj.calc_entry_scope, laborPayers, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                            'No Warr Text For Material in case of INIT
                            'addWarrPayer(INIT, matPayers, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                            'addWarrPayer(INIT, repPayers, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                            'addWarrPayer(INIT, freightPayers, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                            'addWarrPayer(INIT, servPayers, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)

                            Dim adj_beforeDisc As CScopeAtivityCalcEntry = Nothing
                            Dim adj_disc As CScopeAtivityCalcEntry = Nothing
                            Dim adj_afterDisc As CScopeAtivityCalcEntry = Nothing
                            If is_adj Then
                                addCalcGroup(INIT_ADJ, quoActLk.init_quote_entry_obj.calc_entry_scope_adj, quoActLk.init_quote_entry_obj.quote_obj.payer_obj, quoActLk.init_quote_entry_obj.calc_entry_scope_label_adj)
                                adj_beforeDisc = getBeforeDiscountEntry(quoActLk.init_quote_entry_obj.calc_entry_scope_adj, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                                adj_disc = getDiscountEntry(quoActLk.init_quote_entry_obj.calc_entry_scope_adj, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                                adj_afterDisc = getAfterDiscountEntry(quoActLk.init_quote_entry_obj.calc_entry_scope_adj, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)

                                ' init payer for oem warr text
                                addWarrPayer(quoActLk.init_quote_entry_obj.calc_entry_scope_adj, laborPayers, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                                'No Warr Text For Material in case of INIT
                                'addWarrPayer(INIT_ADJ, matPayers, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                                'addWarrPayer(INIT_ADJ, repPayers, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                                'addWarrPayer(INIT_ADJ, freightPayers, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                                'addWarrPayer(INIT_ADJ, servPayers, quoActLk.init_quote_entry_obj.quote_obj.payer_obj)
                            End If

                            'activity is main activity
                            beforeDisc.hours_sold_calc = 0
                            total_befor_disc.hours_sold_calc = 0
                            If is_adj Then
                                adj_beforeDisc.hours_sold_calc = 0
                            End If
                            beforeDisc.labor_sold_calc = MCalc.toLabCurr(quoActLk.init_quote_entry_obj.labor_price, quoActLk.init_quote_entry_obj.quote_obj.lab_curr)
                            total_befor_disc.labor_sold_calc = total_befor_disc.labor_sold_calc + beforeDisc.labor_sold_calc
                            total_befor_disc.labor_sold_no_disc_calc = total_befor_disc.labor_sold_no_disc_calc + beforeDisc.labor_sold_calc

                            If is_adj Then
                                adj_beforeDisc.labor_sold_calc = MCalc.toLabCurr(quoActLk.init_quote_entry_obj.labor_price_adj, quoActLk.init_quote_entry_obj.quote_obj.lab_curr)
                                total_befor_disc.labor_sold_calc = total_befor_disc.labor_sold_calc + adj_beforeDisc.labor_sold_calc
                                total_befor_disc.labor_sold_no_disc_calc = total_befor_disc.labor_sold_no_disc_calc + adj_beforeDisc.labor_sold_calc
                            End If

                            beforeDisc.mat_sold_calc = MCalc.toMatCurr(quoActLk.init_quote_entry_obj.mat_price, quoActLk.init_quote_entry_obj.quote_obj.mat_curr)
                            total_befor_disc.mat_sold_calc = total_befor_disc.mat_sold_calc + beforeDisc.mat_sold_calc
                            total_befor_disc.mat_sold_no_disc_calc = total_befor_disc.mat_sold_no_disc_calc + beforeDisc.mat_sold_calc
                            If is_adj Then
                                adj_beforeDisc.mat_sold_calc = MCalc.toMatCurr(quoActLk.init_quote_entry_obj.mat_price_adj, quoActLk.init_quote_entry_obj.quote_obj.mat_curr)
                                total_befor_disc.mat_sold_calc = total_befor_disc.mat_sold_calc + adj_beforeDisc.mat_sold_calc
                                total_befor_disc.mat_sold_no_disc_calc = total_befor_disc.mat_sold_no_disc_calc + adj_beforeDisc.mat_sold_calc

                            End If

                            beforeDisc.freight_sold_calc = MCalc.toMatCurr(quoActLk.init_quote_entry_obj.freight_price, quoActLk.init_quote_entry_obj.quote_obj.mat_curr)
                            total_befor_disc.freight_sold_calc = total_befor_disc.freight_sold_calc + beforeDisc.freight_sold_calc
                            total_befor_disc.freight_sold_no_disc_calc = total_befor_disc.freight_sold_no_disc_calc + beforeDisc.freight_sold_calc
                            If is_adj Then
                                adj_beforeDisc.freight_sold_calc = MCalc.toMatCurr(quoActLk.init_quote_entry_obj.freight_price_adj, quoActLk.init_quote_entry_obj.quote_obj.mat_curr)
                                total_befor_disc.freight_sold_calc = total_befor_disc.freight_sold_calc + adj_beforeDisc.freight_sold_calc
                                total_befor_disc.freight_sold_no_disc_calc = total_befor_disc.freight_sold_no_disc_calc + adj_beforeDisc.freight_sold_calc
                            End If

                            beforeDisc.rep_sold_calc = MCalc.toMatCurr(quoActLk.init_quote_entry_obj.repair_price, quoActLk.init_quote_entry_obj.quote_obj.mat_curr)
                            total_befor_disc.rep_sold_calc = total_befor_disc.rep_sold_calc + beforeDisc.rep_sold_calc
                            total_befor_disc.rep_sold_no_disc_calc = total_befor_disc.rep_sold_no_disc_calc + beforeDisc.rep_sold_calc
                            If is_adj Then
                                adj_beforeDisc.rep_sold_calc = MCalc.toMatCurr(quoActLk.init_quote_entry_obj.repair_price_adj, quoActLk.init_quote_entry_obj.quote_obj.mat_curr)
                                total_befor_disc.rep_sold_calc = total_befor_disc.rep_sold_calc + adj_beforeDisc.rep_sold_calc
                                total_befor_disc.rep_sold_no_disc_calc = total_befor_disc.rep_sold_no_disc_calc + adj_beforeDisc.rep_sold_calc
                            End If

                            beforeDisc.serv_sold_calc = MCalc.toMatCurr(quoActLk.init_quote_entry_obj.third_party_price, quoActLk.init_quote_entry_obj.quote_obj.mat_curr)
                            total_befor_disc.serv_sold_calc = total_befor_disc.serv_sold_calc + beforeDisc.serv_sold_calc
                            total_befor_disc.serv_sold_no_disc_calc = total_befor_disc.serv_sold_no_disc_calc + beforeDisc.serv_sold_calc
                            If is_adj Then
                                adj_beforeDisc.serv_sold_calc = MCalc.toMatCurr(quoActLk.init_quote_entry_obj.third_party_price_adj, quoActLk.init_quote_entry_obj.quote_obj.mat_curr)
                                total_befor_disc.serv_sold_calc = total_befor_disc.serv_sold_calc + adj_beforeDisc.serv_sold_calc
                                total_befor_disc.serv_sold_no_disc_calc = total_befor_disc.serv_sold_no_disc_calc + adj_beforeDisc.serv_sold_calc
                            End If

                            'no discount for init scope, before disc is after disc
                            beforeDisc.copyValuesTo(afterDisc)
                            If is_adj Then
                                adj_beforeDisc.copyValuesTo(adj_afterDisc)
                            End If
                            'copy all total values cause they will be used for discount at activity level
                            total_befor_disc.copyValuesTo(total_after_disc)
                            'IF ALL PRICE AT ZERO CONSIDER AS FREE
                            If quoActLk.init_quote_entry_obj.isZero Then
                                beforeDisc.labor_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_VAL_ANALYSIS_CNCL_CFWD_TXT).value
                                beforeDisc.mat_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_VAL_ANALYSIS_CNCL_CFWD_TXT).value
                                beforeDisc.rep_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_VAL_ANALYSIS_CNCL_CFWD_TXT).value
                                beforeDisc.freight_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_VAL_ANALYSIS_CNCL_CFWD_TXT).value
                                beforeDisc.serv_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_VAL_ANALYSIS_CNCL_CFWD_TXT).value
                            End If
                        Else
                            'refer to quote
                            beforeDisc.labor_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                            beforeDisc.mat_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                            beforeDisc.freight_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                            beforeDisc.rep_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                            beforeDisc.serv_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                        End If
                    End If
                Next
            End If

            'if is not added value
            Dim isActNonAddedValue As Boolean = act.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_NOT_ADD_V_KEY).value _
                    OrElse act.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_WAST_KEY).value _
                    OrElse act.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_CANC_FWD_KEY).value
            Dim isCancelled As Boolean = act.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_CANC_FWD_KEY).value

            Dim isFixLabor As Boolean = False
            Dim isActLabOrMatAWQ As Boolean = False
            'handle fix labor
            If act.fix_labor > 0 AndAlso Not isLaborRedMark AndAlso Not isActNonAddedValue AndAlso Not act.is_cost_collector Then
                isFixLabor = True
                Dim beforeDisc As CScopeAtivityCalcEntry
                Dim disc As CScopeAtivityCalcEntry
                Dim afterDisc As CScopeAtivityCalcEntry

                addCalcGroup(ADD, ADD, customer, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD).value)
                beforeDisc = getBeforeDiscountEntry(ADD, customer)
                disc = getDiscountEntry(ADD, customer)
                afterDisc = getAfterDiscountEntry(ADD, customer)

                ' add payer for oem warr text
                addWarrPayer(ADD, laborPayers, customer)

                'total always exists for customer
                Dim total_befor_disc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(TOTAL, customer)
                Dim total_disc As CScopeAtivityCalcEntry = getDiscountEntry(TOTAL, customer)
                Dim total_after_disc As CScopeAtivityCalcEntry = getAfterDiscountEntry(TOTAL, customer)

                beforeDisc.labor_sold_calc = act.fix_labor

                total_befor_disc.labor_sold_calc = total_befor_disc.labor_sold_calc + act.fix_labor

                'discount current scope and total scope
                'for exclusive discount
                total_befor_disc.labor_sold_no_disc_calc = total_befor_disc.labor_sold_no_disc_calc + act.fix_labor

                'after discount current scope and total scope
                afterDisc.labor_sold_calc = act.fix_labor

                total_after_disc.labor_sold_calc = total_after_disc.labor_sold_calc + act.fix_labor
            End If

            'use to handle TBA if no sold hours
            Dim isAnySoldHours As Boolean = False
            'handle awq and additional scope under threshold
            For Each soldH As CSoldHours In act.sold_hour_list
                'add
                Dim beforeDisc As CScopeAtivityCalcEntry = Nothing
                Dim disc As CScopeAtivityCalcEntry = Nothing
                Dim afterDisc As CScopeAtivityCalcEntry = Nothing
                'if removed awq is set
                Dim cancel_beforeDisc As CScopeAtivityCalcEntry = Nothing
                Dim cancel_disc As CScopeAtivityCalcEntry = Nothing
                Dim cancel_afterDisc As CScopeAtivityCalcEntry = Nothing

                'is sold hours removed from AWQ
                Dim cancelAWR As Boolean = False
                'if sent to customer show in total
                Dim addToToTal As Boolean = False
                Dim cancelAddToToTal As Boolean = False
                'consider calculated sold hours
                Dim isPriceCalculated As Boolean = False
                'ignore deleted
                If soldH.system_status = MConstants.CONST_SYS_ROW_DELETED Then
                    Continue For
                End If

                'ignore FOC
                If soldH.red_mark = MConstants.constantConf(MConstants.CONST_CONF_LOV_RED_MARK_FOC_KEY).value Then
                    Continue For
                End If

                If Not soldH.is_reported OrElse Not IsNothing(soldH.init_quote_entry_obj) Then
                    Continue For
                End If

                'ignore archived
                If Not IsNothing(soldH.remove_awq_obj) AndAlso Not IsNothing(soldH.remove_awq_obj.master_obj) _
                    AndAlso Not IsNothing(soldH.remove_awq_obj.master_obj.latest_REV) AndAlso soldH.remove_awq_obj.master_obj.latest_REV.is_archived Then
                    Continue For
                End If

                isAnySoldHours = True


                'case below treshold and no redmarks, no non addedvalue
                If IsNothing(soldH.awq_obj) AndAlso IsNothing(soldH.remove_awq_obj) AndAlso Not isLaborRedMark AndAlso Not isActNonAddedValue Then
                    If isFixLabor Then
                        'if fix labor and no awq ignore
                        Continue For
                    ElseIf Not act.is_cost_collector Then
                        'consider calculate price
                        isPriceCalculated = True
                        addCalcGroup(ADD, ADD, soldH.payer_obj, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD).value)
                        'add payer for oem warr text
                        addWarrPayer(ADD, laborPayers, soldH.payer_obj)

                        beforeDisc = getBeforeDiscountEntry(ADD, soldH.payer_obj)
                        disc = getDiscountEntry(ADD, soldH.payer_obj)
                        afterDisc = getAfterDiscountEntry(ADD, soldH.payer_obj)
                        addToToTal = True
                    End If
                ElseIf Not IsNothing(soldH.awq_obj) Then
                    isActLabOrMatAWQ = True
                    addCalcGroup(ADD_AWQ, soldH.awq_obj.reference_revision, soldH.payer_obj, soldH.awq_obj.reference_revision)
                    beforeDisc = getBeforeDiscountEntry(soldH.awq_obj.reference_revision, soldH.payer_obj)
                    disc = getDiscountEntry(soldH.awq_obj.reference_revision, soldH.payer_obj)
                    afterDisc = getAfterDiscountEntry(soldH.awq_obj.reference_revision, soldH.payer_obj)
                    addWarrPayer(soldH.awq_obj.reference_revision, laborPayers, soldH.payer_obj)
                    isPriceCalculated = True

                    'add to total if awq has been sent
                    If soldH.awq_obj.has_been_sent Then
                        addToToTal = True
                    End If

                    If soldH.awq_obj.is_approved_closed Then
                        addToToTal = True
                        If Not IsNothing(soldH.remove_awq_obj) AndAlso Not soldH.remove_awq_obj.is_cancelled_or_superseded Then
                            'ignore if cancelled or superseded
                            cancelAWR = True
                            addWarrPayer(soldH.remove_awq_obj.reference_revision, laborPayers, soldH.payer_obj)
                            addCalcGroup(ADD_AWQ, soldH.remove_awq_obj.reference_revision, soldH.payer_obj, soldH.remove_awq_obj.reference_revision)
                            cancel_beforeDisc = getBeforeDiscountEntry(soldH.remove_awq_obj.reference_revision, soldH.payer_obj)
                            cancel_disc = getDiscountEntry(soldH.remove_awq_obj.reference_revision, soldH.payer_obj)
                            cancel_afterDisc = getAfterDiscountEntry(soldH.remove_awq_obj.reference_revision, soldH.payer_obj)

                            If soldH.remove_awq_obj.is_approved_closed OrElse soldH.remove_awq_obj.is_open_and_sent_to_customer Then
                                cancelAddToToTal = True
                            End If
                        End If
                    End If

                End If

                'calculate if nedded, isPriceCalculated already exclude below treshold where there is a red mark or no added value or 
                If isPriceCalculated Then
                    'total
                    addCalcGroup(TOTAL, TOTAL, soldH.payer_obj, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_TOTAL).value)
                    Dim total_befor_disc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(TOTAL, soldH.payer_obj)
                    Dim total_disc As CScopeAtivityCalcEntry = getDiscountEntry(TOTAL, soldH.payer_obj)
                    Dim total_after_disc As CScopeAtivityCalcEntry = getAfterDiscountEntry(TOTAL, soldH.payer_obj)

                    'hours before discount current scope and total scope
                    beforeDisc.hours_sold_calc = beforeDisc.hours_sold_calc + soldH.sold_hrs

                    If addToToTal And Not cancelAddToToTal Then
                        total_befor_disc.hours_sold_calc = total_befor_disc.hours_sold_calc + soldH.sold_hrs
                    End If
                    If cancelAWR Then
                        cancel_beforeDisc.hours_sold_calc = cancel_beforeDisc.hours_sold_calc - soldH.sold_hrs
                    End If

                    'labor befor discount current scope and total scope
                    beforeDisc.labor_sold_calc = beforeDisc.labor_sold_calc + soldH.calc_price_before_disc
                    If addToToTal And Not cancelAddToToTal Then
                        total_befor_disc.labor_sold_calc = total_befor_disc.labor_sold_calc + soldH.calc_price_before_disc
                    End If
                    If cancelAWR Then
                        cancel_beforeDisc.labor_sold_calc = cancel_beforeDisc.labor_sold_calc - soldH.calc_price_before_disc
                    End If
                    'for awq form
                    If _fillMatLabList AndAlso (soldH.calc_price_before_disc > 0 OrElse soldH.isRedMarkAndReportableToCustomer) Then
                        beforeDisc.laborList.Add(soldH)
                        If cancelAddToToTal Then
                            cancel_beforeDisc.laborList.Add(soldH)
                            cancel_beforeDisc.labMatDeletedList.Add(soldH)
                        End If
                    End If

                    'discount current scope and total scope
                    disc.labor_sold_calc = disc.labor_sold_calc + soldH.calc_discount
                    If cancelAWR Then
                        cancel_disc.labor_sold_calc = cancel_disc.labor_sold_calc - soldH.calc_discount
                    End If

                    If addToToTal And Not cancelAddToToTal Then
                        total_disc.labor_sold_calc = total_disc.labor_sold_calc + soldH.calc_discount
                        'for exclusive discount
                        If IsNothing(soldH.discount_obj) OrElse soldH.discount_obj.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value Then
                            total_befor_disc.labor_sold_no_disc_calc = total_befor_disc.labor_sold_no_disc_calc + soldH.calc_price_before_disc
                        End If
                    End If

                    'after discount current scope and total scope
                    afterDisc.hours_sold_calc = afterDisc.hours_sold_calc + soldH.sold_hrs
                    afterDisc.labor_sold_calc = afterDisc.labor_sold_calc + soldH.calc_price
                    If cancelAWR Then
                        cancel_afterDisc.hours_sold_calc = cancel_afterDisc.hours_sold_calc - soldH.sold_hrs
                        cancel_afterDisc.labor_sold_calc = cancel_afterDisc.labor_sold_calc - soldH.calc_price
                    End If

                    If addToToTal And Not cancelAddToToTal Then
                        total_after_disc.hours_sold_calc = total_after_disc.hours_sold_calc + soldH.sold_hrs
                        total_after_disc.labor_sold_calc = total_after_disc.labor_sold_calc + soldH.calc_price
                    End If
                End If
            Next

            Dim isAnyMat As Boolean = False

            'additional material => To be detailed in AWR
            Dim is_all_mat_red_mark = isMatRedMark AndAlso isRepRedMark AndAlso isServRedMark AndAlso isFreightRedMark

            For Each mat As CMaterial In act.material_list

                'ignore deleted
                If mat.system_status = MConstants.CONST_SYS_ROW_DELETED Then
                    Continue For
                End If
                'ignore archived
                If Not IsNothing(mat.remove_awq_obj) AndAlso Not IsNothing(mat.remove_awq_obj.master_obj) _
                    AndAlso Not IsNothing(mat.remove_awq_obj.master_obj.latest_REV) AndAlso mat.remove_awq_obj.master_obj.latest_REV.is_archived Then
                    Continue For
                End If
                If mat.is_scope_init OrElse Not mat.is_reported Then
                    If mat.is_scope_init AndAlso mat.is_reported Then
                        'handle material info
                        addCalcGroup(TOTAL, TOTAL, mat.payer_obj, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_TOTAL).value)
                        Dim myentry_total As CScopeAtivityCalcEntry = getBeforeDiscountEntry(TOTAL, mat.payer_obj)
                        addMaterialInfo(mat.calc_material_info, myentry_total)

                        If isActInit AndAlso Not IsNothing(mat.init_quote_entry_obj) Then
                            addCalcGroup(INIT, mat.init_quote_entry_obj.calc_entry_scope, mat.payer_obj, mat.init_quote_entry_obj.calc_entry_scope_label)
                            Dim ini_before_disc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(mat.init_quote_entry_obj.calc_entry_scope, mat.payer_obj)
                            addMaterialInfo(mat.calc_material_info, ini_before_disc)
                        End If
                    End If

                    Continue For
                End If

                isAnyMat = True
                addCalcGroup(TOTAL, TOTAL, mat.payer_obj, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_TOTAL).value)
                Dim total_befor_disc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(TOTAL, mat.payer_obj)
                Dim total_disc As CScopeAtivityCalcEntry = getDiscountEntry(TOTAL, mat.payer_obj)
                Dim total_after_disc As CScopeAtivityCalcEntry = getAfterDiscountEntry(TOTAL, mat.payer_obj)

                'add
                Dim beforeDisc As CScopeAtivityCalcEntry = Nothing
                Dim disc As CScopeAtivityCalcEntry = Nothing
                Dim afterDisc As CScopeAtivityCalcEntry = Nothing
                Dim addToToTal As Boolean = False

                Dim cancel_beforeDisc As CScopeAtivityCalcEntry = Nothing
                Dim cancel_disc As CScopeAtivityCalcEntry = Nothing
                Dim cancel_afterDisc As CScopeAtivityCalcEntry = Nothing
                Dim cancelAWR As Boolean = False
                Dim cancelAddToToTal As Boolean = False
                Dim isPriceCalculated As Boolean = False
                Dim isAdditionalScope As Boolean = False

                If IsNothing(mat.awq_obj) AndAlso IsNothing(mat.remove_awq_obj) AndAlso Not isActNonAddedValue AndAlso Not is_all_mat_red_mark Then
                    isPriceCalculated = True
                    addCalcGroup(ADD, ADD, mat.payer_obj, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD).value)

                    beforeDisc = getBeforeDiscountEntry(ADD, mat.payer_obj)
                    disc = getDiscountEntry(ADD, mat.payer_obj)
                    afterDisc = getAfterDiscountEntry(ADD, mat.payer_obj)
                    addToToTal = True
                    isAdditionalScope = True
                ElseIf Not IsNothing(mat.awq_obj) Then
                    isActLabOrMatAWQ = True
                    isPriceCalculated = True
                    addCalcGroup(ADD_AWQ, mat.awq_obj.reference_revision, mat.payer_obj, mat.awq_obj.reference_revision)
                    beforeDisc = getBeforeDiscountEntry(mat.awq_obj.reference_revision, mat.payer_obj)
                    disc = getDiscountEntry(mat.awq_obj.reference_revision, mat.payer_obj)
                    afterDisc = getAfterDiscountEntry(mat.awq_obj.reference_revision, mat.payer_obj)

                    'add to total if awq has been sent
                    If mat.awq_obj.has_been_sent Then
                        addToToTal = True
                    End If

                    If mat.awq_obj.is_approved_closed Then
                        addToToTal = True
                        If Not IsNothing(mat.remove_awq_obj) AndAlso Not mat.remove_awq_obj.is_cancelled_or_superseded Then
                            cancelAWR = True
                            addCalcGroup(ADD_AWQ, mat.remove_awq_obj.reference_revision, mat.payer_obj, mat.remove_awq_obj.reference_revision)
                            cancel_beforeDisc = getBeforeDiscountEntry(mat.remove_awq_obj.reference_revision, mat.payer_obj)
                            cancel_disc = getDiscountEntry(mat.remove_awq_obj.reference_revision, mat.payer_obj)
                            cancel_afterDisc = getAfterDiscountEntry(mat.remove_awq_obj.reference_revision, mat.payer_obj)

                            If mat.remove_awq_obj.is_approved_closed OrElse mat.remove_awq_obj.is_open_and_sent_to_customer Then
                                cancelAddToToTal = True
                            End If
                        End If
                    End If

                End If

                'ignore non added value
                If isPriceCalculated Then
                    'handle material info
                    addMaterialInfo(mat.calc_material_info, beforeDisc)
                    If addToToTal And Not cancelAddToToTal Then
                        addMaterialInfo(mat.calc_material_info, total_befor_disc)
                    End If
                    If cancelAWR Then
                        addMaterialInfo(MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_REMOVED_AWQ_PREF).value & mat.calc_material_info, cancel_beforeDisc)
                    End If

                    If mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_MAT_KEY).value AndAlso Not (isAdditionalScope AndAlso isMatRedMark) Then

                        'handle oem text
                        If IsNothing(mat.awq_obj) Then
                            addWarrPayer(ADD, matPayers, mat.payer_obj)
                        Else
                            addWarrPayer(mat.awq_obj.reference_revision, matPayers, mat.payer_obj)
                            If cancelAWR Then
                                addWarrPayer(mat.remove_awq_obj.reference_revision, matPayers, mat.payer_obj)
                            End If
                        End If

                        'befor discount current scope and total scope
                        beforeDisc.mat_sold_calc = beforeDisc.mat_sold_calc + mat.calc_total_material_in_mat_curr_bef_disc
                        'use for awq form details
                        If _fillMatLabList AndAlso (mat.calc_total_material_in_mat_curr_bef_disc > 0 OrElse Not String.IsNullOrWhiteSpace(mat.text5)) Then
                            beforeDisc.materialList.Add(mat)
                            If cancelAddToToTal Then
                                cancel_beforeDisc.materialList.Add(mat)
                                cancel_beforeDisc.labMatDeletedList.Add(mat)
                            End If
                        End If

                        If cancelAWR Then
                            cancel_beforeDisc.mat_sold_calc = cancel_beforeDisc.mat_sold_calc - mat.calc_total_material_in_mat_curr_bef_disc
                        End If
                        If addToToTal And Not cancelAddToToTal Then
                            total_befor_disc.mat_sold_calc = total_befor_disc.mat_sold_calc + mat.calc_total_material_in_mat_curr_bef_disc
                        End If

                        'discount
                        disc.mat_sold_calc = disc.mat_sold_calc + mat.calc_discount_total_material_in_mat_curr
                        If cancelAWR Then
                            cancel_disc.mat_sold_calc = cancel_disc.mat_sold_calc - mat.calc_discount_total_material_in_mat_curr
                        End If

                        If addToToTal And Not cancelAddToToTal Then
                            'exclusive discount
                            total_disc.mat_sold_calc = total_disc.mat_sold_calc + mat.calc_discount_total_material_in_mat_curr
                            If IsNothing(mat.discount_obj) OrElse mat.discount_obj.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value Then
                                total_befor_disc.mat_sold_no_disc_calc = total_befor_disc.mat_sold_no_disc_calc + mat.calc_total_material_in_mat_curr_bef_disc
                            End If
                        End If

                        'after discount
                        afterDisc.mat_sold_calc = afterDisc.mat_sold_calc + mat.calc_total_material_in_mat_curr
                        If cancelAddToToTal Then
                            cancel_afterDisc.mat_sold_calc = cancel_afterDisc.mat_sold_calc - mat.calc_total_material_in_mat_curr
                        End If

                        If addToToTal And Not cancelAddToToTal Then
                            total_after_disc.mat_sold_calc = total_after_disc.mat_sold_calc + mat.calc_total_material_in_mat_curr
                        End If

                    ElseIf mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_REP_KEY).value AndAlso Not (isAdditionalScope AndAlso isRepRedMark) Then

                        'handle oem text
                        If IsNothing(mat.awq_obj) Then
                            addWarrPayer(ADD, repPayers, mat.payer_obj)
                        Else
                            addWarrPayer(mat.awq_obj.reference_revision, repPayers, mat.payer_obj)
                            If cancelAWR Then
                                addWarrPayer(mat.remove_awq_obj.reference_revision, repPayers, mat.payer_obj)
                            End If
                        End If

                        'befor discount current scope and total scope
                        beforeDisc.rep_sold_calc = beforeDisc.rep_sold_calc + mat.calc_total_material_in_mat_curr_bef_disc
                        If cancelAWR Then
                            cancel_beforeDisc.rep_sold_calc = cancel_beforeDisc.rep_sold_calc - mat.calc_total_material_in_mat_curr_bef_disc
                        End If
                        If addToToTal AndAlso Not cancelAddToToTal Then
                            total_befor_disc.rep_sold_calc = total_befor_disc.rep_sold_calc + mat.calc_total_material_in_mat_curr_bef_disc
                        End If

                        'use for awq form details
                        If _fillMatLabList AndAlso (mat.calc_total_material_in_mat_curr_bef_disc > 0 OrElse Not String.IsNullOrWhiteSpace(mat.text5)) Then
                            beforeDisc.repairList.Add(mat)
                            If cancelAddToToTal Then
                                cancel_beforeDisc.repairList.Add(mat)
                                cancel_beforeDisc.labMatDeletedList.Add(mat)
                            End If
                        End If

                        'discount
                        disc.rep_sold_calc = disc.rep_sold_calc + mat.calc_discount_total_material_in_mat_curr
                        If cancelAWR Then
                            cancel_disc.rep_sold_calc = cancel_disc.rep_sold_calc - mat.calc_discount_total_material_in_mat_curr
                        End If
                        If addToToTal AndAlso Not cancelAddToToTal Then
                            total_disc.rep_sold_calc = total_disc.rep_sold_calc + mat.calc_discount_total_material_in_mat_curr
                            'exclusive discount
                            If mat.calc_discount_total_material_in_mat_curr = 0 Then
                                total_befor_disc.rep_sold_no_disc_calc = total_befor_disc.rep_sold_no_disc_calc + mat.calc_total_material_in_mat_curr_bef_disc
                            End If
                        End If

                        'after discount
                        afterDisc.rep_sold_calc = afterDisc.rep_sold_calc + mat.calc_total_material_in_mat_curr
                        If cancelAWR Then
                            cancel_afterDisc.rep_sold_calc = cancel_afterDisc.rep_sold_calc - mat.calc_total_material_in_mat_curr
                        End If
                        If addToToTal AndAlso Not cancelAddToToTal Then
                            total_after_disc.rep_sold_calc = total_after_disc.rep_sold_calc + mat.calc_total_material_in_mat_curr
                        End If

                    ElseIf mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_3RD_P_KEY).value AndAlso Not (isAdditionalScope AndAlso isServRedMark) Then

                        'handle oem text
                        If IsNothing(mat.awq_obj) Then
                            addWarrPayer(ADD, servPayers, mat.payer_obj)
                        Else
                            addWarrPayer(mat.awq_obj.reference_revision, servPayers, mat.payer_obj)
                            If cancelAWR Then
                                addWarrPayer(mat.remove_awq_obj.reference_revision, servPayers, mat.payer_obj)
                            End If
                        End If

                        'befor discount current scope and total scope
                        beforeDisc.serv_sold_calc = beforeDisc.serv_sold_calc + mat.calc_total_material_in_mat_curr_bef_disc
                        If cancelAWR Then
                            cancel_beforeDisc.serv_sold_calc = cancel_beforeDisc.serv_sold_calc - mat.calc_total_material_in_mat_curr_bef_disc
                        End If
                        If addToToTal And Not cancelAddToToTal Then
                            total_befor_disc.serv_sold_calc = total_befor_disc.serv_sold_calc + mat.calc_total_material_in_mat_curr_bef_disc
                        End If

                        'use for awq form details
                        If _fillMatLabList AndAlso (mat.calc_total_material_in_mat_curr_bef_disc > 0 OrElse Not String.IsNullOrWhiteSpace(mat.text5)) AndAlso Not beforeDisc.servList.Contains(mat) Then
                            beforeDisc.servList.Add(mat)
                            If cancelAddToToTal Then
                                cancel_beforeDisc.servList.Add(mat)
                                cancel_beforeDisc.labMatDeletedList.Add(mat)
                            End If
                        End If

                        'discount
                        disc.serv_sold_calc = disc.serv_sold_calc + mat.calc_discount_total_material_in_mat_curr
                        If cancelAWR Then
                            cancel_disc.serv_sold_calc = cancel_disc.serv_sold_calc - mat.calc_discount_total_material_in_mat_curr
                        End If
                        If addToToTal AndAlso Not cancelAddToToTal Then
                            total_disc.serv_sold_calc = total_disc.serv_sold_calc + mat.calc_discount_total_material_in_mat_curr
                            'exclusive discount
                            If mat.calc_discount_total_material_in_mat_curr = 0 Then
                                total_befor_disc.serv_sold_no_disc_calc = total_befor_disc.serv_sold_no_disc_calc + mat.calc_total_material_in_mat_curr_bef_disc
                            End If
                        End If

                        'after discount
                        afterDisc.serv_sold_calc = afterDisc.serv_sold_calc + mat.calc_total_material_in_mat_curr
                        If cancelAWR Then
                            cancel_afterDisc.serv_sold_calc = cancel_afterDisc.serv_sold_calc - mat.calc_total_material_in_mat_curr
                        End If
                        If addToToTal AndAlso Not cancelAddToToTal Then
                            total_after_disc.serv_sold_calc = total_after_disc.serv_sold_calc + mat.calc_total_material_in_mat_curr
                        End If

                    ElseIf mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_FREIGHT_KEY).value AndAlso Not (isAdditionalScope AndAlso isFreightRedMark) Then

                        'handle oem text
                        If IsNothing(mat.awq_obj) Then
                            addWarrPayer(ADD, freightPayers, mat.payer_obj)
                        Else
                            addWarrPayer(mat.awq_obj.reference_revision, freightPayers, mat.payer_obj)
                            If cancelAWR Then
                                addWarrPayer(mat.remove_awq_obj.reference_revision, freightPayers, mat.payer_obj)
                            End If
                        End If

                        'befor discount current scope and total scope
                        beforeDisc.freight_sold_calc = beforeDisc.freight_sold_calc + mat.calc_total_material_in_mat_curr_bef_disc
                        If cancelAWR Then
                            cancel_beforeDisc.freight_sold_calc = cancel_beforeDisc.freight_sold_calc - mat.calc_total_material_in_mat_curr_bef_disc
                        End If
                        If addToToTal AndAlso Not cancelAddToToTal Then
                            total_befor_disc.freight_sold_calc = total_befor_disc.freight_sold_calc + mat.calc_total_material_in_mat_curr_bef_disc
                        End If

                        'use for awq form details
                        If _fillMatLabList AndAlso (mat.calc_total_material_in_mat_curr_bef_disc > 0 OrElse Not String.IsNullOrWhiteSpace(mat.text5)) AndAlso Not beforeDisc.freightList.Contains(mat) Then
                            beforeDisc.freightList.Add(mat)
                            If cancelAddToToTal Then
                                cancel_beforeDisc.freightList.Add(mat)
                                cancel_beforeDisc.labMatDeletedList.Add(mat)
                            End If
                        End If

                        'discount
                        disc.freight_sold_calc = disc.freight_sold_calc + mat.calc_discount_total_material_in_mat_curr
                        If cancelAWR Then
                            cancel_disc.freight_sold_calc = cancel_disc.freight_sold_calc - mat.calc_discount_total_material_in_mat_curr
                        End If
                        If addToToTal And Not cancelAddToToTal Then
                            total_disc.freight_sold_calc = total_disc.freight_sold_calc + mat.calc_discount_total_material_in_mat_curr
                            'exclusive discount
                            If mat.calc_discount_total_material_in_mat_curr = 0 Then
                                total_befor_disc.freight_sold_no_disc_calc = total_befor_disc.freight_sold_no_disc_calc + mat.calc_total_material_in_mat_curr_bef_disc
                            End If
                        End If

                        'after discount
                        afterDisc.freight_sold_calc = afterDisc.freight_sold_calc + mat.calc_total_material_in_mat_curr
                        If cancelAWR Then
                            cancel_afterDisc.freight_sold_calc = cancel_afterDisc.freight_sold_calc - mat.calc_total_material_in_mat_curr
                        End If
                        If addToToTal And Not cancelAddToToTal Then
                            total_after_disc.freight_sold_calc = total_after_disc.freight_sold_calc + mat.calc_total_material_in_mat_curr
                        End If

                    End If


                    If Not (isAdditionalScope AndAlso isFreightRedMark) Then
                        'freight always applicable
                        'befor discount current scope and total scope
                        If mat.calc_total_freight_befor_disc_in_freight_curr > 0 Then
                            'handle oem text
                            If IsNothing(mat.awq_obj) Then
                                addWarrPayer(ADD, freightPayers, mat.payer_obj)
                            Else
                                addWarrPayer(mat.awq_obj.reference_revision, freightPayers, mat.payer_obj)
                                If cancelAWR Then
                                    addWarrPayer(mat.remove_awq_obj.reference_revision, freightPayers, mat.payer_obj)
                                End If
                            End If
                        End If

                        beforeDisc.freight_sold_calc = beforeDisc.freight_sold_calc + mat.calc_total_freight_befor_disc_in_freight_curr
                        If cancelAWR Then
                            cancel_beforeDisc.freight_sold_calc = cancel_beforeDisc.freight_sold_calc - mat.calc_total_freight_befor_disc_in_freight_curr
                        End If
                        If addToToTal And Not cancelAddToToTal Then
                            total_befor_disc.freight_sold_calc = total_befor_disc.freight_sold_calc + mat.calc_total_freight_befor_disc_in_freight_curr
                        End If

                        'use for awq form details
                        If _fillMatLabList And mat.calc_total_freight_befor_disc_in_freight_curr > 0 AndAlso Not beforeDisc.freightList.Contains(mat) Then
                            beforeDisc.freightList.Add(mat)
                            If cancelAddToToTal Then
                                cancel_beforeDisc.freightList.Add(mat)
                                cancel_beforeDisc.labMatDeletedList.Add(mat)
                            End If
                        End If

                        'discount
                        disc.freight_sold_calc = disc.freight_sold_calc + mat.calc_total_freight_disc_in_freight_curr
                        If cancelAWR Then
                            cancel_disc.freight_sold_calc = cancel_disc.freight_sold_calc - mat.calc_total_freight_disc_in_freight_curr
                        End If
                        If addToToTal And Not cancelAddToToTal Then
                            total_disc.freight_sold_calc = total_disc.freight_sold_calc + mat.calc_total_freight_disc_in_freight_curr
                            'exclusive discount
                            If mat.calc_total_freight_disc_in_freight_curr = 0 Then
                                total_befor_disc.freight_sold_no_disc_calc = total_befor_disc.freight_sold_no_disc_calc + mat.calc_total_freight_befor_disc_in_freight_curr
                            End If
                        End If

                        'after discount
                        afterDisc.freight_sold_calc = afterDisc.freight_sold_calc + mat.calc_total_freight_in_freight_curr
                        If cancelAWR Then
                            cancel_afterDisc.freight_sold_calc = cancel_afterDisc.freight_sold_calc - mat.calc_total_freight_in_freight_curr
                        End If
                        If addToToTal And Not cancelAddToToTal Then
                            total_after_disc.freight_sold_calc = total_after_disc.freight_sold_calc + mat.calc_total_freight_in_freight_curr
                        End If
                    End If


                    If Not (isAdditionalScope AndAlso isServRedMark) Then
                        'zcuss always applicable

                        If mat.calc_handling_zcus_value_bef_disc_in_mat_curr Then
                            'handle oem text
                            If IsNothing(mat.awq_obj) Then
                                addWarrPayer(ADD, servPayers, mat.payer_obj)
                            Else
                                addWarrPayer(mat.awq_obj.reference_revision, servPayers, mat.payer_obj)
                                If cancelAWR Then
                                    addWarrPayer(mat.remove_awq_obj.reference_revision, servPayers, mat.payer_obj)
                                End If
                            End If
                        End If

                        'befor discount current scope and total scope
                        beforeDisc.serv_sold_calc = beforeDisc.serv_sold_calc + mat.calc_handling_zcus_value_bef_disc_in_mat_curr
                        If cancelAWR Then
                            cancel_beforeDisc.serv_sold_calc = cancel_beforeDisc.serv_sold_calc - mat.calc_handling_zcus_value_bef_disc_in_mat_curr
                        End If
                        If addToToTal And Not cancelAddToToTal Then
                            total_befor_disc.serv_sold_calc = total_befor_disc.serv_sold_calc + mat.calc_handling_zcus_value_bef_disc_in_mat_curr
                        End If

                        'use for awq form details
                        If _fillMatLabList And mat.calc_handling_zcus_value_bef_disc_in_mat_curr > 0 AndAlso Not beforeDisc.servList.Contains(mat) Then
                            beforeDisc.servList.Add(mat)
                            If cancelAddToToTal Then
                                cancel_beforeDisc.servList.Add(mat)
                                cancel_beforeDisc.labMatDeletedList.Add(mat)
                            End If
                        End If

                        'discount
                        disc.serv_sold_calc = disc.serv_sold_calc + mat.calc_handling_zcus_disc_in_mat_curr
                        If cancelAWR Then
                            cancel_disc.serv_sold_calc = cancel_disc.serv_sold_calc - mat.calc_handling_zcus_disc_in_mat_curr
                        End If
                        If addToToTal And Not cancelAddToToTal Then
                            total_disc.serv_sold_calc = total_disc.serv_sold_calc + mat.calc_handling_zcus_disc_in_mat_curr
                            'exclusive discount
                            If mat.calc_handling_zcus_disc_in_mat_curr = 0 Then
                                total_befor_disc.serv_sold_no_disc_calc = total_befor_disc.serv_sold_no_disc_calc + mat.calc_handling_zcus_disc_in_mat_curr
                            End If
                        End If

                        'after discount
                        afterDisc.serv_sold_calc = afterDisc.serv_sold_calc + mat.calc_handling_zcus_value_in_mat_curr
                        If cancelAWR Then
                            cancel_afterDisc.serv_sold_calc = cancel_afterDisc.serv_sold_calc - mat.calc_handling_zcus_value_in_mat_curr
                        End If
                        If addToToTal And Not cancelAddToToTal Then
                            total_after_disc.serv_sold_calc = total_after_disc.serv_sold_calc + mat.calc_handling_zcus_value_in_mat_curr
                        End If
                    End If
                End If
            Next

            'handle activities linked to ab AWQ as secondary object with no mat or labor directly linked to awq
            'if all awq are cancelled, no need to report secondary act as a refer to quote
            Dim isActLinkedToAWQAndReportable = False
            Dim awq_payer_list As New List(Of CPayer)

            'no labor and mat found, but act is linked to awq.=> dependant activity
            If Not isActLabOrMatAWQ AndAlso isActLinkedToAWQ Then
                Dim myAWQ As CAWQuotation = Nothing
                For Each awq_rev As CAWQuotation In act.awq_master_obj.revision_list
                    If Not awq_rev.is_cancelled_or_superseded AndAlso awq_rev.has_been_sent Then
                        If IsNothing(myAWQ) Then
                            myAWQ = awq_rev
                            isActLinkedToAWQAndReportable = True
                        Else
                            If awq_rev.revision > myAWQ.revision Then
                                myAWQ = awq_rev
                                isActLinkedToAWQAndReportable = True
                            End If
                        End If
                    End If
                Next
                'if act is dependant activity with no lab and mat linked and has awq has been sent, report as additional 
                If isActLinkedToAWQAndReportable Then
                    'check payers
                    For Each awq_act As CScopeActivity In act.awq_master_obj.dependantActList
                        For Each soldH As CSoldHours In act.sold_hour_list
                            If Not IsNothing(soldH.awq_obj) AndAlso soldH.awq_obj.has_been_sent AndAlso Not soldH.awq_obj.is_cancelled_or_superseded Then
                                If Not awq_payer_list.Contains(soldH.payer_obj) Then
                                    awq_payer_list.Add(soldH.payer_obj)
                                End If
                            End If
                        Next
                        For Each mat As CMaterial In act.material_list
                            If Not IsNothing(mat.awq_obj) AndAlso mat.awq_obj.has_been_sent AndAlso Not mat.awq_obj.is_cancelled_or_superseded Then
                                If Not awq_payer_list.Contains(mat.payer_obj) Then
                                    awq_payer_list.Add(mat.payer_obj)
                                End If
                            End If
                        Next
                    Next
                    For Each awq_payer As CPayer In awq_payer_list
                        addCalcGroup(ADD_AWQ, myAWQ.reference_revision, customer, myAWQ.reference_revision)
                        Dim beforeDisc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(myAWQ.reference_revision, customer)
                        'refer to quote
                        beforeDisc.labor_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                        beforeDisc.mat_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                        beforeDisc.freight_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                        beforeDisc.rep_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value
                        beforeDisc.serv_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_REFER_TO_QUOTE_TXT).value

                        'warr text
                        addWarrPayer(myAWQ.reference_revision, laborPayers, awq_payer)
                        addWarrPayer(myAWQ.reference_revision, matPayers, awq_payer)
                        addWarrPayer(myAWQ.reference_revision, repPayers, awq_payer)
                        addWarrPayer(myAWQ.reference_revision, freightPayers, awq_payer)
                        addWarrPayer(myAWQ.reference_revision, servPayers, awq_payer)
                    Next

                End If
            End If

            'handle red marks and non added value
            'if there is awq or init scope, no need to display additional
            'AndAlso Not isActInit AndAlso Not isActLabOrMatAWQ AndAlso Not isActLinkedToAWQAndReportable
            If isActNonAddedValue Then
                'add empty
                addCalcGroup(ADD, ADD, customer, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD).value)
                Dim beforeDisc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(ADD, customer)

                If isCancelled Then
                End If

                beforeDisc.labor_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_VAL_ANALYSIS_CNCL_CFWD_TXT).value
                beforeDisc.mat_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_VAL_ANALYSIS_CNCL_CFWD_TXT).value
                beforeDisc.rep_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_VAL_ANALYSIS_CNCL_CFWD_TXT).value
                beforeDisc.freight_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_VAL_ANALYSIS_CNCL_CFWD_TXT).value
                beforeDisc.serv_sold_calc_str = MConstants.constantConf(MConstants.CONST_RED_MARK_VAL_ANALYSIS_CNCL_CFWD_TXT).value
            Else
                'labor
                If isLaborRedMark Then
                    addCalcGroup(ADD, ADD, customer, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD).value)
                    Dim beforeDisc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(ADD, customer)
                    beforeDisc.labor_sold_calc_str = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_PRICE_RED_MARK)(act.labor_sold_mark).value
                ElseIf Not isAnySoldHours AndAlso Not isAnyMat AndAlso Not isActInit AndAlso Not isActLabOrMatAWQ AndAlso Not isActLinkedToAWQAndReportable Then
                    'TBA if no hours
                    addCalcGroup(ADD, ADD, customer, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD).value)
                    Dim beforeDisc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(ADD, customer)
                    beforeDisc.labor_sold_calc_str = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_PRICE_RED_MARK)(MConstants.constantConf(MConstants.CONST_CONF_LOV_RED_MARK_TBA_KEY).value).value
                End If
                'mat
                If isMatRedMark Then
                    addCalcGroup(ADD, ADD, customer, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD).value)
                    Dim beforeDisc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(ADD, customer)
                    beforeDisc.mat_sold_calc_str = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_PRICE_RED_MARK)(act.mat_sold_mark).value
                End If
                If isRepRedMark Then
                    addCalcGroup(ADD, ADD, customer, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD).value)
                    Dim beforeDisc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(ADD, customer)
                    beforeDisc.rep_sold_calc_str = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_PRICE_RED_MARK)(act.rep_sold_mark).value
                End If
                If isMatRedMark Then
                    addCalcGroup(ADD, ADD, customer, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD).value)
                    Dim beforeDisc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(ADD, customer)
                    beforeDisc.serv_sold_calc_str = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_PRICE_RED_MARK)(act.serv_sold_mark).value
                End If
                If isMatRedMark Then
                    addCalcGroup(ADD, ADD, customer, MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_SCP_ADD).value)
                    Dim beforeDisc As CScopeAtivityCalcEntry = getBeforeDiscountEntry(ADD, customer)
                    beforeDisc.freight_sold_calc_str = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_PRICE_RED_MARK)(act.freight_sold_mark).value
                End If
            End If


            'handle OEM warranty Text
            'labor
            For Each key As String In laborPayers.Keys
                Dim entry As CScopeAtivityCalcEntry
                Dim entry2 As CScopeAtivityCalcEntry
                If Not laborPayers(key).Contains(customer) And laborPayers(key).Count > 0 Then
                    entry2 = getBeforeDiscountEntry(key, laborPayers(key)(0))
                    addCalcGroup(entry2.init_add, key, customer, entry2.entry_scope_label)
                    entry = getBeforeDiscountEntry(key, customer)
                    For Each payer As CPayer In laborPayers(key)
                        If String.IsNullOrWhiteSpace(entry.labor_sold_calc_str) Then
                            entry.labor_sold_calc_str = payer.warr_text
                        Else
                            entry.labor_sold_calc_str = entry.labor_sold_calc_str & Chr(10) & payer.warr_text
                        End If
                    Next
                End If
            Next
            'mat
            For Each key As String In matPayers.Keys
                Dim entry As CScopeAtivityCalcEntry
                Dim entry2 As CScopeAtivityCalcEntry
                If Not matPayers(key).Contains(customer) And matPayers(key).Count > 0 Then
                    entry2 = getBeforeDiscountEntry(key, matPayers(key)(0))
                    addCalcGroup(entry2.init_add, key, customer, entry2.entry_scope_label)
                    entry = getBeforeDiscountEntry(key, customer)
                    For Each payer As CPayer In matPayers(key)
                        If String.IsNullOrWhiteSpace(entry.mat_sold_calc_str) Then
                            entry.mat_sold_calc_str = payer.warr_text
                        Else
                            entry.mat_sold_calc_str = entry.mat_sold_calc_str & Chr(10) & payer.warr_text
                        End If
                    Next
                End If
            Next
            'rep
            For Each key As String In repPayers.Keys
                Dim entry As CScopeAtivityCalcEntry
                Dim entry2 As CScopeAtivityCalcEntry
                If Not repPayers(key).Contains(customer) And repPayers(key).Count > 0 Then
                    entry2 = getBeforeDiscountEntry(key, repPayers(key)(0))
                    addCalcGroup(entry2.init_add, key, customer, entry2.entry_scope_label)
                    entry = getBeforeDiscountEntry(key, customer)
                    For Each payer As CPayer In repPayers(key)
                        If String.IsNullOrWhiteSpace(entry.rep_sold_calc_str) Then
                            entry.rep_sold_calc_str = payer.warr_text
                        Else
                            entry.rep_sold_calc_str = entry.rep_sold_calc_str & Chr(10) & payer.warr_text
                        End If
                    Next
                End If
            Next
            'freight
            For Each key As String In freightPayers.Keys
                Dim entry As CScopeAtivityCalcEntry
                Dim entry2 As CScopeAtivityCalcEntry
                If Not freightPayers(key).Contains(customer) And freightPayers(key).Count > 0 Then
                    entry2 = getBeforeDiscountEntry(key, freightPayers(key)(0))
                    addCalcGroup(entry2.init_add, key, customer, entry2.entry_scope_label)
                    entry = getBeforeDiscountEntry(key, customer)
                    For Each payer As CPayer In freightPayers(key)
                        If String.IsNullOrWhiteSpace(entry.freight_sold_calc_str) Then
                            entry.freight_sold_calc_str = payer.warr_text
                        Else
                            entry.freight_sold_calc_str = entry.freight_sold_calc_str & Chr(10) & payer.warr_text
                        End If
                    Next
                End If
            Next
            'serv
            For Each key As String In servPayers.Keys
                Dim entry As CScopeAtivityCalcEntry
                Dim entry2 As CScopeAtivityCalcEntry
                If Not servPayers(key).Contains(customer) And servPayers(key).Count > 0 Then
                    entry2 = getBeforeDiscountEntry(key, servPayers(key)(0))
                    addCalcGroup(entry2.init_add, key, customer, entry2.entry_scope_label)
                    entry = getBeforeDiscountEntry(key, customer)
                    For Each payer As CPayer In servPayers(key)
                        If String.IsNullOrWhiteSpace(entry.serv_sold_calc_str) Then
                            entry.serv_sold_calc_str = payer.warr_text
                        Else
                            entry.serv_sold_calc_str = entry.serv_sold_calc_str & Chr(10) & payer.warr_text
                        End If
                    Next
                End If
            Next

            'small mat mat info, add it on all scopes except ADJ
            For Each payer As String In _calc_tree_structure.Keys

                Dim listBef As List(Of CScopeAtivityCalcEntry) = getEntry(payer, _calc_tree_structure(payer).Keys.ToList, BEFORE_DISC_SUFF)

                For Each entry As CScopeAtivityCalcEntry In listBef
                    If entry.init_add = CScopeActivityCalculator.INIT_ADJ Then
                        Continue For
                    End If
                    If Not act.is_small_mat Then
                        If String.IsNullOrWhiteSpace(entry.mat_info_calc) Then
                            entry.mat_info_calc = MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_LINE_START).value & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_SMALL_MAT_TXT).value
                        Else
                            entry.mat_info_calc = entry.mat_info_calc & Chr(10) & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_LINE_START).value & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_SMALL_MAT_TXT).value
                        End If
                    End If
                Next

            Next

            If Not act.is_small_mat Then
                _empty_entry.mat_info_calc = MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_LINE_START).value & " " & MConstants.constantConf(MConstants.CONST_CONF_MAT_INFO_SMALL_MAT_TXT).value
            End If

            'calculate after discount
            UpdateAfterDiscount()

            'refresh string values
            postProcessing()

            'update
            _calc_entries_view.Refresh()
        Catch ex As Exception
            Throw New Exception("Updating activity calculations" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try

    End Sub

    Public Sub addMaterialInfo(matInfo As String, entry As CScopeAtivityCalcEntry)
        'handle material info
        If String.IsNullOrWhiteSpace(entry.mat_info_calc) Then
            entry.mat_info_calc = matInfo
        Else
            entry.mat_info_calc = entry.mat_info_calc & Chr(10) & matInfo
        End If
    End Sub

    'list of payers by scope to handle warranty text
    Public Sub addWarrPayer(scope As String, dict As Dictionary(Of String, List(Of CPayer)), payer As CPayer)
        If Not dict.ContainsKey(scope) Then
            dict.Add(scope, New List(Of CPayer))
        End If
        If Not dict(scope).Contains(payer) Then
            dict(scope).Add(payer)
        End If
    End Sub


    'fill string values
    Public Sub postProcessing()
        For Each payer As String In _calc_tree_structure.Keys
            Dim listBef As List(Of CScopeAtivityCalcEntry)
            listBef = getEntry(payer, _calc_tree_structure(payer).Keys.ToList, BEFORE_DISC_SUFF)
            For Each entry As CScopeAtivityCalcEntry In listBef
                'hours
                If String.IsNullOrWhiteSpace(entry.hours_sold_calc_str) Then
                    If entry.hours_sold_calc <> 0 Then
                        entry.hours_sold_calc_str = entry.hours_sold_calc.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
                    End If
                End If

                'labor
                If String.IsNullOrWhiteSpace(entry.labor_sold_calc_str) Then
                    If entry.labor_sold_calc <> 0 Then
                        entry.labor_sold_calc_str = entry.labor_sold_calc.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
                    End If
                End If

                'mat
                If String.IsNullOrWhiteSpace(entry.mat_sold_calc_str) Then
                    If entry.mat_sold_calc <> 0 Then
                        entry.mat_sold_calc_str = entry.mat_sold_calc.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
                    End If
                End If

                'rep
                If String.IsNullOrWhiteSpace(entry.rep_sold_calc_str) Then
                    If entry.rep_sold_calc <> 0 Then
                        entry.rep_sold_calc_str = entry.rep_sold_calc.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
                    End If
                End If

                'freight
                If String.IsNullOrWhiteSpace(entry.freight_sold_calc_str) Then
                    If entry.freight_sold_calc <> 0 Then
                        entry.freight_sold_calc_str = entry.freight_sold_calc.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
                    End If
                End If

                'serv
                If String.IsNullOrWhiteSpace(entry.serv_sold_calc_str) Then
                    If entry.serv_sold_calc <> 0 Then
                        entry.serv_sold_calc_str = entry.serv_sold_calc.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
                    End If
                End If
            Next
        Next
    End Sub


    'add calc groups
    Public Sub addCalcGroup(init_add As String, entry_scope As String, payer As CPayer, entry_scope_label As String)
        Dim calc_entry As CScopeAtivityCalcEntry
        Dim calc_entry_payer_list As Dictionary(Of String, List(Of CScopeAtivityCalcEntry))
        Dim calc_entry_scoper_list As List(Of CScopeAtivityCalcEntry)

        'add payer
        If Not _calc_tree_structure.ContainsKey(payer.payer) Then
            calc_entry_payer_list = New Dictionary(Of String, List(Of CScopeAtivityCalcEntry))
            _calc_tree_structure.Add(payer.payer, calc_entry_payer_list)
        Else
            calc_entry_payer_list = _calc_tree_structure(payer.payer)
        End If
        'add scope
        If Not calc_entry_payer_list.ContainsKey(entry_scope) Then
            calc_entry_scoper_list = New List(Of CScopeAtivityCalcEntry)
            calc_entry_payer_list.Add(entry_scope, calc_entry_scoper_list)

            'before discount
            calc_entry = New CScopeAtivityCalcEntry(entry_scope & BEFORE_DISC_SUFF)
            calc_entry.calc_type = BEFORE_DISC_SUFF
            calc_entry.init_add = init_add
            calc_entry.entry_scope = entry_scope
            calc_entry.payer = payer
            calc_entry.entry_scope_label = entry_scope_label
            calc_entry.label = MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_BEF_DISC).value
            calc_entry.act_obj = act
            'if list of mat lab has to be filled? use for AWQ Form
            If _fillMatLabList Then
                calc_entry.laborList = New List(Of CSoldHours)
                calc_entry.materialList = New List(Of CMaterial)
                calc_entry.repairList = New List(Of CMaterial)
                calc_entry.freightList = New List(Of CMaterial)
                calc_entry.servList = New List(Of CMaterial)
                calc_entry.labMatDeletedList = New List(Of Object)
            End If
            'structure view to ease code research
            calc_entry_scoper_list.Add(calc_entry)
            'flat list for view
            _calc_entries_list.Add(calc_entry)

            'discount
            calc_entry = New CScopeAtivityCalcEntry(entry_scope & DISC_SUFF)
            calc_entry.calc_type = DISC_SUFF
            calc_entry.init_add = init_add
            calc_entry.entry_scope = entry_scope
            calc_entry.payer = payer
            calc_entry.entry_scope_label = entry_scope_label
            calc_entry.act_obj = act
            calc_entry.label = MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_DISC).value
            'structure view to ease code research
            calc_entry_scoper_list.Add(calc_entry)
            'flat list for view
            _calc_entries_list.Add(calc_entry)

            If entry_scope = TOTAL Then
                'add activity discount
                'activity discount
                calc_entry = New CScopeAtivityCalcEntry(entry_scope & ACT_DISC_SUFF)
                calc_entry.calc_type = ACT_DISC_SUFF
                calc_entry.init_add = init_add
                calc_entry.entry_scope = entry_scope
                calc_entry.payer = payer
                calc_entry.entry_scope_label = entry_scope_label
                calc_entry.act_obj = act
                calc_entry.label = MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_DISC).value
                'structure view to ease code research
                calc_entry_scoper_list.Add(calc_entry)
                'flat list for view
                _calc_entries_list.Add(calc_entry)
            End If

            'after discount
            calc_entry = New CScopeAtivityCalcEntry(entry_scope & AFTER_DISC_SUFF)
            calc_entry.calc_type = AFTER_DISC_SUFF
            calc_entry.init_add = init_add
            calc_entry.entry_scope = entry_scope
            calc_entry.payer = payer
            calc_entry.entry_scope_label = entry_scope_label
            calc_entry.act_obj = act
            calc_entry.label = MConstants.constantConf(MConstants.CONST_CONF_ACT_CALC_SUM_AFT_DISC).value
            'structure view to ease code research
            calc_entry_scoper_list.Add(calc_entry)
            'flat list for view
            _calc_entries_list.Add(calc_entry)
        Else
            'already exists
        End If

        'update payer and scope lists
        If Not _calc_payer_list.Contains(payer) Then
            _calc_payer_list.Add(payer)
        End If

        If Not _calc_scope_list_dic.ContainsKey(entry_scope) Then
            _calc_scope_list_dic.Add(entry_scope, entry_scope_label)
            Dim obj As New CConfProperty
            obj.key = entry_scope
            obj.value = entry_scope_label
            _calc_scope_list.Add(obj)
        End If

    End Sub

    'update after discount
    Public Sub UpdateAfterDiscount()
        If IsNothing(act.discount_obj) OrElse act.discount_obj.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value Then
            Exit Sub
        End If

        For Each payer As String In _calc_tree_structure.Keys

            If Not act.discount_obj.payer_obj.payer = payer Then
                'ignore discount if not applicable to payer
                Continue For
            End If

            Dim payer_list As Dictionary(Of String, List(Of CScopeAtivityCalcEntry)) = _calc_tree_structure(payer)

            Dim scopeList As List(Of CScopeAtivityCalcEntry) = payer_list(TOTAL)

            Dim before_entry As CScopeAtivityCalcEntry = getBeforeDiscountEntry(scopeList)
            Dim disc_entry As CScopeAtivityCalcEntry = getDiscountEntry(scopeList)
            Dim after_entry As CScopeAtivityCalcEntry = getAfterDiscountEntry(scopeList)
            Dim act_discount_entry As CScopeAtivityCalcEntry = getActDiscountEntry(scopeList)

            Dim disc As Double

            'labor
            If MUtils.isLaborApplicableLabMat(act.discount_obj.lab_mat_applicability) Then
                disc = act.discount_obj.getDiscountValueExclusive(after_entry.labor_sold_calc, before_entry.labor_sold_no_disc_calc, MConstants.GLB_MSTR_CTRL.project_controller.project.lab_curr)
                act_discount_entry.labor_sold_calc = act_discount_entry.labor_sold_calc - disc
            End If

            If MUtils.isMaterialApplicableLabMat(act.discount_obj.lab_mat_applicability) Then
                disc = act.discount_obj.getDiscountValueExclusive(after_entry.mat_sold_calc, before_entry.mat_sold_no_disc_calc, MConstants.GLB_MSTR_CTRL.project_controller.project.mat_curr)
                act_discount_entry.mat_sold_calc = act_discount_entry.mat_sold_calc - disc
            End If

            If MUtils.isRepairApplicableLabMat(act.discount_obj.lab_mat_applicability) Then
                disc = act.discount_obj.getDiscountValueExclusive(after_entry.rep_sold_calc, before_entry.rep_sold_no_disc_calc, MConstants.GLB_MSTR_CTRL.project_controller.project.repair_curr)
                act_discount_entry.rep_sold_calc = act_discount_entry.rep_sold_calc - disc
            End If

            If MUtils.isServApplicableLabMat(act.discount_obj.lab_mat_applicability) Then
                disc = act.discount_obj.getDiscountValueExclusive(after_entry.serv_sold_calc, before_entry.serv_sold_no_disc_calc, MConstants.GLB_MSTR_CTRL.project_controller.project.third_party_curr)
                act_discount_entry.serv_sold_calc = act_discount_entry.serv_sold_calc - disc
            End If

            If MUtils.isFreightApplicableLabMat(act.discount_obj.lab_mat_applicability) Then
                disc = act.discount_obj.getDiscountValueExclusive(after_entry.freight_sold_calc, before_entry.freight_sold_no_disc_calc, MConstants.GLB_MSTR_CTRL.project_controller.project.freight_curr)
                act_discount_entry.freight_sold_calc = act_discount_entry.freight_sold_calc - disc
            End If

            'apply activity discount on after_discount entries (already include mmatetiald and labor discounts)
            after_entry.labor_sold_calc = after_entry.labor_sold_calc + act_discount_entry.labor_sold_calc
            after_entry.mat_sold_calc = after_entry.mat_sold_calc + act_discount_entry.mat_sold_calc
            after_entry.freight_sold_calc = after_entry.freight_sold_calc + act_discount_entry.freight_sold_calc
            after_entry.rep_sold_calc = after_entry.rep_sold_calc + act_discount_entry.rep_sold_calc
            after_entry.serv_sold_calc = after_entry.serv_sold_calc + act_discount_entry.serv_sold_calc
        Next


    End Sub

    'get calc entry
    Public Function getBeforeDiscountEntry(entry_scope As String, payer As CPayer) As CScopeAtivityCalcEntry
        Dim res As CScopeAtivityCalcEntry = Nothing
        If _calc_tree_structure.ContainsKey(payer.payer) Then
            If _calc_tree_structure(payer.payer).ContainsKey(entry_scope) Then
                For Each entry As CScopeAtivityCalcEntry In _calc_tree_structure(payer.payer)(entry_scope)
                    If entry.entry_id = entry_scope & BEFORE_DISC_SUFF Then
                        res = entry
                        Exit For
                    End If
                Next
            End If
        End If
        Return res
    End Function
    Public Function getBeforeDiscountEntry(scopeList As List(Of CScopeAtivityCalcEntry)) As CScopeAtivityCalcEntry
        Dim res As CScopeAtivityCalcEntry = Nothing
        For Each entry As CScopeAtivityCalcEntry In scopeList
            If entry.calc_type = BEFORE_DISC_SUFF Then
                res = entry
                Exit For
            End If
        Next
        Return res
    End Function
    Public Function getDiscountEntry(entry_scope As String, payer As CPayer) As CScopeAtivityCalcEntry

        Dim res As CScopeAtivityCalcEntry = Nothing
        If _calc_tree_structure.ContainsKey(payer.payer) Then
            If _calc_tree_structure(payer.payer).ContainsKey(entry_scope) Then
                For Each entry As CScopeAtivityCalcEntry In _calc_tree_structure(payer.payer)(entry_scope)
                    If entry.entry_id = entry_scope & DISC_SUFF Then
                        res = entry
                        Exit For
                    End If
                Next
            End If
        End If
        Return res
    End Function

    Public Function getDiscountEntry(scopeList As List(Of CScopeAtivityCalcEntry)) As CScopeAtivityCalcEntry
        Dim res As CScopeAtivityCalcEntry = Nothing
        For Each entry As CScopeAtivityCalcEntry In scopeList
            If entry.calc_type = DISC_SUFF Then
                res = entry
                Exit For
            End If
        Next
        Return res
    End Function
    Public Function getActDiscountEntry(entry_scope As String, payer As CPayer) As CScopeAtivityCalcEntry

        Dim res As CScopeAtivityCalcEntry = Nothing
        If _calc_tree_structure.ContainsKey(payer.payer) Then
            If _calc_tree_structure(payer.payer).ContainsKey(entry_scope) Then
                For Each entry As CScopeAtivityCalcEntry In _calc_tree_structure(payer.payer)(entry_scope)
                    If entry.entry_id = entry_scope & ACT_DISC_SUFF Then
                        res = entry
                        Exit For
                    End If
                Next
            End If
        End If
        Return res
    End Function

    Public Function getActDiscountEntry(scopeList As List(Of CScopeAtivityCalcEntry)) As CScopeAtivityCalcEntry
        Dim res As CScopeAtivityCalcEntry = Nothing
        For Each entry As CScopeAtivityCalcEntry In scopeList
            If entry.calc_type = ACT_DISC_SUFF Then
                res = entry
                Exit For
            End If
        Next
        Return res
    End Function
    Public Function getAfterDiscountEntry(entry_scope As String, payer As CPayer) As CScopeAtivityCalcEntry

        Dim res As CScopeAtivityCalcEntry = Nothing
        If _calc_tree_structure.ContainsKey(payer.payer) Then
            If _calc_tree_structure(payer.payer).ContainsKey(entry_scope) Then
                For Each entry As CScopeAtivityCalcEntry In _calc_tree_structure(payer.payer)(entry_scope)
                    If entry.entry_id = entry_scope & AFTER_DISC_SUFF Then
                        res = entry
                        Exit For
                    End If
                Next
            End If
        End If
        Return res
    End Function

    Public Function getAfterDiscountEntry(scopeList As List(Of CScopeAtivityCalcEntry)) As CScopeAtivityCalcEntry
        Dim res As CScopeAtivityCalcEntry = Nothing
        For Each entry As CScopeAtivityCalcEntry In scopeList
            If entry.calc_type = AFTER_DISC_SUFF Then
                res = entry
                Exit For
            End If
        Next
        Return res
    End Function


    Public Function getDefaultPayerPriceBeforeDiscount() As CScopeAtivityCalcEntry
        Dim res As CScopeAtivityCalcEntry
        Try
            If _calc_tree_structure.ContainsKey(MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value) Then
                Dim scopeList As List(Of CScopeAtivityCalcEntry) = _calc_tree_structure(MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value)(TOTAL)
                res = getBeforeDiscountEntry(scopeList)
            Else
                res = Nothing
            End If

        Catch ex As Exception
            Throw New Exception("Error while getting default payer activity calculation" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        Return res
    End Function

    Public Function getEntry(payer As String, scope As String, entry_type As String) As CScopeAtivityCalcEntry
        Dim res As CScopeAtivityCalcEntry = Nothing
        Try
            If _calc_tree_structure.ContainsKey(payer) Then
                If _calc_tree_structure(payer).ContainsKey(scope) Then
                    Dim scopeList As List(Of CScopeAtivityCalcEntry) = _calc_tree_structure(payer)(scope)
                    If entry_type = AFTER_DISC_SUFF Then
                        res = getAfterDiscountEntry(scopeList)
                    End If
                    If entry_type = ACT_DISC_SUFF Then
                        res = getActDiscountEntry(scopeList)
                    End If
                    If entry_type = DISC_SUFF Then
                        res = getDiscountEntry(scopeList)
                    End If
                    If entry_type = BEFORE_DISC_SUFF Then
                        res = getBeforeDiscountEntry(scopeList)
                    End If
                End If
            End If
            Return res
        Catch ex As Exception
            Throw New Exception("Error while getting default payer activity calculation" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        Return res
    End Function

    Public Function getEntry(payer As String, scopeList As List(Of String), entry_type As String) As List(Of CScopeAtivityCalcEntry)
        Dim res As List(Of CScopeAtivityCalcEntry) = New List(Of CScopeAtivityCalcEntry)
        Try
            If scopeList.Count > 0 Then
                If _calc_tree_structure.ContainsKey(payer) Then
                    For Each scope As String In _calc_tree_structure(payer).Keys
                        If scopeList.Contains(scope) Then
                            Dim entries As List(Of CScopeAtivityCalcEntry) = _calc_tree_structure(payer)(scope)
                            If entry_type = AFTER_DISC_SUFF Then
                                res.Add(getAfterDiscountEntry(entries))
                            End If
                            If entry_type = ACT_DISC_SUFF Then
                                res.Add(getActDiscountEntry(entries))
                            End If
                            If entry_type = DISC_SUFF Then
                                res.Add(getDiscountEntry(entries))
                            End If
                            If entry_type = BEFORE_DISC_SUFF Then
                                res.Add(getBeforeDiscountEntry(entries))
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Throw New Exception("Error while getting default payer activity calculation" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        Return res
    End Function

    'update the view as filter are set
    Public Sub filterView()
        _calc_entries_view.RemoveFilter()
        _calc_entries_view.ApplyFilter(AddressOf applyCurrentFilter)
    End Sub

    Public Function applyCurrentFilter(calEntry As CScopeAtivityCalcEntry) As Boolean
        Dim res As Boolean
        res = calEntry.payer.Equals(_view_current_payer) OrElse _view_current_payer.Equals(MConstants.EMPTY_PAYER)
        res = res AndAlso (calEntry.entry_scope = _view_current_scope.key Or _view_current_scope.Equals(MConstants.EMPTY_CONFPROP))
        res = res AndAlso (_view_current_entry_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Or calEntry.calc_type = _view_current_entry_type Or (_view_current_entry_type = DISC_SUFF And calEntry.calc_type = ACT_DISC_SUFF))
        Return res
    End Function
End Class