﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAdminAboutApp
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.top_panel = New System.Windows.Forms.Panel()
        Me.jet_logo = New System.Windows.Forms.PictureBox()
        Me.main_panel = New System.Windows.Forms.Panel()
        Me.admin_about_appli_dgrid = New System.Windows.Forms.DataGridView()
        Me.bottom_panel = New System.Windows.Forms.Panel()
        Me.reset_session_bt = New System.Windows.Forms.Button()
        Me.version_info_bt = New System.Windows.Forms.PictureBox()
        Me.top_panel.SuspendLayout()
        CType(Me.jet_logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.main_panel.SuspendLayout()
        CType(Me.admin_about_appli_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bottom_panel.SuspendLayout()
        CType(Me.version_info_bt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'top_panel
        '
        Me.top_panel.Controls.Add(Me.version_info_bt)
        Me.top_panel.Controls.Add(Me.jet_logo)
        Me.top_panel.Dock = System.Windows.Forms.DockStyle.Top
        Me.top_panel.Location = New System.Drawing.Point(0, 0)
        Me.top_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.top_panel.Name = "top_panel"
        Me.top_panel.Size = New System.Drawing.Size(1018, 154)
        Me.top_panel.TabIndex = 0
        '
        'jet_logo
        '
        Me.jet_logo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.jet_logo.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.jetLogo
        Me.jet_logo.InitialImage = Nothing
        Me.jet_logo.Location = New System.Drawing.Point(0, 0)
        Me.jet_logo.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.jet_logo.Name = "jet_logo"
        Me.jet_logo.Size = New System.Drawing.Size(1018, 154)
        Me.jet_logo.TabIndex = 0
        Me.jet_logo.TabStop = False
        '
        'main_panel
        '
        Me.main_panel.Controls.Add(Me.admin_about_appli_dgrid)
        Me.main_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.main_panel.Location = New System.Drawing.Point(0, 154)
        Me.main_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.main_panel.Name = "main_panel"
        Me.main_panel.Size = New System.Drawing.Size(1018, 666)
        Me.main_panel.TabIndex = 1
        '
        'admin_about_appli_dgrid
        '
        Me.admin_about_appli_dgrid.AllowUserToAddRows = False
        Me.admin_about_appli_dgrid.AllowUserToDeleteRows = False
        Me.admin_about_appli_dgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.admin_about_appli_dgrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.admin_about_appli_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.admin_about_appli_dgrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.admin_about_appli_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.admin_about_appli_dgrid.Location = New System.Drawing.Point(0, 0)
        Me.admin_about_appli_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.admin_about_appli_dgrid.Name = "admin_about_appli_dgrid"
        Me.admin_about_appli_dgrid.Size = New System.Drawing.Size(1018, 666)
        Me.admin_about_appli_dgrid.TabIndex = 4
        '
        'bottom_panel
        '
        Me.bottom_panel.Controls.Add(Me.reset_session_bt)
        Me.bottom_panel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.bottom_panel.Location = New System.Drawing.Point(0, 820)
        Me.bottom_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.bottom_panel.Name = "bottom_panel"
        Me.bottom_panel.Size = New System.Drawing.Size(1018, 154)
        Me.bottom_panel.TabIndex = 5
        '
        'reset_session_bt
        '
        Me.reset_session_bt.Location = New System.Drawing.Point(4, 9)
        Me.reset_session_bt.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.reset_session_bt.Name = "reset_session_bt"
        Me.reset_session_bt.Size = New System.Drawing.Size(132, 35)
        Me.reset_session_bt.TabIndex = 0
        Me.reset_session_bt.Text = "Reset Session"
        Me.reset_session_bt.UseVisualStyleBackColor = True
        '
        'version_info_bt
        '
        Me.version_info_bt.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.version_info_bt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.version_info_bt.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.admin_info
        Me.version_info_bt.Location = New System.Drawing.Point(959, 9)
        Me.version_info_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.version_info_bt.Name = "version_info_bt"
        Me.version_info_bt.Size = New System.Drawing.Size(50, 54)
        Me.version_info_bt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.version_info_bt.TabIndex = 5
        Me.version_info_bt.TabStop = False
        '
        'FormAdminAboutApp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1018, 974)
        Me.Controls.Add(Me.main_panel)
        Me.Controls.Add(Me.bottom_panel)
        Me.Controls.Add(Me.top_panel)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "FormAdminAboutApp"
        Me.Text = "About Application"
        Me.top_panel.ResumeLayout(False)
        CType(Me.jet_logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.main_panel.ResumeLayout(False)
        CType(Me.admin_about_appli_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bottom_panel.ResumeLayout(False)
        CType(Me.version_info_bt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents top_panel As Panel
    Friend WithEvents jet_logo As PictureBox
    Friend WithEvents main_panel As Panel
    Friend WithEvents admin_about_appli_dgrid As DataGridView
    Friend WithEvents bottom_panel As Panel
    Friend WithEvents reset_session_bt As Button
    Friend WithEvents version_info_bt As PictureBox
End Class
