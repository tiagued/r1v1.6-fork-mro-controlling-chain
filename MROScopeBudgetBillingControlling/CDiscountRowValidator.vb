﻿
Imports Equin.ApplicationFramework

Public Class CDiscountRowValidator
    Public Shared instance As CDiscountRowValidator
    Public Shared Function getInstance() As CDiscountRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CDiscountRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(disc As CDiscount)
        disc.calc_is_in_error_state = False
    End Sub

    'label cannot be empty and is unique
    Public Function label(disc As CDiscount) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(disc.label) Then
            err = "Label cannot be empty"
            res = False
            disc.calc_is_in_error_state = True
        Else
            Dim unique As Boolean = True
            For Each view As ObjectView(Of CDiscount) In GLB_MSTR_CTRL.billing_cond_controller.discountList
                If Not IsNothing(view.Object.label) AndAlso Not view.Object.Equals(disc) AndAlso view.Object.label.Equals(disc.label, StringComparison.OrdinalIgnoreCase) Then
                    unique = False
                    Exit For
                End If
            Next
            If Not unique Then
                err = "Label is already use by another entry. Label must be unique"
                res = False
                disc.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'description cannot be empty 
    Public Function description(disc As CDiscount) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(disc.description) Then
            err = "Description cannot be empty"
            res = False
            disc.calc_is_in_error_state = True
        End If

        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function lab_mat_applicability(disc As CDiscount) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(disc.lab_mat_applicability) OrElse disc.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Lab/Mat Applicability cannot be empty"
            res = False
            disc.calc_is_in_error_state = True
        ElseIf disc.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_DISC_SCOPE_APP_LAB_CC_KEY).value Then
            If MUtils.isMaterialApplicableLabMat(disc.lab_mat_applicability) OrElse MUtils.isRepairApplicableLabMat(disc.lab_mat_applicability) _
                OrElse MUtils.isFreightApplicableLabMat(disc.lab_mat_applicability) OrElse MUtils.isServApplicableLabMat(disc.lab_mat_applicability) Then
                err = "If scope applicability is set at CC Lab level, Lab/Mat applicability must be Lab"
                res = False
                disc.calc_is_in_error_state = True
            End If
        ElseIf disc.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_DISC_SCOPE_APP_MAT_PO_KEY).value Then
            If MUtils.isLaborApplicableLabMat(disc.lab_mat_applicability) Then
                err = "If scope applicability is set at Mat PO level, Lab/Mat applicability must be Mat & Freight, Mat, Small Mat, Freight or 3rd Party"
                res = False
                disc.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function scope_applicability(disc As CDiscount) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(disc.scope_applicability) OrElse disc.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Scope Applicability cannot be empty"
            res = False
            disc.calc_is_in_error_state = True
        ElseIf disc.is_flat AndAlso disc.scope_applicability <> MConstants.constantConf(MConstants.CONST_CONF_LOV_DISC_SCOPE_APP_PROJ_KEY).value Then
            err = "If discount is a flat value. scope applicability shall be at Project level"
            res = False
            disc.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function rate(disc As CDiscount) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If disc.rate < 0 OrElse disc.rate > 1 Then
            err = "Rate shall be between 0 and 1 (0% and 100%)"
            res = False
            disc.calc_is_in_error_state = True
        ElseIf disc.is_flat AndAlso disc.rate <> 0 Then
            err = "If discount is flat, rate shall be set to 0"
            res = False
            disc.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function discount_value(disc As CDiscount) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If disc.discount_value <> 0 AndAlso Not disc.is_flat Then
            err = "Discount Value shall be 0 if discount is not flat"
            res = False
            disc.calc_is_in_error_state = True
        ElseIf disc.discount_value = 0 AndAlso disc.is_flat Then
            err = "Discount Value cannot be null if discount is flat"
            res = False
            disc.calc_is_in_error_state = True
        ElseIf disc.discount_value < 0 Then
            err = "Discount Value shall be positive"
            res = False
            disc.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function value_curr(disc As CDiscount) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(disc.value_curr) OrElse disc.value_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            If disc.is_flat Then
                err = "Discount Value Curreny Cannot be empty if discount is flat"
                res = False
                disc.calc_is_in_error_state = True
            End If
        ElseIf Not disc.is_flat Then
            err = "Discount Value sCurrency Shall be empty if Discount is not Flat"
            res = False
            disc.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function cap_value(disc As CDiscount) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If disc.cap_value <> 0 AndAlso disc.is_flat Then
            err = "Cap Value Shall Be null if Discount is flat"
            res = False
            disc.calc_is_in_error_state = True
        ElseIf disc.cap_value < 0 Then
            err = "Cap Value Shall Be a Positive Number"
            res = False
            disc.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function cap_value_curr(disc As CDiscount) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(disc.cap_value_curr) OrElse disc.cap_value_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            If Not disc.is_flat And disc.cap_value > 0 Then
                err = "Cap Value Currency Cannot Be empty if Cap Value is not null"
                res = False
                disc.calc_is_in_error_state = True
            End If
        ElseIf disc.is_flat Then
            err = "Cap Value Currency Shall Be null if Discount is flat"
            res = False
            disc.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function payer_obj(disc As CDiscount) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If disc.payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Payer cannot be empty"
            res = False
            disc.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function payer(disc As CDiscount) As KeyValuePair(Of Boolean, String)
        Return payer_obj(disc)
    End Function

End Class
