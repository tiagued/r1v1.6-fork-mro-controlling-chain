﻿Public Class CImportMatObject


    Private _network_activity As String
    Public Property network_activity() As String
        Get
            Return _network_activity
        End Get
        Set(ByVal value As String)
            _network_activity = value
            setActProj()
        End Set
    End Property

    Private _network As String
    Public Property network() As String
        Get
            Return _network
        End Get
        Set(ByVal value As String)
            _network = value
        End Set
    End Property

    Private _activity As String
    Public Property activity() As String
        Get
            Return _activity
        End Get
        Set(ByVal value As String)
            _activity = value
        End Set
    End Property

    Public Sub setActProj()
        _network = ""
        _activity = ""

        If IsNothing(_network_activity) Then
            Exit Sub
        End If

        If Len(_network_activity) >= CInt(MConstants.constantConf(MConstants.CONST_CONF_MAT_IMP_FILE_PROJ_DIGIT).value) Then
            _network = Strings.Left(_network_activity, CInt(MConstants.constantConf(MConstants.CONST_CONF_MAT_IMP_FILE_PROJ_DIGIT).value))
            _activity = Strings.Replace(_network_activity, _network, "")
        Else
            _network = _network_activity
        End If
    End Sub

    Private _quantity As Double
    Public Property quantity() As Double
        Get
            Return _quantity
        End Get
        Set(ByVal value As Double)
            _quantity = value
        End Set
    End Property

    Private _unit As String
    Public Property unit() As String
        Get
            Return _unit
        End Get
        Set(ByVal value As String)
            _unit = value
        End Set
    End Property

    Private _part_number As String
    Public Property part_number() As String
        Get
            Return _part_number
        End Get
        Set(ByVal value As String)
            _part_number = value
        End Set
    End Property

    Private _short_text As String
    Public Property short_text() As String
        Get
            Return _short_text
        End Get
        Set(ByVal value As String)
            _short_text = value
        End Set
    End Property

    Private _or_rea As String
    Public Property or_rea() As String
        Get
            Return _or_rea
        End Get
        Set(ByVal value As String)
            _or_rea = value
        End Set
    End Property

    Private _w_sp_code As String
    Public Property w_sp_code() As String
        Get
            Return _w_sp_code
        End Get
        Set(ByVal value As String)
            _w_sp_code = value
        End Set
    End Property

    Private _po_number As String
    Public Property po_number() As String
        Get
            Return _po_number
        End Get
        Set(ByVal value As String)
            _po_number = value
        End Set
    End Property

    Private _item As String
    Public Property item() As String
        Get
            Return _item
        End Get
        Set(ByVal value As String)
            _item = value
        End Set
    End Property

    Private _price As Double
    Public Property price() As Double
        Get
            Return _price
        End Get
        Set(ByVal value As Double)
            _price = value
        End Set
    End Property

    Private _curr As String
    Public Property curr() As String
        Get
            Return _curr
        End Get
        Set(ByVal value As String)
            _curr = value
        End Set
    End Property

    Private _estimate As String
    Public Property estimate() As String
        Get
            Return _estimate
        End Get
        Set(ByVal value As String)
            _estimate = value
        End Set
    End Property

    Private _order_date As Date
    Public Property order_date() As Date
        Get
            Return _order_date
        End Get
        Set(ByVal value As Date)
            _order_date = value
        End Set
    End Property

    Public Function getUnique() As String
        Return _network_activity & "_" & po_number & "_" & _item
    End Function

    Public Function getMaterial() As CMaterial
        Dim mat As New CMaterial()
        mat.project_activity = _network_activity
        mat.qty = _quantity
        mat.uom = _unit
        mat.part_number = _part_number
        mat.description = _short_text
        mat.or_rea = _or_rea
        mat.warr_code = _w_sp_code
        mat.po_number = _po_number
        mat.po_item = _item
        mat.po_cost = _price
        mat.po_curr = _curr
        mat.po_estimated = _estimate
        mat.po_created = _order_date
        mat.is_sap_import = True
        If _or_rea = MConstants.constantConf(MConstants.CONST_CONF_MAT_TYPE_3RD_P_OR_REA_VALUE).value OrElse String.IsNullOrWhiteSpace(_or_rea) OrElse String.IsNullOrWhiteSpace(_part_number) Then
            mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_3RD_P_KEY).value
        Else
            mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_MAT_KEY).value
        End If
        Return mat
    End Function

End Class