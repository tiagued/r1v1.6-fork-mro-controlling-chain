﻿
Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CDownPayment
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    'used to created new ids
    Public Shared currentMaxID As Integer


    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_DOWNPAYMENT_LOADED Then
            _lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _payment_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_PAYMENT_STAT_UNPAID_KEY).value
            _due_date = MConstants.APP_NULL_DATE
            _payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value
            _payer_obj = MConstants.listOfPayer(_payer)
        End If
    End Sub

    Public Sub setID()
        If MConstants.GLOB_DOWNPAYMENT_LOADED Then
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
        End If
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return _id > 0
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_DOWNPAYMENT_DB_READ
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res As Boolean = False
        Dim mess As String = ""

        Try
            If _system_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_PAYMENT_STAT_PAID_KEY).value Then
                res = False
                mess = "Cannot delete a Downpayment that has been paid"
            Else
                Me.system_status = MConstants.CONST_SYS_ROW_DELETED
                MConstants.GLB_MSTR_CTRL.billing_cond_controller.deleteObjectDownPayment(Me)
                res = True
            End If
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.ToString & Chr(10) & ex.Message
        End Try
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function


    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = True
        Dim mess As String = ""
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_DISCOUNT_LOADED Then
                    CDownPayment.currentMaxID = Math.Max(CDownPayment.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _number As String
    Public Property number() As String
        Get
            Return _number
        End Get
        Set(ByVal value As String)
            If Not _number = value Then
                _number = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            If Not _description = value Then
                _description = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _lab_mat_applicability As String
    Public Property lab_mat_applicability() As String
        Get
            Return _lab_mat_applicability
        End Get
        Set(ByVal value As String)
            If Not _lab_mat_applicability = value Then
                _lab_mat_applicability = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _dp_value As Double
    Public Property dp_value() As Double
        Get
            Return _dp_value
        End Get
        Set(ByVal value As Double)
            If Not _dp_value = value Then
                _dp_value = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _due_date As Date
    Public Property due_date() As Date
        Get
            Return _due_date
        End Get
        Set(ByVal value As Date)
            If Not _due_date = value Then
                _due_date = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _payer As String
    Public Property payer() As String
        Get
            Return _payer
        End Get
        Set(ByVal value As String)
            If Not _payer = value Then
                _payer = value
                If MConstants.listOfPayer.ContainsKey(_payer) Then
                    Me.payer_obj = MConstants.listOfPayer(_payer)
                Else
                    Throw New Exception("Payer does not exist in payer list : " & _payer)
                    Me.payer_obj = Nothing
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _payer_obj As CPayer
    Public Property payer_obj() As CPayer
        Get
            Return _payer_obj
        End Get
        Set(ByVal value As CPayer)
            If Not Object.Equals(_payer_obj, value) Then
                _payer_obj = value
                If IsNothing(_payer_obj) Then
                    'use Me to force a new value of payer_obj
                    Me.payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
                Else
                    _payer = _payer_obj.payer
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_is_in_error_state = value Then
                _calc_is_in_error_state = value
            End If
        End Set
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not value = _calc_error_text Then
                _calc_error_text = value
            End If
        End Set
    End Property

    Private _payment_status As String
    Public Property payment_status() As String
        Get
            Return _payment_status
        End Get
        Set(ByVal value As String)
            If Not _payment_status = value Then
                _payment_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public ReadOnly Property payment_status_view() As String
        Get
            If _payment_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_PAYMENT_STAT_UNPAID_KEY).value Then
                If _due_date <= Date.Now Then
                    Return MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_PAYMENT_STAT_UNPAID_DUE_TXT).value
                Else
                    Return MConstants.constantConf(MConstants.CONST_CONF_LOV_DP_PAYMENT_STAT_UNPAID_NOT_DUE_TXT).value
                End If
            Else
                Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_DOWN_PAYMENT_PAY_STAT)(_payment_status).value
            End If
        End Get
    End Property

    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'to string
    Public Overrides Function tostring() As String
        Return "Dowmpayment: " & _number & ", Description: " & _description
    End Function
End Class