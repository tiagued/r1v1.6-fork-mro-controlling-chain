﻿Public Class FormTransferLabMat
    Public ctrl As CAWQFakeActivityTransferDelegate

    'add or remove dependant activities
    Private Sub select_act_right_Click(sender As Object, e As EventArgs) Handles select_act_right.Click
        ctrl.addDependantActivities()
    End Sub
    'add or remove dependant activities
    Private Sub select_act_left_Click(sender As Object, e As EventArgs) Handles select_act_left.Click
        ctrl.RemoveDependantActivities()
    End Sub

    'select an item among dependant activities to edit hours
    Private Sub act_dep_selected_listb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles act_dep_selected_listb.SelectedIndexChanged
        ctrl.updateHoursAndMaterialsViews()
    End Sub

    'change main activity
    Private Sub select_main_act_cb_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles select_main_act_cb.SelectionChangeCommitted
        ctrl.main_activity_changed()
    End Sub

    Private Sub cancel_bt_Click(sender As Object, e As EventArgs) Handles cancel_bt.Click
        Me.Close()
    End Sub

    Private Sub FormTransferLabMat_Closing(sender As Object, e As FormClosingEventArgs) Handles MyBase.Closing
        e.Cancel = ctrl.close()
    End Sub

    Private Sub apply_transfer_bt_Click(sender As Object, e As EventArgs) Handles apply_transfer_bt.Click
        ctrl.perform_transfer()
    End Sub

    Public Sub grid_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles awq_labor_dgrid.CellClick, awq_material_dgrid.CellClick, labor_map_dgrid.CellClick, material_map_dgrid.CellClick
        Try
            Dim dgrid As DataGridView = CType(sender, DataGridView)
            ' Ignore clicks that are not on button cells. 
            If e.RowIndex >= 0 And e.ColumnIndex > 0 Then
                'cancel button
                If dgrid.Columns(e.ColumnIndex).Name = MConstants.CONST_CONF_TRANSFER_FAKE_ACT_CANCEL Then
                    ctrl.cancelItem(dgrid, e.RowIndex)
                ElseIf dgrid.Columns(e.ColumnIndex).Name = MConstants.CONST_CONF_TRANSFER_FAKE_ACT_AS_NEW Then
                    'add as new material and labor
                    If dgrid.Equals(awq_labor_dgrid) Then
                        ctrl.addNewLabor(e.RowIndex)
                    ElseIf dgrid.Equals(awq_material_dgrid) Then
                        ctrl.addNewMaterial(e.RowIndex)
                    End If
                ElseIf dgrid.Columns(e.ColumnIndex).Name = MConstants.CONST_CONF_TRANSFER_FAKE_ACT_AS_EXISTING Then
                    'add existing material and labor
                    If dgrid.Equals(awq_labor_dgrid) Then
                        ctrl.addExistingLabor(e.RowIndex)
                    ElseIf dgrid.Equals(awq_material_dgrid) Then
                        ctrl.addExistingMaterial(e.RowIndex)
                    End If
                End If
            Else
                Return
            End If
        Catch ex As Exception
            MsgBox("Following error happened " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
End Class