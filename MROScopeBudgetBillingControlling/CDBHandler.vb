﻿Imports System.ComponentModel
Imports System.Data.OleDb
Imports System.Reflection

Public Class CDBHandler
    Private glb_oconn As OleDbConnection
    Private _conn_str As String
    Private _file_str As String

    'db model save lists
    Public items_to_be_updated As List(Of Object)
    Public items_project_to_be_updated As List(Of CViewModelMap)
    Public items_to_be_deleted As List(Of Object)
    Public items_to_be_inserted As List(Of Object)

    Public proj_map_list_by_class_field As Dictionary(Of String, CViewModelMap)

    Public Sub New(conn As String, file_str As String)
        _conn_str = conn
        _file_str = file_str
        openConn()
        items_to_be_updated = New List(Of Object)
        items_to_be_deleted = New List(Of Object)
        items_to_be_inserted = New List(Of Object)
        items_project_to_be_updated = New List(Of CViewModelMap)
    End Sub


    Public Function getConn() As String
        Return _conn_str
    End Function

    Public Sub openConn()
        glb_oconn = New OleDbConnection(_conn_str)
        glb_oconn.Open()
    End Sub

    Public Sub closeConn()
        Try
            glb_oconn.Dispose()
            glb_oconn.Close()
            glb_oconn = Nothing
        Catch ex As Exception
        End Try
    End Sub

    'perform select and return a list of objects where properties are feld with db values
    Public Function performSelectObject(query As String, mapping As CViewModelMapList, returnList As Boolean) As List(Of Object)
        Dim tbl As DataTable
        Dim res As New List(Of Object)
        Dim entry As Object
        Dim propInf As PropertyInfo
        Dim value As Object


        tbl = performSelect(query)
        Try
            If returnList Then
                'check if all columns exist
                For Each map As CViewModelMap In mapping.list.Values
                    If Not tbl.Columns.Contains(map.db_col) Then
                        Throw New Exception("Table " & tbl.TableName & " from query " & query & " does not contains the column " & map.db_col)
                    Else
                        map.ole_db_type = MGeneralFunctionsDB.getValueOledbType(tbl.Columns(map.db_col).DataType.Name)
                    End If
                Next
                For Each row As DataRow In tbl.Rows
                    entry = Activator.CreateInstance(mapping.object_class)

                    For Each col As DataColumn In tbl.Columns
                        If Not mapping.list.Keys.Contains(col.ColumnName) Then
                            Throw New Exception(" The DB Table column Is Not mapped To business Object Property " & col.ColumnName & " On file " & _conn_str)
                        End If
                        propInf = mapping.object_class.GetProperty(mapping.list(col.ColumnName).class_field)
                        value = MGeneralFunctionsDB.formatValue(row.Item(col), propInf.PropertyType.Name)
                        CallByName(entry, mapping.list(col.ColumnName).class_field, CallType.Let, New Object() {value})
                    Next
                    res.Add(entry)
                Next
            Else
                'rather than returning a list, return one object where its properties are not columns but rows in the DB
                entry = Activator.CreateInstance(mapping.object_class)
                Dim rows() As DataRow
                Dim row As DataRow
                For Each mapO As CViewModelMap In mapping.list.Values
                    rows = tbl.Select(mapO.key_col & " = '" & mapO.key_value & "'")
                    If rows.Count > 0 Then
                        row = rows(0)
                        propInf = mapping.object_class.GetProperty(mapO.class_field)
                        value = MGeneralFunctionsDB.formatValue(row.Item(mapO.db_col), propInf.PropertyType.Name)
                        CallByName(entry, mapO.class_field, CallType.Let, New Object() {value})
                    End If
                Next
                res.Add(entry)
            End If

        Catch ex As Exception
            Throw New Exception("An error occured while creating business objects from query " & query & " on file " & _conn_str & Chr(10) & ex.Message, ex)
        End Try
        Return res
    End Function

    'perform updateon an object on which properties are row not column
    Public Sub performUpdateProject(map As CViewModelMap)

        Dim query As String
        Dim value As Object
        Dim mapList = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_PROJ_DB_READ)
        Dim propInf As PropertyInfo

        query = "UPDATE [" & mapList.table_name & "_q] SET [_value] = @value WHERE [" & mapList.table_name & "_q].[key] = @key;"

        Dim proj As CProjectModel = MConstants.GLB_MSTR_CTRL.project_controller.project

        Try
            Using oledb_cmd As OleDbCommand = glb_oconn.CreateCommand
                oledb_cmd.CommandText = query

                propInf = mapList.object_class.GetProperty(map.class_field)
                value = CallByName(proj, map.class_field, CallType.Get)
                'ignore id first. order of parameters shall be the same as in the query CViewModelMapList.getDefaultUpdate
                'https://stackoverflow.com/questions/52648299/oledb-update-command-with-parameters-not-working-on-access-db

                oledb_cmd.Parameters.Add("@value", OleDbType.LongVarChar).Value = MGeneralFunctionsDB.formatValueAsString(value)
                oledb_cmd.Parameters.Add("@key", OleDbType.LongVarChar).Value = map.key_value
                Dim res As Integer = oledb_cmd.ExecuteNonQuery()
                If res <> 1 Then
                    'Throw New Exception("Query update " & query & " have not return 1 updated entry for key " & map.key_value)
                End If
            End Using

        Catch ex As Exception
            'to be handle
            Throw New Exception("An error occured while performing the query " & query & " on file " & _conn_str & Chr(10) & ex.Message, ex)
        End Try
    End Sub

    Public Sub performUpdate(obj As Object)
        Dim query As String = ""
        Try
            Dim dbMapListName As String = CType(obj, IDataGridViewRecordable).getDBMapperName
            Dim mapList As CViewModelMapList = MConstants.GLB_MSTR_CTRL.modelViewMap(dbMapListName)
            Dim value As Object
            Dim propInf As PropertyInfo

            Using oledb_cmd As OleDbCommand = glb_oconn.CreateCommand
                oledb_cmd.CommandText = mapList.getDefaultUpdate
                query = oledb_cmd.CommandText
                Dim oledbParamID As OleDbParameter = Nothing
                'set values
                For Each map As CViewModelMap In mapList.list.Values
                    propInf = mapList.object_class.GetProperty(map.class_field)
                    value = CallByName(obj, map.class_field, CallType.Get)
                    'ignore id first. order of parameters shall be the same as in the query CViewModelMapList.getDefaultUpdate
                    'https://stackoverflow.com/questions/52648299/oledb-update-command-with-parameters-not-working-on-access-db
                    If map.db_col = "id" Then
                        oledbParamID = New OleDbParameter("@" & map.db_col, MGeneralFunctionsDB.formatValue(value, propInf.PropertyType.Name))
                        oledbParamID.OleDbType = map.ole_db_type
                        Continue For
                    Else
                        oledb_cmd.Parameters.Add("@" & map.db_col, map.ole_db_type).Value = MGeneralFunctionsDB.formatValue(value, propInf.PropertyType.Name)
                    End If
                Next

                'add id at the end cause it is at the end of the query
                oledb_cmd.Parameters.Add(oledbParamID)

                Dim res As Integer = oledb_cmd.ExecuteNonQuery()
                If res <> 1 Then

                End If
            End Using
        Catch ex As Exception
            'to be handle
            Throw New Exception("An error occured while updating an item. Query " & query & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Sub performDelete(obj As Object)
        Dim query As String = ""
        Try
            Dim recordable As IDataGridViewRecordable = CType(obj, IDataGridViewRecordable)
            Dim dbMapListName As String = recordable.getDBMapperName
            Dim mapList As CViewModelMapList = MConstants.GLB_MSTR_CTRL.modelViewMap(dbMapListName)
            Dim id_map As CViewModelMap = mapList.list("id")
            Using oledb_cmd As OleDbCommand = New OleDbCommand("DELETE FROM " & mapList.table_name & " WHERE id =@id", glb_oconn)
                oledb_cmd.Parameters.Add("@id", id_map.ole_db_type).Value = recordable.getID()
                query = oledb_cmd.CommandText
                oledb_cmd.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            'to be handle
            Throw New Exception("An error occured while deleting an item. Query " & query & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Sub performInsert(obj As Object)
        Dim query As String = ""
        Try
            Dim recordable As IDataGridViewRecordable = CType(obj, IDataGridViewRecordable)
            Dim dbMapListName As String = recordable.getDBMapperName
            Dim mapList As CViewModelMapList = MConstants.GLB_MSTR_CTRL.modelViewMap(dbMapListName)
            Dim value As Object
            Dim propInf As PropertyInfo
            Dim new_id As Long
            Using oledb_cmd As OleDbCommand = glb_oconn.CreateCommand
                oledb_cmd.CommandText = mapList.getDefaulInsert
                query = oledb_cmd.CommandText
                'set values
                For Each map As CViewModelMap In mapList.list.Values
                    If map.db_col = "id" Then
                        Continue For
                    End If
                    propInf = mapList.object_class.GetProperty(map.class_field)
                    value = CallByName(obj, map.class_field, CallType.Get)
                    oledb_cmd.Parameters.Add("@" & map.db_col, map.ole_db_type).Value = MGeneralFunctionsDB.formatValue(value, propInf.PropertyType.Name)
                Next
                oledb_cmd.ExecuteNonQuery()
                oledb_cmd.CommandText = "SELECT @@IDENTITY;"
                new_id = oledb_cmd.ExecuteScalar()
                If new_id = 0 Then
                    Throw New Exception("Cannot get the ID of the new inserted item in the DB. Object " & obj.ToString)
                End If
                recordable.setID(new_id)
            End Using

        Catch ex As Exception
            'to be handle
            Throw New Exception("An error occured while inserting an item. Query " & query & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Function performSelect(query As String) As DataTable
        'perform the select and return a datable object
        Dim dtb As New DataTable
        Try
            Using dad As New OleDbDataAdapter(query, glb_oconn)
                dad.Fill(dtb)
            End Using
        Catch ex As Exception
            'to be handle
            Throw New Exception("An error occured while performing the query " & query & " on file " & _conn_str & Chr(10) & ex.Message, ex)
        End Try
        Return dtb
    End Function

    'add item to be updated
    Public Sub add_item_to_be_updated(obj As Object)
        If MConstants.GLOB_APP_LOADED Then
            If Not items_to_be_updated.Contains(obj) Then
                Dim recordable As IGenericInterfaces.IDataGridViewRecordable = CType(obj, IGenericInterfaces.IDataGridViewRecordable)
                If recordable.getID > 0 Then
                    items_to_be_updated.Add(obj)
                End If
            End If
        End If
    End Sub

    'add item to be deleted
    Public Sub add_item_to_be_deleted(obj As Object)
        If MConstants.GLOB_APP_LOADED Then
            If Not items_to_be_deleted.Contains(obj) Then
                Dim recordable As IGenericInterfaces.IDataGridViewRecordable = CType(obj, IGenericInterfaces.IDataGridViewRecordable)
                If recordable.getID > 0 Then
                    items_to_be_deleted.Add(obj)
                End If

                'remove from other lists
                If items_to_be_inserted.Contains(obj) Then
                    items_to_be_inserted.Remove(obj)
                End If
                If items_to_be_updated.Contains(obj) Then
                    items_to_be_updated.Remove(obj)
                End If
            End If
        End If
    End Sub

    'add item to be inserted
    Public Sub add_item_to_be_inserted(obj As Object)
        If MConstants.GLOB_APP_LOADED Then
            If Not items_to_be_inserted.Contains(obj) Then
                Dim recordable As IGenericInterfaces.IDataGridViewRecordable = CType(obj, IGenericInterfaces.IDataGridViewRecordable)
                If recordable.getID = 0 Then
                    items_to_be_inserted.Add(obj)
                End If
            End If
        End If
    End Sub

    'add item to be inserted and execute
    Public Sub add_item_to_be_inserted_and_run(obj As Object)
        add_item_to_be_inserted(obj)
        MConstants.GLB_MSTR_CTRL.save_controller.startAsync(CSaveWorkerArgs.getInsertAll)
    End Sub

    'add item to be inserted
    Public Sub add_project_item_to_be_updated(class_field_key As String)
        If MConstants.GLOB_APP_LOADED Then

            If IsNothing(proj_map_list_by_class_field) Then
                Dim mapList = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_PROJ_DB_READ)
                proj_map_list_by_class_field = mapList.getListClassFieldAsKey
            End If

            If Not (proj_map_list_by_class_field.ContainsKey(class_field_key)) Then
                Throw New Exception("Class field " & class_field_key & " does not exist in the model mapper")
            End If
            Dim map As CViewModelMap = proj_map_list_by_class_field(class_field_key)

            If Not items_project_to_be_updated.Contains(map) Then
                items_project_to_be_updated.Add(map)
            End If
        End If
    End Sub

    Public Function hasBeenValidated(obj As Object) 
        'check if item has been validate : either id > 0 or item in queue for insertion
        Dim recordable As IGenericInterfaces.IDataGridViewRecordable = CType(obj, IGenericInterfaces.IDataGridViewRecordable)
        If items_to_be_inserted.Contains(obj) OrElse recordable.getID > 0 Then
            Return True
        End If
        Return False
    End Function
End Class
