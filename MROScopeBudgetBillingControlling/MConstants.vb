﻿Module MConstants
    'global controller
    Public GLB_MSTR_CTRL As CMasterController

    'cultur info
    Public CULT_INFO As System.Globalization.CultureInfo
    'constant list from conf in excel file
    Public constantConf As Dictionary(Of String, CConfProperty)

    'list of business value list from excel file
    Public listOfValueConf As Dictionary(Of String, Dictionary(Of String, CConfProperty))
    'list of business value list from excel file
    Public listOfReportConf As Dictionary(Of String, Dictionary(Of String, List(Of CReportConfObject)))
    Public Const REPORT_CONF_SUMMARY_LIST_NAME = "Summary"
    'list of editable business value list from excel file
    Public listOfValueConf_edit As Dictionary(Of String, Dictionary(Of String, CConfProperty))
    'list of dependencies between lov
    Public lisOfValueDependencies As Dictionary(Of String, Dictionary(Of String, List(Of String)))
    'list of payers
    Public listOfPayer As Dictionary(Of String, CPayer)
    'list of cost centers
    Public listOfCC As Dictionary(Of String, CCostCenter)
    'generic date picker
    Public DATE_PICKER As CDataGridViewDateEditControl
    'program min date
    Public APP_NULL_DATE As Date
    'list of db cconfprop writable
    Public listOfDBTblConfPropWritable As New List(Of String)

    'project summary keys
    Public Const CONST_CONF_PROJ_SUM_DEAR_CUSTOMER_TXT As String = "project_summary_dear_customer_text"
    Public Const CONST_CONF_PROJ_SUM_SUBTOTAL As String = "project_summary_subtotal_key"
    Public Const CONST_CONF_PROJ_SUM_TOTAL As String = "project_summary_total_after_disc_surch_key"
    Public Const CONST_CONF_PROJ_SUM_ECOTAX As String = "project_summary_ecotax_key"
    Public Const CONST_CONF_PROJ_SUM_GUM As String = "project_summary_gum_key"
    Public Const CONST_CONF_PROJ_SUM_SURCH_TOTAL As String = "project_summary_surcharge_key"
    Public Const CONST_CONF_PROJ_SUM_DISC_TOTAL As String = "project_summary_discount_total_key"
    Public Const CONST_CONF_PROJ_SUM_NTE_TOTAL As String = "project_summary_nte_total_key"
    Public Const CONST_CONF_PROJ_SUM_NTE_GROSS_TOTAL As String = "project_summary_nte_total_gross_key"
    Public Const CONST_CONF_PROJ_SUM_NTE_CHARG_TOTAL As String = "project_summary_nte_total_charg_key"
    Public Const CONST_CONF_PROJ_SUM_ECOTAX_TEXT As String = "project_summary_ecotax_text"
    Public Const CONST_CONF_PROJ_SUM_GUM_TEXT As String = "project_summary_gum_text"
    Public Const CONST_CONF_PROJ_SUM_SUBTOTAL_AFT_SUR As String = "project_summary_subtotal_after_surch_bef_disc_key"
    Public Const CONST_CONF_PROJ_SUM_DP_PAID As String = "project_summary_dp_paid_key"
    Public Const CONST_CONF_PROJ_SUM_DP_UNPAID As String = "project_summary_dp_un_paid_key"
    Public Const CONST_CONF_PROJ_SUM_DP_UNPAID_NOT_DUE As String = "project_summary_dp_un_paid_not_due_key"
    Public Const CONST_CONF_PROJ_SUM_DP_TOTAL As String = "project_summary_dp_total_key"
    Public Const CONST_CONF_PROJ_SUM_OUTSTANDING As String = "project_summary_outstanding"
    Public Const CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_INIT As String = "project_summary_subtotal_init_key"
    Public Const CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_INIT_ADJ As String = "project_summary_subtotal_init_adj_key"
    Public Const CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_INIT_TOTAL As String = "project_summary_subtotal_init_total_key"
    Public Const CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_ADD_BELOW_THR As String = "project_summary_subtotal_add_below_thr_key"
    Public Const CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_ADD_APPR As String = "project_summary_subtotal_add_appr_key"
    Public Const CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_ADD_BLWTHR_AND_APPR As String = "project_summary_subtotal_add_blwthr_and_appr_key"
    Public Const CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_ADD_PENDING As String = "project_summary_subtotal_add_pending_key"
    Public Const CONST_CONF_PROJ_SUM_SUBTOTAL_SCOPE_ADD_TOTAL As String = "project_summary_subtotal_add_total_key"
    'awq
    Public Const CONST_CONF_PROJ_AWQ_SUM_SENT_AWQ_PENDING As String = "project_awq_summary_sent_pending"
    Public Const CONST_CONF_PROJ_AWQ_SUM_SENT_AWQ_APPROVED As String = "project_awq_summary_sent_approved"
    Public Const CONST_CONF_PROJ_AWQ_SUM_SENT_AWQ_PENDING_AND_APP As String = "project_awq_summary_sent_pending_approved"
    'empty discount, nte
    Public EMPTY_DISC As CDiscount
    Public EMPTY_NTE As CNotToExceed
    Public NTEN_NO_NOTE As CNotToExceed
    Public EMPTY_ACT As CScopeActivity
    Public LABOR_CTRL_SELECT_ALL_ACT As CScopeActivity
    Public LABOR_CTRL_SELECT_ALL_CC As CCostCenter
    Public LABOR_CTRL_ALL_CC As CCostCenter
    Public EMPTY_TASKRATE As CTaskRate
    Public EMPTY_PAYER As CPayer
    Public EMPTY_QUOTE As CSalesQuote
    Public EMPTY_CONFPROP As CConfProperty
    Public EMPTY_AWQ As CAWQuotation
    Public EMPTY_CALC_ENTRY As CScopeAtivityCalcEntry
    Public EMPTY_GUM As CSurcharge
    Public EMPTY_ECO As CSurcharge

    'reporting list
    'awq
    Public Const REPORT_CONF_CUST_REL_AWQ_SHEET_NAME As String = "customer_awq_list"
    Public Const REPORT_CONF_CUST_REL_AWQ_LIST_NAME As String = "awq_list"
    'proj sum
    Public Const REPORT_CONF_CUST_REL_PROJ_SUM_SHEET_NAME As String = "project_summary"
    'proj details
    Public Const REPORT_CONF_CUST_REL_PROJ_DET_SHEET_NAME As String = "project_details"
    Public Const REPORT_CONF_CUST_REL_LIGHT_PROJ_DET_SHEET_NAME As String = "proj_details_light_list"
    Public Const REPORT_CONF_CUST_REL_PROJ_DET_LIST_NAME As String = "scope_list"
    Public Const REPORT_CONF_CUST_REL_PROJ_DET_DISC_LIST_NAME As String = "discount_list"
    Public Const REPORT_CONF_CUST_REL_PROJ_DET_NTE_LIST_NAME As String = "nte_list"
    'dp
    Public Const REPORT_CONF_CUST_REL_DP_SHEET_NAME As String = "down_payment_list"
    Public Const REPORT_CONF_CUST_REL_DP_LIST_NAME As String = "dp_list"
    'nte details
    Public Const REPORT_CONF_CUST_REL_NTE_DET_SHEET_NAME As String = "nte_details_list"
    Public Const REPORT_CONF_CUST_REL_NTE_DET_ACT_LIST_NAME As String = "nte_act_list"
    Public Const REPORT_CONF_CUST_REL_NTE_DET_LIST_NAME As String = "nte_obj_list"

    'awq form reporting conf
    Public Const REPORT_CONF_AWQ_FORM_FROMT_PAGE_SHEET_NAME As String = "awq_form_front_page"
    Public Const REPORT_CONF_AWQ_FORM_QUOTE_REF_TXT As String = "awq_form_quote_ref_text"
    Public Const REPORT_CONF_AWQ_FORM_WORK_ACCP_REF_TXT As String = "awq_form_work_accpt_ref_text"

    Public Const REPORT_CONF_AWQ_FORM_DETAILS_PAGE_SHEET_NAME As String = "awq_details_page"
    Public Const REPORT_CONF_AWQ_FORM_DETAILS_PAGE_LABOR_LIST As String = "labor_list"
    Public Const REPORT_CONF_AWQ_FORM_DETAILS_PAGE_MAT_LIST As String = "material_list"

    'labor controlling hours statement report conf
    'proj sum
    Public Const REPORT_CONF_HRS_STAT_SUMMARY_SHEET_NAME As String = "lab_ctrl_hrs_stat_summary"
    'lab ctrl obj list
    Public Const REPORT_CONF_HRS_WO_CC_HRS_SHEET_NAME As String = "labor_ctrl_act_cc_data_page"
    Public Const REPORT_CONF_HRS_WO_CC_HRS_LIST_NAME As String = "wo_cc_list"
    'budget adj
    Public Const REPORT_CONF_HRS_BUDGET_ADJ_SHEET_NAME As String = "labor_ctrl_budget_adj_page"
    Public Const REPORT_CONF_HRS_BUDGET_ADJ_LIST_NAME As String = "budget_adj_list"

    'data consistency report
    'proj sum
    Public Const REPORT_DATA_CHECK_SUMMARY_SHEET_NAME As String = "data_consistency_summary"
    Public Const REPORT_DATA_CHECK_CNT_PER_BO_LIST_NAME As String = "cnt_per_business_obj_list"
    'err details
    Public Const REPORT_DATA_CHECK_DETAILS_SHEET_NAME As String = "data_consistency_details"
    Public Const REPORT_DATA_CHECK_DETAILS_LIST_NAME As String = "details_error_list"
    'awr date buffers
    Public Const AWR_VALIDITY_DATE_BUFFER As String = "awq_validity_date_buffer"
    Public Const AWR_FDBCK_DUE_DATE_BUFFER As String = "awq_feedback_due_date_buffer"


    'function name of model view mapper to read Project data in db
    Public Const MOD_VIEW_MAP_PROJ_DB_READ As String = "project_details_db_read"
    'function name of model view mapper to read exchange rate data in db
    Public Const MOD_VIEW_MAP_EXCHG_RATE_DB_READ As String = "exchange_rate_db_read"
    'function name of model view mapper to read material markup data in db
    Public Const MOD_VIEW_MAP_MATERIAL_MKP_DB_READ As String = "mat_makup_db_read"
    'function name of model view mapper to read material markup data in db
    Public Const MOD_VIEW_MAP_FREIGHT_DB_READ As String = "freight_db_read"
    'function name of model view mapper to read surcharge  data in db
    Public Const MOD_VIEW_MAP_SURCHARGE_DB_READ As String = "surcharge_db_read"
    'function name of model view mapper to read downpayments  data in db
    Public Const MOD_VIEW_MAP_DOWNPAYMENT_DB_READ As String = "downpayment_read_db"
    'function name of model view mapper to read discount  data in db
    Public Const MOD_VIEW_MAP_DISCOUNT_DB_READ As String = "discount_db_read"
    'function name of model view mapper to read nte  data in db
    Public Const MOD_VIEW_MAP_NTE_DB_READ As String = "nte_db_read"
    'function name of model view mapper to read labor rate  data in db
    Public Const MOD_VIEW_MAP_LAB_RATE_DB_READ As String = "labor_rate_db_read"
    'function name of model view mapper to read payers  data in db
    Public Const MOD_VIEW_MAP_PAYER_DB_READ As String = "payer_read_db"
    'function name of model view mapper to read cost centers  data in db
    Public Const MOD_VIEW_MAP_CC_DB_READ As String = "cc_read_db"
    'function name of model view mapper to read quotes  data in db
    Public Const MOD_VIEW_MAP_QUOTE_DB_READ As String = "quote_read_db"
    'function name of model view mapper to read quotes entries  data in db
    Public Const MOD_VIEW_MAP_QUOTE_ENTRIES_DB_READ As String = "quote_entries_read_db"
    'function name of model view mapper to read quotes entries to activity link  data in db
    Public Const MOD_VIEW_MAP_QUOTE_ENTRIES_TO_ACT_LINK_DB_READ As String = "quote_sum_act_link_read_db"
    'function name of model view mapper to read scope import  data in db
    Public Const MOD_VIEW_MAP_SCOPE_IMPORT As String = "sap_scope_import_read_db"
    'function name of model view mapper to read scope import  data in db
    Public Const MOD_VIEW_MAP_MAT_IMPORT As String = "sap_material_import_read_db"
    'function name of model view mapper to read activities  data in db
    Public Const MOD_VIEW_MAP_ACT_DB_READ As String = "activity_read_db"
    'function name of model view mapper to read sold hours  data in db
    Public Const MOD_VIEW_MAP_SOLD_HRS_DB_READ As String = "sold_hours_read_db"
    'function name of model view mapper to read materials  data in db
    Public Const MOD_VIEW_MAP_MATERIAL_DB_READ As String = "material_read_db"
    'function name of model view mapper to read task rate  data in db
    Public Const MOD_VIEW_MAP_TASKRATE_DB_READ As String = "task_rate_read_db"
    'awq db read
    Public Const MOD_VIEW_MAP_AWQ_DB_READ As String = "awq_read_db"
    'report conf read
    Public Const MOD_VIEW_MAP_REPORT_CONF_DB_READ As String = "report_conf_read_db"
    'function name of model view mapper to read actual labor  data in db
    Public Const MOD_VIEW_MAP_ACTUAL_LABOR_DB_READ As String = "actual_labor_read_db"
    'function name of model view mapper to read hours transfer  data in db
    Public Const MOD_VIEW_MAP_HOURS_TRANSFER_DB_READ As String = "hours_transfer_read_db"
    'function name of model view mapper to read cc transfer  data in db
    Public Const MOD_VIEW_MAP_CC_TRANSFER_DB_READ As String = "cc_transfer_read_db"
    'function name of model view mapper to read deviation justification  data in db
    Public Const MOD_VIEW_MAP_DEV_JUST_DB_READ As String = "deviation_just_read_db"
    'function name of model view mapper to read cost collector scope import  data in db
    Public Const MOD_VIEW_MAP_COST_COLLECTOR_IMPORT As String = "sap_scope_cost_collector_import_read_db"
    'function name of model view mapper to read actual hours import  data in db
    Public Const MOD_VIEW_MAP_ACTUAL_HOURS_IMPORT As String = "sap_actual_hours_import_read_db"
    'function name of model view mapper to read system data in db
    Public Const MOD_VIEW_MAP_SYSTEM_DATA As String = "system_data_read_db"

    'function name of model view mapper to display Project data in view
    Public Const MOD_VIEW_MAP_PROJ_VIEW_DISPLAY As String = "project_details_view"
    'function name of model view mapper to display exchange rate data in view
    Public Const MOD_VIEW_MAP_EXCHG_RATE_VIEW_DISPLAY As String = "exchange_rate_view"
    'function name of model view mapper to display material markup data in view 
    Public Const MOD_VIEW_MAP_MAT_MKP_VIEW_DISPLAY As String = "material_markup_view"
    'function name of model view mapper to display material freight data in view
    Public Const MOD_VIEW_MAP_FREIGHT_VIEW_DISPLAY As String = "freight_view"
    'function name of model view mapper to display GUMs data in view
    Public Const MOD_VIEW_MAP_GUM_VIEW_DISPLAY As String = "surcharge_gum_view"
    'function name of model view mapper to display ECOTAX data in view
    Public Const MOD_VIEW_MAP_ECOTAX_VIEW_DISPLAY As String = "surcharge_eco_view"
    'function name of model view mapper to display DOWNPayment data in view
    Public Const MOD_VIEW_MAP_DOWNPAYMENT_VIEW_DISPLAY As String = "downpayment_view"
    'function name of model view mapper to display DISCOUNT data in view
    Public Const MOD_VIEW_MAP_DISCOUNT_VIEW_DISPLAY As String = "discount_view"
    'function name of model view mapper to display NTE data in view
    Public Const MOD_VIEW_MAP_NTE_VIEW_DISPLAY As String = "nte_view"
    'function name of model view mapper to display quote data in view
    Public Const MOD_VIEW_MAP_QUOTE_VIEW_DISPLAY As String = "quote_view"
    'function name of model view mapper to display quote pricing summary data in view
    Public Const MOD_VIEW_MAP_QUOTE_ENTRIES_VIEW_DISPLAY As String = "quote_entries_view"
    'function name of model view mapper to display activities data in view
    Public Const MOD_VIEW_MAP_ADD_SCOP_ACT_VIEW_DISPLAY As String = "activity_view"
    'function name of model view mapper to conf constants data in db
    Public Const MOD_VIEW_MAP_CONST_DB_READ As String = "general_const_read_db"
    'function name of model view mapper to list of values
    Public Const MOD_VIEW_MAP_LIST_VAL_DB_READ As String = "general_list_of_value_read_db"
    'function name of model view mapper to list of values that are editable
    Public Const MOD_VIEW_MAP_EDIT_LIST_VAL_DB_READ As String = "general_list_of_value_editable_read_db"
    'function name of model view mapper to display labor rate std 
    Public Const MOD_VIEW_MAP_LAB_RATE_STD_VIEW_DISPLAY As String = "labor_rate_std_view"
    'function name of model view mapper to display labor rate oem 
    Public Const MOD_VIEW_MAP_LAB_RATE_OEM_VIEW_DISPLAY As String = "labor_rate_oem_view"
    'function name of model view mapper to display labor rate nte 
    Public Const MOD_VIEW_MAP_LAB_RATE_NTE_VIEW_DISPLAY As String = "labor_rate_nte_view"
    'function name of model view mapper to display task rate nte 
    Public Const MOD_VIEW_MAP_TASK_RATE_VIEW_DISPLAY As String = "task_rate_view"
    'function name of model view mapper to display hours init scope 
    Public Const MOD_VIEW_MAP_INIT_HOURS_VIEW_DISPLAY As String = "init_scope_hours_view"
    'function name of model view mapper to display hours add scope
    Public Const MOD_VIEW_MAP_ADD_HOURS_VIEW_DISPLAY As String = "add_scope_hours_view"
    'function name of model view mapper to display activity edit view
    Public Const MOD_VIEW_MAP_ACT_EDIT_VIEW_DISPLAY As String = "activity_edit_view"
    'function name of model view mapper to display material edit view
    Public Const MOD_VIEW_MAP_MAT_EDIT_VIEW_DISPLAY As String = "material_edit_view"
    'function name of model view mapper to display activity edit view
    Public Const MOD_VIEW_MAP_ACT_EDIT_IN_MAT_EDIT_VIEW_DISPLAY As String = "activity_edit_in_material_edit_view"
    'function name of model view mapper to display material main view
    Public Const MOD_VIEW_MAP_MAT_MAIN_VIEW_DISPLAY As String = "material_main_view"
    'function name of model view mapper to display activity calculations
    Public Const MOD_VIEW_MAP_ACTT_CALC_VIEW_DISPLAY As String = "activity_calculation_details_view"
    'function name of model view mapper to display admin view
    Public Const MOD_VIEW_MAP_ADMIN_EDIT_VIEW_DISPLAY As String = "admin_edit_view"
    'function name of model view mapper to display awq data in view
    Public Const MOD_VIEW_MAP_AWQ_VIEW_DISPLAY As String = "awq_main_view"
    'function name of model view mapper to display awq edit view
    Public Const MOD_VIEW_MAP_AWQ_EDIT_VIEW_DISPLAY As String = "awq_edit_view"
    'function name of model view mapper to display editable lov view
    Public Const MOD_VIEW_MAP_LOV_EDIT_VIEW_DISPLAY As String = "lov_edit_view"
    'function name of model view mapper to display AWQ Labor transferable 
    Public Const MOD_VIEW_MAP_AWQ_LABOR_TRANSFER_VIEW_DISPLAY As String = "transfer_labor_awq_view"
    'function name of model view mapper to display AWQ material transferable 
    Public Const MOD_VIEW_MAP_AWQ_MATERIAL_TRANSFER_VIEW_DISPLAY As String = "transfer_mat_awq_view"
    'function name of model view mapper to display New Activity transfered Labor  
    Public Const MOD_VIEW_MAP_ACT_LABOR_TRANSFERED_VIEW_DISPLAY As String = "transfer_labor_new_act_view"
    'function name of model view mapper to display New Activity transfered Material 
    Public Const MOD_VIEW_MAP_ACT_MATERIAL_TRANSFERED_VIEW_DISPLAY As String = "transfer_mat_new_act_view"
    'function name of model view mapper to display labor conf transfer CC data in view
    Public Const MOD_VIEW_MAP_LAB_CTRL_CONF_TRANSF_CC_VIEW_DISPLAY As String = "labor_ctrl_conf_transfer_cc_view"
    'function name of model view mapper to display labor controlling main view
    Public Const MOD_VIEW_MAP_LAB_CTRL_MAIN_VIEW_DISPLAY As String = "labor_ctrl_main_view"
    'edit labor ctrling object actual hours view
    Public Const MOD_VIEW_MAP_LAB_CTRL_ACT_HRS_VIEW As String = "actual_booking_list_view"
    'edit labor ctrling object deviation view gain
    Public Const MOD_VIEW_MAP_LAB_CTRL_GAIN_VIEW As String = "dev_just_gain_view"
    'edit labor ctrling object deviation view overshoot
    Public Const MOD_VIEW_MAP_LAB_CTRL_OVERSHOT_VIEW As String = "dev_just_overshoot_view"
    'edit labor ctrling object transfer budget to view
    Public Const MOD_VIEW_MAP_LAB_CTRL_BUDGET_TO_VIEW As String = "budget_transfer_to_view"
    'edit labor ctrling object transfer budget from view
    Public Const MOD_VIEW_MAP_LAB_CTRL_BUDGET_FROM_VIEW As String = "budget_transfer_from_view"
    'edit labor ctrling object transfer actual from
    Public Const MOD_VIEW_MAP_LAB_CTRL_ACTUAL_TO_VIEW As String = "actual_transfer_to_view"
    'edit labor ctrling object transfer actual to
    Public Const MOD_VIEW_MAP_LAB_CTRL_ACTUAL_FROM_VIEW As String = "actual_transfer_from_view"
    'edit labor ctrling object transfer actual to
    Public Const MOD_VIEW_MAP_LAB_CTRL_OBJ_VIEW As String = "labor_control_objet_view"
    'function name of model view mapper to display app info view
    Public Const MOD_VIEW_MAP_APPLICATION_INFO_VIEW As String = "app_info_list_view"

    'reporting conf constants
    Public CONST_REPORT_CONF_ITEM_TYPE_COL = "column"
    Public CONST_REPORT_CONF_ITEM_TYPE_CELL = "cell"
    'redmark refer to quote text
    Public CONST_RED_MARK_REFER_TO_QUOTE_TXT = "red_mark_refer_to_quote_text"
    'redmark refer to quote text
    Public CONST_RED_MARK_VAL_ANALYSIS_CNCL_CFWD_TXT = "red_mark_cancel_cfwd_text"
    'debug mode
    Public CONST_CONF_DEBUG_MODE As String = "system_debug_mode"
    'auto save
    Public CONST_CONF_AUTO_SAVE_MODE_ENABLE As String = "auto_save_mode_enable"
    'auto save elapsed
    Public CONST_CONF_AUTO_SAVE_MODE_ELAPSE_MS As String = "auto_save_mode_elapse_milli_second"

    'activate datavalidator on activity view
    Public Const CONST_CONF_ACT_VIEW_VALIDATOR_ACTIVE As String = "main_activity_view_validator_active"

    'columns internal name exchange rate
    Public Const CONST_CONF_BILL_EXCG_RATE_FROM As String = "from"
    Public Const CONST_CONF_BILL_EXCG_RATE_TO As String = "to"
    Public Const CONST_CONF_BILL_EXCG_RATE_RATE As String = "rate"
    Public Const CONST_CONF_BILL_EXCG_RATE_DATE As String = "date"

    'columns internal name material markup
    Public Const CONST_CONF_BILL_MAT_MKP_LABEL As String = "label"
    Public Const CONST_CONF_BILL_MAT_MKP_PRICE As String = "price"
    Public Const CONST_CONF_BILL_MAT_MKP_CURR As String = "curr"
    Public Const CONST_CONF_BILL_MAT_MKP_PERCENT As String = "percent"

    'columns internal name material freight view
    Public Const CONST_CONF_BILL_FREIGHT_LABEL As String = "label"
    Public Const CONST_CONF_BILL_FREIGHT_PRICE As String = "price"
    Public Const CONST_CONF_BILL_FREIGHT_CURR As String = "curr"
    Public Const CONST_CONF_BILL_FREIGHT_FR_TYPE As String = "freight_type"

    'columns internal name surcharge view
    Public Const CONST_CONF_BILL_SURCH_LABEL As String = "label"
    Public Const CONST_CONF_BILL_SURCH_RATE As String = "rate"
    Public Const CONST_CONF_BILL_SURCH_CAP As String = "cap_value"
    Public Const CONST_CONF_BILL_SURCH_CURR As String = "curr"

    'columns internal name discounr view
    Public Const CONST_CONF_BILL_DISC_LABEL As String = "label"
    Public Const CONST_CONF_BILL_DISC_SCOPE_APP As String = "scope_app"
    Public Const CONST_CONF_BILL_DISC_LABMAT_APP As String = "lab_mat_app"
    Public Const CONST_CONF_BILL_DISC_IS_FLAT As String = "is_flat"
    Public Const CONST_CONF_BILL_DISC_RATE As String = "rate"
    Public Const CONST_CONF_BILL_DISC_CAP_VAL As String = "cap_value"
    Public Const CONST_CONF_BILL_DISC_CAP_CURR As String = "cap_value_curr"
    Public Const CONST_CONF_BILL_DISC_DISC_VAL As String = "discount_value"
    Public Const CONST_CONF_BILL_DISC_DISC_VAL_CURR As String = "discount_value_curr"

    'columns internal name AWQ lab mat applicability view
    Public Const CONST_CONF_AWQ_ADD_LABMAT As String = "add_awq_lab_mat"
    Public Const CONST_CONF_AWQ_REMOVE_LABMAT As String = "remove_awq_lab_mat"
    'columns internal name prodct summary lab mat applicability view
    Public Const CONST_CONF_QUOTE_PRODUCT_SUMM_ADD_LABMAT As String = "add_product_summary_lab_mat"

    'columns internal name nte view
    Public Const CONST_CONF_BILL_NTE_LABEL As String = "label"
    Public Const CONST_CONF_BILL_NTE_DESC As String = "description"
    Public Const CONST_CONF_BILL_NTE_LABMAT_APP As String = "lab_mat_app"
    Public Const CONST_CONF_BILL_NTE_VALUE As String = "nte_value"
    Public Const CONST_CONF_BILL_NTE_CURR As String = "curr"
    Public Const CONST_CONF_BILL_NTE_TYPE As String = "nte_type"
    Public Const CONST_CONF_BILL_NTE_SCOPE As String = "scope_app"

    'columns internal name labor rate view
    Public Const CONST_CONF_LABRATE_LABEL As String = "label"
    Public Const CONST_CONF_LABRATE_PAYER As String = "payer"
    Public Const CONST_CONF_LABRATE_SAPID As String = "sap_id"
    Public Const CONST_CONF_LABRATE_RATE As String = "rate"
    Public Const CONST_CONF_LABRATE_CURR As String = "curr"

    'columns internal name quotes view
    Public Const CONST_CONF_QUOTE_NR As String = "number"
    Public Const CONST_CONF_QUOTE_DESC As String = "description"
    Public Const CONST_CONF_QUOTE_LAB_CURR As String = "lab_curr"
    Public Const CONST_CONF_QUOTE_MAT_CURR As String = "mat_curr"
    Public Const CONST_CONF_QUOTE_PAYER As String = "payer"

    'columns internal name quotes summary entries view
    Public Const CONST_CONF_QUOTE_ENTRY_QUOTE As String = "quote_id"
    Public Const CONST_CONF_QUOTE_ENTRY_PRDCT As String = "product"
    Public Const CONST_CONF_QUOTE_ENTRY_LAB As String = "labor_price"
    Public Const CONST_CONF_QUOTE_ENTRY_LAB_ADJ As String = "labor_price_adj"
    Public Const CONST_CONF_QUOTE_ENTRY_LAB_CURR As String = "labor_curr"
    Public Const CONST_CONF_QUOTE_ENTRY_MAT As String = "material_price"
    Public Const CONST_CONF_QUOTE_ENTRY_REP As String = "repair_price"
    Public Const CONST_CONF_QUOTE_ENTRY_3rdP As String = "third_party_price"
    Public Const CONST_CONF_QUOTE_ENTRY_FREIGHT As String = "freight_price"
    Public Const CONST_CONF_QUOTE_ENTRY_MAT_ADJ As String = "material_price_adj"
    Public Const CONST_CONF_QUOTE_ENTRY_REP_ADJ As String = "repair_price_adj"
    Public Const CONST_CONF_QUOTE_ENTRY_3rdP_ADJ As String = "third_party_price_adj"
    Public Const CONST_CONF_QUOTE_ENTRY_FREIGHT_ADJ As String = "freight_price_adj"
    Public Const CONST_CONF_QUOTE_ENTRY_MAT_CURR As String = "material_curr"
    Public Const CONST_CONF_QUOTE_ENTRY_MAIN_ACT As String = "main_activity"
    Public Const CONST_CONF_QUOTE_ENTRY_DEP_ACTS As String = "dependent_activities"
    Public Const CONST_CONF_QUOTE_ENTRY_SET_ACT As String = "set_activities"

    'transfer awq data col
    Public Const CONST_CONF_TRANSFER_FAKE_ACT_CANCEL As String = "cancel"
    Public Const CONST_CONF_TRANSFER_FAKE_ACT_AS_NEW As String = "add_as_new"
    Public Const CONST_CONF_TRANSFER_FAKE_ACT_AS_EXISTING As String = "add_as_existing"

    'edit activity controls names
    Public Const CONST_EDIT_ACT_NTE_CB As String = "act_nte_val"
    Public Const CONST_EDIT_ACT_DISC_CB As String = "act_discount_val"
    Public Const CONST_EDIT_ACT_PROJ_CB As String = "act_proj_val"
    'edit activity controls names
    Public Const CONST_EDIT_MAT_ACT_CB As String = "change_activity_val"
    Public Const CONST_EDIT_MAT_NTE_CB As String = "mat_nte_val"
    Public Const CONST_EDIT_MAT_DISC_CB As String = "mat_discount_val"
    Public Const CONST_EDIT_MAT_PAYER_CB As String = "mat_payer_val"
    Public Const CONST_EDIT_MAT_FREIGHT_ZFRP_CB As String = "freight_zfrp_val"

    'columns internal name activities view
    Public Const CONST_CONF_ACT_NTE_CAT As String = "nte_category"
    Public Const CONST_CONF_ACT_DISC As String = "discount"
    Public Const CONST_CONF_ACT_EDIT As String = "edit"

    'columns internal name sold hrs view additional scope
    Public Const CONST_CONF_SOLD_HRS_CC As String = "cc"
    Public Const CONST_CONF_SOLD_HRS_PAYER As String = "payer"
    Public Const CONST_CONF_SOLD_HRS_DISC As String = "discount"
    Public Const CONST_CONF_SOLD_HRS_NTE As String = "nte_category"
    Public Const CONST_CONF_SOLD_HRS_TASK_RATE As String = "task_rate"
    Public Const CONST_CONF_MAT_EDIT As String = "edit"

    'columns internal name downpayment view 
    Public Const CONST_CONF_DOWMPAYMENT_PAYER As String = "payer"

    'columns internal name material main
    Public Const CONST_CONF_MAT_IS_REPORTED As String = "is_reported"
    Public Const CONST_CONF_MAT_MAIN_ACT As String = "activity_id"
    Public Const CONST_CONF_MAT_MAIN_PAYER As String = "payer"
    Public Const CONST_CONF_MAT_MAIN_DISC As String = "discount_id"
    Public Const CONST_CONF_MAT_MAIN_NTE As String = "nte_id"
    Public Const CONST_CONF_MAT_MAIN_FREIGHT_ZFRA As String = "overwrite_freight_zfra"
    Public Const CONST_CONF_MAT_MAIN_MARKUP_ZMAR As String = "overwrite_markup_zmar"
    Public Const CONST_CONF_MAT_MAIN_FREIGHT_ZFRP As String = "freight_zfrp"

    'columns internal labor controling transfers
    Public Const CONST_CONF_TRANSFER_HOURS_TO_ACT As String = "to_act_id"
    Public Const CONST_CONF_TRANSFER_HOURS_TO_CC As String = "to_cc"

    'dev just scope col
    Public Const CONST_CONF_DEV_JUST_SCOPE As String = "scope"

    'awq main view columns
    Public Const CONST_CONF_AWQ_EDIT As String = "edit"

    'labor controlling main view columns
    Public Const CONST_CONF_LABOR_CTRL_EDIT As String = "edit"

    'system constant
    Public Const CONST_SYS_ROW_DELETED = "sys_deleted"

    'awq revision prefix awq_revision_prefix
    Public Const CONST_CONF_AWQ_REVISION_PREFIX As String = "awq_revision_prefix"

    'constant keys from cust stat excel file
    'date format
    Public Const CONST_CONF_DATE_FORMAT As String = "date_format"
    Public Const CONST_CONF_DATE_TIME_FORMAT As String = "date_time_format"
    'planning files for salesforce
    Public Const CONST_CONF_PLAN_TASK_F As String = "planning_task_file"
    Public Const CONST_CONF_PLAN_PROJ_F As String = "planning_project_file"
    Public Const CONST_CONF_PROJ_PARAM As String = "project_parameter"
    Public Const CONST_CONF_DATE_PARAM As String = "date_parameter"
    Public Const CONST_CONF_AWQ_PARAM As String = "awq_parameter"
    Public Const CONST_CONF_PAYER_PARAM As String = "payer_parameter"

    'datagridview_double_format
    Public Const CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT As String = "datagridview_double_format"

    'customer release reports file names
    Public Const CONST_CONF_RELEASE_CUST_REPORT_NAME As String = "release_to_customer_report_name"
    Public Const CONST_CONF_RELEASE_CUST_REPORT_TMPL_FILE As String = "release_to_customer_template_file"
    'awq
    Public Const CONST_CONF_RELEASE_CUST_REPORT_AWQ_TAB_NAME As String = "release_to_customer_report_awq_tab_name"
    'proj sum
    Public Const CONST_CONF_RELEASE_CUST_REPORT_PROJSUM_TAB_NAME As String = "release_to_customer_report_projsum_tab_name"
    'proj details
    Public Const CONST_CONF_RELEASE_CUST_REPORT_PROJDETAIL_TAB_NAME As String = "release_to_customer_report_projdetails_tab_name"
    'proj details
    Public Const CONST_CONF_RELEASE_CUST_REPORT_LIGHTPROJDETAIL_TAB_NAME As String = "release_to_customer_report_lightprojdetails_tab_name"
    'proj down payment
    Public Const CONST_CONF_RELEASE_CUST_REPORT_DOWN_PAY_TAB_NAME As String = "release_to_customer_report_down_pay_tab_name"
    'nte details list
    Public Const CONST_CONF_RELEASE_CUST_REPORT_NTE_DETAILS_TAB_NAME As String = "release_to_customer_report_nte_det_tab_name"

    'AWQ form reports file names
    Public Const CONST_CONF_AWQ_FORM_REPORT_NAME As String = "awq_form_report_name"
    Public Const CONST_CONF_AWQ_FORM_REPORT_TMPL_FILE As String = "awq_form_report_template_file"
    'awq
    Public Const CONST_CONF_AWQ_FORM_FRONT_PAGE_TAB_NAME As String = "awq_form_front_page_tab_name"
    'details page
    Public Const CONST_CONF_AWQ_FORM_DETAILS_PAGE_TAB_NAME As String = "awq_form_details_tab_name"

    'Controlling labor report template
    Public Const CONST_CONF_HRS_STATEMENT_REPORT_NAME As String = "hours_statement_report_name"
    Public Const CONST_CONF_HRS_STATEMENT_TMPL_FILE As String = "hours_statement_report_template_file"

    'Summary sheet
    Public Const CONST_CONF_HRS_STATEMENT_SUM_TAB_NAME As String = "hours_statement_report_sum_tab_name"
    'hours data sheet
    Public Const CONST_CONF_HRS_STATEMENT_DATA_TAB_NAME As String = "hours_statement_report_hours_tab_name"
    'budget adj list sheet
    Public Const CONST_CONF_HRS_STATEMENT_BUDG_ADJ_TAB_NAME As String = "hours_statement_report_budget_adj_tab_name"
    'paramaters to get the hours statement saving path
    Public Const CONST_CONF_HRS_STATEMENT_SAVE_PATH As String = "hours_statement_save_path"
    Public Const CONST_CONF_HRS_STATEMENT_ROOT_BROWSE_UP_LVL As String = "hours_statement_save_root_browse_up_level"

    'Data check report template
    Public Const CONST_CONF_DATA_CHCK_REPORT_NAME As String = "data_chck_report_name"
    Public Const CONST_CONF_DATA_CHCK_TEMPLATE_FILE As String = "data_chck_report_template_file"
    'data check summary page
    Public Const CONST_CONF_DATA_CHK_SUMM_TAB_NAME As String = "data_chck_report_sum_tab"
    'data check details page
    Public Const CONST_CONF_DATA_CHK_DETAILS_TAB_NAME As String = "data_chck_report_details_tab"


    'awq below threshold customer status
    Public Const CONST_CONF_RELEASE_CUST_REPORT_PROJDETAIL_BEL_THR_CUST_STAT As String = "release_to_cust_proj_details_below_thr_cust_status"
    'A/C group list Dassault key
    Public Const CONST_CONF_PROJ_AC_GRP_DASS_KEY As String = "ac_group_dassaul_key"
    'labor rate type values
    Public Const CONST_CONF_LAB_RATE_TYPE_STD As String = "rate_type_standard"
    Public Const CONST_CONF_LAB_RATE_TYPE_NTE As String = "rate_type_nte"
    Public Const CONST_CONF_LAB_RATE_TYPE_OEM As String = "rate_type_oem"
    'scope import constants
    Public Const CONST_CONF_SCOPE_IMP_FILE_EXT As String = "zimro_file_extension"
    Public Const CONST_CONF_SCOPE_IMP_FILE_CONN_STR As String = "zimro_file_connexion_string"
    'scope import constants
    Public Const CONST_CONF_CN47N_IMP_FILE_EXT As String = "cn47n_file_extension"
    Public Const CONST_CONF_CN47N_IMP_FILE_CONN_STR As String = "cn47n_file_connexion_string"
    'actual labor import constants
    Public Const CONST_CONF_CIJ3_IMP_FILE_EXT As String = "cij3_file_extension"
    Public Const CONST_CONF_CIJ3_IMP_FILE_CONN_STR As String = "cij3_file_connexion_string"
    'material import constants
    Public Const CONST_CONF_MAT_IMP_FILE_EXT As String = "zmel_file_extension"
    Public Const CONST_CONF_MAT_IMP_FILE_DELIM As String = "zmel_file_delimiter"
    Public Const CONST_CONF_MAT_IMP_FILE_FIRST_COL As String = "zmel_file_first_col"
    Public Const CONST_CONF_MAT_IMP_FILE_PROJ_DIGIT As String = "zmel_proj_number_digit"

    'material info line start
    Public Const CONST_CONF_MAT_INFO_LINE_START As String = "material_info_line_start"
    Public Const CONST_CONF_MAT_INFO_SN_PREFIX As String = "material_info_sn_prefix"
    Public Const CONST_CONF_MAT_INFO_FREIGHT_PREFIX As String = "material_info_freight_prefix"
    Public Const CONST_CONF_MAT_INFO_ZCUS_PREFIX As String = "material_info_zcus_prefix"
    Public Const CONST_CONF_MAT_INFO_SMALL_MAT_TXT As String = "material_info_small_mat_text"
    Public Const CONST_CONF_MAT_INFO_REMOVED_AWQ_PREF As String = "material_info_removed_awq_prefix"

    'activity summary table labels
    Public Const CONST_CONF_ACT_CALC_SUM_BEF_DISC As String = "act_calc_summary_bef_disc_key"
    Public Const CONST_CONF_ACT_CALC_SUM_DISC As String = "act_calc_summary_disc_key"
    Public Const CONST_CONF_ACT_CALC_SUM_ACT_DISC As String = "act_calc_summary_act_disc_key"
    Public Const CONST_CONF_ACT_CALC_SUM_AFT_DISC As String = "act_calc_summary_after_disc_key"

    'activity summary  scope labels
    Public Const CONST_CONF_ACT_CALC_SUM_SCP_INI As String = "act_calc_sum_scope_init_key"
    Public Const CONST_CONF_ACT_CALC_SUM_SCP_INI_ADJ As String = "act_calc_sum_scope_init_adj_key"
    Public Const CONST_CONF_ACT_CALC_SUM_SCP_ADD As String = "act_calc_sum_scope_add_key"
    Public Const CONST_CONF_ACT_CALC_SUM_SCP_TOTAL As String = "act_calc_sum_scope_total_key"
    'key for reports where add scope has no price
    Public Const CONST_CONF_ACT_CALC_SUM_SCP_ADD_REPORT_NO_PRICE As String = "act_calc_sum_scope_add_report_key"
    'cust feedback N/A
    Public Const CONST_CONF_REPORT_CUST_FEEDBACK_NA As String = "customer_report_cust_feedback_not_applicable"

    Public Const CONST_CONF_ACT_CALC_SUM_SCP_AWQ_QUOTE_TXT As String = "act_calc_sum_scope_awq_quote_txt"


    'freeze first col of activity view
    Public Const CONST_CONF_ACT_VIEW_FREEZE_FIRST_COL_CNT As String = "activity_view_freeze_col_count"
    'freeze first col of material view
    Public Const CONST_CONF_MAT_VIEW_FREEZE_FIRST_COL_CNT As String = "material_view_freeze_col_count"
    'freeze first col of awq view
    Public Const CONST_CONF_AWQ_VIEW_FREEZE_FIRST_COL_CNT As String = "awq_view_freeze_col_count"


    'default empty key
    Public Const CONST_CONF_LOV_EMPTY_KEY As String = "lov_empty_key"
    Public CONST_CONF_LOV_EMPTY_VAL As String
    Public Const CONST_CONF_LOV_EMPTY_LNG_KEY As String = "lov_empty_key_lng"
    Public Const CONST_CONF_LOV_OVERWRITABLE_DEFAULT_LABEL = "lov_overwritable_list_default_label"
    'warr code values for nte and ntey
    Public Const CONST_CONF_WARR_CODE_NTE_KEY As String = "warr_code_nte_value"
    Public Const CONST_CONF_WARR_CODE_NTE_EXCL_KEY As String = "warr_code_nte_excl_value"
    'warr code values for nte and ntey=>Labor rate list
    Public Const CONST_CONF_WARR_CODE_NTE_LAB_RATE_KEY As String = "warr_code_nte_labor_rate"
    Public Const CONST_CONF_WARR_CODE_NTE_EXC_LAB_RATEL_KEY As String = "warr_code_nte_excl_labor_rate"
    'customer payer default key
    Public Const CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER = "default_payer_as_customer_key"

    'discount app keys
    Public Const CONST_CONF_LOV_DISC_SCOPE_APP_PROJ_KEY As String = "lov_disc_scope_app_proj_key"
    Public Const CONST_CONF_LOV_DISC_SCOPE_APP_ACT_KEY As String = "lov_disc_scope_app_act_key"
    Public Const CONST_CONF_LOV_DISC_SCOPE_APP_MAT_PO_KEY As String = "lov_disc_scope_app_mat_po_key"
    Public Const CONST_CONF_LOV_DISC_SCOPE_APP_LAB_CC_KEY As String = "lov_disc_scope_app_lab_cc_key"
    'nte app keys
    Public Const CONST_CONF_LOV_NTE_SCOPE_APP_ACT_KEY As String = "lov_nte_scope_app_act_key"
    Public Const CONST_CONF_LOV_NTE_SCOPE_APP_MAT_PO_KEY As String = "lov_nte_scope_app_mat_po_key"
    Public Const CONST_CONF_LOV_NTE_SCOPE_APP_LAB_CC_KEY As String = "lov_nte_scope_app_lab_cc_key"
    'mat lab app
    Public Const CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_SERV_KEY As String = "lov_labmat_app_lab_mat_rep_frgh_serv_key"
    Public Const CONST_CONF_LOV_LABMAT_APP_LAB_KEY As String = "lov_labmat_app_lab_key"
    Public Const CONST_CONF_LOV_LABMAT_APP_MAT_KEY As String = "lov_labmat_app_mat_key"
    Public Const CONST_CONF_LOV_LABMAT_APP_REP_KEY As String = "lov_labmat_app_rep_key"
    Public Const CONST_CONF_LOV_LABMAT_APP_SERV_KEY As String = "lov_labmat_app_serv_key"
    Public Const CONST_CONF_LOV_LABMAT_APP_FREIGHT_KEY As String = "lov_labmat_app_frgh_key"
    Public Const CONST_CONF_LOV_LABMAT_APP_MAT_REP_FRGH_SERV_KEY As String = "lov_labmat_app_mat_rep_frgh_serv_key"
    Public Const CONST_CONF_LOV_LABMAT_APP_LAB_MAT_KEY As String = "lov_labmat_app_lab_mat_key"
    Public Const CONST_CONF_LOV_LABMAT_APP_MAT_REP_KEY As String = "lov_labmat_app_mat_rep_key"
    Public Const CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_KEY As String = "lov_labmat_app_lab_mat_rep_key"
    Public Const CONST_CONF_LOV_LABMAT_APP_MAT_REP_FRGH_KEY As String = "lov_labmat_app_mat_rep_frgh_key"
    Public Const CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_KEY As String = "lov_labmat_app_lab_mat_rep_frgh_key"

    'material type keys
    Public Const CONST_CONF_LOV_MAT_TYPE_MAT_KEY As String = "mat_type_list_mat_key"
    Public Const CONST_CONF_LOV_MAT_TYPE_REP_KEY As String = "mat_type_list_rep_key"
    Public Const CONST_CONF_LOV_MAT_TYPE_3RD_P_KEY As String = "mat_type_list_3rdp_key"
    Public Const CONST_CONF_LOV_MAT_TYPE_FREIGHT_KEY As String = "mat_type_list_freight_key"
    'value analysis keys
    Public Const CONST_CONF_LOV_VAL_ANAL_ADD_V_KEY As String = "lov_value_anal_value_added_key"
    Public Const CONST_CONF_LOV_VAL_ANAL_NOT_ADD_V_KEY As String = "lov_value_anal_non_value_added_key"
    Public Const CONST_CONF_LOV_VAL_ANAL_WAST_KEY As String = "lov_value_anal_waste_key"
    Public Const CONST_CONF_LOV_VAL_ANAL_CANC_FWD_KEY As String = "lov_value_anal_cancel_cfwd_key"
    'red mark keys
    Public Const CONST_CONF_LOV_RED_MARK_TBA_KEY As String = "red_mark_list_tba_key"
    Public Const CONST_CONF_LOV_RED_MARK_AS_PER_RES_KEY As String = "red_mark_list_as_per_result_key"
    Public Const CONST_CONF_LOV_RED_MARK_FOC_KEY As String = "red_mark_list_foc_key"
    Public Const CONST_CONF_LOV_RED_MARK_RISK_RESV_KEY As String = "red_mark_risk_reserve_key"

    'lov keys of AWQ internal status
    Public Const CONST_CONF_LOV_AWQ_STAT_NOT_START_KEY As String = "awq_internal_stat_costing_not_started"
    Public Const CONST_CONF_LOV_AWQ_STAT_QRF_PROGR_KEY As String = "awq_internal_stat_qrf_in_progress"
    Public Const CONST_CONF_LOV_AWQ_STAT_WAIT_CUST_INF_KEY As String = "awq_internal_stat_waiting_cust_info"
    Public Const CONST_CONF_LOV_AWQ_STAT_WAIT_CUST_MAT_P_KEY As String = "awq_internal_stat_waiting_cust_mat_info_price"
    Public Const CONST_CONF_LOV_AWQ_STAT_COST_IN_PROGR_KEY As String = "awq_internal_stat_costing_in_progress"
    Public Const CONST_CONF_LOV_AWQ_STAT_QUOTE_READY_KEY As String = "awq_internal_stat_quote_ready"
    Public Const CONST_CONF_LOV_AWQ_STAT_WAIT_PM_APP_KEY As String = "awq_internal_stat_waiting_pm_approval"
    Public Const CONST_CONF_LOV_AWQ_STAT_RELEASED_KEY As String = "awq_internal_stat_released"
    Public Const CONST_CONF_LOV_AWQ_STAT_ARCHIVED_KEY As String = "awq_internal_stat_archived"
    Public Const CONST_CONF_LOV_AWQ_STAT_SUPERS_REVISED_KEY As String = "awq_internal_stat_superseded_revised"
    Public Const CONST_CONF_LOV_AWQ_STAT_CANCEL_KEY As String = "awq_internal_stat_cancelled"
    Public Const CONST_CONF_LOV_AWQ_STAT_CLOSED_KEY As String = "awq_internal_stat_closed"
    'lov keys of AWQ customer feedback
    Public Const CONST_CONF_LOV_AWQ_CUST_FBK_APPROVED_KEY As String = "awq_cust_feedback_approved_key"
    Public Const CONST_CONF_LOV_AWQ_CUST_FBK_SUPERS_REVISED_KEY As String = "awq_cust_feedback_superseded_rev_key"
    Public Const CONST_CONF_LOV_AWQ_CUST_FBK_DECLINED_KEY As String = "awq_cust_feedback_declined_key"
    Public Const CONST_CONF_LOV_AWQ_CUST_FBK_PENDING_KEY As String = "awq_cust_feedback_pending_key"
    'awq default values
    Public Const CONST_CONF_AWQ_GROUND_TIME_DEFAULT As String = "awq_ground_time_default_value"
    Public Const CONST_CONF_AWQ_AIRWORTHY_LOV_NO_IMP_KEY As String = "awq_airworthiness_lov_no_imp_key"
    Public Const CONST_CONF_AWQ_WEIGHT_BAL_DEFAULT As String = "awq_weight_and_bal_default_value"
    Public Const CONST_CONF_AWQ_PRICING_LOV_FIX_KEY As String = "awq_pricing_lov_fix_key"
    Public Const CONST_CONF_AWQ_PAYMENT_COND_DEFAULT As String = "awq_payment_terms_default_value"

    'down payment labmat applicability keys
    Public Const CONST_CONF_LOV_DP_LABMAT_APP_LABMAT_KEY As String = "dp_labmat_app_lov_labmat_key"
    Public Const CONST_CONF_LOV_DP_LABMAT_APP_LAB_KEY As String = "dp_labmat_app_lov_lab_key"
    Public Const CONST_CONF_LOV_DP_LABMAT_APP_MAT_KEY As String = "dp_labmat_app_lov_mat_key"

    'down payment payment status keys
    Public Const CONST_CONF_LOV_DP_PAYMENT_STAT_UNPAID_KEY As String = "dp_payment_status_lov_unpaid_key"
    Public Const CONST_CONF_LOV_DP_PAYMENT_STAT_PAID_KEY As String = "dp_payment_status_lov_paid_key"
    Public Const CONST_CONF_LOV_DP_PAYMENT_STAT_CANCEL_KEY As String = "dp_payment_status_lov_cancel_key"
    'DP fake status
    Public Const CONST_CONF_LOV_DP_PAYMENT_STAT_UNPAID_DUE_TXT As String = "dp_payment_status_lov_unpaid_due_text"
    Public Const CONST_CONF_LOV_DP_PAYMENT_STAT_UNPAID_NOT_DUE_TXT As String = "dp_payment_status_lov_unpaid_not_due_text"

    'orrea value for 3rd party
    Public Const CONST_CONF_MAT_TYPE_3RD_P_OR_REA_VALUE As String = "mat_type_third_party_or_rea_value"

    'nte key 
    Public Const CONST_CONF_LOV_NTE_TYPE_NTEY_KEY As String = "lov_nte_type_ntey_key"
    Public Const CONST_CONF_LOV_NTE_TYPE_NTEN_KEY As String = "lov_nte_type_nten_key"
    'billing open status key
    Public Const CONST_CONF_LOV_BILL_STATUS_OPEN_KEY As String = "billing_working_status_open"

    'conf lov type :lov
    Public Const CONST_CONF_LOV_TYPE_LOV As String = "lov_type_lov"
    Public Const CONST_CONF_LOV_TYPE_LOV_DEP As String = "lov_type_dependency"
    'surcharge types
    Public Const CONST_CONF_SURCHARGE_TYPE_GUM As String = "surcharge_gum"
    Public Const CONST_CONF_SURCHARGE_TYPE_ECO As String = "surcharge_ecotax"
    'column types
    Public Const CONST_CONF_COL_TYPE_TEXT As String = "col_type_text"
    Public Const CONST_CONF_COL_TYPE_COMBO As String = "col_type_combo"
    Public Const CONST_CONF_COL_TYPE_CHECK As String = "col_type_check"
    Public Const CONST_CONF_COL_TYPE_BUTTON As String = "col_type_button"
    Public Const CONST_CONF_COL_TYPE_IMAGE As String = "col_type_image"
    Public Const CONST_CONF_COL_TYPE_DATE As String = "col_type_date"

    'lov lists
    Public Const CONST_CONF_LOV_BILLING_GRP As String = "billing_group"
    Public Const CONST_CONF_LOV_AC_GRP As String = "ac_group"
    Public Const CONST_CONF_LOV_MAT_MRKUP As String = "material_markup"
    Public Const CONST_CONF_LOV_AC_GRP_DEP_MAT_MRKUP As String = "ac_group_material_markup"
    Public Const CONST_CONF_LOV_DEP_PAYER_REPORT_LAB_CURR As String = "payer_report_lab_currency"
    Public Const CONST_CONF_LOV_DEP_PAYER_REPORT_MAT_CURR As String = "payer_report_mat_currency"
    Public Const CONST_CONF_LOV_CURR As String = "currency_list"
    Public Const CONST_CONF_LOV_GUM As String = "gum_list"
    Public Const CONST_CONF_LOV_ECOTAX As String = "ecotax_list"
    Public Const CONST_CONF_LOV_LABRATE_STD As String = "lab_rate_std_list"
    Public Const CONST_CONF_LOV_AWQ_INT_STATUS As String = "awq_internal_status_list"
    Public Const CONST_CONF_LOV_AWQ_CUST_FBK As String = "awq_customer_fdbk_list"
    Public Const CONST_CONF_EDIT_LOV_ACT_CATEGORY As String = "activity_category"
    Public Const CONST_CONF_LOV_AWQ_AIRW_STATUS As String = "awq_airworthiness_list"
    Public Const CONST_CONF_LOV_PRICE_RED_MARK As String = "price_red_mark_list"
    Public Const CONST_CONF_LOV_DOWN_PAYMENT_PAY_STAT As String = "dp_payment_status_list"
    Public Const CONST_CONF_LOV_STATUS_FRIENDLY_LIST As String = "status_friendly_list"
    Public Const CONST_CONF_LOV_SAP_ID_SKILL_LIST As String = "sap_id_label_list"
    Public Const CONST_CONF_LOV_MATERIAL_TYPE_LIST As String = "material_type_list"
    Public Const CONST_CONF_LOV_MATERIAL_TRANSAC_COND_LIST As String = "mat_trans_cond_list"
    Public Const CONST_CONF_LOV_LAB_CTRL_DEV_JUST_SCOPE_LIST As String = "lab_ctrl_dev_just_scope"
    Public Const CONST_CONF_LOV_CUST_REPORT_SURCH_APPL_LIST As String = "cust_report_surch_appl_payer_list"
    Public Const CONST_CONF_LOV_DEV_JUST_OVERSHOOT_REASON As String = "dev_just_overshoot_reason"
    Public Const CONST_CONF_LOV_DEV_JUST_GAIN_REASON As String = "dev_just_gain_reason"
    Public Const CONST_CONF_LOV_LAB_MAP_APPLICABILITY_LIST As String = "labmat_app_list"
    Public Const CONST_CONF_LOV_LAB_MAP_LABOR_REDMARK_LIST As String = "soldhours_red_mark_list"

    'labor controlling scopes
    Public Const CONST_CONF_LAB_CTRL_SCOPE_ODI As String = "lab_ctrl_scope_odi"
    Public Const CONST_CONF_LAB_CTRL_SCOPE_INIT As String = "lab_ctrl_scope_init"
    Public Const CONST_CONF_LAB_CTRL_SCOPE_ADD_BLW_THR As String = "lab_ctrl_scope_add_blw_thr"
    Public Const CONST_CONF_LAB_CTRL_SCOPE_ADD_APPR As String = "lab_ctrl_scope_add_appr"
    Public Const CONST_CONF_LAB_CTRL_SCOPE_ADD_PEND As String = "lab_ctrl_scope_add_pend"
    Public Const CONST_CONF_LAB_CTRL_SCOPE_ADD_INWK As String = "lab_ctrl_scope_add_inwk"
    Public Const CONST_CONF_LAB_CTRL_SCOPE_FOC_INIT As String = "lab_ctrl_scope_foc_init"
    Public Const CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_BLW_THR As String = "lab_ctrl_scope_foc_add_blw_thr"
    Public Const CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_APPR As String = "lab_ctrl_scope_foc_add_appr"
    Public Const CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_PEND As String = "lab_ctrl_scope_foc_add_pend"
    Public Const CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_INWK As String = "lab_ctrl_scope_foc_add_inwk"

    'UoM default
    Public Const CONST_CONF_UOM_HOURS_LABEL As String = "hours_uom_label"
    Public Const CONST_CONF_UOM_EA_LABEL As String = "ea_uom_label"

    'currencies
    Public Const CONST_CONF_CURR_CHF_KEY As String = "chf_key"
    Public Const CONST_CONF_CURR_EUR_KEY As String = "eur_key"
    Public Const CONST_CONF_CURR_USD_KEY As String = "usd_key"

    'backups
    Public Const CONST_CONF_BACKUP_FILE_YESTERDAY As String = "backup_file_day_before_name"
    Public Const CONST_CONF_BACKUP_FILE_LASTWEEK As String = "backup_file_week_before_name"
    'WBS Separator
    Public Const CONST_CONF_WBS_SAP_SEPARATOR As String = "wbs_separator"

    'default activities constants
    Public Const SUPV_ACT_NUMBER = "SUPV"
    Public Const ALL_PROJ_ACT_NUMBER = "ALL_PROJ"

    'labor controling hours transfer type
    Public Const CONST_CONF_CTRL_HRS_TRANSF_TYPE_BUDGET As String = "Budget"
    Public Const CONST_CONF_CTRL_HRS_TRANSF_TYPE_ACTUAL As String = "Actual"

    'labor controling hours transfer type
    Public Const CONST_CONF_CTRL_JUST_SAVING As String = "Saving"
    Public Const CONST_CONF_CTRL_JUST_OVERSHOOT As String = "Overshoot"
    Public Const CONST_CONF_CTRL_JUST_OPENING_EAC_PREFIX As String = "OEAC "

    'fake activity prefix
    Public Const CONST_CONF_FAKE_ACT_PREFIX = "fake_activity_prefix"

    'global variable
    'markup have been loaded from db
    Public GLOB_MAT_MKP_LOADED As Boolean
    'exchange rate
    Public GLOB_EXCHG_RATE_LOADED As Boolean
    'freight have been loaded from db
    Public GLOB_MAT_FREIGHT_LOADED As Boolean
    'surcharge have been loaded from db
    Public GLOB_SURCHARGE_LOADED As Boolean
    'dowmpay have been loaded from db
    Public GLOB_DOWNPAYMENT_LOADED As Boolean
    'discount have been loaded from db
    Public GLOB_DISCOUNT_LOADED As Boolean
    'discount have been loaded from db
    Public GLOB_NTE_LOADED As Boolean
    'labor rate have been loaded
    Public GLOB_LABOR_RATE_LOADED As Boolean
    'quotes have been loaded
    Public GLOB_QUOTES_LOADED As Boolean
    'quotes entries have been loaded
    Public GLOB_QUOTE_ENTRIES_LOADED As Boolean
    'activity have been loaded
    Public GLOB_ACTIVITY_LOADED As Boolean
    'soldhrs have been loaded
    Public GLOB_SOLDHRS_LOADED As Boolean
    'actuals have been loaded
    Public GLOB_ACTUAL_HRS_LOADED As Boolean
    'hrs transfer have been loaded
    Public GLOB_TRANSFER_HRS_LOADED As Boolean
    'cc transfer have been loaded
    Public GLOB_TRANSFER_CC_LOADED As Boolean
    'deviation justification transfer have been loaded
    Public GLOB_DEV_JUST_LOADED As Boolean
    'an import is being run
    Public GLOB_SAP_IMPOT_RUNNING As Boolean
    'materials have been loaded
    Public GLOB_MATERIAL_LOADED As Boolean
    'tsk rate have been loaded from db
    Public GLOB_TASK_RATE_LOADED As Boolean
    'awq have been loaded
    Public GLOB_AWQ_LOADED As Boolean
    'awq have been loaded
    Public GLOB_SALEENTRY_TO_ACT_LK_LOADED As Boolean

    'all APP have been loaded
    Public GLOB_APP_LOADED As Boolean

    Public Const SEP_COMMA As String = ","
    Public Const SEP_DEFAULT As String = "¬"

    'revision control status
    Public Const CONST_REV_CONTROL_STATUS_NEW As String = "New"
    Public Const CONST_REV_CONTROL_STATUS_UPDATED As String = "Updated"
    Public Const CONST_REV_CONTROL_STATUS_NOCHANGE As String = "No Change"
    Public Const CONST_REV_CONTROL_STATUS_NOSAP As String = "Not in SAP"

    Public Sub initialize(ctrl As CMasterController)
        'read constants
        initializeConfConstants(ctrl)
        'read list of values
        initializeConfListOfVAlues()
        'payer list
        initializePayers(ctrl)
        'cc list
        initializeCostCenters(ctrl)

        GLOB_MAT_MKP_LOADED = False
        GLOB_EXCHG_RATE_LOADED = False
        GLOB_MAT_FREIGHT_LOADED = False
        GLOB_SURCHARGE_LOADED = False
        GLOB_DISCOUNT_LOADED = False
        GLOB_NTE_LOADED = False
        GLOB_LABOR_RATE_LOADED = False
        GLOB_QUOTES_LOADED = False
        GLOB_QUOTE_ENTRIES_LOADED = False
        GLOB_ACTIVITY_LOADED = False
        GLOB_SOLDHRS_LOADED = False
        GLOB_MATERIAL_LOADED = False
        GLOB_TASK_RATE_LOADED = False
        GLOB_SAP_IMPOT_RUNNING = False
        GLOB_AWQ_LOADED = False
        GLOB_APP_LOADED = False
        GLOB_ACTUAL_HRS_LOADED = False
        GLOB_TRANSFER_HRS_LOADED = False
        GLOB_DEV_JUST_LOADED = False
        GLOB_TRANSFER_CC_LOADED = False

        CONST_CONF_LOV_EMPTY_VAL = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        'initialize empty objects
        EMPTY_DISC = CDiscount.getEmptyDisc
        EMPTY_NTE = CNotToExceed.getEmptyNTE
        NTEN_NO_NOTE = CNotToExceed.getNTENNoNote
        EMPTY_ACT = CScopeActivity.getEmptyAct
        LABOR_CTRL_SELECT_ALL_ACT = CScopeActivity.getSelectAllAct
        LABOR_CTRL_ALL_CC = CCostCenter.getAllCC
        LABOR_CTRL_SELECT_ALL_CC = CCostCenter.getSelectAllCC
        EMPTY_TASKRATE = CTaskRate.getEmptyLabRate
        EMPTY_PAYER = CPayer.getEmptypayer
        EMPTY_QUOTE = CSalesQuote.getEmptyQuote
        EMPTY_CONFPROP = CConfProperty.getEmpty
        EMPTY_AWQ = CAWQuotation.getEmptyAWQ
        EMPTY_CALC_ENTRY = CScopeAtivityCalcEntry.getEmpty()
       
        'init culture info
        CULT_INFO = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
        CULT_INFO.NumberFormat.NumberGroupSeparator = "'"

        'defalut dgrid datetime picker
        DATE_PICKER = New CDataGridViewDateEditControl
        'min date
        APP_NULL_DATE = DATE_PICKER.MinDate
        
    End Sub
    'a lot of constants are defined in the customer statement template. This function retrieve them and store in the dictionary
    Public Sub initializeConfConstants(ctrl As CMasterController)
        Dim listO As List(Of Object)
        Dim prop As CConfProperty
        ctrl.statusBar.status_strip_sub_label = "Loading customer statement constants..."
        constantConf = New Dictionary(Of String, CConfProperty)
        Try
            listO = ctrl.csDB.performSelectObject(ctrl.modelViewMap(MOD_VIEW_MAP_CONST_DB_READ).getDefaultSelect, ctrl.modelViewMap(MOD_VIEW_MAP_CONST_DB_READ), True)
            For Each obj As Object In listO
                prop = CType(obj, CConfProperty)
                prop.db_table = MOD_VIEW_MAP_CONST_DB_READ
                If Not constantConf.Keys.Contains(prop.key) Then
                    constantConf.Add(prop.key, prop)
                Else
                    Throw New Exception("An error occured when loading constants from Excel template file. The property " & prop.key & " is declared more than once")
                End If

            Next

        Catch ex As Exception
            Throw New Exception("An error occured while loading constant data configurations from customer statement template" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        ctrl.statusBar.status_strip_sub_label = "End Loading customer statement constants..."
    End Sub
    'initialize list constant
    Public Sub initializePayers(ctrl As CMasterController)
        Dim listO As List(Of Object)
        Try
            listO = ctrl.csDB.performSelectObject(ctrl.modelViewMap(MConstants.MOD_VIEW_MAP_PAYER_DB_READ).getDefaultSelect, ctrl.modelViewMap(MConstants.MOD_VIEW_MAP_PAYER_DB_READ), True)
            'add empty payer
            Dim payerEmpty As New CPayer
            payerEmpty.payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            'do not let label empty=> it creates bug in comboboxes
            payerEmpty.label = " "
            listO.Add(payerEmpty)
            listOfPayer = New Dictionary(Of String, CPayer)
            For Each payer As CPayer In listO
                If Not listOfPayer.ContainsKey(payer.payer) Then
                    listOfPayer.Add(payer.payer, payer)
                Else
                    Throw New Exception("The key " & payer.payer & " appears more than once in the payer DB")
                End If
            Next

        Catch ex As Exception
            Throw New Exception("Error occured when reading payers list from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
    End Sub
    'initialize cost centers constant
    Public Sub initializeCostCenters(ctrl As CMasterController)
        Dim listO As List(Of Object)
        Try
            listO = ctrl.csDB.performSelectObject(ctrl.modelViewMap(MConstants.MOD_VIEW_MAP_CC_DB_READ).getDefaultSelect, ctrl.modelViewMap(MConstants.MOD_VIEW_MAP_CC_DB_READ), True)
            'add empty payer
            Dim ccEmpty As New CCostCenter
            ccEmpty.cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            'do not let label empty=> it creates bug in comboboxes
            ccEmpty.label = " "
            listO.Add(ccEmpty)

            listOfCC = New Dictionary(Of String, CCostCenter)
            For Each cc As CCostCenter In listO

                If Not listOfCC.ContainsKey(cc.cc) Then
                    listOfCC.Add(cc.cc, cc)
                Else
                    Throw New Exception("The key " & cc.cc & " appears more than once in the cc DB")
                End If
            Next
        Catch ex As Exception
            Throw New Exception("Error occured when reading cost centers list from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
    End Sub
    'a lot of list of values are used in combo box, they are retrieve in the excel file
    Public Sub initializeConfListOfVAlues()
        Dim listO As List(Of Object)

        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Loading customer statement business list of values..."

        Try
            listOfValueConf = New Dictionary(Of String, Dictionary(Of String, CConfProperty))
            lisOfValueDependencies = New Dictionary(Of String, Dictionary(Of String, List(Of String)))
            listOfValueConf_edit = New Dictionary(Of String, Dictionary(Of String, CConfProperty))
            listO = GLB_MSTR_CTRL.csDB.performSelectObject(GLB_MSTR_CTRL.modelViewMap(MOD_VIEW_MAP_LIST_VAL_DB_READ).getDefaultSelect, GLB_MSTR_CTRL.modelViewMap(MOD_VIEW_MAP_LIST_VAL_DB_READ), True)
            organizeListOfValues(GLB_MSTR_CTRL, listO, listOfValueConf, MOD_VIEW_MAP_LIST_VAL_DB_READ)
            listO = GLB_MSTR_CTRL.csDB.performSelectObject(GLB_MSTR_CTRL.modelViewMap(MOD_VIEW_MAP_EDIT_LIST_VAL_DB_READ).getDefaultSelect, GLB_MSTR_CTRL.modelViewMap(MOD_VIEW_MAP_EDIT_LIST_VAL_DB_READ), True)
            organizeListOfValues(GLB_MSTR_CTRL, listO, listOfValueConf_edit, MOD_VIEW_MAP_EDIT_LIST_VAL_DB_READ)
            'make it automatically updatable
            If Not listOfDBTblConfPropWritable.Contains(MOD_VIEW_MAP_EDIT_LIST_VAL_DB_READ) Then
                listOfDBTblConfPropWritable.Add(MOD_VIEW_MAP_EDIT_LIST_VAL_DB_READ)
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while loading list of values data configurations from customer statement template" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Loading customer statement business list of values..."
    End Sub

    Public Sub organizeListOfValues(ctrl As CMasterController, listO As List(Of Object), hierarchyList As Dictionary(Of String, Dictionary(Of String, CConfProperty)), db_table As String)
        Dim prop As CConfProperty
        Dim lov As Dictionary(Of String, CConfProperty)
        Dim lovDep As Dictionary(Of String, List(Of String))
        Dim listEntry As List(Of String)

        Try
            For Each obj As Object In listO
                prop = CType(obj, CConfProperty)
                prop.db_table = db_table
                If prop.key = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Or prop.key = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value Then
                    'do not let label empty=> it creates bug in comboboxes
                    prop.value = " "
                End If
                'lov
                If prop.lov_type.ToLower = constantConf(CONST_CONF_LOV_TYPE_LOV).value.ToLower Then
                    If Not hierarchyList.Keys.Contains(prop.list) Then
                        lov = New Dictionary(Of String, CConfProperty)
                        hierarchyList.Add(prop.list, lov)
                    Else
                        lov = hierarchyList(prop.list)
                    End If

                    If Not lov.Keys.Contains(prop.key) Then
                        lov.Add(prop.key, prop)
                    Else
                        Throw New Exception("An error occured when loading list of values from Excel template file. The property " & prop.key & " is declared more than once in the list " & prop.list)
                    End If
                    'lov dependencies
                ElseIf prop.lov_type.ToLower = constantConf(CONST_CONF_LOV_TYPE_LOV_DEP).value.ToLower Then
                    If Not lisOfValueDependencies.Keys.Contains(prop.list) Then
                        lovDep = New Dictionary(Of String, List(Of String))
                        lisOfValueDependencies.Add(prop.list, lovDep)
                    Else
                        lovDep = lisOfValueDependencies(prop.list)
                    End If

                    If Not lovDep.Keys.Contains(prop.key) Then
                        listEntry = New List(Of String)
                        lovDep.Add(prop.key, listEntry)
                    Else
                        listEntry = lovDep(prop.key)
                    End If
                    listEntry.Add(prop.value)
                Else
                    Throw New Exception("Lov type nnot recognized. LOV list " & prop.list & ", lov entry " & prop.key)
                End If
            Next

        Catch ex As Exception
            Throw New Exception("An error occured while loading list of values data configurations from customer statement template" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
    End Sub
    'get mapping to read mapping in DB automatically
    Public Function getMapMaping() As CViewModelMapList
        Dim mapEntry As CViewModelMap
        'dynamically create a mapper to read the excel file conf
        Dim modelViewMapSelf As New CViewModelMapList("model_view_map_model", "", "db_tech_conf_model_view_map", "CViewModelMap")

        'id
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "id"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "id"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'function
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "function"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "func"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'db_table
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "db_table"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "db_table"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'db_col
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "db_col"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "db_col"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'key_value
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "key_value"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "key_value"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'key_col
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "key_col"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "key_col"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'class
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "class"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "class_name"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'class_field
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "class_field"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "class_field"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'key
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "key"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "key"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'view_control
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "view_control"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "view_control"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'view_col
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "view_col"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "view_col"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'view_row
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "view_col_sys_name"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "view_col_sys_name"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'view_list_sheet
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "view_list_sheet"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "view_list_sheet"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'view_list_name
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "view_list_name"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "view_list_name"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'col_width
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "col_width"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "col_width"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'col_editable
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "col_editable"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "col_editable"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'col_control_type
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "col_control_type"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "col_control_type"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        'col_format
        mapEntry = New CViewModelMap
        mapEntry.func = "model_view_map_model"
        mapEntry.db_table = "db_tech_conf_model_view_map"
        mapEntry.db_col = "col_format"
        mapEntry.class_name = "CViewModelMap"
        mapEntry.class_field = "col_format"
        modelViewMapSelf.list.Add(mapEntry.db_col, mapEntry)

        Return modelViewMapSelf

    End Function


End Module
