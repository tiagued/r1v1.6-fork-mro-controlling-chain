﻿Imports System.ComponentModel
Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CInitScopeDelegate
    Public mctrl As CMasterController
    Private _quote_list As BindingListView(Of CSalesQuote)
    Private _quote_list_filter_handler As BindingListView(Of CSalesQuote)
    Private _quote_entries_list As BindingListView(Of CSalesQuoteEntry)
    Private _quote_entries_to_act_link_list As List(Of CSaleQuoteEntryToActLink)
    Public _quote_lov As BindingSource

    Public Sub New(_ctrl As CMasterController)
        mctrl = _ctrl
    End Sub

    'get quote list
    Public ReadOnly Property quote_list() As BindingListView(Of CSalesQuote)
        Get
            Return _quote_list
        End Get
    End Property
    'get quote entries list
    Public ReadOnly Property quote_entries_list() As BindingListView(Of CSalesQuoteEntry)
        Get
            Return _quote_entries_list
        End Get
    End Property
    'get quote entries to activity link list
    Public ReadOnly Property quote_entries_to_act_link_list() As List(Of CSaleQuoteEntryToActLink)
        Get
            Return _quote_entries_to_act_link_list
        End Get
    End Property

    'read data from Quote DB
    Public Sub model_read_init_scopeDB()
        mctrl.statusBar.status_strip_sub_label = "perform queries on initial scope tables..."
        Try
            model_read_quote()
            model_read_quote_entries()
            model_read_quote_entries_to_act_link()
        Catch ex As Exception
            Throw New Exception("Error occured when reading initial scope data from the DB" & Chr(10) & ex.Message, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform queries on initial scope tables"
    End Sub

    'read data from quote DB
    Private Sub model_read_quote()
        Dim listO As List(Of Object)
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to quotes table..."
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_QUOTE_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_QUOTE_DB_READ), True)
            _quote_list = New BindingListView(Of CSalesQuote)(listO)
            _quote_list_filter_handler = New BindingListView(Of CSalesQuote)(listO)
            _quote_lov = New BindingSource

            GLOB_QUOTES_LOADED = True
        Catch ex As Exception
            Throw New Exception("Error occured when reading quote data from the DB" & Chr(10) & ex.Message, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to quotes table"
    End Sub


    'get quote reference from ID
    Public Function getQuoteObject(id As Long) As CSalesQuote

        For Each quote As CSalesQuote In _quote_list.DataSource
            If quote.id = id Then
                Return quote
            End If
        Next
        If id = MConstants.EMPTY_QUOTE.id Then
            Return MConstants.EMPTY_QUOTE
        End If
        Throw New Exception("Quote object with ID " & id & " does not exist")
    End Function
    'get quote entry reference from ID
    Public Function getQuoteEntryObject(id As Long) As CSalesQuoteEntry

        For Each quoteE As CSalesQuoteEntry In _quote_entries_list.DataSource
            If quoteE.id = id Then
                Return quoteE
            End If
        Next
        Throw New Exception("Quote Entry object with ID " & id & " does not exist")
    End Function
    'read data from quote entries DB
    Private Sub model_read_quote_entries()
        Dim listO As List(Of Object)
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to quote entries table..."
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_QUOTE_ENTRIES_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_QUOTE_ENTRIES_DB_READ), True)
            _quote_entries_list = New BindingListView(Of CSalesQuoteEntry)(listO)

            GLOB_QUOTE_ENTRIES_LOADED = True
        Catch ex As Exception
            Throw New Exception("Error occured when reading quote entries data from the DB" & Chr(10) & ex.Message, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to quote entries table"
    End Sub

    'read data from quote entries DB
    Private Sub model_read_quote_entries_to_act_link()
        Dim listO As List(Of Object)
        _quote_entries_to_act_link_list = New List(Of CSaleQuoteEntryToActLink)
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to quote entries to act link table..."
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_QUOTE_ENTRIES_TO_ACT_LINK_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_QUOTE_ENTRIES_TO_ACT_LINK_DB_READ), True)
            _quote_entries_to_act_link_list = listO.OfType(Of CSaleQuoteEntryToActLink).ToList
        Catch ex As Exception
            Throw New Exception("Error occured when reading quote entries to act link data from the DB" & Chr(10) & ex.Message, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to quote entries to act link table"
    End Sub

    Public Sub bind_init_scope_views()
        mctrl.statusBar.status_strip_sub_label = "bind init scope views..."
        Try
            bind_quotes_view()
            bind_quotes_entries_view()
        Catch ex As Exception
            Throw New Exception("Error occured when binding init scope views" & Chr(10) & ex.Message, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End bind init scope views"
    End Sub

    'bind the controls items in the initial scope view view with the relevant data object
    Private Sub bind_quotes_view()
        mctrl.statusBar.status_strip_sub_label = "Bind Quote data to view controls..."
        Try
            Dim mapQuotes As CViewModelMapList

            'get quote map view
            mapQuotes = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_QUOTE_VIEW_DISPLAY)

            'quote datagridview
            mctrl.mainForm.init_scope_quotes_grid.AllowUserToAddRows = True
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.init_scope_quotes_grid)

            For Each map As CViewModelMap In mapQuotes.list.Values

                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapQuotes.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.init_scope_quotes_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapQuotes.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_QUOTE_PAYER.ToLower Then
                        bds.DataSource = MConstants.listOfPayer.Values.ToList
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "label"
                        dgcol.ValueMember = "payer"
                    Else
                        Throw New Exception("Error occured when binding quote view. List " & map.view_list_name & " does not exist in LOV")
                    End If
                    dgcol.ReadOnly = Not map.col_editable
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    mctrl.mainForm.init_scope_quotes_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.init_scope_quotes_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_BUTTON).value Then
                    Dim dgcol As New DataGridViewButtonColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Text = map.view_col
                    dgcol.UseColumnTextForButtonValue = True
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    mctrl.mainForm.init_scope_quotes_grid.Columns.Add(dgcol)
                End If
            Next
            'bind datasource
            mctrl.mainForm.init_scope_quotes_grid.DataSource = _quote_list
            MGeneralFuntionsViewControl.resizeDataGridWidth(mctrl.mainForm.init_scope_quotes_grid)
            _quote_list.ApplyFilter(AddressOf filterQuoteDeletedDelegate)

            'current cell to nothing after filter (filter select cell). this is done to avoid cell selection that will trigger validation
            'the goal is to do validation withoud selecting a cell when dgrid view get loaded
            mctrl.mainForm.init_scope_quotes_grid.CurrentCell = Nothing

            'init dependat views
            refreshQuoteDependantViews()

            'handle event
            Dim handler = New CViewRefreshHandler
            handler.datagridView = mctrl.mainForm.init_scope_quotes_grid
            handler.listView = _quote_list
            handler.delegateList.Add(AddressOf refreshQuoteDependantViews)
            handler.flexibleHeigtControl = mctrl.mainForm.init_scope_quotes_grid
            handler.bindingListViewList.Add(_quote_entries_list)
            CViewRefreshHandler.addEntry(Of CSalesQuote)(handler)
            handler.refreshHandlerViewsHeight()

            'handle row validation events
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.init_scope_quotes_grid, CSalesQuoteRowValidator.getInstance, mapQuotes)
            dhler.validateAddDelegate = AddressOf quote_view_ValidateAdd
            dhler.cancelAddDelegate = AddressOf quote_view_CancelAdd
        Catch ex As Exception
            Throw New Exception("An error occured while loading Quote  view", ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind Quote data to view controls"
    End Sub
    'validate new
    Public Sub quote_view_ValidateAdd(saleQ As ObjectView(Of CSalesQuote), rowIndex As Integer)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(saleQ.Object) Then
                'if new item
                saleQ.Object.setID()
                If Not _quote_list.DataSource.Contains(saleQ.Object) Then
                    _quote_list.EndNew(rowIndex)
                End If
                refreshQuoteDependantViews()
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new quote" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'cancel new
    Public Function quote_view_CancelAdd(saleQ As ObjectView(Of CSalesQuote), rowIndex As Integer) As Boolean
        'return true to block leaving and return false to allow leaving
        Dim res As Boolean
        Try
            'check if new
            If saleQ.Object.id > 0 Then
                res = True
            Else
                If _quote_list.DataSource.Contains(saleQ.Object) Then
                    res = True
                Else
                    saleQ.CancelEdit()
                    _quote_list.CancelNew(rowIndex)
                    res = False
                End If
            End If

        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new quote" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return res
    End Function
    'filter dele^ted quotes
    Public Function filterQuoteDeletedDelegate(quot As CSalesQuote) As Boolean
        Return quot.system_status <> MConstants.CONST_SYS_ROW_DELETED
    End Function
    'filter dele^ted quotes
    Public Function filterQuoteLOVDeletedDelegate(quot As CSalesQuote) As Boolean
        Return quot.system_status <> MConstants.CONST_SYS_ROW_DELETED And quot.isRecordable
    End Function
    Public Sub refreshQuoteDependantViews()
        Dim listO As List(Of CSalesQuote)
        'all quotes
        _quote_list_filter_handler.RemoveFilter()
        _quote_list_filter_handler.ApplyFilter(AddressOf filterQuoteLOVDeletedDelegate)
        listO = MGeneralFuntionsViewControl.getQuoteFromListView(_quote_list_filter_handler)
        listO.Add(MConstants.EMPTY_QUOTE)
        _quote_lov.DataSource = listO
        'refresh lov only update the combo box list
        _quote_lov.ResetBindings(False)
    End Sub

    'bind the controls items in the initial scope view with the relevant data object
    Private Sub bind_quotes_entries_view()
        mctrl.statusBar.status_strip_sub_label = "Bind Quote entries data to view controls..."
        Try
            Dim mapQuotesE As CViewModelMapList

            'get discount map view
            mapQuotesE = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_QUOTE_ENTRIES_VIEW_DISPLAY)

            'discount datagridview
            mctrl.mainForm.init_scope_pricing_sum_grid.AllowUserToAddRows = True
            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.init_scope_pricing_sum_grid)

            'handler of click button
            AddHandler mctrl.mainForm.init_scope_pricing_sum_grid.CellContentClick, AddressOf MViewEventHandler.init_scope_pricing_sum_grid_CellContentClick

            For Each map As CViewModelMap In mapQuotesE.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapQuotesE.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.init_scope_pricing_sum_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapQuotesE.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_QUOTE_ENTRY_QUOTE.ToLower Then
                        dgcol.DataSource = _quote_lov
                        dgcol.DisplayMember = "number"
                        dgcol.ValueMember = "id"
                    Else
                        Throw New Exception("Error occured when binding summary pricing view. List " & map.view_list_name & " does not exist in LOV")
                    End If
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.init_scope_pricing_sum_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.init_scope_pricing_sum_grid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_BUTTON).value Then
                    Dim dgcol As New DataGridViewButtonColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Text = map.view_col
                    dgcol.UseColumnTextForButtonValue = True
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    mctrl.mainForm.init_scope_pricing_sum_grid.Columns.Add(dgcol)
                End If
            Next

            'bind datasource
            mctrl.mainForm.init_scope_pricing_sum_grid.DataSource = _quote_entries_list
            MGeneralFuntionsViewControl.resizeDataGridWidth(mctrl.mainForm.init_scope_pricing_sum_grid)
            _quote_entries_list.ApplyFilter(AddressOf filterDeletedQuoteEntriesListDelegate)
            'current cell to nothing after filter (filter select cell). this is done to avoid cell selection that will trigger validation
            'the goal is to do validation withoud selecting a cell when dgrid view get loaded
            mctrl.mainForm.init_scope_pricing_sum_grid.CurrentCell = Nothing

            'handle refresh event
            Dim handler = New CViewRefreshHandler
            handler.datagridView = mctrl.mainForm.init_scope_pricing_sum_grid
            handler.listView = _quote_entries_list
            handler.flexibleHeigtControl = mctrl.mainForm.init_scope_pricing_sum_grid
            handler.addingNewItemDelegate = AddressOf quote_entries_view_AddingNew
            CViewRefreshHandler.addEntry(Of CSalesQuoteEntry)(handler)
            handler.refreshHandlerViewsHeight()

            'handle row validation events
            Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.init_scope_pricing_sum_grid, CSalesQuoteEntryRowValidator.getInstance, mapQuotesE)
            dhler.validateAddDelegate = AddressOf quote_entries_view_ValidateAdd
            dhler.cancelAddDelegate = AddressOf quote_entries_view_CancelAdd
        Catch ex As Exception
            Throw New Exception("An error occured while loading quote entries view", ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind Quote entries data to view controls"
    End Sub
    'validate new
    Public Sub quote_entries_view_ValidateAdd(saleEntry As ObjectView(Of CSalesQuoteEntry), rowIndex As Integer)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(saleEntry.Object) Then
                'if new item
                saleEntry.Object.setID()
                If Not _quote_entries_list.DataSource.Contains(saleEntry.Object) Then
                    _quote_entries_list.EndNew(rowIndex)
                End If
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new quote summary " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'cancel new
    Public Function quote_entries_view_CancelAdd(saleEntry As ObjectView(Of CSalesQuoteEntry), rowIndex As Integer) As Boolean
        'return true to block leaving and return false to allow leaving
        Dim res As Boolean
        Try
            'check if new
            If saleEntry.Object.id > 0 Then
                res = True
            Else
                If _quote_entries_list.DataSource.Contains(saleEntry.Object) Then
                    res = True
                Else
                    saleEntry.CancelEdit()
                    _quote_entries_list.CancelNew(rowIndex)
                    res = False
                End If
            End If

        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new quote summary " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return res
    End Function

    'when added new row to material view, set the activity object
    Public Sub quote_entries_view_AddingNew(ByVal sender As Object, ByVal e As AddingNewEventArgs)

        Dim bds As BindingListView(Of CSalesQuoteEntry) = sender
        Dim quoteEntry As New CSalesQuoteEntry
        Dim list As List(Of CSalesQuote) = CType(_quote_lov.DataSource, List(Of CSalesQuote))
        Try
            If list.Count > 0 Then
                quoteEntry.quote_id = list(0).id
                e.NewObject = quoteEntry
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while creating new quote summary " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'filter quote entries
    Private Function filterDeletedQuoteEntriesListDelegate(ByVal quoteE As CSalesQuoteEntry)
        'delegate for filtering
        Return Not quoteE.system_status = MConstants.CONST_SYS_ROW_DELETED
    End Function

    'first quote number
    Public Function getProjectQuoteNumber(payer As String) As String
        For Each quote As ObjectView(Of CSalesQuote) In _quote_list
            If Not IsNothing(quote.Object.payer_obj) AndAlso quote.Object.payer_obj.payer = payer Then
                Return quote.Object.number
            End If
        Next
        Return ""
    End Function

    'perform delete
    Public Sub deleteObjectQuote(quote As CSalesQuote)
        Try
            If _quote_list.DataSource.Contains(quote) Then
                _quote_list.DataSource.Remove(quote)
            End If
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(quote)
        Catch ex As Exception
        End Try
    End Sub

    'perform delete
    Public Sub deleteObjectQuoteEntry(quoteE As CSalesQuoteEntry)
        Try
            If _quote_entries_list.DataSource.Contains(quoteE) Then
                _quote_entries_list.DataSource.Remove(quoteE)
            End If
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(quoteE)
        Catch ex As Exception
        End Try
    End Sub

    'perform delete
    Public Sub deleteQuoteEntryToActLink(quoteActLink As CSaleQuoteEntryToActLink)
        Try

            If Not IsNothing(quoteActLink.act_obj) AndAlso quoteActLink.act_obj.quote_entry_link_list.Contains(quoteActLink) Then
                quoteActLink.act_obj.quote_entry_link_list.Remove(quoteActLink)
            End If
            If Not IsNothing(quoteActLink.init_quote_entry_obj) AndAlso quoteActLink.init_quote_entry_obj.quote_entry_link_list.Contains(quoteActLink) Then
                quoteActLink.init_quote_entry_obj.quote_entry_link_list.Remove(quoteActLink)
            End If

            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(quoteActLink)
        Catch ex As Exception
        End Try
    End Sub

End Class
