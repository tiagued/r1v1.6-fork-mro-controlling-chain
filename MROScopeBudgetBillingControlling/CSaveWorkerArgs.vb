﻿Imports System.Threading

Public Class CSaveWorkerArgs
    Public Shared UPD_DEL_ALL As String = "UPDATE_DELETE_ALL"
    Public Shared INSERT_ALL As String = "INSERT_ALL"
    Public Shared WAIT_DONE As String = "WAIT_DONE"

    'mass update and delete
    Public Shared Function getUpdateDeleteAll() As CSaveWorkerArgs
        Dim res As New CSaveWorkerArgs
        res.is_user_action = True
        res.calculation = CSaveWorkerArgs.UPD_DEL_ALL
        Return res
    End Function
    'automatic mass update and delete
    Public Shared Function getAutomaticUpdateDeleteAll() As CSaveWorkerArgs
        Dim res As New CSaveWorkerArgs
        res.is_user_action = False
        res.calculation = CSaveWorkerArgs.UPD_DEL_ALL
        Return res
    End Function
    'for mass imports
    Public Shared Function getInsertAll() As CSaveWorkerArgs
        Dim res As New CSaveWorkerArgs
        res.is_user_action = False
        res.calculation = CSaveWorkerArgs.INSERT_ALL
        Return res
    End Function

    'just to get latest saver thread
    Public Shared Function getDoneEventArg() As CSaveWorkerArgs
        Dim res As New CSaveWorkerArgs
        res.calculation = CSaveWorkerArgs.WAIT_DONE
        Return res
    End Function

    Public is_user_action As Boolean
    Public calculation As String
    Public progBar As ProgressBar
End Class
