﻿Imports System.IO

Public Class CImportMaterialDelegate
    Private Shared instance As CImportMaterialDelegate

    Public Shared Function getInstance() As CImportMaterialDelegate
        If IsNothing(instance) Then
            instance = New CImportMaterialDelegate
        End If
        Return instance
    End Function
    'list of items
    Private projList As List(Of String)
    Private matList As Dictionary(Of String, CImportMatObject)

    Private cnt_zmel_lines As Long
    Private cnt_not_imported As Long
    Private cnt_added As Long
    Private cnt_updated As Long
    Private cnt_deleted As Long
    Private cnt_constant As Long
    Private cnt_initial As Long
    Private warning_mess As String

    Dim progressForm As FormProgressBar

    'log message
    Delegate Sub safeCallDelegate(message As String)
    Private Sub appendLogMessage(message As String)
        Try
            If progressForm.final_message_txt.InvokeRequired Then
                Dim deleg As New safeCallDelegate(AddressOf appendLogMessage)
                progressForm.final_message_txt.Invoke(deleg, New Object() {message})
            Else
                progressForm.final_message_txt.AppendText(message & Environment.NewLine & Environment.NewLine)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub launchMaterialImport()
        Dim dialogRes As DialogResult
        Dim filepath As String = ""
        cnt_zmel_lines = 0
        cnt_added = 0
        cnt_updated = 0
        cnt_deleted = 0
        cnt_constant = 0
        cnt_not_imported = 0
        cnt_initial = 0
        warning_mess = ""
        Try
            GLB_MSTR_CTRL.statusBar.status_strip_main_label = "Import SAP Materials..."
            'open file dialog
            GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Select ZMEL file..."
            GLB_MSTR_CTRL.mainForm.open_file_dialog.AddExtension = True
            GLB_MSTR_CTRL.mainForm.open_file_dialog.CheckFileExists = True
            GLB_MSTR_CTRL.mainForm.open_file_dialog.CheckPathExists = True
            GLB_MSTR_CTRL.mainForm.open_file_dialog.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            GLB_MSTR_CTRL.mainForm.open_file_dialog.Multiselect = False
            GLB_MSTR_CTRL.mainForm.open_file_dialog.DereferenceLinks = True
            'filter file types
            GLB_MSTR_CTRL.mainForm.open_file_dialog.Filter = "ZMEL TXT Document|*" & MConstants.constantConf(MConstants.CONST_CONF_MAT_IMP_FILE_EXT).value

            dialogRes = GLB_MSTR_CTRL.mainForm.open_file_dialog.ShowDialog()

            If dialogRes = DialogResult.OK Then
                'check file extention
                filepath = GLB_MSTR_CTRL.mainForm.open_file_dialog.FileName
                'if file is mhtml
                If Not filepath.EndsWith(MConstants.constantConf(MConstants.CONST_CONF_MAT_IMP_FILE_EXT).value, StringComparison.OrdinalIgnoreCase) Then
                    MsgBox("The file you selected is not a " & MConstants.constantConf(MConstants.CONST_CONF_MAT_IMP_FILE_EXT).value & " file", MsgBoxStyle.Critical, "Wrong file type")
                    Exit Sub
                End If
            Else
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("An error occured when intializing the import " & Chr(10) & ex.Message, MsgBoxStyle.Critical, "Error when picking ZMEL file")
            Exit Sub
        End Try

        Try
            'new form
            progressForm = New FormProgressBar

            'block calculation on sold hours and activity
            MConstants.GLOB_SAP_IMPOT_RUNNING = True

            Dim listO As List(Of Object)
            Dim warnMess As String = ""
            Dim mapList As CViewModelMapList = GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_MAT_IMPORT)
            Dim txtHandler As New CTextFileHandler
            Dim delim As String

            GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Load ZMEL file..."
            delim = MConstants.constantConf(MConstants.CONST_CONF_MAT_IMP_FILE_DELIM).value
            If delim.ToLower = "vbtab" Then
                delim = vbTab
            End If
            listO = txtHandler.performSelectObject(filepath, MConstants.constantConf(MConstants.CONST_CONF_MAT_IMP_FILE_FIRST_COL).value, delim, mapList)

            'check data consistency
            Dim checkResult As KeyValuePair(Of Boolean, String)
            checkResult = checkDataConsistency(listO)
            If checkResult.Key Then
                MsgBox(checkResult.Value, MsgBoxStyle.Critical, "Cannot load data from the file")
                Exit Sub
            End If

            'if ok, continue import
            loadData()

            'save in db
            Dim arg_save As CSaveWorkerArgs
            arg_save = CSaveWorkerArgs.getInsertAll
            arg_save.progBar = progressForm.progress_bar_new
            arg_save.is_user_action = False
            MConstants.GLB_MSTR_CTRL.save_controller.startAsync(arg_save)
            'updated items.
            arg_save = CSaveWorkerArgs.getUpdateDeleteAll
            arg_save.progBar = progressForm.progress_bar_update
            arg_save.is_user_action = False
            MConstants.GLB_MSTR_CTRL.save_controller.startAsync(arg_save)

            appendLogMessage("ZMEL Import successfully done !" & Environment.NewLine)
            appendLogMessage("ZMEL File__ =" & cnt_zmel_lines)
            appendLogMessage("Failed______ =" & cnt_not_imported)
            appendLogMessage("Initial_______ =" & cnt_initial)
            appendLogMessage("Added______ =" & cnt_added)
            appendLogMessage("Updated____ =" & cnt_updated)
            appendLogMessage("Unchanged_ =" & cnt_constant)
            appendLogMessage("Not in SAP__ =" & (cnt_initial - cnt_updated - cnt_constant))

            If Not String.IsNullOrWhiteSpace(warning_mess) Then
                appendLogMessage(Environment.NewLine & "Please be aware of the following warings! :")
                appendLogMessage(warning_mess)
            End If

            progressForm.ShowDialog()
            progressForm.BringToFront()

            'refresh view
            GLB_MSTR_CTRL.material_controller.matList.Refresh()
        Catch ex As Exception
            MsgBox("An error occured when reading data from ZIMRO file. The file might not be in the right template, please check and try again " & Chr(10) & Chr(10) & Chr(10) & ex.Message, MsgBoxStyle.Critical, "Error when loading ZIMRO file")
        Finally
            'allow calculation on sold hours and activity
            MConstants.GLOB_SAP_IMPOT_RUNNING = False
        End Try
        GLB_MSTR_CTRL.statusBar.status_strip_main_label = "END Import SAP Scope"
    End Sub

    Private Function checkDataConsistency(listO As List(Of Object)) As KeyValuePair(Of Boolean, String)

        Dim errMessage As String = ""
        Dim is_error As Boolean = False
        cnt_zmel_lines = listO.Count
        matList = New Dictionary(Of String, CImportMatObject)
        projList = New List(Of String)
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Check SAP Data consistency ..."

        Try
            For Each mat_imp As CImportMatObject In listO
                'get ac reg
                If Not projList.Contains(mat_imp.network) Then
                    projList.Add(mat_imp.network)
                End If

                If Not matList.ContainsKey(mat_imp.getUnique) Then
                    matList.Add(mat_imp.getUnique, mat_imp)
                Else
                    warning_mess = warning_mess & Chr(10) & "Following item appears more than once in the ZMEL file " & mat_imp.getUnique
                End If
            Next

            errMessage = "Material upload failed. Error details : " & Chr(10)

            'check that all project are applicable to this customer statement
            For Each proj As String In projList
                If Not GLB_MSTR_CTRL.project_controller.project.project_list.Contains(proj) Then
                    errMessage = errMessage & Chr(10) & "-Project " & proj & " is contained in the ZMEL file but is not part of the current customer statement scope"
                    is_error = True
                End If
            Next

        Catch ex As Exception
            Throw New Exception("An error occured when checking ZMEL data consistency" & Chr(10) & ex.Message, ex)
        End Try
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End Check SAP Data consistency"

        Return New KeyValuePair(Of Boolean, String)(is_error, errMessage)
    End Function

    Public Sub loadData()
        'proj ref
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Merging SAP data with current customer statement data..."
        Try

            Dim materials As New Dictionary(Of String, CMaterial)
            Dim activities As New Dictionary(Of String, CScopeActivity)

            'get activities list in order to link them with materials
            For Each act_ As CScopeActivity In GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource
                'handle items only concerned with the ongoing import
                If projList.Contains(act_.proj) Then
                    'take first activity,even if several notifications
                    If Not activities.ContainsKey(act_.getMatNetWorkActivity) Then
                        activities.Add(act_.getMatNetWorkActivity, act_)
                    End If
                End If
            Next

            'to trackitems  not in sap
            Dim materialsNotInSAPList As New List(Of CMaterial)

            'organize materials for easily processing
            For Each mat_ As CMaterial In GLB_MSTR_CTRL.material_controller.matList.DataSource
                'handle items only concerned with the ongoing import
                If projList.Contains(mat_.project) And mat_.is_sap_import Then
                    If Not materials.ContainsKey(mat_.getUniqueSAPID) Then
                        materials.Add(mat_.getUniqueSAPID, mat_)
                        materialsNotInSAPList.Add(mat_)
                        'log count
                        cnt_initial = cnt_initial + 1
                    End If
                End If
            Next

            Dim mat As CMaterial

            For Each import As CImportMatObject In matList.Values

                'check if activity exist before adding it
                If Not activities.ContainsKey(import.network_activity) Then
                    cnt_not_imported = cnt_not_imported + 1
                    warning_mess = warning_mess & Chr(10) & "Cannot found activity related to the material : " & import.network_activity
                    Continue For
                End If

                'handle mat update
                If Not materials.ContainsKey(import.getUnique) Then
                    mat = import.getMaterial
                    mat.is_sap_import = True
                    mat.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NEW
                    mat.logRevisionControlText(mat.revision_control_status)
                    'set activity
                    mat.activity_obj = activities(mat.project_activity)
                    mat.activity_obj.material_list.Add(mat)
                    'add to view
                    GLB_MSTR_CTRL.material_controller.matList.DataSource.Add(mat)
                    cnt_added = cnt_added + 1
                    'add to list for saving
                    MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted(mat)
                    'log activity
                    mat.activity_obj.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_UPDATED
                    mat.activity_obj.logRevisionControlText("New Material Added " & mat.getUniqueSAPID)
                Else
                    mat = materials(import.getUnique)
                    'update only items that have not yet been updated, because one activity is repeated if there are several cost centers
                    If (mat.UpdateFromSAP(import)) Then
                        cnt_updated = cnt_updated + 1
                    Else
                        cnt_constant = cnt_constant + 1
                    End If
                    'remove from items not in sap
                    If materialsNotInSAPList.Contains(mat) Then
                        materialsNotInSAPList.Remove(mat)
                    End If
                End If
            Next

            'log items not in sap
            For Each matNotInSAP As CMaterial In materialsNotInSAPList
                matNotInSAP.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NOSAP
                matNotInSAP.logRevisionControlText("Material Not Found in SAP")
                'log in act
                matNotInSAP.activity_obj.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_UPDATED
                matNotInSAP.activity_obj.logRevisionControlText("Material " & matNotInSAP.getUniqueSAPID & " Removed from SAP")
            Next

        Catch ex As Exception
            Throw New Exception("An error occured while mergin SAP data with customer statement data" & Chr(10) & ex.Message, ex)
        End Try
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "END Merging SAP data with current customer statement data"
    End Sub


End Class
