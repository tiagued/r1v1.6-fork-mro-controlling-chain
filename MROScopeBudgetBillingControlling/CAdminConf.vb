﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CAdminConf
    Implements INotifyPropertyChanged

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub


    Public Property auto_save_timer_enable() As Boolean
        Get
            Return MConstants.GLB_MSTR_CTRL.save_controller.auto_save_timer_enable
        End Get
        Set(ByVal value As Boolean)
            MConstants.GLB_MSTR_CTRL.save_controller.auto_save_timer_enable = value
            NotifyPropertyChanged()
        End Set
    End Property

    Public Property auto_save_timer_elapse() As Double
        Get
            Return MConstants.GLB_MSTR_CTRL.save_controller.auto_save_timer_elapse
        End Get
        Set(ByVal value As Double)
            MConstants.GLB_MSTR_CTRL.save_controller.auto_save_timer_elapse = value
            NotifyPropertyChanged()
        End Set
    End Property

End Class
