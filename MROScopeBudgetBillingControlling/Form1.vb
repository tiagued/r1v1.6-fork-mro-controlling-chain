﻿Public Class Form1
    Public mastercontroller As CMasterController
    Private load_failed As Boolean
    'use to validate the leaving page in the conf tab control

    Public Sub New()
        Try
            load_failed = False
            ' This call is required by the designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call.

            'instantiate controller
            mastercontroller = New CMasterController(Me)
            mastercontroller.initializeForm()

            'navigation story handlers
            MTabNavigationStory.addDefaultNavigationStoryHandlers()

        Catch ex As Exception
            MsgBox("Application Loading Error" & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
            load_failed = True
        End Try
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) _
     Handles Me.Load
        If load_failed Then
            Me.Close()
        End If
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) _
     Handles Me.Closing
        e.Cancel = MConstants.GLB_MSTR_CTRL.closeApp()
    End Sub

    'handle click on Menu items=> Focus on the group
    Private Sub conf_bill_menu_curr_lbl_Click(sender As Object, e As EventArgs) Handles conf_bill_menu_curr_lbl.Click
        Me.ActiveControl = Me.conf_project_layout
        Me.ActiveControl = Me.conf_bill_curr_grp
    End Sub
    Private Sub conf_bill_menu_markup_lbl_Click(sender As Object, e As EventArgs) Handles conf_bill_menu_markup_lbl.Click
        Me.ActiveControl = Me.conf_project_layout
        Me.ActiveControl = Me.conf_bill_markup_grp
    End Sub
    Private Sub conf_bill_menu_freight_lbl_Click(sender As Object, e As EventArgs) Handles conf_bill_menu_freight_lbl.Click
        Me.ActiveControl = Me.conf_project_layout
        Me.ActiveControl = Me.conf_bill_freight_grp
    End Sub
    Private Sub conf_bill_menu_surcharge_lbl_Click(sender As Object, e As EventArgs) Handles conf_bill_menu_surcharge_lbl.Click
        Me.ActiveControl = Me.conf_project_layout
        Me.ActiveControl = Me.conf_bill_surcharge_grp
    End Sub
    Private Sub conf_bill_menu_downpay_lbl_Click(sender As Object, e As EventArgs) Handles conf_bill_menu_downpay_lbl.Click
        Me.ActiveControl = Nothing
        Me.ActiveControl = Me.conf_bill_down_pay_grp
    End Sub
    Private Sub conf_bill_menu_discount_lbl_Click(sender As Object, e As EventArgs) Handles conf_bill_menu_discount_lbl.Click
        Me.ActiveControl = Me.conf_project_layout
        Me.ActiveControl = Me.conf_bill_discount_grp
    End Sub
    Private Sub conf_bill_menu_nte_lbl_Click(sender As Object, e As EventArgs) Handles conf_bill_menu_nte_lbl.Click
        Me.ActiveControl = Me.conf_project_layout
        Me.ActiveControl = Me.conf_bill_nte_grp
    End Sub
    Private Sub conf_labrate_menu_std_lbl_Click(sender As Object, e As EventArgs) Handles conf_labrate_menu_std_lbl.Click
        Me.conf_labrate_layout.ScrollControlIntoView(Me.conf_labrate_labrate_std_grp)
    End Sub
    Private Sub conf_labrate_menu_oem_lbl_Click(sender As Object, e As EventArgs) Handles conf_labrate_menu_oem_lbl.Click
        Me.conf_labrate_layout.ScrollControlIntoView(Me.conf_labrate_labrate_oem_grp)
    End Sub
    Private Sub conf_labrate_menu_nte_lbl_Click(sender As Object, e As EventArgs) Handles conf_labrate_menu_nte_lbl.Click
        Me.conf_labrate_layout.ScrollControlIntoView(Me.conf_labrate_labrate_nte_grp)
    End Sub
    Private Sub conf_labrate_menu_task_rate_lbl_Click(sender As Object, e As EventArgs) Handles conf_labrate_menu_task_rate_lbl.Click
        Me.conf_labrate_layout.ScrollControlIntoView(Me.conf_labrate_task_rate_grp)
    End Sub
    Private Sub init_scope_menu_quots_Click(sender As Object, e As EventArgs) Handles init_scope_menu_quots.Click
        Me.init_scope_main_layout.ScrollControlIntoView(Me.init_scope_menu_quots_grp)
    End Sub
    Private Sub init_scope_menu_pricing_sum_Click(sender As Object, e As EventArgs) Handles init_scope_menu_pricing_sum.Click
        Me.init_scope_main_layout.ScrollControlIntoView(Me.init_scope_menu_pricing_sum_grp)
    End Sub

    'if tagk date value change, put values in the column of rate
    Private Sub lbl_cnf_bill_tagk_date_val_ValueChanged(sender As Object, e As EventArgs) Handles lbl_cnf_bill_tagk_date_val.ValueChanged
        mastercontroller.billing_cond_controller.updateRateDate(lbl_cnf_bill_tagk_date_val.Value)
    End Sub
    'reset tagk date to null if tagk change
    Private Sub conf_bill_exchg_rate_grid_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles conf_bill_exchg_rate_grid.CellValueChanged
        mastercontroller.billing_cond_controller.updateRateDate(MConstants.APP_NULL_DATE)
        mastercontroller.project_controller.project.tagk_date = MConstants.APP_NULL_DATE
    End Sub

    Private Sub top_menu_import_zimro_bt_Click(sender As Object, e As EventArgs) Handles top_menu_import_zimro_bt.Click
        CImportScopeDelegate.getInstance().launchScopeImport()
    End Sub

    Private Sub top_menu_import_CN47N_bt_Click(sender As Object, e As EventArgs) Handles top_menu_import_CN47N_bt.Click
        CImportCostCollectorDelegate.getInstance().launchScopeImport()
    End Sub

    Private Sub top_menu_import_CIJ3_bt_Click(sender As Object, e As EventArgs) Handles top_menu_import_CIJ3_bt.Click
        CImportActualHoursDelegate.getInstance.launchActualImport()
    End Sub

    Private Sub top_menu_home_tab_info_bt_Click(sender As Object, e As EventArgs) Handles top_menu_home_tab_info_bt.Click
        Dim aboutDel As New CApplicationInformationViewDelegate
        aboutDel.launch()
    End Sub

    Private Sub top_menu_admin_bt_Click(sender As Object, e As EventArgs) Handles top_menu_admin_bt.Click
        mastercontroller.admin_conf_controller.launch()
    End Sub

    Private Sub top_menu_import_zmel_bt_Click(sender As Object, e As EventArgs) Handles top_menu_import_zmel_bt.Click
        CImportMaterialDelegate.getInstance().launchMaterialImport()
    End Sub
    Private Sub top_menu_customer_release_bt_Click(sender As Object, e As EventArgs) Handles top_menu_customer_release_bt.Click
        CReportingCustomerRelease.getInstance.prepareCustomerReleaseView()
    End Sub
    Private Sub top_menu_hours_stat_bt_Click(sender As Object, e As EventArgs) Handles top_menu_hours_stat_bt.Click
        CReportingHoursStatement.getInstance.prepareControllingReportView()
    End Sub
    Private Sub top_menu_data_check_bt_Click(sender As Object, e As EventArgs) Handles top_menu_data_check_bt.Click
        CReportingDataConsistency.getInstance.prepareDataCheckReportView()
    End Sub
    'activity main view, handle click on edit button
    Private Sub add_scope_main_datagrid_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles add_scope_main_datagrid.CellContentClick
        ' Ignore clicks that are not on button cells. 
        If e.RowIndex >= 0 AndAlso add_scope_main_datagrid.Columns(e.ColumnIndex).Name = MConstants.CONST_CONF_ACT_EDIT Then
            Dim activity As CScopeActivity
            activity = CType(add_scope_main_datagrid.Rows(e.RowIndex).DataBoundItem, Equin.ApplicationFramework.ObjectView(Of CScopeActivity)).Object
            MConstants.GLB_MSTR_CTRL.add_scope_hours_edit_ctrl.launchEditForm(activity)
        Else
            Return
        End If
    End Sub

    'material main view, handle click on edit button
    Private Sub material_main_dgrid_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles material_main_dgrid.CellContentClick
        ' Ignore clicks that are not on button cells. 
        If e.RowIndex >= 0 AndAlso material_main_dgrid.Columns(e.ColumnIndex).Name = MConstants.CONST_CONF_MAT_EDIT Then
            Dim material As CMaterial
            Try
                material = CType(material_main_dgrid.Rows(e.RowIndex).DataBoundItem, Equin.ApplicationFramework.ObjectView(Of CMaterial)).Object
                MConstants.GLB_MSTR_CTRL.material_edit_ctrl.launchEditForm(material, CMaterialAddRowValidator.getInstance)
                CGridViewRowValidationHandler.initializeGridErrorState(material_main_dgrid)
            Catch ex As Exception
                MGeneralFuntionsViewControl.displayMessage("Cannot launch Edit Form", "An error occured while laoding the Edit form" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
            End Try
        Else
            Return
        End If
    End Sub

    'awq main view, handle click on edit button
    Private Sub awq_main_dgrid_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles awq_main_dgrid.CellContentClick
        ' Ignore clicks that are not on button cells. 
        If e.RowIndex >= 0 AndAlso awq_main_dgrid.Columns(e.ColumnIndex).Name = MConstants.CONST_CONF_AWQ_EDIT Then
            Dim awq As CAWQuotation
            Try
                awq = CType(awq_main_dgrid.Rows(e.RowIndex).DataBoundItem, Equin.ApplicationFramework.ObjectView(Of CAWQuotation)).Object
                MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.launchEditForm(awq, True)
            Catch ex As Exception
                MGeneralFuntionsViewControl.displayMessage("Cannot launch Edit Form", "An error occured while laoding the Edit form" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
            End Try
        Else
            Return
        End If
    End Sub

    'controlling main view, handle click on edit button
    Private Sub labor_ctrl_main_dgrid_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles labor_ctrl_main_dgrid.CellContentClick
        ' Ignore clicks that are not on button cells. 
        If e.RowIndex >= 0 AndAlso labor_ctrl_main_dgrid.Columns(e.ColumnIndex).Name = MConstants.CONST_CONF_LABOR_CTRL_EDIT Then
            Dim lab_ctrl As CLaborControlMainViewObj
            Try
                lab_ctrl = CType(labor_ctrl_main_dgrid.Rows(e.RowIndex).DataBoundItem, Equin.ApplicationFramework.ObjectView(Of CLaborControlMainViewObj)).Object
                MConstants.GLB_MSTR_CTRL.labor_controlling_cost_node_ctrl.launchEditForm(lab_ctrl)
            Catch ex As Exception
                MGeneralFuntionsViewControl.displayMessage("Cannot launch Cost Node View Form", "An error occured while laoding the Cost Node View form" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
            End Try
        Else
            Return
        End If
    End Sub

    Private Sub top_menu_save_Click(sender As Object, e As EventArgs) Handles top_menu_save.Click
        MConstants.GLB_MSTR_CTRL.save_controller.startAsync(CSaveWorkerArgs.getInsertAll)
        MConstants.GLB_MSTR_CTRL.save_controller.startAsync(CSaveWorkerArgs.getUpdateDeleteAll)
    End Sub

    Private Sub top_menu_save_status_Click(sender As Object, e As EventArgs) Handles top_menu_save_status.Click
        MsgBox(top_menu_save_status.Tag)
    End Sub

    Private Sub new_awq_no_activity_Click(sender As Object, e As EventArgs) Handles new_awq_no_activity.Click
        MConstants.GLB_MSTR_CTRL.awq_controller.createAWQWithNoActivity()
    End Sub

    Private Sub awq_mass_closure_bt_Click(sender As Object, e As EventArgs) Handles awq_mass_closure_bt.Click
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.massClosure()
    End Sub
End Class
