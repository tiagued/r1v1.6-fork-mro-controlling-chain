﻿Module MAdmin
    Public Sub getColWidthToClipBoard(dg As DataGridView)
        Dim text As String = ""
        For Each col As DataGridViewColumn In dg.Columns
            text = text & col.Width & Chr(10)
        Next
        Clipboard.SetText(text)
    End Sub
End Module
