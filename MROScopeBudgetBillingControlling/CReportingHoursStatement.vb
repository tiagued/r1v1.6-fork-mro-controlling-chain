﻿Imports System.Text
Imports Equin.ApplicationFramework
Imports Microsoft.Office.Interop.Excel

Public Class CReportingHoursStatement
    Private Shared instance As CReportingHoursStatement

    Public form As FormReportHoursStatement

    'project
    Private hoursStatSummaryConf As List(Of CReportConfObject)
    'Hours
    Private ctrlLabListConf As List(Of CReportConfObject)
    'budget Adj
    Private budgetAdjListConf As List(Of CReportConfObject)

    Private reportWB As Microsoft.Office.Interop.Excel.Workbook

    'list of data
    Private transposed_data_list As New List(Of CLaborCtrlReportingObj)
    Private budget_adj_list As New List(Of CLaborCtrlBudgetAdjRepObj)

    Public Shared Function getInstance() As CReportingHoursStatement
        Try
            If IsNothing(instance) Then
                instance = New CReportingHoursStatement

                'proj summ
                instance.hoursStatSummaryConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_HRS_STAT_SUMMARY_SHEET_NAME)(MConstants.REPORT_CONF_SUMMARY_LIST_NAME)
                'ctrl details
                instance.ctrlLabListConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_HRS_WO_CC_HRS_SHEET_NAME)(MConstants.REPORT_CONF_HRS_WO_CC_HRS_LIST_NAME)
                'budget adj
                instance.budgetAdjListConf = MConstants.listOfReportConf(MConstants.REPORT_CONF_HRS_BUDGET_ADJ_SHEET_NAME)(MConstants.REPORT_CONF_HRS_BUDGET_ADJ_LIST_NAME)
            End If
        Catch ex As Exception
            instance = Nothing
            MGeneralFuntionsViewControl.displayMessage("Customer Release Error", "An error occured while preparing customer release report view." & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return instance
    End Function

    Public Sub log_text(text As String)
        Try
            form.log_bt.Text = text
        Catch ex As Exception
            Throw New Exception("An error occured while logging progress" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    'bind form controls to report object properties
    Public Sub prepareControllingReportView()
        Try
            form = New FormReportHoursStatement

            'log
            log_text("Click Run to generate report")

            form.ShowDialog()
            form.BringToFront()
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Controlling Error", "An error occured while preparing controlling reporting view." & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub launch()
        Try
            'log
            log_text("Starting...")

            Dim template As String = MConstants.constantConf(MConstants.CONST_CONF_HRS_STATEMENT_TMPL_FILE).value
            If Not System.IO.File.Exists(template) Then
                Throw New Exception("The Hours Statement Template File " & template & " Does not exists")
            End If

            'generate excel file
            'log
            log_text("calculating saving path")
            Dim reportName As String = MConstants.constantConf(MConstants.CONST_CONF_HRS_STATEMENT_REPORT_NAME).value
            Dim reportFolder As String = ""
            'replace variable in report name
            reportName = reportName.Replace(MConstants.constantConf(MConstants.CONST_CONF_PROJ_PARAM).value, MConstants.GLB_MSTR_CTRL.project_controller.project.project_def)
            reportName = reportName.Replace(MConstants.constantConf(MConstants.CONST_CONF_DATE_PARAM).value, DateTime.Now.ToString("yyyy.MM.dd hh.mm.ss"))

            'saving path
            reportFolder = MConstants.GLB_MSTR_CTRL.csFile.db_folder
            If MConstants.constantConf(MConstants.CONST_CONF_HRS_STATEMENT_ROOT_BROWSE_UP_LVL).value > 0 Then
                Try
                    For i As Integer = 1 To MConstants.constantConf(MConstants.CONST_CONF_HRS_STATEMENT_ROOT_BROWSE_UP_LVL).value
                        reportFolder = IO.Directory.GetParent(reportFolder).FullName
                    Next
                    reportFolder = reportFolder & "\" & MConstants.constantConf(MConstants.CONST_CONF_HRS_STATEMENT_SAVE_PATH).value
                Catch ex As Exception
                    reportFolder = MConstants.GLB_MSTR_CTRL.csFile.db_folder
                End Try
            End If

            If IO.Directory.Exists(reportFolder) Then
                reportName = reportFolder & "\" & reportName
            Else
                reportName = MConstants.GLB_MSTR_CTRL.csFile.db_folder & "\" & reportName
            End If

            'log
            log_text("Copy and Open template to " & reportName)
            reportWB = MConstants.GLB_MSTR_CTRL.csFile.userxlAppOpenTemplate(template, reportName)

            Try
                MConstants.GLB_MSTR_CTRL.csFile.OffCalc(reportWB.Application)

                'load tabs
                'log
                log_text("Calculating data")
                loadLists()
                'log
                log_text("Filling Hours Statement summary")
                loadHoursStatementSummary()
                'log
                log_text("Hours Statement Details Data")
                loadProjectDetailsData()
                'log
                log_text("Filling Budget Adjustment list")
                loadBudgetAdjList()
                'clear memory
                budget_adj_list.Clear()
                transposed_data_list.Clear()
                'refresh views
                'log
                log_text("Updating Pivot Table Paths and Refresh Pivot Table")
                Dim template_file_name = "[" & System.IO.Path.GetFileName(template) & "]"
                Dim regE As New RegularExpressions.Regex(".*!")
                For Each ws As Worksheet In reportWB.Worksheets
                    For Each piv As PivotTable In ws.PivotTables
                        Dim sourceData As String = piv.PivotCache.SourceData
                        Dim match As RegularExpressions.Match = regE.Match(sourceData)
                        If Not String.IsNullOrWhiteSpace(sourceData) AndAlso match.Success Then
                            sourceData = sourceData.Replace(match.Value, "")
                            piv.ChangePivotCache(reportWB.PivotCaches.Create(SourceType:=XlPivotTableSourceType.xlDatabase, SourceData:=sourceData.Replace(template_file_name, "")))
                        End If
                        piv.PivotCache.Refresh()
                    Next
                Next

            Catch ex As Exception
                Throw New Exception("Error " & Chr(10) & ex.Message & ex.StackTrace)
            Finally
                MConstants.GLB_MSTR_CTRL.csFile.OnCalc(reportWB.Application)
            End Try
            'log
            log_text("Saving Report")
            MConstants.GLB_MSTR_CTRL.csFile.safeWorkbookSave(reportWB)
            reportWB.Activate()
            Dim proj_sum_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_HRS_STATEMENT_SUM_TAB_NAME).value)
            proj_sum_ws.Activate()
            'log
            log_text("Report successfully generated")
            MGeneralFuntionsViewControl.displayMessage("Hours Statement Success", "The hours statement report has been generated successfully :" & reportWB.FullName, MsgBoxStyle.Information)
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Hours Statement Error", "An error occured while preparing hours statement report view." & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        Finally
            form.Close()
        End Try
    End Sub
    Public Sub loadLists()
        Try
            budget_adj_list.Clear()
            transposed_data_list.Clear()

            Dim value As Object
            Dim initLoaded As Boolean = True
            If Not MConstants.GLB_MSTR_CTRL.labor_controlling_controller.LABOR_CONTROL_VIEW_LOADABLE Then
                MConstants.GLB_MSTR_CTRL.labor_controlling_controller.loadControllingModule()
                initLoaded = False
            End If

            Dim propertyList As Dictionary(Of String, String) = MLaborControlReportDataConf.getPropertyList

            'get transposed data
            For Each _costNodeObjView As ObjectView(Of CLaborControlMainViewObj) In MConstants.GLB_MSTR_CTRL.labor_controlling_controller.laborControlCostNodeList
                Dim _costNodeObj As CLaborControlMainViewObj = _costNodeObjView.Object
                Dim costNode As String = _costNodeObj.activity_key
                Dim emptyCostNodeForDim = Nothing
                Dim hasCostNodeDim As Boolean = False
                For Each _lab_ctrl As CLaborControlMainViewObj In _costNodeObj.calculation_children
                    For Each prop As String In propertyList.Keys
                        value = CallByName(_lab_ctrl, prop, CallType.Get, New Object() {})
                        If Not IsNothing(value) AndAlso value <> 0 Then
                            Dim reportLine As New CLaborCtrlReportingObj(_lab_ctrl)
                            reportLine.cost_node = costNode
                            reportLine.hours_type = propertyList(prop)
                            reportLine.qty = value
                            transposed_data_list.Add(reportLine)
                            'add a dimension for cost node
                            If IsNothing(emptyCostNodeForDim) Then
                                emptyCostNodeForDim = _lab_ctrl
                            End If
                            If Not hasCostNodeDim AndAlso String.Equals(costNode, _lab_ctrl.activity_key) Then
                                hasCostNodeDim = True
                            End If
                        End If
                    Next
                    'dev just
                    For Each devJust As CDevJust In _lab_ctrl.deviationJustification
                        Dim reportLine As New CLaborCtrlBudgetAdjRepObj(_lab_ctrl)
                        reportLine.dev_scope = devJust.scope_str
                        If devJust.type = MConstants.CONST_CONF_CTRL_JUST_OVERSHOOT Then
                            reportLine.dev_type = "Overshoot"
                            reportLine.qty = devJust.qty
                            reportLine.dev_reason = MConstants.listOfValueConf(CONST_CONF_LOV_DEV_JUST_OVERSHOOT_REASON)(devJust.reason).value
                        Else
                            reportLine.dev_type = "Gain"
                            reportLine.qty = -devJust.qty
                            reportLine.dev_reason = MConstants.listOfValueConf(CONST_CONF_LOV_DEV_JUST_GAIN_REASON)(devJust.reason).value
                        End If
                        reportLine.dev_desc = devJust.description
                        reportLine.claimed_by = devJust.claimed_by
                        reportLine.approved_by = devJust.approved_by
                        reportLine.approved_on = devJust.approved_on
                        reportLine.is_opening_eac = devJust.is_opening_eac
                        budget_adj_list.Add(reportLine)
                    Next
                Next
                'handle cost node dimension
                If Not hasCostNodeDim AndAlso Not IsNothing(emptyCostNodeForDim) Then
                    Dim reportLine As New CLaborCtrlReportingObj(_costNodeObj)
                    reportLine.setCC(emptyCostNodeForDim)
                    reportLine.cost_node = costNode
                    reportLine.hours_type = propertyList.First.Value
                    reportLine.qty = 0
                    transposed_data_list.Add(reportLine)
                End If
            Next

            If Not initLoaded Then
                MConstants.GLB_MSTR_CTRL.labor_controlling_controller.unload_labor_controlling()
            End If
        Catch ex As Exception
            Throw New Exception("An error occured while preparing data for hours statement" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub
    Public Sub loadHoursStatementSummary()
        Try
            Dim proj_sum_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_HRS_STATEMENT_SUM_TAB_NAME).value)

            Dim value As Object
            'fill summary values
            For Each confO As CReportConfObject In hoursStatSummaryConf
                If confO.item_object = "CReportingHoursStatement" Then
                    value = CallByName(Me, confO.item_property, CallType.Get)
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    proj_sum_ws.Cells(confO.item_row, confO.item_column) = value
                Else
                    Throw New Exception("Summary Report, conf object type not found " & confO.item_object)
                End If
            Next
        Catch ex As Exception
            Throw New Exception("An error occured while generating Summary view of the report" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Sub loadProjectDetailsData()
        Try
            Dim labor_data_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_HRS_STATEMENT_DATA_TAB_NAME).value)

            Dim value As Object

            'act list
            Dim reportArray(transposed_data_list.Count - 1, ctrlLabListConf.Count - 1) As Object
            Dim i As Integer = 0
            'build array
            For Each entry As CLaborCtrlReportingObj In transposed_data_list
                For Each confO As CReportConfObject In ctrlLabListConf
                    If confO.item_object = "CLaborCtrlReportingObj" Then
                        value = CallByName(entry, confO.item_property, CallType.Get)
                    Else
                        Throw New Exception("Hours Report, conf object type not found " & confO.item_object)
                    End If
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    reportArray(i, confO.item_column - 1) = value
                Next
                i = i + 1
            Next
            'insert
            Dim xlrg As Microsoft.Office.Interop.Excel.Range
            Dim xlrg_start As Microsoft.Office.Interop.Excel.Range
            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(ctrlLabListConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange
            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & ctrlLabListConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try
            'startrow + 1 to insert from the line below to propagate the good formating
            xlrg = CType(labor_data_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)

            'reset range as lines have been added
            xlrg = CType(labor_data_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            xlrg = labor_data_ws.Range(xlrg, xlrg.Offset(transposed_data_list.Count - 1, ctrlLabListConf.Count - 1))
            xlrg.Value = reportArray


        Catch ex As Exception
            Throw New Exception("An error occured while generating act cc hours list" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Sub loadBudgetAdjList()
        Try
            Dim budget_adj_ws As Microsoft.Office.Interop.Excel.Worksheet = reportWB.Worksheets(MConstants.constantConf(MConstants.CONST_CONF_HRS_STATEMENT_BUDG_ADJ_TAB_NAME).value)

            Dim reportArray(budget_adj_list.Count - 1, budgetAdjListConf.Count - 1) As Object
            Dim i As Integer = 0
            Dim value As Object
            'build array
            For Each entry As CLaborCtrlBudgetAdjRepObj In budget_adj_list
                For Each confO As CReportConfObject In budgetAdjListConf
                    If confO.item_object = "CLaborCtrlBudgetAdjRepObj" Then
                        value = CallByName(entry, confO.item_property, CallType.Get)
                    Else
                        Throw New Exception("Budget Adjustment Report, conf object type not found " & confO.item_object)
                    End If
                    If TypeOf (value) Is Date OrElse TypeOf (value) Is DateTime Then
                        If CDate(value) = MConstants.APP_NULL_DATE Then
                            value = ""
                        Else
                            Dim _d As Date = CDate(value)
                            value = _d.ToString(confO.item_format)
                        End If
                    End If
                    reportArray(i, confO.item_column - 1) = value
                Next
                i = i + 1
            Next

            'insert
            Dim xlrg As Microsoft.Office.Interop.Excel.Range
            Dim xlrg_start As Microsoft.Office.Interop.Excel.Range
            Try
                Dim TheRangeName As Microsoft.Office.Interop.Excel.Name = reportWB.Names.Item(budgetAdjListConf(0).list_start_cell)
                xlrg_start = TheRangeName.RefersToRange
            Catch ex As Exception
                Throw New Exception("An error occured while retrieving the first list cell, reference name :" & budgetAdjListConf(0).list_start_cell & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
            End Try
            'startrow + 1 to insert from the line below to propagate the good formating
            xlrg = CType(budget_adj_ws.Cells(xlrg_start.Row + 1, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            'insert rows
            If budget_adj_list.Count > 1 Then
                MConstants.GLB_MSTR_CTRL.csFile.insertLines(reportWB, budget_adj_ws, xlrg, budget_adj_list.Count - 1)
            End If
            'reset range as lines have been added
            xlrg = CType(budget_adj_ws.Cells(xlrg_start.Row, xlrg_start.Column), Microsoft.Office.Interop.Excel.Range)
            xlrg = budget_adj_ws.Range(xlrg, xlrg.Offset(budget_adj_list.Count - 1, budgetAdjListConf.Count - 1))
            xlrg.Value = reportArray

        Catch ex As Exception
            Throw New Exception("An error occured while generating Budget Adjustment list report" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub


    Public ReadOnly Property Aircraft() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.ac_reg & "(" & MConstants.GLB_MSTR_CTRL.project_controller.project.ac_master & ")"
        End Get
    End Property

    Public ReadOnly Property Project() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.project_list_str
        End Get
    End Property

    Public ReadOnly Property ProjectDescription() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.project_desc
        End Get
    End Property

    Public ReadOnly Property getDate() As Date
        Get
            Return DateTime.Now
        End Get
    End Property

    Public ReadOnly Property AWQSpecialist() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.awq_specialist
        End Get
    End Property


    Public ReadOnly Property getProjectStartDate() As Date
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.input_date
        End Get
    End Property

    Public ReadOnly Property getProjectFinishDate() As Date
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.fcst_finish_date
        End Get
    End Property

    Public ReadOnly Property getLatestActualBookingDate() As Date
        Get
            Return MConstants.GLB_MSTR_CTRL.labor_controlling_controller.latest_actual_booking
        End Get
    End Property

    Public ReadOnly Property CustomerCompany() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.customer
        End Get
    End Property

    Public ReadOnly Property CustomerName() As String
        Get
            Return MConstants.GLB_MSTR_CTRL.project_controller.project.customer_rep_name
        End Get
    End Property
End Class
