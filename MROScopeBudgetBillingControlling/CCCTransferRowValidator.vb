﻿
Public Class CCCTransferRowValidator
    Public Shared instance As CCCTransferRowValidator
    Public Shared Function getInstance() As CCCTransferRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CCCTransferRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(ccTrans As CCCTransfer)
        ccTrans.calc_is_in_error_state = False
    End Sub

    Public Function to_cc(ccTrans As CCCTransfer) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(ccTrans.to_cc) OrElse ccTrans.to_cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "To Cost Center Shall not be empty"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function from_cc(ccTrans As CCCTransfer) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(ccTrans.from_cc) OrElse ccTrans.from_cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "From Cost Center Shall not be empty"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function
End Class