﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAdmin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.admin_form_main_ly = New System.Windows.Forms.FlowLayoutPanel()
        Me.show_cs_file_lbl = New System.Windows.Forms.Label()
        Me.show_cs_file_val = New System.Windows.Forms.CheckBox()
        Me.enable_auto_save_lbl = New System.Windows.Forms.Label()
        Me.enable_auto_save_val = New System.Windows.Forms.CheckBox()
        Me.auto_save_interval_lbl = New System.Windows.Forms.Label()
        Me.auto_save_interval_val = New System.Windows.Forms.TextBox()
        Me.cancel_save_bt = New System.Windows.Forms.Button()
        Me.ace_driver_version_bt = New System.Windows.Forms.Button()
        Me.culture_info_bt = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.admin_form_main_ly.SuspendLayout()
        Me.SuspendLayout()
        '
        'admin_form_main_ly
        '
        Me.admin_form_main_ly.Controls.Add(Me.show_cs_file_lbl)
        Me.admin_form_main_ly.Controls.Add(Me.show_cs_file_val)
        Me.admin_form_main_ly.Controls.Add(Me.enable_auto_save_lbl)
        Me.admin_form_main_ly.Controls.Add(Me.enable_auto_save_val)
        Me.admin_form_main_ly.Controls.Add(Me.auto_save_interval_lbl)
        Me.admin_form_main_ly.Controls.Add(Me.auto_save_interval_val)
        Me.admin_form_main_ly.Controls.Add(Me.cancel_save_bt)
        Me.admin_form_main_ly.Controls.Add(Me.ace_driver_version_bt)
        Me.admin_form_main_ly.Controls.Add(Me.culture_info_bt)
        Me.admin_form_main_ly.Dock = System.Windows.Forms.DockStyle.Fill
        Me.admin_form_main_ly.Location = New System.Drawing.Point(0, 59)
        Me.admin_form_main_ly.Name = "admin_form_main_ly"
        Me.admin_form_main_ly.Size = New System.Drawing.Size(1283, 499)
        Me.admin_form_main_ly.TabIndex = 0
        '
        'show_cs_file_lbl
        '
        Me.show_cs_file_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.show_cs_file_lbl.AutoSize = True
        Me.show_cs_file_lbl.Location = New System.Drawing.Point(3, 8)
        Me.show_cs_file_lbl.Name = "show_cs_file_lbl"
        Me.show_cs_file_lbl.Size = New System.Drawing.Size(79, 13)
        Me.show_cs_file_lbl.TabIndex = 1
        Me.show_cs_file_lbl.Text = "Show CS File ?"
        '
        'show_cs_file_val
        '
        Me.show_cs_file_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.show_cs_file_val.AutoSize = True
        Me.show_cs_file_val.Location = New System.Drawing.Point(88, 7)
        Me.show_cs_file_val.Name = "show_cs_file_val"
        Me.show_cs_file_val.Size = New System.Drawing.Size(15, 14)
        Me.show_cs_file_val.TabIndex = 0
        Me.show_cs_file_val.UseVisualStyleBackColor = True
        '
        'enable_auto_save_lbl
        '
        Me.enable_auto_save_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.enable_auto_save_lbl.AutoSize = True
        Me.enable_auto_save_lbl.Location = New System.Drawing.Point(109, 8)
        Me.enable_auto_save_lbl.Name = "enable_auto_save_lbl"
        Me.enable_auto_save_lbl.Size = New System.Drawing.Size(102, 13)
        Me.enable_auto_save_lbl.TabIndex = 2
        Me.enable_auto_save_lbl.Text = "Enable Auto Save ?"
        '
        'enable_auto_save_val
        '
        Me.enable_auto_save_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.enable_auto_save_val.AutoSize = True
        Me.enable_auto_save_val.Location = New System.Drawing.Point(217, 7)
        Me.enable_auto_save_val.Name = "enable_auto_save_val"
        Me.enable_auto_save_val.Size = New System.Drawing.Size(15, 14)
        Me.enable_auto_save_val.TabIndex = 3
        Me.enable_auto_save_val.UseVisualStyleBackColor = True
        '
        'auto_save_interval_lbl
        '
        Me.auto_save_interval_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.auto_save_interval_lbl.AutoSize = True
        Me.auto_save_interval_lbl.Location = New System.Drawing.Point(238, 8)
        Me.auto_save_interval_lbl.Name = "auto_save_interval_lbl"
        Me.auto_save_interval_lbl.Size = New System.Drawing.Size(95, 13)
        Me.auto_save_interval_lbl.TabIndex = 4
        Me.auto_save_interval_lbl.Text = "Auto Save Interval"
        '
        'auto_save_interval_val
        '
        Me.auto_save_interval_val.Location = New System.Drawing.Point(339, 3)
        Me.auto_save_interval_val.Name = "auto_save_interval_val"
        Me.auto_save_interval_val.Size = New System.Drawing.Size(100, 20)
        Me.auto_save_interval_val.TabIndex = 5
        '
        'cancel_save_bt
        '
        Me.cancel_save_bt.AllowDrop = True
        Me.cancel_save_bt.Location = New System.Drawing.Point(445, 3)
        Me.cancel_save_bt.Name = "cancel_save_bt"
        Me.cancel_save_bt.Size = New System.Drawing.Size(102, 23)
        Me.cancel_save_bt.TabIndex = 6
        Me.cancel_save_bt.Text = "Cancel Save"
        Me.cancel_save_bt.UseVisualStyleBackColor = True
        '
        'ace_driver_version_bt
        '
        Me.ace_driver_version_bt.Location = New System.Drawing.Point(553, 3)
        Me.ace_driver_version_bt.Name = "ace_driver_version_bt"
        Me.ace_driver_version_bt.Size = New System.Drawing.Size(88, 23)
        Me.ace_driver_version_bt.TabIndex = 7
        Me.ace_driver_version_bt.Text = "Get ACE Driver Version"
        Me.ace_driver_version_bt.UseVisualStyleBackColor = True
        '
        'culture_info_bt
        '
        Me.culture_info_bt.Location = New System.Drawing.Point(647, 3)
        Me.culture_info_bt.Name = "culture_info_bt"
        Me.culture_info_bt.Size = New System.Drawing.Size(88, 23)
        Me.culture_info_bt.TabIndex = 8
        Me.culture_info_bt.Text = "Culture Info"
        Me.culture_info_bt.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1283, 59)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'FormAdmin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1283, 558)
        Me.Controls.Add(Me.admin_form_main_ly)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "FormAdmin"
        Me.Text = "Administration Settings"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.admin_form_main_ly.ResumeLayout(False)
        Me.admin_form_main_ly.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents admin_form_main_ly As FlowLayoutPanel
    Friend WithEvents show_cs_file_lbl As Label
    Friend WithEvents show_cs_file_val As CheckBox
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents enable_auto_save_lbl As Label
    Friend WithEvents enable_auto_save_val As CheckBox
    Friend WithEvents auto_save_interval_lbl As Label
    Friend WithEvents auto_save_interval_val As TextBox
    Friend WithEvents cancel_save_bt As Button
    Friend WithEvents ace_driver_version_bt As Button
    Friend WithEvents culture_info_bt As Button
End Class
