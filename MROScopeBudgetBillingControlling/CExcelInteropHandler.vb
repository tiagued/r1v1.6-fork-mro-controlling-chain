﻿Imports System.Reflection
Public Class CExcelInteropHandler

    Private full_name As String


    Public Sub New(_path As String)
        full_name = _path
        'always need user app for reports
        getUserApp()
    End Sub

    Public ReadOnly Property db_folder As String
        Get
            Return IO.Path.GetDirectoryName(MConstants.GLB_MSTR_CTRL.file_path)
        End Get
    End Property


    Private _xlappUser As Microsoft.Office.Interop.Excel.Application
    Public ReadOnly Property xlappUser() As Microsoft.Office.Interop.Excel.Application
        Get
            Dim isError As Boolean = True
            Try
                Dim count As Integer = _xlappUser.Workbooks.Count
                isError = False
            Catch ex As Exception
            End Try
            If (isError) Then
                getUserApp()
                _xlappUser.Visible = True
                Return _xlappUser
            Else
                _xlappUser.Visible = True
                Return _xlappUser
            End If
        End Get
    End Property

    'get App of the current user session
    Public Sub getUserApp()
        Dim exists As Boolean = False
        Try
            If Not IsNothing(_xlappUser) Then
                _xlappUser.Visible = True
                exists = True
            End If
        Catch ex As Exception
            exists = False
        End Try
        If Not exists Then
            Try
                _xlappUser = Nothing
                _xlappUser = CType(System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application"), Microsoft.Office.Interop.Excel.Application)
                _xlappUser.Visible = True
            Catch ex As Exception
            End Try
            If IsNothing(_xlappUser) Then
                _xlappUser = New Microsoft.Office.Interop.Excel.Application()
                _xlappUser.Visible = True
            End If
        End If
    End Sub

    Public Sub OnCalc(app As Microsoft.Office.Interop.Excel.Application)
        'not possible when wb is hidden
        Try
            app.Calculation = Microsoft.Office.Interop.Excel.XlCalculation.xlCalculationAutomatic
        Catch ex As System.Runtime.InteropServices.COMException
        End Try
        Try
            app.ScreenUpdating = True
            app.EnableEvents = True
            app.DisplayAlerts = True
        Catch ex As System.Runtime.InteropServices.COMException
            Throw New Exception("An error occured while creating the connexion to the Excel file, Please check that Excel is not in Edit Mode")
        End Try
    End Sub

    Public Sub OffCalc(app As Microsoft.Office.Interop.Excel.Application)
        'not possible when wb is hidden
        Try
            app.Calculation = Microsoft.Office.Interop.Excel.XlCalculation.xlCalculationManual
        Catch ex As System.Runtime.InteropServices.COMException
        End Try
        Try
            app.ScreenUpdating = False
            app.EnableEvents = False
            app.DisplayAlerts = False
        Catch ex As System.Runtime.InteropServices.COMException
            Throw New Exception("An error occured while creating the connexion to the Excel file, Please check that Excel is not in Edit Mode")
        End Try
    End Sub


    'open user App template
    Public Function userxlAppOpenTemplate(template As String, newLoc As String) As Microsoft.Office.Interop.Excel.Workbook
        Dim res As Microsoft.Office.Interop.Excel.Workbook
        Try
            getUserApp()
            'copy
            System.IO.File.Copy(template, newLoc, True)
            OffCalc(_xlappUser)
            res = _xlappUser.Workbooks.Open(newLoc, Microsoft.Office.Interop.Excel.XlUpdateLinks.xlUpdateLinksNever, False,,,,,,, True, True,,,,)
        Catch ex As Exception
            Throw New Exception("An error occured while instanciating template " & template & " to new location " & newLoc & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        Finally
            OnCalc(_xlappUser)
        End Try
        Return res
    End Function

    'open user App template
    Public Sub safeWorkbookSave(wb As Microsoft.Office.Interop.Excel.Workbook)
        Try
            OffCalc(wb.Application)
            wb.Save()
        Catch ex As Exception
            Throw New Exception("An error occured while saving workbook " & wb.FullName & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        Finally
            OnCalc(wb.Application)
        End Try
    End Sub

    'insert lines for report
    Public Sub insertLines(report_wb As Microsoft.Office.Interop.Excel.Workbook, report_sht As Microsoft.Office.Interop.Excel.Worksheet,
                           xlrg As Microsoft.Office.Interop.Excel.Range, rows As Integer)
        With report_sht.Range(xlrg.Row & ":" & xlrg.Row + rows - 1)
            .Insert()
        End With
    End Sub

    Public Sub closeUserAppReadOnly()
        Try
            'close any readonly file in user app (opend by oledb)
            For Each wb As Microsoft.Office.Interop.Excel.Workbook In _xlappUser.Workbooks
                If wb.FullName = full_name Then
                    wb.Close(False)
                End If
            Next
        Catch ex As Exception
            'no throw exception cause it willo overwrite previous exception
        End Try
    End Sub


    Private Function IsFileLocked(exception As Exception) As Boolean
        Try
            Dim errorCode As Integer = Runtime.InteropServices.Marshal.GetHRForException(exception) And ((1 << 16) - 1)
            Return errorCode = 32 OrElse errorCode = 33 OrElse errorCode = 1726 OrElse errorCode = 1722
        Catch ex As Exception
            Return False
        End Try
    End Function

    'kill CS File
    Public Sub tryCloseCSFile()
        Try
            Dim lstExcelProcess As List(Of Process)
            Dim fileName As String = System.IO.Path.GetFileName(full_name)
            'when application is hidden MainWindowTitle is empty
            lstExcelProcess = (From p As Process In Process.GetProcesses Where p.ProcessName.ToUpper Like "Excel*".ToUpper And (p.MainWindowTitle Like fileName & "*" Or String.IsNullOrWhiteSpace(p.MainWindowTitle))).ToList
            For Each p As Process In lstExcelProcess
                If Not IsNothing(p) Then
                    Try
                        p.Kill()
                    Catch ex As Exception
                    End Try
                End If
            Next
        Catch ex As Exception
        End Try
    End Sub

    'Release the objects
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

End Class
