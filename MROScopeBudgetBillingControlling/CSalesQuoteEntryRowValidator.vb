﻿Imports Equin.ApplicationFramework
Public Class CSalesQuoteEntryRowValidator
    Public Shared instance As CSalesQuoteEntryRowValidator
    Public Shared Function getInstance() As CSalesQuoteEntryRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CSalesQuoteEntryRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(quoteE As CSalesQuoteEntry)
        quoteE.calc_is_in_error_state = False
    End Sub

    Public Function quote_id(quoteE As CSalesQuoteEntry) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If quoteE.quote_id <= 0 Or IsNothing(quoteE.quote_obj) Then
            err = "Cannot create a Summary Price entry with an empty quote reference"
            res = False
            quoteE.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function main_activity_id(quoteE As CSalesQuoteEntry) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If quoteE.main_activity_id <= 0 Or IsNothing(quoteE.main_activity_obj) Then
            err = "Main Activity Shall not be empty"
            res = False
            quoteE.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function main_activity_obj(quoteE As CSalesQuoteEntry) As KeyValuePair(Of Boolean, String)
        Return main_activity_id(quoteE)
    End Function


    Public Function product(quoteE As CSalesQuoteEntry) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(quoteE.product) Then
            err = "Number cannot be empty"
            res = False
            quoteE.calc_is_in_error_state = True
        Else
            Dim unique As Boolean = True
            For Each view As ObjectView(Of CSalesQuoteEntry) In GLB_MSTR_CTRL.ini_scope_controller.quote_entries_list
                If Not IsNothing(view.Object.product) AndAlso Not view.Object.Equals(quoteE) AndAlso view.Object.product.Equals(quoteE.product, StringComparison.OrdinalIgnoreCase) Then
                    unique = False
                    Exit For
                End If
            Next
            If Not unique Then
                err = "Number is already use by another entry. Number must be unique"
                res = False
                quoteE.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function labor_price_adj(quoteE As CSalesQuoteEntry) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If quoteE.labor_price_adj > 0 Then
            err = "Adjustment must be a negative value"
            res = False
            quoteE.calc_is_in_error_state = True
        ElseIf quoteE.labor_price_adj + quoteE.labor_price < 0 Then
            err = "Adjustment must not be greather than the original value"
            res = False
            quoteE.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function mat_price_adj(quoteE As CSalesQuoteEntry) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If quoteE.mat_price_adj > 0 Then
            err = "Adjustment must be a negative value"
            res = False
            quoteE.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function freight_price_adj(quoteE As CSalesQuoteEntry) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If quoteE.freight_price_adj > 0 Then
            err = "Adjustment must be a negative value"
            res = False
            quoteE.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function repair_price_adj(quoteE As CSalesQuoteEntry) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If quoteE.repair_price_adj > 0 Then
            err = "Adjustment must be a negative value"
            res = False
            quoteE.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function third_party_price_adj(quoteE As CSalesQuoteEntry) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If quoteE.third_party_price_adj > 0 Then
            err = "Adjustment must be a negative value"
            res = False
            quoteE.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

End Class
