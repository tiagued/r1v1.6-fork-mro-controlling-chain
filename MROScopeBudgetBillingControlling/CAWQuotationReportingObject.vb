﻿Public Class CAWQuotationReportingObject
    Public Sub New(_the_awq As CAWQuotation)
        _awq = _the_awq
    End Sub

    Private _awq As CAWQuotation
    Public Property awq() As CAWQuotation
        Get
            Return _awq
        End Get
        Set(ByVal value As CAWQuotation)
            _awq = value
        End Set
    End Property

    Private _lab_sold_approved_calc As Double
    Public Property lab_sold_approved_calc() As Double
        Get
            Return _lab_sold_approved_calc
        End Get
        Set(ByVal value As Double)
            _lab_sold_approved_calc = value
        End Set
    End Property

    Private _lab_sold_pending_calc As Double
    Public Property lab_sold_pending_calc() As Double
        Get
            Return _lab_sold_pending_calc
        End Get
        Set(ByVal value As Double)
            _lab_sold_pending_calc = value
        End Set
    End Property

    Private _lab_sold_approved_and_pending_calc As Double
    Public ReadOnly Property lab_sold_approved_and_pending_calc() As Double
        Get
            Return _lab_sold_approved_calc + _lab_sold_pending_calc
        End Get
    End Property

    Private _mat_sold_approved_calc As Double
    Public Property mat_sold_approved_calc() As Double
        Get
            Return _mat_sold_approved_calc
        End Get
        Set(ByVal value As Double)
            _mat_sold_approved_calc = value
        End Set
    End Property

    Private _mat_sold_pending_calc As Double
    Public Property mat_sold_pending_calc() As Double
        Get
            Return _mat_sold_pending_calc
        End Get
        Set(ByVal value As Double)
            _mat_sold_pending_calc = value
        End Set
    End Property

    Private _mat_sold_approved_and_pending_calc As Double
    Public ReadOnly Property mat_sold_approved_and_pending_calc() As Double
        Get
            Return _mat_sold_approved_calc + _mat_sold_pending_calc
        End Get
    End Property

    Private _is_latest_sent As Boolean
    Public Property is_latest_sent() As Boolean
        Get
            Return _is_latest_sent
        End Get
        Set(ByVal value As Boolean)
            _is_latest_sent = value
        End Set
    End Property

End Class
