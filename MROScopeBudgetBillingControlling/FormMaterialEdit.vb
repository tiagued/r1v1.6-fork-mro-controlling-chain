﻿Public Class FormMaterialEdit
    'navigate activity with arrows
    Private Sub material_select_left_pic_Click(sender As Object, e As EventArgs) Handles material_select_left_pic.Click
        GLB_MSTR_CTRL.material_edit_ctrl.selectedMaterialBackWard()
    End Sub

    'navigate activity with arrows
    Private Sub material_select_right_pic_Click(sender As Object, e As EventArgs) Handles material_select_right_pic.Click
        GLB_MSTR_CTRL.material_edit_ctrl.selectedMaterialForward()
    End Sub

    'material change event
    Private Sub cb_material_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_material.SelectedIndexChanged
        GLB_MSTR_CTRL.material_edit_ctrl.checkComboboxSelectedMaterial()
    End Sub

    Private Sub FormMaterialEdit_FormClosed(sender As Object, e As EventArgs) Handles MyBase.FormClosed

        'MConstants.GLB_MSTR_CTRL.writeAll()
    End Sub

    Private Sub material_error_state_pic_Click(sender As Object, e As EventArgs) Handles material_error_state_pic.Click
        MsgBox(material_error_state_pic.Tag)
    End Sub

    Private Sub change_activity_val_SelectedIndexChanged(sender As Object, e As EventArgs) Handles change_activity_val.SelectedIndexChanged
        GLB_MSTR_CTRL.material_edit_ctrl.comboboxActivityChanged()
    End Sub
End Class