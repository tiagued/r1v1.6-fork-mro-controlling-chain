﻿Imports Equin.ApplicationFramework

Public Class CSalesQuoteRowValidator
    Private Shared instance As CSalesQuoteRowValidator
    Public Shared Function getInstance() As CSalesQuoteRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CSalesQuoteRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(quote As CSalesQuote)
        quote.calc_is_in_error_state = False
    End Sub

    'curr cannot be empty 
    Public Function lab_curr(quote As CSalesQuote) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If quote.lab_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Or String.IsNullOrWhiteSpace(quote.lab_curr) Then
            err = "Currency cannot be empty"
            res = False
            quote.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'curr cannot be empty 
    Public Function mat_curr(quote As CSalesQuote) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If quote.mat_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Or String.IsNullOrWhiteSpace(quote.mat_curr) Then
            err = "Currency cannot be empty"
            res = False
            quote.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'number cannot be empty 
    Public Function number(quote As CSalesQuote) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(quote.number) Then
            err = "Number cannot be empty"
            res = False
            quote.calc_is_in_error_state = True
        Else
            Dim unique As Boolean = True
            For Each view As ObjectView(Of CSalesQuote) In GLB_MSTR_CTRL.ini_scope_controller.quote_list
                If Not IsNothing(view.Object.number) AndAlso Not view.Object.Equals(quote) AndAlso view.Object.number.Equals(quote.number, StringComparison.OrdinalIgnoreCase) Then
                    unique = False
                    Exit For
                End If
            Next
            If Not unique Then
                err = "Number is already use by another entry. Number must be unique"
                res = False
                quote.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'number cannot be empty 
    Public Function description(quote As CSalesQuote) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(quote.description) Then
            err = "Description cannot be empty"
            res = False
            quote.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'number cannot be empty 
    Public Function payer(quote As CSalesQuote) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If quote.payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value OrElse IsNothing(quote.payer_obj) OrElse String.IsNullOrWhiteSpace(quote.payer) Then
            err = "Payer cannot be empty"
            res = False
            quote.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

End Class
