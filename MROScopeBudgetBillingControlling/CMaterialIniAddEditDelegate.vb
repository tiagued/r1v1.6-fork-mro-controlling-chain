﻿Imports System.ComponentModel
Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CMaterialIniAddEditDelegate
    Public form As FormMaterialEdit
    Private latestEditedMaterial As CMaterial
    Private latestMaterialActivity As CScopeActivity
    'launch form
    Public Sub launchEditForm(mat As CMaterial, validator As Object, Optional act As CScopeActivity = Nothing)
        Try
            latestEditedMaterial = Nothing
            latestMaterialActivity = Nothing

            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "preparing view to edit material..."
            form = New FormMaterialEdit

            'initialize activity selection combobox
            Dim matListHandler As BindingListView(Of CMaterial)
            If act Is Nothing Then
                matListHandler = New BindingListView(Of CMaterial)(MConstants.GLB_MSTR_CTRL.material_controller.matList.DataSource)
            Else
                matListHandler = New BindingListView(Of CMaterial)(act.material_list)
            End If
            'remove deleted
            matListHandler.ApplyFilter(AddressOf MConstants.GLB_MSTR_CTRL.material_controller.filterDeletedMaterials)

            Dim bds As New BindingSource
            bds.DataSource = MGeneralFuntionsViewControl.getMaterialFromListView(matListHandler)

            'set combobox list
            form.cb_material.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            form.cb_material.DropDownStyle = ComboBoxStyle.DropDownList
            form.cb_material.DataSource = bds
            MViewEventHandler.addDefaultComboBoxEventHandler(form.cb_material)
            'set selected object
            form.cb_material.SelectedItem = mat

            'fill data in views
            updateView()

            'add error handlers
            Dim matEditMap As CViewModelMapList
            matEditMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_MAT_EDIT_VIEW_DISPLAY)
            For Each map As CViewModelMap In matEditMap.list.Values
                Dim _ctrl As Control = Nothing
                _ctrl = form.Controls.Find(map.view_control, True).FirstOrDefault
                If Not IsNothing(_ctrl) Then
                    CEditPanelViewRowValidationHandler.addNew(_ctrl, validator, matEditMap, mat.GetType, form.material_error_state_pic, form)
                End If
            Next

            form.ShowDialog()
            form.BringToFront()
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End preparing view to edit material"
        Catch ex As Exception
            MsgBox("An error occured while preparing the windows to edit material " & Chr(10) & Chr(10) & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    'if index changed selected activity
    Public Sub selectedMaterialChanged()
        Dim mat As CMaterial = form.cb_material.SelectedItem
        'use this to update view only when selected material change. cause due to object binding, when any field of material is updated, all the controls are updated even the selected material combobox
        If latestEditedMaterial IsNot Nothing AndAlso Not latestEditedMaterial.Equals(mat) Then
            updateView()
        End If

    End Sub
    'Update page
    Public Sub updateView()
        Try
            'mat details
            Dim mat As CMaterial = form.cb_material.SelectedItem

            'update act view
            updateActivityView()

            latestEditedMaterial = mat
            'bind material data
            Dim matEditMap As CViewModelMapList

            Dim bds As BindingSource
            Dim tbbd As Binding
            'Bind current material to the view
            matEditMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_MAT_EDIT_VIEW_DISPLAY)
            For Each map As CViewModelMap In matEditMap.list.Values
                Dim _ctrl As Control = Nothing
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim textBox As TextBox
                    textBox = CType(form.Controls.Find(map.view_control, True).FirstOrDefault, TextBox)
                    _ctrl = textBox
                    textBox.DataBindings.Clear()
                    tbbd = textBox.DataBindings.Add("Text", mat, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    'format double
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        tbbd.FormatInfo = MConstants.CULT_INFO
                        tbbd.FormatString = map.col_format
                        tbbd.FormattingEnabled = True
                    End If
                    textBox.Enabled = map.col_editable
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim comboBox As ComboBox
                    comboBox = CType(form.Controls.Find(map.view_control, True).FirstOrDefault, ComboBox)
                    _ctrl = comboBox
                    comboBox.DataBindings.Clear()
                    comboBox.AutoCompleteMode = AutoCompleteMode.None
                    comboBox.DropDownStyle = ComboBoxStyle.DropDownList
                    comboBox.Enabled = map.col_editable
                    MViewEventHandler.addDefaultComboBoxEventHandler(comboBox)

                    bds = New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        comboBox.DataSource = bds
                        comboBox.DisplayMember = "value"
                        comboBox.ValueMember = "key"
                        comboBox.DataBindings.Add("SelectedValue", mat, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        comboBox.DataSource = bds
                        comboBox.DisplayMember = "value"
                        comboBox.ValueMember = "key"
                        comboBox.DataBindings.Add("SelectedValue", mat, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    ElseIf map.view_control = MConstants.CONST_EDIT_MAT_ACT_CB Then
                        comboBox.DataSource = MConstants.GLB_MSTR_CTRL.add_scope_controller._scopeListSorted_bds
                        comboBox.DisplayMember = "act_notif_opcode"
                        comboBox.DataBindings.Add("SelectedItem", mat, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    ElseIf map.view_control = MConstants.CONST_EDIT_MAT_DISC_CB Then
                        comboBox.DataSource = GLB_MSTR_CTRL.billing_cond_controller._discountList_lov_mat_po
                        comboBox.DisplayMember = "label"
                        comboBox.DataBindings.Add("SelectedItem", mat, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    ElseIf map.view_control = MConstants.CONST_EDIT_MAT_NTE_CB Then
                        comboBox.DataSource = GLB_MSTR_CTRL.billing_cond_controller._nteList_lov_mat_po
                        comboBox.DisplayMember = "label"
                        comboBox.DataBindings.Add("SelectedItem", mat, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    ElseIf map.view_control = MConstants.CONST_EDIT_MAT_PAYER_CB Then
                        bds.DataSource = MConstants.listOfPayer.Values
                        comboBox.DataSource = bds
                        comboBox.DisplayMember = "label"
                        comboBox.DataBindings.Add("SelectedItem", mat, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    ElseIf map.view_control = MConstants.CONST_EDIT_MAT_FREIGHT_ZFRP_CB Then
                        bds.DataSource = GLB_MSTR_CTRL.billing_cond_controller.freightList.DataSource
                        comboBox.DataSource = bds
                        comboBox.DisplayMember = "freight_to_string"
                        comboBox.DataBindings.Add("SelectedItem", mat, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    Else
                        Throw New Exception("Error occured when binding material edit view. Property " & map.view_control & ". List " & map.view_list_name & " does not exist in LOV")
                    End If
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim chk As CheckBox
                    chk = CType(form.Controls.Find(map.view_control, True).FirstOrDefault, CheckBox)
                    _ctrl = chk
                    chk.DataBindings.Clear()
                    chk.DataBindings.Add("Checked", mat, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    chk.Enabled = map.col_editable
                End If
                If Not IsNothing(_ctrl) Then
                    'initialize control validation
                    CEditPanelViewRowValidationHandler.validateControl(_ctrl)
                End If
                If Not mat.isEditable.Key Then
                    _ctrl.Enabled = False
                End If
            Next

        Catch ex As Exception
            MsgBox("An error occured while loading material data in edit form view" & Chr(10) & Chr(10) & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub updateActivityView(Optional act_cb_change As Boolean = False)
        'mat details
        Dim mat As CMaterial = form.cb_material.SelectedItem

        Dim updateAct As Boolean = False

        'update activity view only if activity changed
        updateAct = act_cb_change AndAlso (IsNothing(latestMaterialActivity) OrElse Not latestMaterialActivity.Equals(mat.activity_obj))
        If Not updateAct Then
            'update due to navigation via Material List
            updateAct = Not act_cb_change AndAlso (IsNothing(latestEditedMaterial) OrElse Not latestEditedMaterial.activity_obj.Equals(mat.activity_obj))
        End If
        If updateAct And Not IsNothing(mat) Then
            Dim actEditMap As CViewModelMapList = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ACT_EDIT_IN_MAT_EDIT_VIEW_DISPLAY)
            GLB_MSTR_CTRL.add_scope_hours_edit_ctrl.updateActivityEditView(mat.activity_obj, actEditMap, form)
            latestMaterialActivity = mat.activity_obj
        End If
    End Sub
    Public Function checkCurrentItemError(check_previous As Boolean) As Boolean
        Dim mat As CMaterial
        If check_previous Then
            mat = latestEditedMaterial
            'if no previous material return no error by default
            If IsNothing(mat) Then
                Return False
            End If
        Else
            mat = form.cb_material.SelectedItem
        End If

        If mat.calc_is_in_error_state Then
            MsgBox("Fix Errors on current Item", vbCritical)
            Return True
        End If
        Return False
    End Function
    'check there are no errors on the previous material object
    Public Sub checkComboboxSelectedMaterial()
        Dim mat As CMaterial = form.cb_material.SelectedItem
        'if no previous material, allow the move
        If IsNothing(latestEditedMaterial) Then
            Exit Sub
        End If
        'if user selected the same material, exit
        If latestEditedMaterial.Equals(mat) Then
            Exit Sub
        End If

        'if error on previous
        If checkCurrentItemError(True) Then
            form.cb_material.SelectedItem = latestEditedMaterial
        Else
            'allow view refresh
            selectedMaterialChanged()
        End If
    End Sub

    'update activity view
    Public Sub comboboxActivityChanged()
        updateActivityView(True)
    End Sub

    'select new activity via arrows
    'navigate forward quote entries
    Public Sub selectedMaterialForward()
        Try

            'check if error on current material
            If checkCurrentItemError(False) Then
                Exit Sub
            End If
            If Not form.cb_material.SelectedIndex + 1 > form.cb_material.Items.Count - 1 Then
                form.cb_material.SelectedIndex = form.cb_material.SelectedIndex + 1
            End If
        Catch ex As Exception
            Throw New Exception("Error occured when moving mat " & ex.Message)
        End Try
    End Sub
    'navigate backward quote entries
    Public Sub selectedMaterialBackWard()
        Try
            'check if error on current material
            If checkCurrentItemError(False) Then
                Exit Sub
            End If
            If Not form.cb_material.SelectedIndex - 1 < 0 Then
                form.cb_material.SelectedIndex = form.cb_material.SelectedIndex - 1
            End If
        Catch ex As Exception
            Throw New Exception("Error occured when moving mat " & ex.Message)
        End Try

    End Sub

End Class
