﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CScopeActivity
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewListable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable
    'used to created new ids
    Public Shared currentMaxID As Integer
    Public Shared currentMaxFakeAct As Integer

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        If Not IsNothing(MConstants.GLB_MSTR_CTRL.notifHelper) Then
            MConstants.GLB_MSTR_CTRL.notifHelper.OnPropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
        End If
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        If Not _calc_is_in_error_state AndAlso Not String.IsNullOrWhiteSpace(_calc_error_text_thrown) Then
            Return New KeyValuePair(Of Boolean, String)(True, _calc_error_text_thrown)
        Else
            Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
        End If
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res As Boolean = False
        Dim mess As String = ""
        Try
            If Not _is_fake_activity Then
                res = False
                mess = "Item is imported from SAP so cannot be deleted, use appropriate report flags to hide/show it in customer reports"
            Else
                Dim isUsed As KeyValuePair(Of Boolean, List(Of String)) = MDataConsistency.isFakeActivityUsed(Me)
                If isUsed.Key Then
                    res = False
                    mess = "Item " & Me.tostring & " Cannot be deleted because it is used in " & isUsed.Value.Count & " AWQ(s) " & Chr(10) & "-" & String.Join(Chr(10) & "-", isUsed.Value)
                Else
                    Me.system_status = MConstants.CONST_SYS_ROW_DELETED
                    MConstants.GLB_MSTR_CTRL.add_scope_controller.deleteObjectActivity(Me)
                    res = True
                End If
            End If
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.tostring & Chr(10) & ex.Message & Chr(10) & ex.StackTrace
        End Try
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Return New KeyValuePair(Of Boolean, String)(True, "")
    End Function

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return _id > 0
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
        For Each soldH As CSoldHours In sold_hour_list
            soldH.act_id = _id
        Next
        For Each mat As CMaterial In material_list
            mat.activity_id = _id
        Next
        'set other objects
        If Not IsNothing(_awq_master_obj) AndAlso Object.Equals(_awq_master_obj.main_activity_obj, Me) Then
            _awq_master_obj.main_activity_id = _id
        End If

        For Each quote_act_lk As CSaleQuoteEntryToActLink In _quote_entry_link_list
            quote_act_lk.act_id = _id
            If Not IsNothing(quote_act_lk.init_quote_entry_obj) AndAlso Object.Equals(quote_act_lk.init_quote_entry_obj.main_activity_obj, Me) Then
                quote_act_lk.init_quote_entry_obj.main_activity_id = _id
            End If
        Next
    End Sub

    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_ACT_DB_READ
    End Function

    Public Function isEmptyItem() As Boolean Implements IGenericInterfaces.IDataGridViewListable.isEmptyItem
        Return _id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
    End Function

    Public calculator As CScopeActivityCalculator

    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_ACTIVITY_LOADED Then
            'set combo emppty
            _category = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _nte_category_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
            _nte_obj = MConstants.EMPTY_NTE
            _billing_work_status = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _discount_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
            _discount_obj = MConstants.EMPTY_DISC
            _value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _proj = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            'marks
            _labor_sold_mark = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _mat_sold_mark = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _rep_sold_mark = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _serv_sold_mark = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _freight_sold_mark = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        End If
        _sold_hour_list = New List(Of CSoldHours)
        _material_list = New List(Of CMaterial)
        _quote_entry_link_list = New List(Of CSaleQuoteEntryToActLink)
        calculator = New CScopeActivityCalculator(Me)
    End Sub
    Public Shared Function getEmptyAct() As CScopeActivity
        Dim currState As Boolean
        currState = MConstants.GLOB_ACTIVITY_LOADED
        MConstants.GLOB_ACTIVITY_LOADED = False
        Dim empty As New CScopeActivity
        empty.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
        'if proj nothing it will cause issue in combobox selection
        empty.proj = " "
        MConstants.GLOB_ACTIVITY_LOADED = currState
        Return empty
    End Function

    Public Shared Function getSelectAllAct() As CScopeActivity
        Dim currState As Boolean
        currState = MConstants.GLOB_ACTIVITY_LOADED
        MConstants.GLOB_ACTIVITY_LOADED = False
        Dim allAct As New CScopeActivity
        allAct.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
        'if proj nothing it will cause issue in combobox selection
        allAct.proj = " "
        allAct.act = "Select All"
        MConstants.GLOB_ACTIVITY_LOADED = currState
        Return allAct
    End Function

    Public Sub setFakeAct()
        CScopeActivity.currentMaxFakeAct = CScopeActivity.currentMaxFakeAct + 1
        Dim act_str = ""
        If CScopeActivity.currentMaxFakeAct < 100 Then
            act_str = CScopeActivity.currentMaxFakeAct.ToString("000")
        Else
            act_str = CScopeActivity.currentMaxFakeAct.ToString()
        End If
        _act = MConstants.constantConf(MConstants.CONST_CONF_FAKE_ACT_PREFIX).value & act_str
    End Sub

    'Activity Data
    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_ACTIVITY_LOADED Then
                    CScopeActivity.currentMaxID = Math.Max(CScopeActivity.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _proj As String
    Public Property proj() As String
        Get
            Return _proj
        End Get
        Set(ByVal value As String)
            If Not _proj = value Then
                _proj = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _act As String
    Public Property act() As String
        Get
            Return _act
        End Get
        Set(ByVal value As String)
            If Not _act = value Then
                _act = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _notification As String
    Public Property notification() As String
        Get
            Return _notification
        End Get
        Set(ByVal value As String)
            If Not _notification = value Then
                _notification = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _job_card As String
    Public Property job_card() As String
        Get
            Return _job_card
        End Get
        Set(ByVal value As String)
            If Not _job_card = value Then
                If value.Length < 4 Then
                    Dim valInt As Integer
                    If Integer.TryParse(value, valInt) Then
                        value = valInt.ToString("D4")
                    End If
                End If
                _job_card = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _status As String
    Public Property status() As String
        Get
            Return _status
        End Get
        Set(ByVal value As String)
            If Not _status = value Then
                _status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_cost_collector As Boolean
    Public Property is_cost_collector() As Boolean
        Get
            Return _is_cost_collector
        End Get
        Set(ByVal value As Boolean)
            If Not _is_cost_collector = value Then
                _is_cost_collector = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public ReadOnly Property status_view() As String
        Get
            If MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_STATUS_FRIENDLY_LIST).ContainsKey(Trim(_status)) Then
                Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_STATUS_FRIENDLY_LIST)(Trim(_status)).value
            Else
                Return Trim(_status)
            End If
        End Get
    End Property

    'text to check if there are any error
    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_is_in_error_state = value Then
                _calc_is_in_error_state = value
            End If
        End Set
    End Property

    Private _category As String
    Public Property category() As String
        Get
            Return _category
        End Get
        Set(ByVal value As String)
            If Not _category = value Then
                _category = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _category_view As String
    Public ReadOnly Property category_view() As String
        Get
            If _warr_code = MConstants.constantConf(MConstants.CONST_CONF_LOV_NTE_TYPE_NTEN_KEY).value OrElse
                     _warr_code = MConstants.constantConf(MConstants.CONST_CONF_LOV_NTE_TYPE_NTEY_KEY).value Then
                Return MConstants.listOfValueConf_edit(MConstants.CONST_CONF_EDIT_LOV_ACT_CATEGORY)(_category).value & "(" & _warr_code & ")"
            Else
                Return MConstants.listOfValueConf_edit(MConstants.CONST_CONF_EDIT_LOV_ACT_CATEGORY)(_category).value
            End If
        End Get
    End Property

    Private _nte_category_id As Long
    Public Property nte_category_id() As Long
        Get
            Return _nte_category_id
        End Get
        Set(ByVal value As Long)
            If Not _nte_category_id = value Then
                _nte_category_id = value
                'retrieve only for none empty
                If _nte_category_id > 0 Then
                    _nte_obj = MConstants.GLB_MSTR_CTRL.billing_cond_controller.getNtetObject(_nte_category_id)
                Else
                    _nte_obj = MConstants.EMPTY_NTE
                    _nte_category_id = MConstants.EMPTY_NTE.id
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _nte_obj As CNotToExceed
    Public Property nte_obj() As CNotToExceed
        Get
            Return _nte_obj
        End Get
        Set(ByVal value As CNotToExceed)
            If Not Object.Equals(_nte_obj, value) Then
                _nte_obj = value
                'set ID
                If IsNothing(_nte_obj) Then
                    _nte_category_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _nte_category_id = _nte_obj.id
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _act_desc As String
    Public Property act_desc() As String
        Get
            Return _act_desc
        End Get
        Set(ByVal value As String)
            If Not _act_desc = value Then
                _act_desc = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _act_long_desc As String
    Public Property act_long_desc() As String
        Get
            Return _act_long_desc
        End Get
        Set(ByVal value As String)
            If Not _act_long_desc = value Then
                _act_long_desc = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _check_type As String
    Public Property check_type() As String
        Get
            Return _check_type
        End Get
        Set(ByVal value As String)
            If Not _check_type = value Then
                _check_type = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _warr_code As String
    Public Property warr_code() As String
        Get
            Return _warr_code
        End Get
        Set(ByVal value As String)
            If Not _warr_code = value Then
                _warr_code = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _operator_code As String
    Public Property operator_code() As String
        Get
            Return _operator_code
        End Get
        Set(ByVal value As String)
            If Not _operator_code = value Then
                _operator_code = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    'revision control
    Private _revision_control_status As String
    Public Property revision_control_status() As String
        Get
            Return _revision_control_status
        End Get
        Set(ByVal value As String)
            If Not _revision_control_status = value Then
                _revision_control_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _corrective_action As String
    Public Property corrective_action() As String
        Get
            Return _corrective_action
        End Get
        Set(ByVal value As String)
            If Not _corrective_action = value Then
                _corrective_action = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _revision_control_txt As String
    Public Property revision_control_txt() As String
        Get
            Return _revision_control_txt
        End Get
        Set(ByVal value As String)
            If Not _revision_control_txt = value Then
                _revision_control_txt = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _billing_work_status As String
    Public Property billing_work_status() As String
        Get
            Return _billing_work_status
        End Get
        Set(ByVal value As String)
            If Not _billing_work_status = value Then
                _billing_work_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _sold_hour_list As List(Of CSoldHours)
    Public Property sold_hour_list() As List(Of CSoldHours)
        Get
            Return _sold_hour_list
        End Get
        Set(ByVal value As List(Of CSoldHours))
            _sold_hour_list = value
        End Set
    End Property

    Private _material_list As List(Of CMaterial)
    Public Property material_list() As List(Of CMaterial)
        Get
            Return _material_list
        End Get
        Set(ByVal value As List(Of CMaterial))
            _material_list = value
        End Set
    End Property

    Private _quote_entry_link_list As List(Of CSaleQuoteEntryToActLink)
    Public Property quote_entry_link_list() As List(Of CSaleQuoteEntryToActLink)
        Get
            Return _quote_entry_link_list
        End Get
        Set(ByVal value As List(Of CSaleQuoteEntryToActLink))
            _quote_entry_link_list = value
        End Set
    End Property

    'oem
    Private _oem_approved_status As String
    Public Property oem_approved_status() As String
        Get
            Return _oem_approved_status
        End Get
        Set(ByVal value As String)
            If Not _oem_approved_status = value Then
                _oem_approved_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_fake_activity As Boolean
    Public Property is_fake_activity() As Boolean
        Get
            Return _is_fake_activity
        End Get
        Set(ByVal value As Boolean)
            If Not _is_fake_activity = value Then
                _is_fake_activity = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Function isActLinkedToAnyNonCancelledAWQ() As Boolean
        Dim res As Boolean = False
        If Not IsNothing(_awq_master_obj) Then
            For Each awq As CAWQuotation In _awq_master_obj.revision_list
                If awq.is_approved_closed OrElse awq.is_in_work OrElse awq.is_open_and_sent_to_customer Then
                    res = True
                    Exit For
                End If
            Next
        End If
        Return res
    End Function

    'billing data
    Private _fix_labor As Double
    Public Property fix_labor() As Double
        Get
            Return _fix_labor
        End Get
        Set(ByVal value As Double)
            If Not _fix_labor = value Then
                _fix_labor = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _discount_id As Long
    Public Property discount_id() As Long
        Get
            Return _discount_id
        End Get
        Set(ByVal value As Long)
            If Not _discount_id = value Then
                _discount_id = value
                If _discount_id > 0 Then
                    _discount_obj = MConstants.GLB_MSTR_CTRL.billing_cond_controller.getDiscountObject(_discount_id)
                Else
                    _discount_obj = MConstants.EMPTY_DISC
                    _discount_id = MConstants.EMPTY_DISC.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _discount_obj As CDiscount
    Public Property discount_obj() As CDiscount
        Get
            Return _discount_obj
        End Get
        Set(ByVal value As CDiscount)
            If Not Object.Equals(value, _discount_obj) Then
                _discount_obj = value
                'set ID
                If IsNothing(_discount_obj) Then
                    _discount_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _discount_id = _discount_obj.id
                End If
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'master AWQ
    Private _awq_master_id As Long
    Public Property awq_master_id() As Long
        Get
            Return _awq_master_id
        End Get
        Set(ByVal value As Long)
            If Not _awq_master_id = value Then
                _awq_master_id = value
                If MConstants.GLOB_AWQ_LOADED Then
                    'retrieve only for none empty
                    If _awq_master_id > 0 Then
                        If Not (Not IsNothing(_awq_master_obj) AndAlso _awq_master_obj.id = _awq_master_id) Then
                            _awq_master_obj = MConstants.GLB_MSTR_CTRL.awq_controller.getAWQObject(_awq_master_id)
                        End If
                    Else
                        _awq_master_obj = Nothing
                    End If
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _awq_master_obj As CAWQuotation
    Public Property awq_master_obj() As CAWQuotation
        Get
            Return _awq_master_obj
        End Get
        Set(ByVal value As CAWQuotation)
            If Not Object.Equals(_awq_master_obj, value) Then
                _awq_master_obj = value
                'set ID
                If IsNothing(_awq_master_obj) Then
                    Me.awq_master_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    Me.awq_master_id = _awq_master_obj.id
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _value_analysis As String
    Public Property value_analysis() As String
        Get
            Return _value_analysis
        End Get
        Set(ByVal value As String)
            If Not _value_analysis = value Then
                _value_analysis = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _waste_cause As String
    Public Property waste_cause() As String
        Get
            Return _waste_cause
        End Get
        Set(ByVal value As String)
            If Not _waste_cause = value Then
                _waste_cause = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_odi As Boolean
    Public Property is_odi() As Boolean
        Get
            Return _is_odi
        End Get
        Set(ByVal value As Boolean)
            If Not _is_odi = value Then
                _is_odi = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_hidden As Boolean
    Public Property is_hidden() As Boolean
        Get
            Return _is_hidden
        End Get
        Set(ByVal value As Boolean)
            If Not _is_hidden = value Then
                _is_hidden = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_small_mat As Boolean
    Public Property is_small_mat() As Boolean
        Get
            Return _is_small_mat
        End Get
        Set(ByVal value As Boolean)
            If Not _is_small_mat = value Then
                _is_small_mat = value
                updateCal()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_init_calc As Boolean
    Public Property is_init_calc() As Boolean
        Get
            'calculated value
            Return _quote_entry_link_list.Count > 0
        End Get
        Set(ByVal value As Boolean)
            'just to force display
        End Set
    End Property

    'to string
    Public Overrides Function tostring() As String
        Dim opcode_str As String = ""
        If Not String.IsNullOrWhiteSpace(_operator_code) Then
            opcode_str = "  OPCODE-" & operator_code
        End If
        Return _proj & "-" & _act & "  JC-" & _job_card & opcode_str & " " & _act_desc
    End Function
    Public ReadOnly Property act_notif_opcode As String
        Get
            Return tostring()
        End Get
    End Property

    'marks
    Private _labor_sold_mark As String
    Public Property labor_sold_mark() As String
        Get
            Return _labor_sold_mark
        End Get
        Set(ByVal value As String)
            If Not value = _labor_sold_mark Then
                _labor_sold_mark = value
                updateCal()
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _mat_sold_mark As String
    Public Property mat_sold_mark() As String
        Get
            Return _mat_sold_mark
        End Get
        Set(ByVal value As String)
            If Not value = _mat_sold_mark Then
                _mat_sold_mark = value
                updateCal()
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _rep_sold_mark As String
    Public Property rep_sold_mark() As String
        Get
            Return _rep_sold_mark
        End Get
        Set(ByVal value As String)
            If Not value = _rep_sold_mark Then
                _rep_sold_mark = value
                updateCal()
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _serv_sold_mark As String
    Public Property serv_sold_mark() As String
        Get
            Return _serv_sold_mark
        End Get
        Set(ByVal value As String)
            If Not value = _serv_sold_mark Then
                _serv_sold_mark = value
                updateCal()
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _freight_sold_mark As String
    Public Property freight_sold_mark() As String
        Get
            Return _freight_sold_mark
        End Get
        Set(ByVal value As String)
            If Not value = _freight_sold_mark Then
                _freight_sold_mark = value
                updateCal()
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property



    'calculated
    Private _hours_sold_calc As Double
    Public Property hours_sold_calc() As Double
        Get
            Return _hours_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _hours_sold_calc Then
                _hours_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _labor_sold_calc As Double
    Public Property labor_sold_calc() As Double
        Get
            Return _labor_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _labor_sold_calc Then
                _labor_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _mat_sold_calc As Double
    Public Property mat_sold_calc() As Double
        Get
            Return _mat_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _mat_sold_calc Then
                _mat_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _rep_sold_calc As Double
    Public Property rep_sold_calc() As Double
        Get
            Return _rep_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _rep_sold_calc Then
                _rep_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _serv_sold_calc As Double
    Public Property serv_sold_calc() As Double
        Get
            Return _serv_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _serv_sold_calc Then
                _serv_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _freight_sold_calc As Double
    Public Property freight_sold_calc() As Double
        Get
            Return _freight_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _freight_sold_calc Then
                _freight_sold_calc = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _mat_info_calc As String
    Public Property mat_info_calc() As String
        Get
            Return _mat_info_calc
        End Get
        Set(ByVal value As String)
            _mat_info_calc = value
            NotifyPropertyChanged()
        End Set
    End Property

    'string values
    Private _labor_sold_calc_str As String
    Public Property labor_sold_calc_str() As String
        Get
            Return _labor_sold_calc_str
        End Get
        Set(ByVal value As String)
            If Not value = _labor_sold_calc_str Then
                _labor_sold_calc_str = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _mat_sold_calc_str As String
    Public Property mat_sold_calc_str() As String
        Get
            Return _mat_sold_calc_str
        End Get
        Set(ByVal value As String)
            If Not value = _mat_sold_calc_str Then
                _mat_sold_calc_str = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _rep_sold_calc_str As String
    Public Property rep_sold_calc_str() As String
        Get
            Return _rep_sold_calc_str
        End Get
        Set(ByVal value As String)
            If Not value = _rep_sold_calc_str Then
                _rep_sold_calc_str = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _serv_sold_calc_str As String
    Public Property serv_sold_calc_str() As String
        Get
            Return _serv_sold_calc_str
        End Get
        Set(ByVal value As String)
            If Not value = _serv_sold_calc_str Then
                _serv_sold_calc_str = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _freight_sold_calc_str As String
    Public Property freight_sold_calc_str() As String
        Get
            Return _freight_sold_calc_str
        End Get
        Set(ByVal value As String)
            If Not value = _freight_sold_calc_str Then
                _freight_sold_calc_str = value
                NotifyPropertyChanged()
            End If
        End Set
    End Property

    Private _comment As String
    Public Property comment() As String
        Get
            Return _comment
        End Get
        Set(ByVal value As String)
            If Not _comment = value Then
                _comment = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not _calc_error_text = value Then
                _calc_error_text = value
            End If
        End Set
    End Property


    Private _calc_error_text_thrown As String
    Public Property calc_error_text_thrown() As String
        Get
            Return _calc_error_text_thrown
        End Get
        Set(ByVal value As String)
            NotifyPropertyChanged()
        End Set
    End Property

    'refresh all calculated and refresh labor and material
    Public Sub updateMatLabCal()
        For Each soldHr In _sold_hour_list
            soldHr.updateCal(False)
        Next
        For Each mat In _material_list
            mat.updateCal(False)
        Next
        updateCal()
    End Sub


    Public Function formatDouble(value As Double) As String
        Return value.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
    End Function

    'refresh all calculated values of activity only
    Public Sub updateCal(Optional refreshProj As Boolean = False)

        Try
            'done it only in runtime, not when reading DB
            If MConstants.GLOB_ACTIVITY_LOADED And Not MConstants.GLOB_SAP_IMPOT_RUNNING Then
                _calc_error_text_thrown = ""
                Me.hours_sold_calc = 0
                Me.labor_sold_calc = 0
                Me.mat_sold_calc = 0
                Me.freight_sold_calc = 0
                Me.rep_sold_calc = 0
                Me.serv_sold_calc = 0
                Me.mat_info_calc = 0

                'displayed values
                Me.labor_sold_calc_str = ""
                Me.mat_sold_calc_str = ""
                Me.freight_sold_calc_str = ""
                Me.rep_sold_calc_str = ""
                Me.serv_sold_calc_str = ""

                calculator.update()
                Dim finalPrice As CScopeAtivityCalcEntry = calculator.getDefaultPayerPriceBeforeDiscount
                If IsNothing(finalPrice) Then
                    finalPrice = calculator.emptry_entry
                End If
                Me.hours_sold_calc = finalPrice.hours_sold_calc
                Me.labor_sold_calc = finalPrice.labor_sold_calc
                Me.mat_sold_calc = finalPrice.mat_sold_calc
                Me.freight_sold_calc = finalPrice.freight_sold_calc
                Me.rep_sold_calc = finalPrice.rep_sold_calc
                Me.serv_sold_calc = finalPrice.serv_sold_calc
                Me.mat_info_calc = finalPrice.mat_info_calc

                If refreshProj Then
                    MConstants.GLB_MSTR_CTRL.proj_calculator.update(MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value, Nothing)
                End If
            End If
        Catch ex As Exception
            _calc_error_text_thrown = "Calculation Error:" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace
        End Try
    End Sub
    'reset revision control data before sap update
    'Public Sub reset_revision_control()
    '    _revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NOSAP
    '    For Each soldhrs As CSoldHours In _sold_hour_list
    '        soldhrs.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NOSAP
    '        soldhrs.revision_control_txt = ""
    '    Next
    'End Sub

    Public Function UpdateFromSAP(act As CImportScopeObject) As Boolean
        Dim updated As Boolean
        Dim mess As String = ""
        'job card
        'status
        If Not _job_card = act.job_card Then
            mess = mess & "JC:" & _job_card & "=>" & act.job_card & ", "
            _job_card = act.job_card
            updated = True
        End If
        'status
        If Not _status = act.user_stat Then
            mess = mess & "status:" & _status & "=>" & act.user_stat & ", "
            _status = act.user_stat
            updated = True
        End If
        'desc
        If Not _act_desc = act.notif_desc Then
            mess = mess & "desc:" & _act_desc & "=>" & act.notif_desc & ", "
            _act_desc = act.notif_desc
            updated = True
        End If
        'long desc
        If Not _act_long_desc = act.notif_text Then
            mess = mess & "long desc:" & _act_long_desc & "=>" & act.notif_text & ", "
            _act_long_desc = act.notif_text
            updated = True
        End If
        'check type
        If Not _check_type = act.check_type Then
            mess = mess & "check type:" & _check_type & "=>" & act.check_type & ", "
            _check_type = act.check_type
            updated = True
        End If

        If Not _warr_code = act.warr_code Then
            mess = mess & "warr code:" & _warr_code & "=>" & act.warr_code & ", "
            _warr_code = act.warr_code
            updated = True
        End If

        If updated Then
            If mess.EndsWith(", ") Then
                mess = mess.Substring(0, mess.Length - 2)
            End If

            logRevisionControlText(mess)

            Me.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_UPDATED
        Else
            Me.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NOCHANGE
        End If

        'systematically update corr actions
        Me.corrective_action = String.Join(Chr(10), act.correctiveActionList)

        Return updated
    End Function

    Public Sub logRevisionControlText(mess As String)
        Try
            If String.IsNullOrWhiteSpace(_revision_control_txt) Then
                Me.revision_control_txt = DateTime.Now.ToString("dd/MM/yy hh:mm:ss") & " " & mess
            Else
                Me.revision_control_txt = DateTime.Now.ToString("dd/MM/yy hh:mm:ss") & " " & mess & Chr(10) & _revision_control_txt
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Function UpdateCostCollectorFromSAP(act As CImportScopeObject) As Boolean
        Dim updated As Boolean
        Dim mess As String = ""

        'status
        If Not _status = act.user_stat Then
            mess = mess & "status:" & _status & "=>" & act.user_stat & ", "
            _status = act.user_stat
            updated = True
        End If
        'desc
        If Not _act_desc = act.notif_desc Then
            mess = mess & "desc:" & _act_desc & "=>" & act.notif_desc & ", "
            _act_desc = act.notif_desc
            _act_long_desc = _act_desc
            updated = True
        End If

        If updated Then
            If mess.EndsWith(", ") Then
                mess = mess.Substring(0, mess.Length - 2)
            End If

            logRevisionControlText(mess)

            Me.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_UPDATED
        Else
            Me.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NOCHANGE
        End If

        Return updated
    End Function

    Public Function getActKey() As String
        Return Trim(_proj) & MConstants.SEP_DEFAULT & Trim(_act) & MConstants.SEP_DEFAULT & Trim(_notification)
    End Function
    'same as material key
    Public Function getMatNetWorkActivity() As String
        Return _proj & _act
    End Function


    'get sap imported hours only
    Public Function getSAPActivities() As Dictionary(Of String, CSoldHours)
        Dim res As New Dictionary(Of String, CSoldHours)
        For Each csold As CSoldHours In sold_hour_list
            If csold.is_sap_import AndAlso Not res.ContainsKey(csold.plan_cc) Then
                res.Add(csold.plan_cc, csold)
            End If
        Next
        Return res
    End Function

    Public Function isWarrCodeNTE() As Boolean
        Return _warr_code = MConstants.constantConf(MConstants.CONST_CONF_WARR_CODE_NTE_KEY).value
    End Function

    Public Function isWarrCodeNTEExclusion() As Boolean
        Return _warr_code = MConstants.constantConf(MConstants.CONST_CONF_WARR_CODE_NTE_EXCL_KEY).value
    End Function

    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

End Class
