﻿Imports System.ComponentModel
Imports System.Reflection


Public Class CProjectDelegate

    'reference to master control
    Public mctrl As CMasterController
    Private _project As CProjectModel
    Public project_cb_list As Dictionary(Of String, String)

    Public Property project() As CProjectModel
        Get
            Return _project
        End Get
        Set(ByVal value As CProjectModel)
            _project = value
        End Set
    End Property

    Public Sub New(ctrl As CMasterController)
        mctrl = ctrl
    End Sub

    Public Sub model_readProjectDB()
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to project table..."
            _project = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_PROJ_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_PROJ_DB_READ), False)(0)
            _project.master_controller = mctrl
        Catch ex As Exception
            Throw New Exception("Error occured when reading PROJECT DETAILS data from the DB" & Chr(10) & ex.Message, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to project table"
    End Sub



    'bind the controls items in the project view with the project object
    Public Sub bind_project_view()
        mctrl.statusBar.status_strip_sub_label = "Bind project data to view controls..."
        Try
            Dim layout_list As New List(Of Object)
            layout_list.Add(mctrl.mainForm.conf_project_layout)
            For Each lay As Object In mctrl.mainForm.conf_project_layout.Controls
                If lay.GetType Is GetType(TableLayoutPanel) Then
                    layout_list.Add(lay)
                End If
            Next

            Dim map As CViewModelMapList
            map = mctrl.modelViewMap(MOD_VIEW_MAP_PROJ_VIEW_DISPLAY)

            For Each layout As TableLayoutPanel In layout_list
                For Each viewcontrol As Control In layout.Controls
                    If TypeName(viewcontrol) = "TextBox" Then
                        viewcontrol.DataBindings.Add("Text", _project, map.list(viewcontrol.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    ElseIf TypeName(viewcontrol) = "DateTimePicker" Then
                        Dim datePick As DateTimePicker
                        datePick = CType(viewcontrol, DateTimePicker)
                        datePick.Format = DateTimePickerFormat.Custom
                        datePick.DataBindings.Add("Value", _project, map.list(datePick.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                        datePick.DataBindings.Add("CustomFormat", _project, map.list(datePick.Name & "/CustomFormat").class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                        datePick.DataBindings.Add("Checked", _project, map.list(datePick.Name & "/Checked").class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    ElseIf TypeName(viewcontrol) = "ComboBox" Then
                        Dim cb As ComboBox
                        cb = CType(viewcontrol, ComboBox)
                        MGeneralFuntionsViewControl.setComboBoxValue(cb, MConstants.listOfValueConf(map.list(cb.Name).view_list_name))
                        cb.DataBindings.Add("SelectedValue", _project, map.list(cb.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                        cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
                        cb.DropDownStyle = ComboBoxStyle.DropDownList
                        MViewEventHandler.addDefaultComboBoxEventHandler(cb)
                    ElseIf TypeName(viewcontrol) = "CheckBox" Then
                        viewcontrol.DataBindings.Add("Checked", _project, map.list(viewcontrol.Name).class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    End If
                Next
            Next

            'bind properties to control text box applicable to dassault
            mctrl.mainForm.lbl_cnf_prj_det_ac_flight_hrs_val.DataBindings.Add("Enabled", _project, map.list(mctrl.mainForm.lbl_cnf_prj_det_ac_flight_hrs_val.Name & "/Enabled").class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            mctrl.mainForm.lbl_cnf_prj_det_ac_flight_cycl_val.DataBindings.Add("Enabled", _project, map.list(mctrl.mainForm.lbl_cnf_prj_det_ac_flight_cycl_val.Name & "/Enabled").class_field, False, DataSourceUpdateMode.OnPropertyChanged)
            mctrl.mainForm.lbl_cnf_prj_det_dass_sales_or_val.DataBindings.Add("Enabled", _project, map.list(mctrl.mainForm.lbl_cnf_prj_det_dass_sales_or_val.Name & "/Enabled").class_field, False, DataSourceUpdateMode.OnPropertyChanged)

            'refresh project list
            refreshPorjList()
        Catch ex As Exception
            Throw New Exception("An error occured while loading the project view", ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind project data to view controls"
    End Sub

    Public Sub refreshPorjList()
        project_cb_list = New Dictionary(Of String, String)
        project_cb_list.Add(MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value, " ")
        For Each entry As String In _project.project_list
            project_cb_list.Add(entry, entry)
        Next
    End Sub


End Class
