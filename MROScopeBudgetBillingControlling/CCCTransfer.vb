﻿
Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CCCTransfer
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    Public Shared currentMaxID As Integer
    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_TRANSFER_CC_LOADED Then
            _from_cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _to_cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        End If
    End Sub

    Public Sub setID()
        If MConstants.GLOB_TRANSFER_CC_LOADED Then
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
        End If
    End Sub

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = True
        Dim mess As String = ""

        Return New KeyValuePair(Of Boolean, String)(True, "")
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res = False
        Dim mess = ""
        Try
            Me.system_status = MConstants.CONST_SYS_ROW_DELETED
            MConstants.GLB_MSTR_CTRL.labor_controlling_controller.deleteObjectCCTransfer(Me)
            res = True
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.ToString & Chr(10) & ex.Message & Chr(10) & ex.StackTrace
        End Try

        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return True
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_CC_TRANSFER_DB_READ
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_TRANSFER_CC_LOADED Then
                    CCCTransfer.currentMaxID = Math.Max(CCCTransfer.currentMaxID, _id)
                End If
            End If
        End Set
    End Property


    Private _to_cc As String
    Public Property to_cc() As String
        Get
            Return _to_cc
        End Get
        Set(ByVal value As String)
            If Not _to_cc = value Then
                _to_cc = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
                'refresh view
                MConstants.GLB_MSTR_CTRL.labor_controlling_controller.refreshAfterCCTransferUpdate()
            End If
        End Set
    End Property

    Private _from_cc As String
    Public Property from_cc() As String
        Get
            Return _from_cc
        End Get
        Set(ByVal value As String)
            If Not _from_cc = value Then
                _from_cc = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
                'refresh view
                MConstants.GLB_MSTR_CTRL.labor_controlling_controller.refreshAfterCCTransferUpdate()
            End If
        End Set
    End Property

    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not value = _calc_error_text Then
                _calc_error_text = value
            End If
        End Set
    End Property

    'text to display any error
    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
        End Set
    End Property

    'to string
    Public Overrides Function tostring() As String
        Return " From CC: " & _from_cc & " To CC " & _to_cc
    End Function

End Class
