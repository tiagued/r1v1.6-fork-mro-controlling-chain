﻿Imports Equin.ApplicationFramework

Public Class CNotToExceedRowValidator
    Public Shared instance As CNotToExceedRowValidator
    Public Shared Function getInstance() As CNotToExceedRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CNotToExceedRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(nte As CNotToExceed)
        nte.calc_is_in_error_state = False
    End Sub

    'label cannot be empty and is unique
    Public Function label(nte As CNotToExceed) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(nte.label) Then
            err = "Label cannot be empty"
            res = False
            nte.calc_is_in_error_state = True
        Else
            Dim unique As Boolean = True
            For Each view As ObjectView(Of CNotToExceed) In GLB_MSTR_CTRL.billing_cond_controller.nteList
                If Not IsNothing(view.Object.label) AndAlso Not view.Object.Equals(nte) AndAlso view.Object.label.Equals(nte.label, StringComparison.OrdinalIgnoreCase) Then
                    unique = False
                    Exit For
                End If
            Next
            If Not unique Then
                err = "Label is already use by another entry. Label must be unique"
                res = False
                nte.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'description cannot be empty 
    Public Function description(nte As CNotToExceed) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(nte.description) Then
            err = "Description cannot be empty"
            res = False
            nte.calc_is_in_error_state = True
        End If

        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function lab_mat_applicability(nte As CNotToExceed) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(nte.lab_mat_applicability) OrElse nte.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Lab/Mat Applicability cannot be empty"
            res = False
            nte.calc_is_in_error_state = True
        ElseIf nte.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_NTE_SCOPE_APP_LAB_CC_KEY).value Then
            If MUtils.isMaterialApplicableLabMat(nte.lab_mat_applicability) OrElse MUtils.isRepairApplicableLabMat(nte.lab_mat_applicability) OrElse
                MUtils.isFreightApplicableLabMat(nte.lab_mat_applicability) OrElse MUtils.isServApplicableLabMat(nte.lab_mat_applicability) Then
                err = "If scope applicability is set at CC Lab level, Lab/Mat applicability must be Lab"
                res = False
                nte.calc_is_in_error_state = True
            End If
        ElseIf nte.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_NTE_SCOPE_APP_MAT_PO_KEY).value Then
            If MUtils.isLaborApplicableLabMat(nte.lab_mat_applicability) Then
                err = "If scope applicability is set at Mat PO level, Lab/Mat applicability must be Mat & Freight, Mat, Small Mat, Freight or 3rd Party"
                res = False
                nte.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function scope_applicability(nte As CNotToExceed) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(nte.scope_applicability) OrElse nte.scope_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Scope Applicability cannot be empty"
            res = False
            nte.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function nte_value(nte As CNotToExceed) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If nte.nte_value <= 0 Then
            err="NTE Value shall be greather than 0"
                             res= False
            nte.calc_is_in_error_state= True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function value_curr(nte As CNotToExceed) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(nte.value_curr) OrElse nte.value_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Curreny Cannot be empty"
            res = False
            nte.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function nte_type(nte As CNotToExceed) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If String.IsNullOrWhiteSpace(nte.nte_type) OrElse nte.nte_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "NTE Type Cannot be empty"
            res = False
            nte.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function payer_obj(nte As CNotToExceed) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If nte.payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Payer cannot be empty"
            res = False
            nte.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function payer(nte As CNotToExceed) As KeyValuePair(Of Boolean, String)
        Return payer_obj(nte)
    End Function

    Public Function is_default(nte As CNotToExceed) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        Dim existing_default_types As New Dictionary(Of String, String)
        If nte.is_default Then
            'brose nte and collect applicability list
            For Each nteObjView As ObjectView(Of CNotToExceed) In MConstants.GLB_MSTR_CTRL.billing_cond_controller.nteList
                Dim nte_obj As CNotToExceed = nteObjView.Object
                If Not nte_obj.is_default OrElse Object.Equals(nte_obj, nte) OrElse nte_obj.nte_type <> nte.nte_type Then
                    Continue For
                End If
                If MUtils.isLaborApplicableLabMat(nte_obj.lab_mat_applicability) Then
                    addNotExist(existing_default_types, MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_KEY).value, nte_obj.tostring)
                ElseIf MUtils.isMaterialApplicableLabMat(nte_obj.lab_mat_applicability) Then
                    addNotExist(existing_default_types, MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_KEY).value, nte_obj.tostring)
                ElseIf MUtils.isRepairApplicableLabMat(nte_obj.lab_mat_applicability) Then
                    addNotExist(existing_default_types, MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_REP_KEY).value, nte_obj.tostring)
                ElseIf MUtils.isFreightApplicableLabMat(nte_obj.lab_mat_applicability) Then
                    addNotExist(existing_default_types, MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_FREIGHT_KEY).value, nte_obj.tostring)
                ElseIf MUtils.isServApplicableLabMat(nte_obj.lab_mat_applicability) Then
                    addNotExist(existing_default_types, MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_SERV_KEY).value, nte_obj.tostring)
                End If
            Next

            Dim m_appl_list As New List(Of String)
            If MUtils.isLaborApplicableLabMat(nte.lab_mat_applicability) Then
                m_appl_list.Add(MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_KEY).value)
            ElseIf MUtils.isMaterialApplicableLabMat(nte.lab_mat_applicability) Then
                m_appl_list.Add(MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_MAT_KEY).value)
            ElseIf MUtils.isRepairApplicableLabMat(nte.lab_mat_applicability) Then
                m_appl_list.Add(MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_REP_KEY).value)
            ElseIf MUtils.isFreightApplicableLabMat(nte.lab_mat_applicability) Then
                m_appl_list.Add(MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_FREIGHT_KEY).value)
            ElseIf MUtils.isServApplicableLabMat(nte.lab_mat_applicability) Then
                m_appl_list.Add(MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_SERV_KEY).value)
            End If

            Dim already_in_nte As String = ""
            Dim is_already_in_nte As Boolean = False

            For Each appl As String In m_appl_list
                If existing_default_types.ContainsKey(appl) Then
                    is_already_in_nte = True
                    already_in_nte = existing_default_types(appl)
                    Exit For
                End If
            Next
            If is_already_in_nte Then
                Dim nte_type = "NTE"
                If Not nte.isNTEYes Then
                    nte_type = "NTE Excl."
                End If

                err = "There must be only one default " & nte_type & " Object for applicability type " & MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_LAB_MAP_APPLICABILITY_LIST)(nte.lab_mat_applicability).value _
                        & ". This item is redundant with Nte Object " & already_in_nte
                res = False
                nte.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Sub addNotExist(_list As Dictionary(Of String, String), key As String, item As String)
        If Not _list.ContainsKey(key) Then
            _list.Add(key, item)
        End If
    End Sub
End Class
