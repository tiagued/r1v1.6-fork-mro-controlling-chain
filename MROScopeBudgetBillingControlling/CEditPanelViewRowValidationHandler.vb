﻿
Imports System.Reflection

Public Class CEditPanelViewRowValidationHandler
    Public Shared handler_list As Dictionary(Of String, CEditPanelViewRowValidationHandler)

    Public Shared Function addNew(_ctrl As Control, _validator As Object, _mapList As CViewModelMapList, _bindedObjectType As Type, _error_picture_box As PictureBox, _form As Form) As CEditPanelViewRowValidationHandler
        Try
            Dim ctrl_id As String
            ctrl_id = getCtrlID(_ctrl)
            Dim hdler As CEditPanelViewRowValidationHandler
            If Not CEditPanelViewRowValidationHandler.handler_list.ContainsKey(ctrl_id) Then
                hdler = New CEditPanelViewRowValidationHandler
                CEditPanelViewRowValidationHandler.handler_list.Add(ctrl_id, hdler)
            Else
                hdler = CEditPanelViewRowValidationHandler.handler_list(ctrl_id)
            End If

            hdler.validator = _validator
            hdler.mapList = _mapList
            hdler.bindedObjectType = _bindedObjectType
            hdler.container_form = _form

            hdler.errorProvider = New ErrorProvider()
            hdler.errorProvider.SetIconAlignment(_ctrl, ErrorIconAlignment.MiddleRight)
            hdler.errorProvider.SetIconPadding(_ctrl, 2)
            hdler.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.BlinkIfDifferentError

            hdler.error_picture_box = _error_picture_box

            'add handler to validate control when living control
            If _ctrl.GetType.Equals(GetType(TextBox)) Then
                Dim txtb As TextBox = CType(_ctrl, TextBox)
                AddHandler txtb.TextChanged, AddressOf CEditPanelViewRowValidationHandler.controlValidating
            End If
            If _ctrl.GetType.Equals(GetType(ComboBox)) Then
                Dim cb As ComboBox = CType(_ctrl, ComboBox)
                AddHandler cb.SelectedIndexChanged, AddressOf CEditPanelViewRowValidationHandler.controlValidating
            End If
            If _ctrl.GetType.Equals(GetType(CheckBox)) Then
                Dim chck As CheckBox = CType(_ctrl, CheckBox)
                AddHandler chck.CheckedChanged, AddressOf CEditPanelViewRowValidationHandler.controlValidating
            End If
            Return hdler
        Catch ex As Exception
            Throw New Exception("An error occured while adding a new error validator event on control " & _ctrl.Name & Chr(10) & ex.Message, ex)
        End Try
    End Function

    Public Shared Function getCtrlID(_ctrl As Control) As String
        Return _ctrl.FindForm.Name & "_" & _ctrl.Name
    End Function
    Public Shared Function getCtrlID(_form As Form, _ctrl_name As String) As String
        Return _form.Name & "_" & _ctrl_name
    End Function

    Public Shared Sub controlValidating(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            validateControl(sender)
        Catch ex As Exception
            Throw New Exception("An error occured while validating control " & sender.name & Chr(10) & ex.Message, ex)
        End Try
    End Sub

    Public Shared Function validateControl(sender As Object) As Boolean
        Dim ctrl As Control = CType(sender, Control)
        Try
            Dim ctrlVaildated As Boolean = True
            Dim ctrlErr As String = ""
            If CEditPanelViewRowValidationHandler.handler_list.ContainsKey(getCtrlID(ctrl)) Then
                Dim hdler As CEditPanelViewRowValidationHandler = CEditPanelViewRowValidationHandler.handler_list(getCtrlID(ctrl))
                Dim otype As Type = hdler.validator.GetType
                Dim bindedObject As Object = Nothing
                'take the good binding object
                For Each databinding As Binding In ctrl.DataBindings
                    If Not IsNothing(databinding) AndAlso Not IsNothing(databinding.DataSource) Then
                        If databinding.DataSource.GetType = hdler.bindedObjectType Then
                            bindedObject = databinding.DataSource
                            Exit For
                        End If
                    End If
                Next

                If IsNothing(bindedObject) Then
                    Return True
                End If
                Dim method As MethodInfo

                'init error state 
                method = otype.GetMethod("initErrorState")
                If method IsNot Nothing Then
                    method.Invoke(hdler.validator, New Object() {bindedObject})
                End If
                'error image box
                hdler.error_picture_box.Image = My.Resources.no_errors
                hdler.error_picture_box.Tag = ""

                Dim validRes As KeyValuePair(Of Boolean, String)
                ctrlErr = "Errors :" & Chr(10)

                For Each map As CViewModelMap In hdler.mapList.list.Values
                    Dim map_hdler As CEditPanelViewRowValidationHandler
                    Dim map_ctrl As Control
                    If CEditPanelViewRowValidationHandler.handler_list.ContainsKey(getCtrlID(hdler.container_form, map.view_control)) Then
                        map_hdler = CEditPanelViewRowValidationHandler.handler_list(getCtrlID(hdler.container_form, map.view_control))
                    Else
                        Continue For
                    End If

                    map_ctrl = map_hdler.container_form.Controls.Find(map.view_control, True).FirstOrDefault
                    If IsNothing(map_ctrl) Then
                        Continue For
                    End If

                    method = otype.GetMethod(map.class_field)
                    If method IsNot Nothing Then
                        validRes = method.Invoke(hdler.validator, New Object() {bindedObject})
                        If Not validRes.Key Then
                            map_hdler.errorProvider.SetError(map_ctrl, validRes.Value)
                            ctrlErr = ctrlErr & Chr(10) & "-" & validRes.Value
                        Else
                            map_hdler.errorProvider.SetError(map_ctrl, String.Empty)
                        End If
                        ctrlVaildated = ctrlVaildated And validRes.Key
                    End If
                Next
                If Not ctrlVaildated Then
                    hdler.error_picture_box.Image = My.Resources.yes_errors
                    hdler.error_picture_box.Tag = ctrlErr
                Else
                    'if row is validated, check if there is any runtime error that happened when calculating
                    method = otype.GetMethod("errorText")
                    If method IsNot Nothing Then
                        validRes = method.Invoke(hdler.validator, New Object() {bindedObject})
                        If Not validRes.Key Then
                            hdler.error_picture_box.Image = My.Resources.yes_errors
                            hdler.error_picture_box.Tag = validRes.Value
                        End If
                        ctrlVaildated = validRes.Key
                    End If
                End If

                'if new item check if should be added or not
                If ctrlVaildated Then
                    If Not IsNothing(hdler.validateAddDelegate) Then
                        hdler.validateAddDelegate(bindedObject)
                    End If
                End If

            End If

            Return ctrlVaildated
        Catch ex As Exception
            Throw New Exception("An error occured while validating control " & ctrl.Name & Chr(10) & ex.Message, ex)
        End Try
    End Function

    Delegate Sub ValidateNewItem(obj As Object)

    Public id As String
    Public validator As Object
    Public mapList As CViewModelMapList
    Public bindedObjectType As Type
    Public errorProvider As ErrorProvider
    Public error_picture_box As PictureBox
    Public container_form As Form
    Public validateAddDelegate As ValidateNewItem

End Class
