﻿Public Class CDataCheckCountReportingObj

    Public Sub New()

    End Sub

    Private _label As String
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            If Not _label = value Then
                _label = value
            End If
        End Set
    End Property

    Private _qty As Integer
    Public Property qty() As Integer
        Get
            Return _qty
        End Get
        Set(ByVal value As Integer)
            If Not _qty = value Then
                _qty = value
            End If
        End Set
    End Property

End Class
