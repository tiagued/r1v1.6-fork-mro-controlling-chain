﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormReportDataCheck
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.run_bt = New System.Windows.Forms.Button()
        Me.cancel_bt = New System.Windows.Forms.Button()
        Me.logs_grp = New System.Windows.Forms.GroupBox()
        Me.log_bt = New System.Windows.Forms.TextBox()
        Me.config_grp = New System.Windows.Forms.GroupBox()
        Me.lab_ctrl_chk = New System.Windows.Forms.CheckBox()
        Me.logs_grp.SuspendLayout()
        Me.config_grp.SuspendLayout()
        Me.SuspendLayout()
        '
        'run_bt
        '
        Me.run_bt.Location = New System.Drawing.Point(135, 402)
        Me.run_bt.Name = "run_bt"
        Me.run_bt.Size = New System.Drawing.Size(92, 23)
        Me.run_bt.TabIndex = 1
        Me.run_bt.Text = "Run"
        Me.run_bt.UseVisualStyleBackColor = True
        '
        'cancel_bt
        '
        Me.cancel_bt.Location = New System.Drawing.Point(363, 402)
        Me.cancel_bt.Name = "cancel_bt"
        Me.cancel_bt.Size = New System.Drawing.Size(92, 23)
        Me.cancel_bt.TabIndex = 2
        Me.cancel_bt.Text = "Cancel"
        Me.cancel_bt.UseVisualStyleBackColor = True
        '
        'logs_grp
        '
        Me.logs_grp.Controls.Add(Me.log_bt)
        Me.logs_grp.Location = New System.Drawing.Point(8, 343)
        Me.logs_grp.Name = "logs_grp"
        Me.logs_grp.Size = New System.Drawing.Size(578, 43)
        Me.logs_grp.TabIndex = 9
        Me.logs_grp.TabStop = False
        Me.logs_grp.Text = "Logs"
        '
        'log_bt
        '
        Me.log_bt.Enabled = False
        Me.log_bt.Location = New System.Drawing.Point(5, 19)
        Me.log_bt.Name = "log_bt"
        Me.log_bt.Size = New System.Drawing.Size(566, 20)
        Me.log_bt.TabIndex = 0
        '
        'config_grp
        '
        Me.config_grp.Controls.Add(Me.lab_ctrl_chk)
        Me.config_grp.Location = New System.Drawing.Point(8, 23)
        Me.config_grp.Name = "config_grp"
        Me.config_grp.Size = New System.Drawing.Size(282, 193)
        Me.config_grp.TabIndex = 10
        Me.config_grp.TabStop = False
        Me.config_grp.Text = "Config"
        '
        'lab_ctrl_chk
        '
        Me.lab_ctrl_chk.AutoSize = True
        Me.lab_ctrl_chk.Location = New System.Drawing.Point(7, 36)
        Me.lab_ctrl_chk.Name = "lab_ctrl_chk"
        Me.lab_ctrl_chk.Size = New System.Drawing.Size(152, 17)
        Me.lab_ctrl_chk.TabIndex = 0
        Me.lab_ctrl_chk.Text = "Include Labor Controlling ?"
        Me.lab_ctrl_chk.UseVisualStyleBackColor = True
        '
        'FormReportDataCheck
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(605, 451)
        Me.Controls.Add(Me.config_grp)
        Me.Controls.Add(Me.logs_grp)
        Me.Controls.Add(Me.cancel_bt)
        Me.Controls.Add(Me.run_bt)
        Me.Name = "FormReportDataCheck"
        Me.Padding = New System.Windows.Forms.Padding(5)
        Me.Text = "Data Check"
        Me.logs_grp.ResumeLayout(False)
        Me.logs_grp.PerformLayout()
        Me.config_grp.ResumeLayout(False)
        Me.config_grp.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents run_bt As Button
    Friend WithEvents cancel_bt As Button
    Friend WithEvents logs_grp As GroupBox
    Friend WithEvents log_bt As TextBox
    Friend WithEvents config_grp As GroupBox
    Friend WithEvents lab_ctrl_chk As CheckBox
End Class
